package smi.rendering.test.pages;

import static org.testng.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.Logger;
import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;


public class ActivevoicesSynthesisRequestsPage extends BasePage {

	
	
public void getReady(ExtentTest test, Logger logger, Connection con, boolean isThereMoreThanOneAssignedDomain) throws InterruptedException, SQLException, FileNotFoundException {
	
		RandomText generateInput = new RandomText();
	
		DriverContext.browser.GoToUrl(Setting.AUT_URL);
		// Login only once then run the following tests
		
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
		
		int timesTried = 0;
		do {
			if (isThereMoreThanOneAssignedDomain) {
				try {
					System.out.println("Choose random project and domain");
	//				LoginUtilities.selectSpecificProjectDomain(GetInstance(SelectPage.class), logger, "Bank Hapoalim Project", "Bank Hapoalim Domain");
					LoginUtilities.selectRandomProjectDomain(GetInstance(SelectPage.class), logger, con);
					System.out.println("Finished choosing project and domain");
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			
			CurrentPage= GetInstance(HomePage.class);
			timesTried =  CurrentPage.As(HomePage.class).isHomepageReady();
			System.out.println("Finished is homepage ready");
			
			// Wait for the page to load
			System.out.println("timesTried: " + timesTried);
			if(timesTried == 0) {
				DriverContext._Driver.navigate().refresh();
				Thread.sleep(2000);
			}
			System.out.println("Before last while check: " + timesTried);
		} while (timesTried == 0);
		System.out.println("After last while check: ");
		
		
		// Select random voice and domain
		Thread.sleep(2000);
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);	
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);
		
		// Click clear text first
		Thread.sleep(1000);
		CurrentPage.As(HomePage.class).clearText.click();
				
		// Enter text in the text box
		//Thread.sleep(3000);
		// Random words
//		String randomText = generateInput.RandomString(10, "words");
		// Random sentences
		String randomText = generateInput.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
	}

public void getReadyForRepeat(ExtentTest test, String server, Logger logger, String voice, String domain, boolean isThereMoreThanOneAssignedDomain) throws InterruptedException, SQLException {
	
	DriverContext.browser.GoToUrl(server);
	// Login only once then run the following tests
	
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
	
	LoginUtilities.selectSpecificProjectDomain(GetInstance(SelectPage.class), logger, "Bank Hapoalim Project", "Bank Hapoalim Domain");
	
	CurrentPage= GetInstance(HomePage.class);
	CurrentPage.As(HomePage.class).isHomepageReady();
	
	// Select random voice and domain
	//String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
	// Select random voice and domain
	CurrentPage.As(HomePage.class).cmbVoice.click();
	Thread.sleep(1000);
	CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbVoice, voice);
	
	//String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
	//LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);
	// Select the random domain
//	CurrentPage.As(HomePage.class).cmbDomain.click();
//	Thread.sleep(1000);
//	CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, domain);
//	LogUltility.log(test, logger, "Chosen Voice: " + voice + ", Chosen Domain: " + domain);
	LogUltility.log(test, logger, "Chosen Voice: " + voice);

	// Click clear text first
	CurrentPage.As(HomePage.class).clearText.click();
			
	// Enter text in the text box
	CurrentPage.As(HomePage.class).Textinput.sendKeys("Hello");
}

	public void synthisizeText(ExtentTest test, Logger logger) throws InterruptedException {

		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		CurrentPage= GetInstance(HomePage.class);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
}
