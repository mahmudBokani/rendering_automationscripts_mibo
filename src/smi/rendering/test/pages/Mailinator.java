package smi.rendering.test.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;

public class Mailinator extends BasePage {

	public Mailinator() {
		
	}

	@FindBy(how=How.ID,using ="inboxfield")
//	@FindBy(how=How.XPATH,using="//input[@id='inboxfieldsm']")
	public WebElement txtinboxfield;
	
//	@FindBy(how=How.XPATH,using ="//button[contains(text(),'Go!')]")
	@FindBy(how=How.XPATH,using ="//*[@id=\"go_inbox1\"]")
	public WebElement btnGo;
	
	//@FindBy(how=How.XPATH,using ="//div[@class='someviewport']")
	//@FindBy(how=How.XPATH,using ="//div[@class='someviewport']/div[1]")
	//@FindBy(how=How.XPATH,using ="//*[@id='inboxpane']")
	//@FindBy(how=How.ID,using ="inboxpane")
	//@FindBy(how=How.XPATH,using ="//*[@id='inboxpane']/li[1]")
	
	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[2]/div[1]/div[3]/div[1]/div[8]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[4]/a[1]")
//	@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[1]/div[1]/div[3]/div[1]/div[6]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[4]")
	public WebElement lnkEmailTitle;
	
	@FindBy(how=How.XPATH,using ="html/body/a")
	public WebElement lnkActivate;
	
	@FindBy(how=How.XPATH,using ="html/body/h3")
	public WebElement msgActivateSuccess;
	
	@FindBy(how=How.PARTIAL_LINK_TEXT,using ="SpeechMorphing Password Reset")
	public WebElement lnkEmail;
	
	@FindBy(how=How.LINK_TEXT,using ="Reset Password")
	public WebElement lnkResetPassword;
	
	
	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/a[1]")
	//@FindBy(how=How.XPATH,using ="html/body/a")
	//@FindBy(how=How.XPATH,using ="html/body")
	//@FindBy(how=How.XPATH,using ="//*[contains(text(), 'Reset Password')]")
	//@FindBy(how=How.CSS,using ="//*[contains(text(), 'html>body>a')]")
	//@FindBy(how=How.PARTIAL_LINK_TEXT,using ="http://click1.clickrouter.com")
	public WebElement lnkResetPassword1;
	//"//*[contains(text(), 'Reset Password')]"
	//html/body/a
	
	@FindBy(how=How.XPATH,using ="//*[@id='public_showmaildiv']/div[1]/div[2]/div[2]/a")
	public WebElement showJson;
	
	@FindBy(how=How.XPATH,using ="//*[@id='msg_body']")
//	@FindBy(how=How.XPATH,using ="//*[@id='msgpane']")
//	@FindBy(how=How.XPATH,using ="//*")
//	@FindBy(how=How.XPATH,using ="body")
//	@FindBy(how=How.XPATH,using ="html/body")
	public WebElement msg_body;

	@FindBy(how=How.XPATH,using ="//*")
	public WebElement bodyText;
	
	// Fill the inbox field and click Go
	public void FillInboxField(String email)
	{
//		txtinboxfield.click();
		txtinboxfield.sendKeys(email);
		btnGo.click();
	}
	
	public void clickResetPassword() throws InterruptedException {
		//txtinboxfield.click();
		Thread.sleep(1000);
		//this.txtinboxfield.sendKeys(Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.TAB, Keys.ENTER);
		Actions builder = new Actions(DriverContext._Driver); 
		builder.sendKeys(Keys.TAB, Keys.TAB, Keys.ENTER);
		builder.build();
		builder.perform();
		
//		Thread.sleep(1000);
//		txtinboxfield.sendKeys(Keys.TAB);
//		Thread.sleep(1000);
//		txtinboxfield.sendKeys(Keys.TAB);
//		Thread.sleep(1000);
		//txtinboxfield.sendKeys(Keys.TAB);
		//Thread.sleep(1000);
		//txtinboxfield.sendKeys(Keys.TAB);
//		Thread.sleep(1000);
//		txtinboxfield.sendKeys(Keys.ENTER);
	}
}
