package smi.rendering.test.pages;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.uiltilies.LogUltility;

/**
 * Define elements and methods for the Signup page
 *
 */
public class SignupPage  extends BasePage {
	
	// First name text field
	@FindBy(how=How.ID,using ="fname")
	public WebElement txtFirstName;
	
	// Last name text field
	@FindBy(how=How.ID,using ="lname")
	public WebElement txtLastName;
	
	// Email text field
	@FindBy(how=How.ID,using ="email")
	public WebElement txtEmail;
	
	// Password text field
	@FindBy(how=How.ID,using ="password")
	public WebElement txtPassword;
	
	// Password confirmation texf field
	@FindBy(how=How.NAME,using ="pass_confirmation")
	public WebElement txtPassConfirm;
	
	// Submit button
	@FindBy(how=How.NAME,using ="SubmitButton")
	public WebElement btnSignup;
	
	// First name error message
	@FindBy(how=How.XPATH,using ="//div[@class='auth-form']/form[@class='has-validation-callback']/div[1]/span[1]")
	public WebElement msgFnameError;
	
	// Last name error message
	@FindBy(how=How.XPATH,using ="//div[@class='auth-form']/form[@class='has-validation-callback']/div[2]/span[1]")
	public WebElement msgLnameError;
	
	// Email error message         
	@FindBy(how=How.XPATH,using ="/html/body/div/div[3]/form/div[3]/span")
	public WebElement msgEmailError;
	
	// Password error message
	@FindBy(how=How.XPATH,using ="/html/body/div/div[3]/form/div[4]/span")
	public WebElement msgPasswordError;
	
	// Password confirmation error message
	@FindBy(how=How.XPATH,using ="/html/body/div/div[3]/form/div[5]/span")
	public WebElement msgPasswordConfirmError;
	
	// Signup confirmation message
	@FindBy(how=How.XPATH,using ="html/body/div/h1")
	public WebElement msgSignupConfirmation;
	
	// Link back to login
	@FindBy(how=How.XPATH,using ="html/body/div/a")
	public WebElement lnkBackToLogin;
	
	// Already signed up link 
	@FindBy(how=How.XPATH,using ="/html/body/div/div[3]/p/a/b")
	public WebElement AlreadySinedup; 
		
	// Login link from the taken email message 
	
	@FindBy(how=How.XPATH,using ="html/body/div[1]/div[2]/form/div[3]/span/a")
	 public WebElement LoginLink;
	
		
	/**
	 * Fill signup fields and click the submit button
	 * @param fname - First name
	 * @param lname - Last name
	 * @param email - User email
	 * @param password - User password
	 * @param pass_confirmation - User confirmation 
	 * @throws InterruptedException 
	 */
	public void Signup(String fname, String lname, String email, String password, String pass_confirmation) throws InterruptedException {
		txtFirstName.sendKeys(fname);
		txtLastName.sendKeys(lname);
		txtEmail.sendKeys(email);
		txtPassword.sendKeys(password);
		txtPassConfirm.sendKeys(pass_confirmation);
		Thread.sleep(2000);
		
		// Scroll Down
		//CurrentPage.As(HomePage.class).scrollDown();
		JavascriptExecutor jse = (JavascriptExecutor)DriverContext._Driver;
	   	jse.executeScript("scroll(0, 250);");
	   	
		btnSignup.click();
	}
	
	public boolean FindElement(String element) {
		  try {
			  switch (element) {
			          case "pass_confirmation":
			        	  return this.txtPassConfirm.isDisplayed();
			          case "fname":
			        	  return this.txtFirstName.isDisplayed();
			  }
	        } catch (NoSuchElementException e) {
	            return false;
	        }
		  return false;
		 }
	
	/**
	 * Check the signup confirmation message : Thank you for signing up! A confirmation email was sent to your email address. Please check your inbox
	 * @return 
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	public void checkSignupSuccessMessage(ExtentTest test, Logger logger) throws InterruptedException {
		int timesToTry = 5;
		CurrentPage = GetInstance(SignupPage.class);
		while (timesToTry != 0)
		{
			try {
				String confirmationMessage = CurrentPage.As(SignupPage.class).msgSignupConfirmation.getText();
				boolean SignUpSuccessfully = confirmationMessage.contains("Thank you for signing up!");
				assertTrue(SignUpSuccessfully);
				LogUltility.log(test, logger, "Check the confirmation message: " + confirmationMessage);
				LogUltility.log(test, logger, "Check the SignUpSuccessfully is True: " + SignUpSuccessfully);
					return;				
			} catch (Exception e) {
				timesToTry--;
				System.out.println(timesToTry);
				Thread.sleep(100);
			}
		}
	}
	
}
