package smi.rendering.test.pages;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.uiltilies.LogUltility;

public class UMSPage extends BasePage {
	
	//@FindBy(how=How.ID,using ="password")
	@FindBy(how=How.XPATH,using ="//*[@id='password']")
	public WebElement txtPassword;
	
	@FindBy(how=How.ID,using ="password2")
	public WebElement txtConfirmPassword;
	
	@FindBy(how=How.ID,using ="enter")
	public WebElement btnResetPassword;
	
	@FindBy(how=How.XPATH,using ="html/body/h4")
	public WebElement msgUpdateSuccess;
	
	@FindBy(how=How.ID,using ="message1")
	public WebElement msgError;
	
	@FindBy(how=How.XPATH,using ="html/body/h3")
	public WebElement msgActivate;
	
	// Reset password page title lable "Reset Password"
	@FindBy(how=How.XPATH,using="/html[1]/body[1]/h2[1]")
	public WebElement resetPasswordLabel ; 
	
	
	/**
	 * Focus on last open tab
	 */
	public void focusOnLastOpenedTab() {
		for (String handle : DriverContext._Driver.getWindowHandles()) {
			DriverContext._Driver.switchTo().window(handle);
			}
	}
	
	/**
	 * Wait for the update confirmation message
	 * @return 
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	public void updateMessageCheck(ExtentTest test, Logger logger, String expectedMessage) throws InterruptedException, AWTException {
		int timesToTry = 5;
		CurrentPage = GetInstance(UMSPage.class);
		String updateMessage;
		while (timesToTry != 0)
		{
			try {
					updateMessage = CurrentPage.As(UMSPage.class).msgUpdateSuccess.getText();
					boolean result = updateMessage.equals(expectedMessage);
					System.out.println(result);
					assertTrue(result);
					LogUltility.log(test, logger, "Check the update confirmation message: " + result);
					return;				
			} catch (Exception e) {
				timesToTry--;
				System.out.println(timesToTry);
				Thread.sleep(100);
			}
		}
	}
	
	/**
	 * reload page if needed
	 */
	public void reloadUMS() {
			
		// refresh page 
			DriverContext._Driver.navigate().refresh();
	}
	
}
