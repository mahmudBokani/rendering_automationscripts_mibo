package smi.rendering.test.pages;	


import static org.testng.Assert.assertEquals;
import java.awt.AWTException;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import com.aventstack.extentreports.ExtentTest;
import com.mongodb.connection.ServerSettings;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Define elements and methods for the Homepage
 *
 */
public class HomePage extends BasePage {
	
	// Voice filter field
	@FindBy(how=How.ID,using ="searchText")
	public WebElement txtVoiceSearch;
	
	// Voice field button
	@FindBy(how=How.ID,using ="searchAction")
	public WebElement btnVoiceSearch;
	
	// Voice filter field
	@FindBy(how=How.ID,using ="searchDomain")
	public WebElement txtDomainSearch;
	
	// Voice field button
	@FindBy(how=How.ID,using ="searchDomainAction")
	public WebElement btnDomainSearch;
	
	// Logout link
	@FindBy(how=How.CLASS_NAME,using ="logout_a")
	public WebElement lnkLogout;
	
	// Gender dropdown
	@FindBy(how=How.XPATH,using ="//div[@class='left_selection']/div[5]")
	//@FindBy(how=How.CLASS_NAME,using ="selectric")
	public WebElement cmbGender;
	
	// Age dropdown
	@FindBy(how=How.XPATH,using ="//div[@class='left_selection']/div[6]")
	public WebElement cmbAge;
	
	// Language dropdown
//	@FindBy(how=How.XPATH,using ="//div[@class='left_selection']/div[2]")
	@FindBy(how=How.XPATH,using="//*[@id=\"mainArea\"]/div[1]/div[2]/div[1]/div/div[2]/p")
	public WebElement cmbLanguage;
	
	// Voice dropdown
	//@FindBy(how=How.XPATH,using ="//div[@class='left_selection']/div[7]/div[1]")
//	@FindBy(how=How.XPATH,using ="//p[contains(text(),'Select Voice')]")
	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[4]/div[1]/div[1]/div[8]/div[1]/div[1]/div[2]/p[1]")
	public WebElement cmbVoice;
	
	// Voice info text
	@FindBy(how=How.XPATH,using ="//*[@id='voiceTitle']")
	public WebElement textVoiceTitle;
	
	// Domain dropdown
		@FindBy(how=How.XPATH,using ="//*[@id=\"mainArea\"]/div[1]/div[4]/div[1]/div/div[2]/p")
	//@FindBy(how=How.XPATH,using ="//div[@class='left_selection']/div[4]//p")
	public WebElement cmbDomain;
	
	@FindBy(how=How.XPATH,using ="//*[@id='mainArea']/div[1]/div[9]/div[1]/div/div[2]/p")
	public WebElement cmbDomaindrop;
	
	// voice selection label  
	@FindBy(how=How.XPATH,using ="//*[@id=\"mainArea\"]/div[1]/h2[2]")
	public WebElement VoiceLabel;
	
	// Domain selection label 
	@FindBy(how=How.XPATH,using ="//*[@id=\"mainArea\"]/div[1]/div[4]/label")
	public WebElement DomainLabel;
	
	// Text area 
	@FindBy(how=How.XPATH,using ="//*[@id='innerText']")
	public WebElement Textinput;
	
	// Save button 
	@FindBy(how=How.ID,using ="saveButton")
	public WebElement SaveButton;
	
	// Play button 
	@FindBy(how=How.XPATH,using ="//*[@id='mainArea']/div[2]/div[2]/div[3]/div[1]/div")
	public WebElement PlayButton;
	
	// Play button playing 
	@FindBy(how=How.XPATH,using ="//*[@id='mainArea']/div[2]/div[2]/div[3]/div[1]/div[@class='play textChanged clipLoaded']")
	//@FindBy(how=How.CLASS_NAME,using ="play textChanged clipLoaded pause")
	//@FindBy(how=How.CSS,using =".play.textChanged.clipLoaded.pause")
	public WebElement PlayButtonPlay;
	
	// Play button with class play 
	@FindBy(how=How.XPATH,using ="//*[@id='mainArea']/div[2]/div[2]/div[3]/div[1]/div[@class='play']")
	public WebElement PlayButton2;
		
	// Play button pausing 
	@FindBy(how=How.XPATH,using ="//*[@id='mainArea']/div[2]/div[2]/div[3]/div[1]/div[@class='play textChanged clipLoaded pause']")		 
//	@FindBy(how=How.XPATH,using="//div[@class='play textChanged clipLoaded pause']")
	public WebElement PlayButtonPause;
	
	// Play button loading 
//	@FindBy(how=How.XPATH,using ="//*[@id='mainArea']/div[2]/div[2]/div[3]/div[1]/div[@class='play textChanged']")
	@FindBy(how=How.ID,using="loading_spinner")
	public WebElement PlayButtonLoading;

	// Volume bar 
	@FindBy(how=How.XPATH,using ="//*[@id='volume']")
	public WebElement VolumeBar;
	
	// Speed bar 
	@FindBy(how=How.XPATH,using ="//*[@id='speed']")
	public WebElement SpeedBar;
	
	// Style dropdown 
	@FindBy(how=How.XPATH,using ="//*[@id='StyleButton']")
	public WebElement StyleValues;
	
	// Gesture dropdown
	@FindBy(how=How.XPATH,using ="//*[@id='GestureButton']")
	public WebElement GestureValues;
	
	@FindBy(how=How.ID,using ="GestureButton")
	public WebElement GestureValues1;
	
	// Gesture Button
    @FindBy(how=How.XPATH,using ="//*[@id='GestureButton']//*[@class='goTriangle']")
    public WebElement btnGesture;
    
    // Gesture Menu 
    @FindBy(how=How.XPATH,using ="//*[@id='gesturesBarSubMenu']")
    public WebElement GestureMenu;
    
    // Gesture = Kiss  
    @FindBy(how=How.XPATH,using ="//*[@id='gestureList']/li[3]")
    public WebElement GestureKiss;
	
	// Mood dropdown
	@FindBy(how=How.XPATH,using ="//*[@id='moodButton']")
	public WebElement MoodValues;
	
	// Excited Mood 
	@FindBy(how=How.XPATH,using ="//*[@id='moodList']/li[3]")
	public WebElement ExcitedMood;
	
	// Bold button 
	@FindBy(how=How.XPATH,using ="//*[@id='BoldButton']")
	public WebElement Boldbtn;
	
	// Editable text for the template  //*[@id='innerText']/span[1] 
	// Bold button 
	@FindBy(how=How.XPATH,using ="//*[@id='innerText']/span[1]")
	public WebElement editableText;
	
	// Text editor, first span
	@FindBy(how=How.XPATH,using ="//*[@id='innerText']/span")
	public WebElement textEditorSpan;
	
	// Mood button arrow  
	@FindBy(how=How.XPATH,using ="//*[@id='moodButton']/span")
	public WebElement moodArrow;
	
	// CSS Body
	@FindBy(how=How.CSS,using ="body")
	public WebElement CssBody;
	
	// Italic button  

	//for point locatino @FindBy(how=How.CSS,using ="#ItalicButton")
	//@FindBy(how=How.ID,using ="ipaText")  //ItalicButton  // ipaText
	//@FindBy(how=How.XPATH,using ="//div[@id='ItalicButton'][@class='barBox no_chices'][contains(text(),'I')]")
	@FindBy(how=How.XPATH,using ="//*[@id='ItalicButton']")
	//@FindBy(how=How.XPATH,using ="//*[@id='lowerBar']/*[@id='ItalicButton']")
	//@FindBy(how=How.XPATH,using ="//*[@class='whiteArea']/div[2]/div[5]")
	public WebElement Italicbtn;
	
	//IPA Popup 
	@FindBy(how=How.XPATH,using ="//*[@id='ipaPopup']")
	public WebElement IpaPopup;
	
	// Say-As popup title 
	@FindBy(how=How.XPATH,using ="//*[@id='ipaPopup']/div[1]")
	public WebElement IpaPopupTitle;
	
	// Open button 
	@FindBy(how=How.XPATH,using ="//*[@id='openButton']")
	public WebElement Openbtn;
		
	// Feedback Link 
	@FindBy(how=How.XPATH,using ="//*[@id='feedback']")
	public WebElement Feedback;
	
	// Message for sharing successfully  
	@FindBy(how=How.XPATH,using ="/html/body/div[9]/div/div[2]/div/span")
	public WebElement displayedMessage;
		
	// Play button
	@FindBy(how=How.CSS,using ="div.play")
	public WebElement btnplay;
	
	// Message when try to save without selecting domain: Please select a domain and open/enter text     X
	@FindBy(how=How.XPATH,using ="html/body/div[7]/div/div[2]/div/span")
	public WebElement SaveErrorMsg;
	
	// Mood Button
	@FindBy(how=How.ID,using ="moodButton")
	public WebElement btnMood;
	
	// Style triangle
	@FindBy(how=How.XPATH,using ="//*[@id='StyleButton']/span[2]")
	public WebElement btnStyle;
	
	// Default text in the textbox :Select a voice and a domain from the menu on the left, and then either open an existing voice text file, or just type some text right into this box 
	@FindBy(how=How.XPATH,using ="//*[@id='innerText']/ul/li[1]/div[2]")
	public WebElement InformationText;
	
	// Volume scroll bar
	@FindBy(how=How.ID,using ="volume")
	public WebElement hsbVolume;
	
	// Speed scroll bar
	@FindBy(how=How.ID,using ="speed")
	public WebElement hsbSpeed;
	
	// Pitch scroll bar
	@FindBy(how=How.ID,using ="pitch")
	public WebElement hsbPitch;
	
	// Audio wave bar              //*[@id='voiceWave']/wave/canvas    //*[@id='voiceWave']/wave/wave/canvas
	//@FindBy(how=How.XPATH,using ="//*[@id='voiceWave']/wave/wave/canvas")
	@FindBy(how=How.XPATH,using ="//*[@id='voiceWave']") 
	public WebElement barWave;
	
	// Bar wave Canvas
	@FindBy(how=How.XPATH,using ="//*[@id=\"voiceWave\"]/wave/canvas") 
	public WebElement barWaveCanvas;
	
	// Bold button
	@FindBy(how=How.ID,using ="BoldButton")
	public WebElement btnBold;
	
	//Text in the text editor 
	@FindBy(how=How.XPATH,using ="//*[@id='innerText']")
	public WebElement EnteredText;
	// Region in wave bar
	@FindBy(how=How.XPATH,using ="//*[@id='voiceWave']/wave/region")
	public WebElement region;
	
	// Style list  
	//@FindBy(how=How.XPATH,using ="//*[@id='StyleButton']/div/ul")
	@FindBy(how=How.XPATH,using ="//*[@id='stylesBarSubMenu']")
	public WebElement TestStyleList;
	
	// formal style 
	@FindBy(how=How.XPATH,using ="//*[@id='styleList']/li[5]")
	public WebElement FormalStyle;
//	// Check for text attribute
//	@FindBy(how=How.XPATH,using ="//*[@id='innerText']/span/span")
//	public WebElement atrIPA;
	
	// Check for italic text
	@FindBy(how=How.XPATH,using ="//*[@id='innerText']/span/span")
	public WebElement txtIPA;
	
	// Check for text highlighted with mood  .//*[@id='innerText']/span
	@FindBy(how=How.XPATH,using ="//*[@id='innerText']/span")
	public WebElement txtMood;
	
	// Domain label
	 @FindBy(how=How.XPATH,using ="//*[@id=\"mainArea\"]/div[1]/div[4]/label")
	 public WebElement lblDomain;
	
	 // text editor span
	 @FindBy(how=How.XPATH,using ="//*[@id='innerText']/span")
	 public WebElement txtSpan;
	
	// X button
	@FindBy(how=How.CLASS_NAME,using ="clear_attr")
	public WebElement xButton;
	 
	// Volume X button 
	 @FindBy(how=How.XPATH,using ="//*[@id=\"mainArea\"]/div[2]/div[2]/div[3]/div[2]/div")
	public WebElement xButtonVolume;
	 
	// Speed X button 
	@FindBy(how=How.XPATH,using ="//*[@id=\"mainArea\"]/div[2]/div[2]/div[3]/div[3]/div")
	public WebElement xButtonSpeed;
	
	// Pitch X button 
	@FindBy(how=How.XPATH,using ="//*[@id=\"mainArea\"]/div[2]/div[2]/div[3]/div[4]/div")
	public WebElement xButtonPitch;
	
	// IPA X button
	@FindBy(how=How.XPATH,using="//div[@id='ItalicButton']//div[@class='clear_attr'][contains(text(),'X')]")
	public WebElement xButtonIPA;
	
	// Mood X button
	@FindBy(how=How.XPATH,using="//div[@id='moodButton']//div[@class='clear_attr'][contains(text(),'X')]")
	public WebElement xButtonMood;
	
	// Bold / emphasize X button
	@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[4]/div[1]/div[2]/div[2]/div[2]/div[2]/div[4]/div[1]")
	public WebElement xButtonBold;
	
	// Download button
//	@FindBy(how=How.CLASS_NAME,using ="save_wav")
	@FindBy(how=How.XPATH,using="//*[@id=\"download_link\"]")
	public WebElement btnDownload;
	
	// pitchVal score
	@FindBy(how=How.ID,using ="pitchVal")
	public WebElement pitchVal;
	
	// speedVal score
	@FindBy(how=How.ID,using ="speedVal")
	public WebElement speedVal;
	
	// volumeVal score
	@FindBy(how=How.ID,using ="volumeVal")
	public WebElement volumeVal;
	
	// Clear text
	@FindBy(how=How.CLASS_NAME,using ="clearText")
	public WebElement clearText;
	
	// restore previous domain button
	@FindBy(how=How.ID,using ="general_confirm_button_no")
	public WebElement restoreDomain;
	
	// restore previous domain button
	@FindBy(how=How.ID,using ="general_confirm_button_yes")
	public WebElement keepNewDomain;
	
	// restore previous domain button
	@FindBy(how=How.ID,using ="volume")
	public WebElement volumeRange;
	
	// For TC:SRA194
    @FindBy(how=How.XPATH,using ="//*[@id='innerText']/span[1]")
    public WebElement spanTC194;
    
    // For ChooseRandomVoice failure message
    @FindBy(how=How.XPATH,using="/html/body")
    public WebElement smorphBody;
    
    // Mood bar
    @FindBy(how=How.ID,using="moodBar")
    public WebElement moodBar;
    
	
    
	/**
	 * Select an item from a listbox
	 * @param lstBox - dropdown element
	 * @param ItemName - value to choose
	 */
	public void Select_Item_In_ListBox(WebElement lstBox,String ItemName)
	{
//		System.out.println("Select_Item_In_ListBox");
		//get all childs
		// List<WebElement> itemlist= lstBox.findElements(By.cssSelector("div.selectricScroll ul>*"));
		List<WebElement> itemlist= lstBox.findElements(By.xpath("//div[@class='selectricScroll']/ul/li"));
		//System.out.println(itemlist);
		for(WebElement e : itemlist)
		{
			int attempts = 3;
	    	while(attempts>0)
	    	{
	    		try {
	    			if (e.getText().equals(ItemName))
						{ e.click();  return;}
	    		} catch (Exception staleelementrException) {
					// TODO: handle exception
	    			}
	    	attempts--;
	    	}
		}
		
	}
	
	/**
	 * Get the values list from a dropdown
	 * @param lstBox - dropdown element
	 * @return dropdown
	 */
	private List<String> Get_List_Items_Value(WebElement lstBox)
	{
		//get all children
	//	List<WebElement> itemlist= lstBox.findElements(By.cssSelector("div.selectricScroll ul>*"));
		List<WebElement> itemlist= lstBox.findElements(By.xpath("//div[@class='selectricScroll']/ul/li"));
		ArrayList<String> itemNames= new ArrayList<String>();
		for(WebElement e : itemlist)
		{
			int attempts = 3;
	    	while(attempts>0)
	    	{
	    		try {
					if(!e.getText().isEmpty())
						{itemNames.add(e.getText());break;}	
		//				itemNames.add(e.getText().replace(" (P)", ""));	
	    		} catch (Exception staleelementrException) {
					// TODO: handle exception
    			 }
	    	attempts--;
	    	}
		}
		return itemNames;
	}
	
	/**
	 * Get Gender values from the Gender dropdown
	 * @return list of Gender values
	 */
	public List<String> getGenderValues() {
		cmbGender.click();
		return Get_List_Items_Value(cmbGender);
	}
	
	/**
	 * Select a value from the Gender dropdown
	 * @param gender - value to select
	 */
	public void select_gender(String gender){
		cmbGender.click();
		Select_Item_In_ListBox(cmbGender,gender);
		
	}
	
	/**
	 * Select a value from the Age dropdown
	 * @param age - value to select
	 */
    public void select_age(String age){
		cmbAge.click();
		Select_Item_In_ListBox(cmbAge,age);
	}
    
    /**
	 * Select a value from the language dropdown
	 * @param language - value to select
	 */
    public void select_language(String language){
    	cmbLanguage.click();
		Select_Item_In_ListBox(cmbLanguage,language);
	}
    
    /**
     * Select Domain value from the Domain dropdown
     * @param domain
     */
    public void select_Domain(String domain){
		cmbDomain.click();
		Select_Item_In_ListBox(cmbDomain,domain);		
    }
    
    /**
     * Select voice value from the Voice dropdown
     * @param domain
     */
    public void select_voice(String voice){
    	cmbVoice.click();
		Select_Item_In_ListBox(cmbVoice,voice);		
    }

    /**
     * Get Age values from the Age dropdown
	 * @return list of Age values
     */
	public List<String> getAgeValues() {
		cmbAge.click();
		return Get_List_Items_Value(cmbAge);
	}
	
	/**
	 * Get Language values from the Language dropdown
	 * @return list of Language values
	 */
	public List<String> getLanguageValues() {
		cmbLanguage.click();
		return Get_List_Items_Value(cmbLanguage);
	}
	
	/**
	 * Get Voice values from the Voice dropdown
	 * @return list of Voice values
	 */
	public List<String> getVoiceValues() {
		cmbVoice.click();
		
		List<String> getValues = Get_List_Items_Value(cmbVoice);
		
		getValues.removeAll(Arrays.asList(""));
		getValues.remove("Select Voice");
		return getValues;
		
//		return Get_List_Items_Value(cmbVoice);
		
	}
	
	
	/**
	 * Get Domain values from the Domain dropdown
	 * @return list of Voice values
	 */
	public List<String> getDomainValues() {
		cmbDomain.click();
		List<String> getValues = Get_List_Items_Value(cmbDomain); 
		getValues.removeAll(Arrays.asList(""));
		getValues.remove("Select Domain");

		return getValues; 
	}
	
	
//	/**
//	 * Select a random value from the domain dropdown
//	 * @return the picked choice
//	 */
//	public String chooseRandomDomain() {
//		// Get the available domain
//		List<String> domainValues = this.getDomainValues();
//		// Delete first empty value
//		domainValues.remove(0);
//		
//		// Choose a random voice
//		Random randomizer = new Random();
//		String randomDomain = domainValues.get(randomizer.nextInt(domainValues.size()));
//		
//		// Select the random domain
//		this.Select_Item_In_ListBox(cmbDomain, randomDomain);
//		
//		return randomDomain;
//	}
	
	/**
	 * Select a random value from the domain dropdown
	 * @return the picked choice
	 * @throws SQLException 
	 */
	public String chooseRandomDomain(Connection con, String chosenProject, String userEmail) throws SQLException {
		
		// Click on the domain dropdown
		cmbDomain.click();
		
		// Get Domains that have values in styles and gestures and moods
		  String query = "select smi_domain_id from smi_domain_allowed_gestures where gesture_id in (select gesture_id from gesture where supported is TRUE) " 
				       + "INTERSECT " 
				       + "select smi_domain_id from smi_domain_allowed_moods where mood_id in (select mood_id from mood where supported is TRUE) " 
				       + "INTERSECT " 
				       + "select smi_domain_id FROM smi_domain_allowed_styles where style_id in (select style_id FROM style where supported is TRUE) "
				       + "INTERSECT " 
				       + "select DISTINCT smi_domain_id from smi_domain where supported is TRUE AND language_id="+Setting.Language 
				       + " ORDER BY smi_domain_id";
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		
		// Get the moods names of the moods ids
		List<String> domains = new ArrayList<String>();
		ResultSet resultSetIn;
	    while (resultSet.next()) {
	    	query = "select domain_name from smi_domain WHERE supported is TRUE and smi_domain_id = " + resultSet.getString(1);
	    	resultSetIn = DatadUltilities.ExecuteQuery(query, con);
	    	resultSetIn.next();
	    	try {
	    		domains.add(resultSetIn.getString(1));
			} catch (Exception e) {
				System.out.println("Domain is not supported, don't add: " + e);
			}
	    }
		
	    List<String> newDomains = new ArrayList<String>();
	    // Use the below function to retrieve available domains for the user
	    List<String> assignedDomainValues = CurrentPage.As(HomePage.class).getVDomainValuesFromDBandPrivatesDomains(con, chosenProject, userEmail);

	    // remove domains that are not in the above list
	    for (String domain : domains) {
			if (assignedDomainValues.contains(domain))
				newDomains.add(domain);
		}
	    
		// Choose a random domain
		Random randomizer = new Random();
		String randomDomain = newDomains.get(randomizer.nextInt(newDomains.size()));
		
		// Select the random domain
		randomDomain = selectDomainFromDropdown(randomDomain);
		
		return randomDomain;
	}
	
	/**
	 * Select a random value from the domain dropdown
	 * @return the picked choice
	 * @throws SQLException 
	 */
	public String chooseRandomDomainWithMood(Connection con, String chosenProject, String userEmail) throws SQLException {
		
		// Click on the domain dropdown
		cmbDomain.click();
		
		// Get Domains that have values in moods
		  String query = "select smi_domain_id from smi_domain_allowed_moods where mood_id in (select mood_id from mood where supported is TRUE AND mood_id<>10) " 
				  	   + "INTERSECT " 
				  	   + "select DISTINCT smi_domain_id from smi_domain where open_to_public is TRUE AND language_id=" + Setting.Language
		               + " ORDER BY smi_domain_id ";
		      
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		
		// Get the moods names of the moods ids
		List<String> domains = new ArrayList<String>();
		ResultSet resultSetIn;
	    while (resultSet.next()) {
	    	query = "select domain_name from smi_domain WHERE supported is TRUE and smi_domain_id = " + resultSet.getString(1);
	    	resultSetIn = DatadUltilities.ExecuteQuery(query, con);
	    	resultSetIn.next();
	    	try {
	    		domains.add(resultSetIn.getString(1));
			} catch (Exception e) {
				System.out.println("Domain is not supported, don't add: " + e);
			}
	    	
	    }
		
	    List<String> newDomains = new ArrayList<String>();
	    // Use the below function to retrieve available domains for the user
//	    List<String> assignedDomainValues = CurrentPage.As(HomePage.class).getVDomainValuesFromDBandPrivatesDomains(con, chosenProject, userEmail);
		List<String> assignedDomainValues = CurrentPage.As(HomePage.class).getDomainsInHomepageDropdownNAMES(con);
		// remove domains that are not in the above list
	    for (String domain : domains) {
			if (assignedDomainValues.contains(domain))
				newDomains.add(domain);
		}
	    
		// Choose a random domain
		Random randomizer = new Random();
		String randomDomain = newDomains.get(randomizer.nextInt(newDomains.size()));
		
		// Select the random domain
		randomDomain = selectDomainFromDropdown(randomDomain);
		
		return randomDomain;
	}
	
	/**
	 * Select a random value from the domain dropdown
	 * @return the picked choice
	 * @throws SQLException 
	 */
	public String chooseRandomDomainWithMoodAndGesture(Connection con, String chosenProject, String userEmail) throws SQLException {
		
		// Click on the domain dropdown
		cmbDomain.click();
		
		// Get Domains that have values in styles and gestures and moods
		  String query = "select smi_domain_id from smi_domain_allowed_moods where mood_id in (select mood_id from mood where supported is TRUE AND mood_id<>10) " 
				  	   + "INTERSECT " 
				  	   + "select smi_domain_id from smi_domain_allowed_gestures where gesture_id in (select gesture_id from gesture where supported is TRUE) " 
				       + "INTERSECT " 
				  	   + "select DISTINCT smi_domain_id from smi_domain where language_id= " + Setting.Language 
		               + " ORDER BY smi_domain_id ";
		      
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		
		// Get the moods names of the moods ids
		List<String> domains = new ArrayList<String>();
		ResultSet resultSetIn;
	    while (resultSet.next()) {
	    	query = "select domain_name from smi_domain WHERE supported is TRUE and smi_domain_id = " + resultSet.getString(1);
	    	resultSetIn = DatadUltilities.ExecuteQuery(query, con);
	    	resultSetIn.next();
	    	try {
	    		domains.add(resultSetIn.getString(1));
			} catch (Exception e) {
				System.out.println("Domain is not supported, don't add: " + e);
			}
	    	
	    }
	    
	    List<String> newDomains = new ArrayList<String>();
	    // Use the below function to retrieve available domains for the user
	    List<String> assignedDomainValues = CurrentPage.As(HomePage.class).getVDomainValuesFromDBandPrivatesDomains(con, chosenProject, userEmail);
	    // if empty , try the default project of System
	    if(assignedDomainValues.size() == 0)
		    assignedDomainValues = CurrentPage.As(HomePage.class).getVDomainValuesFromDBandPrivatesDomains(con, "System", userEmail);

		// remove domains that are not in the above list
	    for (String domain : domains) {
			if (assignedDomainValues.contains(domain))
				newDomains.add(domain);
		}
	    
		// Choose a random domain
		Random randomizer = new Random();
		String randomDomain = newDomains.get(randomizer.nextInt(newDomains.size()));
		
		// Select the random domain
		randomDomain = selectDomainFromDropdown(randomDomain);
		
		return randomDomain;
	}
	
	// Choose domain if it does or does not have (P)
	public String selectDomainFromDropdown(String randomDomain) {
		randomDomain = randomDomain.trim();
		// Get available domains from the open dropdown
		List<String> getValues = Get_List_Items_Value(cmbDomain); 
		getValues.removeAll(Arrays.asList(""));
//		getValues.remove(0);
		
		// check if we have the domain available
		if (getValues.contains(randomDomain + " (P)"))
			randomDomain = randomDomain + " (P)";
		
		this.Select_Item_In_ListBox(cmbDomain, randomDomain);
		return randomDomain;
	}
      
	/**
	 * Select a random value from the domain dropdown
	 * @return the picked choice
	 * @throws SQLException 
	 */
	public String chooseRandomDomainShortVersion(Connection con) throws SQLException {
		
		// Click on the domain dropdown
		cmbDomain.click();
		
		// Get Domains that have values in styles and gestures and moods
		String query = "select DISTINCT smi_domain_id from smi_domain where open_to_public is TRUE "
				+ "ORDER BY smi_domain_id";
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		
		// Get the moods names of the moods ids
		List<String> domains = new ArrayList<String>();
		ResultSet resultSetIn;
	    while (resultSet.next()) {
	    	query = "select domain_name from smi_domain WHERE supported is TRUE and smi_domain_id = " + resultSet.getString(1);
	    	resultSetIn = DatadUltilities.ExecuteQuery(query, con);
	    	resultSetIn.next();
	    	try {
	    		domains.add(resultSetIn.getString(1));
			} catch (Exception e) {
				System.out.println("Domain is not supported, don't add: " + e);
			}

	    }
		
		// Choose a random domain
		Random randomizer = new Random();
		String randomDomain = domains.get(randomizer.nextInt(domains.size()));
		
		// Select the random domain
		randomDomain = selectDomainFromDropdown(randomDomain);
		
		return randomDomain;
	}
	
//	/**
//	 * Select a random value from the domain dropdown
//	 * @return the picked choice
//	 */
//	public String chooseRandomDomain() {		
//		// Get the available domain
//		List<String> domainValues = this.getDomainValues();
//		// Delete first empty value
//		domainValues.remove(0);
//		
//		// Choose a random voice
//		Random randomizer = new Random();
//		String randomDomain = domainValues.get(randomizer.nextInt(domainValues.size()));
//		
//		// Select the random domain
//		this.Select_Item_In_ListBox(cmbDomain, randomDomain);
//		
//		return randomDomain;
//	}
	
	/**
	 * Select a random value from the voice dropdown
	 * @return the picked choice
	 * @throws InterruptedException 
	 */
	public String chooseRandomVoice() throws InterruptedException {
		
		System.out.println("choose random voice func");
		// Get the available voices
		List<String> voiceValues = null;
		boolean tryAgain = false;
		String randomVoice = "";
	
		Thread.sleep(2000);
		
		tryAgain = false;
		do {
			do {
				try {
					System.out.println("try get voices values func");
					voiceValues = this.getVoiceValues();
					System.out.println("voicesValues:" + voiceValues);
				} catch (Exception e) {
				System.out.println("Failed in voice dropdown, Trying again...");
				this.smorphBody.click();
				System.out.println("clicked aside to close dropdown");
				}
				if (voiceValues == null) tryAgain = true;
				else if (voiceValues.isEmpty()) tryAgain = true;
				else tryAgain = false;
				
			} while (tryAgain);
			
			// Delete first empty value
//			voiceValues.remove(0);
			// to be removed later
//			voiceValues.remove("Charles");
//			voiceValues.remove("אהרון");
			// end of removal

//			System.out.println(voiceValues);
			
			// Choose a random voice
			Random randomizer = new Random();
			randomVoice = voiceValues.get(randomizer.nextInt(voiceValues.size()));
			
			System.out.println("Selected random voice: " + randomVoice);
			// Select the random voice
			try {
				this.Select_Item_In_ListBox(cmbVoice, randomVoice);
			} catch (Exception e) {
				System.out.println("Failed to select, try again...");
			}
			
			String checkChosenVoice = CurrentPage.As(HomePage.class).cmbVoice.getText();
			
			if (!checkChosenVoice.equals(randomVoice)) 
			{
				tryAgain = true;
				cmbVoice.click();
			}
			else  tryAgain = false;
			System.out.println("checkChosenVoice: " + checkChosenVoice + ", randomVoice: " + randomVoice);
			
		} while (tryAgain);
		
		return randomVoice;
	}
	
	/**
	 * Select a random value from the age dropdown
	 * @return the picked choice
	 */
	public String chooseRandomAge() {
		// Get the available ages
		List<String> ageValues = this.getAgeValues();
		// Delete first empty value
		ageValues.remove(0);
		
		// Choose a random voice
		Random randomizer = new Random();
		String randomAge = ageValues.get(randomizer.nextInt(ageValues.size()));
		
		// Select the random voice
		this.Select_Item_In_ListBox(cmbAge, randomAge);
		
		return randomAge;
	}
	
	/**
	 * Select a random value from the voice dropdown
	 * @return the picked choice
	 */
		public String filterWithRandomVoice() 
		{
			// Get the available voices
			List<String> voiceValues = this.getVoiceValues();
			
			// Delete first empty value
			voiceValues.remove(0);
			
			// Choose a random voice
			Random randomizer = new Random();
			String randomVoice = voiceValues.get(randomizer.nextInt(voiceValues.size()));
			
			// Type and filter by the random picked voice
			String[] splited = randomVoice.split("\\s+");			
			this.txtVoiceSearch.sendKeys(splited[0]);
			lblDomain.click();
			this.btnVoiceSearch.click();
			
			return splited[0];
		}
		
		/**
		 * Get a random age for an active voice
		 * @param con - get SQL connection 
		 * @return 
		 * @throws SQLException
		 */
		public String getRandomAgeForActiveVoices(Connection con) throws SQLException {
			
			// Get first an age that have a supported voice
			String query = "select distinct age_id from smi_voice WHERE supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		    
		    // Use metdata in order to fetch columns names
		    ResultSetMetaData rsmd = resultSet.getMetaData();

		    // Number of columns
		    int columnsNumber = rsmd.getColumnCount();

		    //To save the fetched rows
		    List<List<String>> table = new ArrayList<List<String>>();
		    List<String> columnNames = new ArrayList<String>();
		    List<String> ageRow = new ArrayList<String>();
		    
		    // Get the columns names
	        for (int i = 1; i <= columnsNumber; i++) {
	        	columnNames.add(rsmd.getColumnName(i));
	        }

		    // Get the ditinct age values
		    while (resultSet.next()) {
		        for (int i = 1; i <= columnsNumber; i++) {
		        	ageRow.add(resultSet.getString(i));
		        }
		    }

		    table.add(columnNames);
		    table.add(ageRow);
		    
		    // Get random age from the retrieved list
		    Random randomizer = new Random();
			String RandomActiveAge = ageRow.get(randomizer.nextInt(ageRow.size()));
			
			// Retrieve the dropdown value for the chosen random active voice
			query = "select name from age WHERE age_id = " + RandomActiveAge;
			resultSet = DatadUltilities.ExecuteQuery(query, con);
			
		    resultSet.next();
		    return resultSet.getString(1);
		}
		
		/**
		 * Get a random language for an active voice
		 * @param con - get SQL connection 
		 * @return 
		 * @throws SQLException
		 */
		public String getRandomLanguageForActiveVoices(Connection con) throws SQLException {
			
			// Get first a Language that have a supported voice
			String query = "select distinct language_id from smi_voice WHERE supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		    
		    // Use metdata in order to fetch columns names
		    ResultSetMetaData rsmd = resultSet.getMetaData();

		    // Number of columns
		    int columnsNumber = rsmd.getColumnCount();

		    //To save the fetched rows
		    List<List<String>> table = new ArrayList<List<String>>();
		    List<String> columnNames = new ArrayList<String>();
		    List<String> languageRow = new ArrayList<String>();
		    
		    // Get the columns names
	        for (int i = 1; i <= columnsNumber; i++) {
	        	columnNames.add(rsmd.getColumnName(i));
	        }

		    // Get the distinct language values
		    while (resultSet.next()) {
		        for (int i = 1; i <= columnsNumber; i++) {
		        	languageRow.add(resultSet.getString(i));
		        }
		    }

		    table.add(columnNames);
		    table.add(languageRow);
		    
		    // Get random language from the retrieved list
		    Random randomizer = new Random();
			String RandomActiveAge = languageRow.get(randomizer.nextInt(languageRow.size()));
			
			// Retrieve the dropdown value for the chosen random active voice
			query = "select name from language WHERE language_id = " + RandomActiveAge;
			resultSet = DatadUltilities.ExecuteQuery(query, con);
			
		    resultSet.next();
		    return resultSet.getString(1);
		}
		
		/**
		 * Get a random language for an active voice
		 * @param con - get SQL connection 
		 * @return 
		 * @throws SQLException
		 */
		public String getRandomGenderForActiveVoices(Connection con) throws SQLException {
			
			// Get first a Gender that have a supported voice
			String query = "select distinct gender_id from smi_voice WHERE supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		    
		    // Use metdata in order to fetch columns names
		    ResultSetMetaData rsmd = resultSet.getMetaData();

		    // Number of columns
		    int columnsNumber = rsmd.getColumnCount();

		    //To save the fetched rows
		    List<List<String>> table = new ArrayList<List<String>>();
		    List<String> columnNames = new ArrayList<String>();
		    List<String> genderRow = new ArrayList<String>();
		    
		    // Get the columns names
	        for (int i = 1; i <= columnsNumber; i++) {
	        	columnNames.add(rsmd.getColumnName(i));
	        }

		    // Get the distinct gender values
		    while (resultSet.next()) {
		        for (int i = 1; i <= columnsNumber; i++) {
		        	genderRow.add(resultSet.getString(i));
		        }
		    }

		    table.add(columnNames);
		    table.add(genderRow);
		    
		    // Get random gender from the retrieved list
		    Random randomizer = new Random();
			String RandomActiveGender = genderRow.get(randomizer.nextInt(genderRow.size()));
			
			// Retrieve the dropdown value for the chosen random active voice
			query = "select name from gender WHERE gender_id = " + genderRow.get(0);
			resultSet = DatadUltilities.ExecuteQuery(query, con);
			
		    resultSet.next();
		    return resultSet.getString(1);
		}

		/**
		 * Get from the database, a list of the active voices that are assigned to a specific age
		 * @param con - DB connection
		 * @param ChosenAge - Search for active voices for this chosen age
		 * @return list of active voices
		 * @throws SQLException
		 */
		public List<String> getActiveVoicesForAge(Connection con, String chosenAge, String accountId) throws SQLException {
			// Get the id of the selected age
			String query = "select age_id from age WHERE name iLike '" + chosenAge + "'";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		    resultSet.next();
		    String agdId = resultSet.getString(1);
		 
		 // Get the voices that were given permission in smi_voice_permissions table
		    List<String> voicesWithPermissions = new ArrayList<String>();
		    List<String> allVoices = new ArrayList<String>();
		    String query2 = "SELECT smi_voice_id voice_key, 'tokenRead' token, supported, queue_name, language_id::text, "
		    + "RTRIM(CASE WHEN open_to_public THEN 'ALL, ' || CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI' ELSE smi_voice_owner_account_id END || ', ' "
		    + "ELSE CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI' "
		    + "ELSE smi_voice_owner_account_id END END || COALESCE(string_agg(account_id, ', '), ''), ', ') "
		    + "permissions_value FROM smi_voice LEFT OUTER JOIN smi_voice_permissions "
		    + "USING(smi_voice_id) WHERE smi_voice_id IN(select smi_voice_id from smi_voice WHERE supported is true and language_id="+Setting.Language+" ) "
		    + "AND supported GROUP BY smi_voice_id, open_to_public";
		    ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query2, con);

		    // Get voices id for the chosen account
		    while(resultSet2.next())
		    if(resultSet2.getString(6).contains(accountId) || resultSet2.getString(6).contains("ALL") )
		    voicesWithPermissions.add(resultSet2.getString(1));

		    // Take only related language of the above list
		    for (String one : voicesWithPermissions) {

		    String query_smi_voice_id = "SELECT name from smi_voice where smi_voice_id = " + one + " AND language_id = " + Setting.Language + " AND supported is TRUE AND age_id="+agdId;
		    resultSet = DatadUltilities.ExecuteQuery(query_smi_voice_id, con);
		    boolean result = resultSet.next();
		    if (result)
		    allVoices.add(resultSet.getString(1).trim());
		    }
		    return allVoices;
		}
		
		/**
		 * Get from the database, a list of the active voices that are assigned to a specific language
		 * @param con - DB connection
		 * @param ChosenAge - Search for active voices for this chosen age
		 * @return list of active voices
		 * @throws SQLException
		 */
		public List<String> getActiveVoicesForLanguage(Connection con, String chosenLanguage, String accountId) throws SQLException {
			// Get the id of the selected age
			String query = "select language_id from language WHERE name iLike '" + chosenLanguage + "'";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		    resultSet.next();
		    String languageId = resultSet.getString(1);
		    
		 // Get the voices that were given permission in smi_voice_permissions table
		    List<String> voicesWithPermissions = new ArrayList<String>();
		    List<String> allVoices = new ArrayList<String>();
		    String query2 = "SELECT smi_voice_id voice_key, 'tokenRead' token, supported, queue_name, language_id::text, "
		    + "RTRIM(CASE WHEN open_to_public THEN 'ALL, ' || CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI' ELSE smi_voice_owner_account_id END || ', ' "
		    + "ELSE CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI' "
		    + "ELSE smi_voice_owner_account_id END END || COALESCE(string_agg(account_id, ', '), ''), ', ') "
		    + "permissions_value FROM smi_voice LEFT OUTER JOIN smi_voice_permissions "
		    + "USING(smi_voice_id) WHERE smi_voice_id IN(select smi_voice_id from smi_voice WHERE supported is true and language_id="+languageId+" ) "
		    + "AND supported GROUP BY smi_voice_id, open_to_public";
		    ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query2, con);

		    // Get voices id for the chosen account
		    while(resultSet2.next())
		    if(resultSet2.getString(6).contains(accountId) || resultSet2.getString(6).contains("ALL"))
		    voicesWithPermissions.add(resultSet2.getString(1));

		    // Take only related language of the above list
		    for (String one : voicesWithPermissions) {

		    String query_smi_voice_id = "SELECT name from smi_voice where smi_voice_id = " + one + " AND language_id = " + languageId + " AND supported is TRUE";
		    resultSet = DatadUltilities.ExecuteQuery(query_smi_voice_id, con);
		    boolean result = resultSet.next();
		    if (result)
		    allVoices.add(resultSet.getString(1).trim());
		    }
		    return allVoices;
		}
		
		/**
		 * Get from the database, a list of the active voices that are assigned to a specific language
		 * @param con - DB connection
		 * @param ChosenAge - Search for active voices for this chosen age
		 * @return list of active voices
		 * @throws SQLException
		 */
		public List<String> getActiveVoicesForGender(Connection con, String chosenGender, String accountId) throws SQLException {
			// Get the id of the selected gender
			String query = "select gender_id from gender WHERE name iLike '" + chosenGender + "'";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		    resultSet.next();
		    String genderId = resultSet.getString(1);
		    List<String> voicesWithPermissions = new ArrayList<String>();
		    List<String> allVoices = new ArrayList<String>();
		    String query2 = "SELECT smi_voice_id voice_key, 'tokenRead' token, supported, queue_name, language_id::text, "
		    + "RTRIM(CASE WHEN open_to_public THEN 'ALL, ' || CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI' ELSE smi_voice_owner_account_id END || ', ' "
		    + "ELSE CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI' "
		    + "ELSE smi_voice_owner_account_id END END || COALESCE(string_agg(account_id, ', '), ''), ', ') "
		    + "permissions_value FROM smi_voice LEFT OUTER JOIN smi_voice_permissions "
		    + "USING(smi_voice_id) WHERE smi_voice_id IN(select smi_voice_id from smi_voice WHERE supported is true and language_id="+Setting.Language+" ) "
		    + "AND supported GROUP BY smi_voice_id, open_to_public";
		    ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query2, con);

		    // Get voices id for the chosen account
		    while(resultSet2.next())
		    if(resultSet2.getString(6).contains(accountId) || resultSet2.getString(6).contains("ALL"))
		    voicesWithPermissions.add(resultSet2.getString(1));
		    
			// Take only related language of the above list
			for (String one : voicesWithPermissions) {
			
				String query_smi_voice_id = "SELECT name from smi_voice where smi_voice_id = " + one + " AND language_id = " + Setting.Language + " AND supported is TRUE AND gender_id = " + genderId;
				resultSet = DatadUltilities.ExecuteQuery(query_smi_voice_id, con);
				boolean result = resultSet.next();
				if (result)
					allVoices.add(resultSet.getString(1));
			}
			return allVoices;
		}

		/**
		 * Get the Gender values from the database
		 * @param con - SQL connection
		 * @return list of gender values
		 * @throws SQLException
		 */
		public List<String> getGenderValuesFromDB(Connection con) throws SQLException 
		{
			// Query to get the Gender values
			String query = "select name from gender WHERE supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

			// Save the Gender values in a list
			List<String> rowsValues = new ArrayList<String>();
		    while (resultSet.next()) {
		    	rowsValues.add(resultSet.getString(1));
		    }		    
			return rowsValues;
		}
		
		/**
		 * Get the Age values from the database
		 * @param con - SQL connection
		 * @return list of Age values
		 * @throws SQLException
		 */
		public List<String> getAgeValuesFromDB(Connection con) throws SQLException 
		{
			// Query to get the Language values
			String query = "select name from age WHERE supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

			// Save the Age values in a list
			List<String> rowsValues = new ArrayList<String>();
		    while (resultSet.next()) {
		    	rowsValues.add(resultSet.getString(1));
		    }		    
			return rowsValues;
		}
		
		/**
		 * 
		 * Get the Language values from the database
		 * @param con - SQL connection
		 * @return list of Language values
		 * @throws SQLException
		 */
		public List<String> getLanguageValuesFromDB(Connection con) throws SQLException 
		{
			// Query to get the Age values
			String query = "select name from language WHERE supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

			// Save the Age values in a list
			List<String> rowsValues = new ArrayList<String>();
		    while (resultSet.next()) {
		    	rowsValues.add(resultSet.getString(1));
		    }		    
			return rowsValues;
		}
		
		/**
		 * 
		 * Get the Voice values from the database
		 * @param con - SQL connection
		 * @return list of Voice values
		 * @throws SQLException
		 */
		public List<String> getVoiceValuesFromDB(Connection con, String accountId) throws SQLException 
		{
			// Get the voices that were given permission in smi_voice_permissions table
			List<String> voicesWithPermissions = new ArrayList<String>();
			List<String> allVoices = new ArrayList<String>();
			String query = "SELECT smi_voice_id voice_key, 'tokenRead' token,  supported, queue_name, language_id::text, "
					+ "RTRIM(CASE WHEN open_to_public THEN 'ALL, ' ||  CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI'  ELSE smi_voice_owner_account_id END || ', ' "
					+ "ELSE  CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI'  "
					+ "ELSE smi_voice_owner_account_id END END ||  COALESCE(string_agg(account_id, ', '), ''), ', ') "
					+ "permissions_value FROM smi_voice LEFT OUTER JOIN smi_voice_permissions "
					+ "USING(smi_voice_id) WHERE smi_voice_id IN(select smi_voice_id from smi_voice WHERE supported is true and language_id="+Setting.Language+" )  "
					+ "AND supported GROUP BY smi_voice_id, open_to_public";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
			
			// Get voices id for the chosen account
			while(resultSet.next())
				if(resultSet.getString(6).contains(accountId) || resultSet.getString(6).contains("ALL"))
					voicesWithPermissions.add(resultSet.getString(1));
			
			// Take only related language of the above list
			for (String one : voicesWithPermissions) {
			
				String query_smi_voice_id = "SELECT name from smi_voice where smi_voice_id = " + one + " AND language_id = " + Setting.Language + " AND supported is TRUE";
				resultSet = DatadUltilities.ExecuteQuery(query_smi_voice_id, con);
				boolean result = resultSet.next();
				if (result)
					allVoices.add(resultSet.getString(1).trim());
			}
			return allVoices;
		}
		
		/**
		 * 
		 * Get the domain values from the database
		 * @param con - SQL connection
		 * @return list of Voice values
		 * @throws SQLException
		 */
		public List<String> getVDomainValuesFromDB(Connection con) throws SQLException 
		{
			// Query to get the active domain names
			String query = "select domain_name from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE AND open_to_public is TRUE";
			System.out.println("query: " + query);
//			String query = "select domain_name from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

			// Save the domain names in a list
			List<String> rowsValues = new ArrayList<String>();
		    while (resultSet.next()) {
		    	rowsValues.add(resultSet.getString(1).replace(" (P)", ""));
		    }		    
			return rowsValues;
		}
		
		public List<String> getChildDomains(Connection con, String domainFather, List<String> resultDomains) throws SQLException
		{
			List<String> childDomains = new ArrayList<String>();
			childDomains.add(domainFather);
			
//			domainFather = domainFather.replace("'", "''").replace(" (P)", "");
			String query_smi_domain_id_domains = "SELECT * FROM project WHERE project_name ILike '" + domainFather.replace("'", "''").replace(" (P)", "") + "'";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id_domains, con);
			resultSet.next();
			String smi_domain_id_domains = resultSet.getString(1);
			
			String query_childs_domains = "SELECT domain_name from smi_domain WHERE smi_domain_owner_project_id= '%" + smi_domain_id_domains + "%' AND open_to_public is TRUE";
			resultSet = DatadUltilities.ExecuteQuery(query_childs_domains, con);
			while(resultSet.next()) {
//				childDomains.add(resultSet.getString(1));
				getChildDomains(con, resultSet.getString(1), resultDomains);
			}
			resultDomains.add(domainFather);
			return resultDomains;
		}
		
		/**
		 * 
		 * Get the domain values from the database
		 * @param con - SQL connection
		 * @return list of Voice values
		 * @throws SQLException
		 */
		public List<String> getVDomainValuesFromDBandPrivatesDomains(Connection con, String chosenProject, String userEmail) throws SQLException 
		{
			List<String> resultDomains = new ArrayList<String>();
			List<String> allDomains =  getChildDomains(con, chosenProject, resultDomains);
			List<String> resultDomains1 = new ArrayList<String>();
			
			// Get all the child domains of the project
			// ---------------------------
			// Now fetch the domains under the above related father domains and projects

//			// Query to get the active domain names
//			String query = "select domain_name from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE AND open_to_public is TRUE";
////			String query = "select domain_name from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE";
//			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
//
//			// Save the domain names in a list
////			List<String> rowsValues = new ArrayList<String>();
//		    while (resultSet.next()) {
//		    	allDomains.add(resultSet.getString(1).replace(" (P)", ""));
//		    }
			
			// Query to get the active domain names
			String project_id = getProjectId(con, chosenProject);
			String query ="SELECT domain_name,smi_domain_owner_project_id,open_to_public FROM smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE AND smi_domain_owner_project_id='"+project_id+"'";
//						String query = "select domain_name from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

			 while (resultSet.next()) {
				 // Add domain name
				    if(resultSet.getString(3).equals("f"))
				    	resultDomains1.add(resultSet.getString(1)+" (P)");
				    else
				    	resultDomains1.add(resultSet.getString(1));	 
			 }
			 
			
			 
//			// Save the domain names in a list
////						List<String> rowsValues = new ArrayList<String>();
//		    while (resultSet.next()) {
//		    	System.out.println("domain to check: " + resultSet.getString(1));
//		    	// Check that this domain have an active project
//		    	String query_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = " + resultSet.getString(1).replace(" (P)", "");
//		    	ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query_project_id, con);
//				boolean exist = resultSet2.next();
//				if (exist) {
//					String smi_domain_id = resultSet2.getString(1);
//					System.out.println("project id: " + resultSet2.getString(1));
//					// Check if project is active
//					String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = " + smi_domain_id + " AND open_to_public is True AND supported is True";
//					resultSet2 = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
//					exist = resultSet2.next();
//					System.out.println("is project active: " + exist);
//					if (exist)
//						allDomains.add(resultSet.getString(1).replace(" (P)", ""));
//				}
//		    }
					    
//		    // Get selected project id
//			query = "select smi_domain_id from smi_domain WHERE domain_name iLIKE '" + chosenProject + "'";
//			resultSet = DatadUltilities.ExecuteQuery(query, con);
//			
//			boolean thereIsData = resultSet.next();
//			String projectID = null;
//			if (thereIsData)
//				projectID = resultSet.getString(1);
			
		    // Get private domains
//		    query="SELECT domain_name FROM smi_domain WHERE smi_domain_id ="+ projectID +" AND smi_domain_owner_email iLike'" + userEmail + "'";
//		    query="SELECT domain_name FROM smi_domain WHERE language_id = " + Setting.Language + " AND smi_domain_owner_email iLike '" + userEmail + "' AND supported is TRUE AND open_to_public is false";
//			resultSet = DatadUltilities.ExecuteQuery(query, con);
//			// Add the domain name to the list
//			while(resultSet.next())
//				allDomains.add(resultSet.getString(1));
			
			// Get the domains that were given permission in smi_domain_permissions table
			List<String> domainsWithPermissions = new ArrayList<String>();
			query = "select * from smi_domain_permissions where project_id ILike '" +  project_id + "'";
			resultSet = DatadUltilities.ExecuteQuery(query, con);
			while(resultSet.next())
				domainsWithPermissions.add(resultSet.getString(1));
			// Take only related language of the above list
			for (String one : domainsWithPermissions) {
			
				String query_smi_domain_id = "SELECT domain_name from smi_domain where smi_domain_id = " + one + " AND language_id = " + Setting.Language + " AND supported is TRUE";
				System.out.println("query_smi_domain_id: "+ query_smi_domain_id);
				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
				boolean result = resultSet.next();
				if (result)
					allDomains.add(resultSet.getString(1));
			}
			
//		    // Go on the domain result list and if one of them is project then remove it
//			List<String> rowsValuesNew = new ArrayList<String>();
//			for (String value : allDomains) {
////				value = value.replace("'", "''");
//				// Get ID
//				int count = StringUtils.countMatches(value, "'");
//				if (count == 1)
//					value = value.replace("'", "''");
//				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where domain_name ILike '" + value + "'";
//				System.out.println("query_smi_domain_id: "+ query_smi_domain_id);
//				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
//				resultSet.next();
//				String smi_domain_id = resultSet.getString(1);
//				String query_parent_project_id = "SELECT parent_project_id from smi_domain where domain_name ILike '" + value + "'";
//				System.out.println("query_parent_project_id: "+ query_parent_project_id);
//				resultSet = DatadUltilities.ExecuteQuery(query_parent_project_id, con);
//				resultSet.next();
//				String parent_project_id = resultSet.getString(1);
//				if (!smi_domain_id.equals(parent_project_id))
//					rowsValuesNew.add(value.replace("''", "'"));
//			}
			
			// Get default open to public domains
			if(resultDomains1.size() == 0)
			{
				 query = "SELECT domain_name,open_to_public FROM smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE AND smi_domain_owner_project_id='S4398046511105-4398046511106'";
				 resultSet = DatadUltilities.ExecuteQuery(query, con);
					while(resultSet.next())
						if(resultSet.getString(2).equals("t"))
							resultDomains1.add(resultSet.getString(1));
			}
			
			return resultDomains1;
		}
		
		public List<String> getAllProjectsForAllPermittedDomains(Connection con, String projectName) throws SQLException 
		{
			List<String> resultDomains = new ArrayList<String>();
			
			List<String> allDomains =  new ArrayList<String>();
			
			// Get all the child domains of the project
			// ---------------------------
			// Now fetch the domains under the above related father domains and projects

			// Query to get the active domain names
			String query = "select smi_domain_id from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE AND open_to_public is TRUE";
//			String query = "select domain_name from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

			// Save the domain names in a list
//			List<String> rowsValues = new ArrayList<String>();
		    while (resultSet.next()) {
		    	System.out.println("domain to check: " + resultSet.getString(1));
		    	// Check that this domain have an active project
		    	String query_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = " + resultSet.getString(1).replace(" (P)", "");
		    	ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query_project_id, con);
				boolean exist = resultSet2.next();
				if (exist) {
					String smi_domain_id = resultSet2.getString(1);
					System.out.println("project id: " + resultSet2.getString(1));
					// Check if project is active
					String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = " + smi_domain_id + " AND open_to_public is True AND supported is True";
					resultSet2 = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
					exist = resultSet2.next();
					System.out.println("is project active: " + exist);
					if (exist)
						allDomains.add(resultSet.getString(1).replace(" (P)", ""));
				}
		    }
		    
//		    // Get selected project id
//			query = "select smi_domain_id from smi_domain WHERE domain_name iLike '" + chosenProject + "'";
//			resultSet = DatadUltilities.ExecuteQuery(query, con);
//			
//			boolean thereIsData = resultSet.next();
//			String projectID = null;
//			if (thereIsData)
//				projectID = resultSet.getString(1);
			
		    // Get private domains
//		    query="SELECT domain_name FROM smi_domain WHERE smi_domain_id ="+ projectID +" AND smi_domain_owner_email iLike'" + userEmail + "'";
		    query="SELECT domain_name FROM smi_domain WHERE language_id = " + Setting.Language + " AND smi_domain_owner_project_id iLike '" + getProjectId(con, projectName) + "' AND supported is TRUE AND open_to_public is false";	
			resultSet = DatadUltilities.ExecuteQuery(query, con);
			// Add the domain name to the list
			while(resultSet.next())
				allDomains.add(resultSet.getString(1));
			
			// Get the domains that were given permission in smi_domain_permissions table
			List<String> domainsWithPermissions = new ArrayList<String>();
			query = "select smi_domain_id from smi_domain_permissions where project_id ILike '" +  getProjectId(con, projectName) + "'";
			resultSet = DatadUltilities.ExecuteQuery(query, con);
			while(resultSet.next())
				domainsWithPermissions.add(resultSet.getString(1));
			// Take only related language of the above list
			for (String one : domainsWithPermissions) {
			
				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = " + one + " AND language_id = " + Setting.Language + " AND supported is TRUE";
				System.out.println("query_smi_domain_id: "+ query_smi_domain_id);
				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
				boolean result = resultSet.next();
				if (result)
					allDomains.add(resultSet.getString(1));
			}
			
			// Check that the collected domains have an active project
			
			List<String> rowsValuesNew = new ArrayList<String>();
//			List<String> domainIDs = new ArrayList<String>();
//			// Get smi domains
//			for (String value : allDomains) {
////				value = value.replace("'", "''");
//				// Get ID
//				int count = StringUtils.countMatches(value, "'");
//				if (count == 1)
//					value = value.replace("'", "''");
//				// Could have more than domain with the same name so get all their smi ids first
//				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = '" + value + "'";
//				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
//				while(resultSet.next())
//					domainIDs.add(resultSet.getString(1));
//			}
//			System.out.println();
			// Check the project of each smi id and check if it is active
			for (String ID : allDomains) {
				// Get project ID
				String query_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = " + ID;
				resultSet = DatadUltilities.ExecuteQuery(query_project_id, con);
				resultSet.next();
				String smi_domain_id = resultSet.getString(1);
				// Check if project is active
				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = " + smi_domain_id + " AND supported is True";
				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
				boolean exist = resultSet.next();
				if (exist)
					rowsValuesNew.add(ID.replace("''", "'"));
			}
			
			// Go on the domain result list and if one of them is project then remove it
			List<String> rowsValuesNew2 = new ArrayList<String>();
			for (String value : rowsValuesNew) {
////							value = value.replace("'", "''");
//				// Get ID
//				int count = StringUtils.countMatches(value, "'");
//				if (count == 1)
//					value = value.replace("'", "''");
//				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where domain_name ILike '" + value + "'";
//				System.out.println("query_smi_domain_id: "+ query_smi_domain_id);
//				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
//				resultSet.next();
//				String smi_domain_id = resultSet.getString(1);
				String query_parent_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = '" + value + "'";
				System.out.println("query_parent_project_id: "+ query_parent_project_id);
				resultSet = DatadUltilities.ExecuteQuery(query_parent_project_id, con);
				resultSet.next();
				String parent_project_id = resultSet.getString(1);
				if (!value.equals(parent_project_id))
					rowsValuesNew2.add(value.replace("''", "'"));
			}
						
			Set<String> hs = new HashSet<>();
			hs.addAll(rowsValuesNew2);
			rowsValuesNew2.clear();
			rowsValuesNew2.addAll(hs);
			
			return rowsValuesNew2;
		}
		
		// Check if we have only one domain within one project
		public boolean isThereMoreThanOneAssignedDomain(Connection con) {
			try
			{
				ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
				List<String> assignedDomains = GetInstance(HomePage.class).As(HomePage.class).getAllProjectsForAllPermittedDomains( con, ExcelUltility.ReadCell("Email",1));
				if (assignedDomains.size() > 1)
					return true;
				else return false;
			}
			catch(Exception e)
			{
				return false;
			}
		}
		
		// Get domains ids that are displayed in homepage dropdown
		public List<String> getDomainsInHomepageDropdown(Connection con) {
			try
			{
				ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
//				List<String> assignedDomains = GetInstance(HomePage.class).As(HomePage.class).getAllProjectsForAllPermittedDomains( con, ExcelUltility.ReadCell("Email",1));
				List<String> assignedDomains = new ArrayList<String>();
				String query = "select smi_domain_id from smi_domain WHERE language_id=" + Setting.Language + " AND supported is TRUE AND open_to_public is TRUE";
//				String query = "select smi_domain_id from smi_domain WHERE language_id=" + Setting.Language + " AND supported is TRUE";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

				// Save the domain names in a list
			    while (resultSet.next())
			    	assignedDomains.add(resultSet.getString(1));
			    return assignedDomains;
			}
			catch(Exception e)
			{
			}
			return null;
		}
		
		// Get domains names that are displayed in homepage dropdown
		public List<String> getDomainsInHomepageDropdownNAMES(Connection con) throws SQLException {
			List<String> domainsIDs = getDomainsInHomepageDropdown(con);
			
			List<String> domainsNames = new ArrayList<String>();
			for (String value : domainsIDs) {
				String query = "select TRIM(domain_name) FROM smi_domain WHERE smi_domain_id = " +  value;
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				resultSet.next();
				domainsNames.add(resultSet.getString(1));
			}
			return domainsNames; 	
		}
				
		public List<String> getTheOnlyProjectAssignedToUser(Connection con, String userEmail) throws SQLException 
		{
			List<String> allDomains =  new ArrayList<String>();
			
			// Get all the child domains of the project
			// ---------------------------
			// Now fetch the domains under the above related father domains and projects

			// Query to get the active domain names
			String query = "select smi_domain_id from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE AND open_to_public is TRUE";
//			String query = "select domain_name from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE";
			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

			// Save the domain names in a list
//			List<String> rowsValues = new ArrayList<String>();
		    while (resultSet.next()) {
		    	System.out.println("domain to check: " + resultSet.getString(1));
		    	// Check that this domain have an active project
		    	String query_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = " + resultSet.getString(1).replace(" (P)", "");
		    	ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query_project_id, con);
				boolean exist = resultSet2.next();
				if (exist) {
					String smi_domain_id = resultSet2.getString(1);
					System.out.println("project id: " + resultSet2.getString(1));
					// Check if project is active
					String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = " + smi_domain_id + " AND open_to_public is True AND supported is True";
					resultSet2 = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
					exist = resultSet2.next();
					System.out.println("is project active: " + exist);
					if (exist)
						allDomains.add(resultSet.getString(1).replace(" (P)", ""));
				}
		    }
		    
//		    // Get selected project id
//			query = "select smi_domain_id from smi_domain WHERE domain_name iLike '" + chosenProject + "'";
//			resultSet = DatadUltilities.ExecuteQuery(query, con);
//			
//			boolean thereIsData = resultSet.next();
//			String projectID = null;
//			if (thereIsData)
//				projectID = resultSet.getString(1);
			
		    // Get private domains
//		    query="SELECT domain_name FROM smi_domain WHERE smi_domain_id ="+ projectID +" AND smi_domain_owner_email iLike'" + userEmail + "'";
		    query="SELECT domain_name FROM smi_domain WHERE language_id = " + Setting.Language + " AND smi_domain_owner_email iLike '" + userEmail + "' AND supported is TRUE AND open_to_public is false";	
			resultSet = DatadUltilities.ExecuteQuery(query, con);
			// Add the domain name to the list
			while(resultSet.next())
				allDomains.add(resultSet.getString(1));
			
			// Get the domains that were given permission in smi_domain_permissions table
			List<String> domainsWithPermissions = new ArrayList<String>();
			query = "select smi_domain_id from smi_domain_permissions where user_email ILike '" +  userEmail + "'";
			resultSet = DatadUltilities.ExecuteQuery(query, con);
			while(resultSet.next())
				domainsWithPermissions.add(resultSet.getString(1));
			// Take only related language of the above list
			for (String one : domainsWithPermissions) {
			
				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = " + one + " AND language_id = " + Setting.Language + " AND supported is TRUE";
				System.out.println("query_smi_domain_id: "+ query_smi_domain_id);
				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
				boolean result = resultSet.next();
				if (result)
					allDomains.add(resultSet.getString(1));
			}
			
			// Check that the collected domains have an active project
			
			List<String> rowsValuesNew = new ArrayList<String>();
//			List<String> domainIDs = new ArrayList<String>();
//			// Get smi domains
//			for (String value : allDomains) {
////				value = value.replace("'", "''");
//				// Get ID
//				int count = StringUtils.countMatches(value, "'");
//				if (count == 1)
//					value = value.replace("'", "''");
//				// Could have more than domain with the same name so get all their smi ids first
//				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = '" + value + "'";
//				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
//				while(resultSet.next())
//					domainIDs.add(resultSet.getString(1));
//			}
//			System.out.println();
			// Check the project of each smi id and check if it is active
			for (String ID : allDomains) {
				// Get project ID
				String query_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = " + ID;
				resultSet = DatadUltilities.ExecuteQuery(query_project_id, con);
				resultSet.next();
				String smi_domain_id = resultSet.getString(1);
				// Check if project is active
				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where smi_domain_id = " + smi_domain_id + " AND supported is True";
				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
				boolean exist = resultSet.next();
				if (exist)
					rowsValuesNew.add(ID.replace("''", "'"));
			}
			
			// Go on the domain result list and if one of them is project then remove it
			List<String> rowsValuesNew2 = new ArrayList<String>();
			for (String value : rowsValuesNew) {
////							value = value.replace("'", "''");
//				// Get ID
//				int count = StringUtils.countMatches(value, "'");
//				if (count == 1)
//					value = value.replace("'", "''");
//				String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where domain_name ILike '" + value + "'";
//				System.out.println("query_smi_domain_id: "+ query_smi_domain_id);
//				resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
//				resultSet.next();
//				String smi_domain_id = resultSet.getString(1);
				String query_parent_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = '" + value + "'";
				System.out.println("query_parent_project_id: "+ query_parent_project_id);
				resultSet = DatadUltilities.ExecuteQuery(query_parent_project_id, con);
				resultSet.next();
				String parent_project_id = resultSet.getString(1);
				if (value.equals(parent_project_id))
					rowsValuesNew2.add(value.replace("''", "'"));
			}

			Set<String> hs = new HashSet<>();
			hs.addAll(rowsValuesNew2);
			rowsValuesNew2.clear();
			rowsValuesNew2.addAll(hs);
			
			// Get the projects names
			List<String> projectsNames = new ArrayList<String>();
			for (String id : rowsValuesNew2) {
				String query_project_name = "SELECT domain_name from smi_domain where smi_domain_id = '" + id + "'";
				System.out.println("query_project_name: "+ query_project_name);
				resultSet = DatadUltilities.ExecuteQuery(query_project_name, con);
				resultSet.next();
				String parent_project_name = resultSet.getString(1);
				projectsNames.add(parent_project_name.replace("''", "'"));
			}
			
			return projectsNames;
		}
		
		/**
		 * Get the Style values from the database
		 * @param con - SQL connection
		 * @return list of gender values
		 * @throws SQLException
		 */
		public List<String> getStylesValuesFromDB(Connection con, String chosenDomain) throws SQLException 
		{
			// Remove private mark from domain name:  (P)
			if (chosenDomain.contains(" (P)"))
				chosenDomain = chosenDomain.replace(" (P)", "");
			
			if(chosenDomain.contains("'"))
				chosenDomain = chosenDomain.replace("'", "''");
			
			// Get allowed domains
			List<String> allowedDomains = getDomainsInHomepageDropdown(con);
						
			// Updated version - need to get moods for the chosen domain
			List<String> chosenStylesIds = new ArrayList<String>();
			
			boolean hasParent = true;
			String DBDomainID = null;
			List<String> DBDomainIDs = new ArrayList<String>();
			
			while(hasParent)
			{
				// Get the id of the chosen domain
				String query = "select smi_domain_id from smi_domain WHERE TRIM(domain_name) iLIKE '" + chosenDomain + "'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
//				boolean thereIsData = resultSet.next();
//				if (thereIsData)
				while (resultSet.next())
					DBDomainIDs.add(resultSet.getString(1));
				
				// Remove domains that are also projects
//				List<String> rowsValuesNew = new ArrayList<String>();
				for (String value : DBDomainIDs) {
					String query_parent_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = " + value;
					System.out.println("query_parent_project_id: "+ query_parent_project_id);
					resultSet = DatadUltilities.ExecuteQuery(query_parent_project_id, con);
					resultSet.next();
					String parent_project_id = resultSet.getString(1);

					if (!value.equals(parent_project_id))
						DBDomainID = value.replace("''", "'");
					else continue;
						
					// Check if the chosen domain is an allowed domain
					if (!allowedDomains.contains(DBDomainID))
						continue;

					// Get the available styles ids for the retrieved domain id
					query = "select style_id from smi_domain_allowed_styles WHERE smi_domain_id = " + DBDomainID;
					resultSet = DatadUltilities.ExecuteQuery(query, con);
					
				    while (resultSet.next()) {
				    	if (!chosenStylesIds.contains(resultSet.getString(1)))
				    		chosenStylesIds.add(resultSet.getString(1));  }
				}
					
				// Check if the domain has a parent domain
				query = "select parent_id from smi_domain WHERE domain_name iLike '" + chosenDomain + "'";
				resultSet = DatadUltilities.ExecuteQuery(query, con);
				boolean thereIsData = resultSet.next();
				String res = resultSet.getString(1);
				if (thereIsData && res!=null)
				{
					DBDomainID = resultSet.getString(1);	
					// Get the parent domain name
					query = "SELECT domain_name FROM smi_domain WHERE smi_domain_id="+DBDomainID;
					resultSet = DatadUltilities.ExecuteQuery(query, con);
					if(resultSet.next())
						chosenDomain = resultSet.getString(1);
				}
				else
					hasParent = false;
		
				}
		
			// Get the styles names of the styles ids
		    List<String> chosenStyles = new ArrayList<String>();
		    for (String styleID : chosenStylesIds) {
		    	String query = "select style_name from style WHERE supported is TRUE and style_id = " + styleID;
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				resultSet.next();
				try {
					chosenStyles.add(resultSet.getString(1));
				} catch (Exception e) {
					// query return no result
					}
				}
		    
			return chosenStyles;
		}

		
//		/**
//		 * Get the Styles values from the database
//		 * @param con - SQL connection
//		 * @return list of gender values
//		 * @throws SQLException
//		 */
//		public List<String> getStylesValuesFromDB(Connection con) throws SQLException 
//		{
//			// Query to get the Styles values
//			String query = "select style_name from style  WHERE supported is TRUE";
//			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
//
//			// Save the Styles values in a list
//			List<String> rowsValues = new ArrayList<String>();
//		    while (resultSet.next()) {
//		    	rowsValues.add(resultSet.getString(1));
//		    }
//		    rowsValues.add("default");
//			return rowsValues;
//		}

		/**
		 * Get the Mood values from the database
		 * @param con - SQL connection
		 * @return list of gender values
		 * @throws SQLException
		 */
		public List<String> getMoodValuesFromDB(Connection con, String chosenDomain) throws SQLException 
		{
			// Remove private mark from domain name:  (P)
			if (chosenDomain.contains(" (P)"))
				chosenDomain = chosenDomain.replace(" (P)", "");
			
			if(chosenDomain.contains("'"))
				chosenDomain = chosenDomain.replace("'", "''");
			
			// Get allowed domains
			List<String> allowedDomains = getDomainsInHomepageDropdown(con);
			
			// Updated version - need to get moods for the chosen domain
			List<String> chosenMoodsIds = new ArrayList<String>();
			
			boolean hasParent = true;
			String DBDomainID = null;
			List<String> DBDomainIDs = new ArrayList<String>();
						
			while(hasParent)
			{
				// Get the id of the chosen domain
				String query = "select smi_domain_id from smi_domain WHERE TRIM(domain_name) iLIKE '" + chosenDomain + "'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
//				boolean thereIsData = resultSet.next();
//				if (thereIsData)
				while (resultSet.next())
					DBDomainIDs.add(resultSet.getString(1));
				
				// Remove domains that are also projects
//				List<String> rowsValuesNew = new ArrayList<String>();
				for (String value : DBDomainIDs) {
					String query_parent_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = " + value;
					System.out.println("query_parent_project_id: "+ query_parent_project_id);
					resultSet = DatadUltilities.ExecuteQuery(query_parent_project_id, con);
					resultSet.next();
					String parent_project_id = resultSet.getString(1);

					if (!value.equals(parent_project_id))
						DBDomainID = value.replace("''", "'");
					else continue;
						
					// Check if the chosen domain is an allowed domain
					if (!allowedDomains.contains(DBDomainID))
						continue;

					// Get the available mood ids for the retrieved domain id
					query = "select mood_id from smi_domain_allowed_moods WHERE smi_domain_id = " + DBDomainID;
					resultSet = DatadUltilities.ExecuteQuery(query, con);
					
				    while (resultSet.next()) {
				    	if (!chosenMoodsIds.contains(resultSet.getString(1)))
				    		chosenMoodsIds.add(resultSet.getString(1));  }
				}
				
				// Check if the domain has a parent domain
				query = "select parent_id from smi_domain WHERE domain_name iLike '" + chosenDomain + "'";
				resultSet = DatadUltilities.ExecuteQuery(query, con);
				boolean thereIsData = resultSet.next();
				String res = resultSet.getString(1);
				if (thereIsData && res!=null)
				{
					DBDomainID = resultSet.getString(1);	
					// Get the parent domain name
					query = "SELECT domain_name FROM smi_domain WHERE smi_domain_id="+DBDomainID;
					resultSet = DatadUltilities.ExecuteQuery(query, con);
					if(resultSet.next())
						chosenDomain = resultSet.getString(1);
				}
				else
					hasParent = false;
		
				}
		
		    // Get the moods names of the moods ids
		    List<String> chosenMoods = new ArrayList<String>();
		    for (String moodID : chosenMoodsIds) {
		    	String query = "select name from mood WHERE supported is TRUE and mood_id = " + moodID;
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				resultSet.next();
				try {
					chosenMoods.add(resultSet.getString(1));
				} catch (Exception e) {
					// query return no result
				}
				
			}
		    
			return chosenMoods;
		}


		/**
		 * Convert IDs to Moods
		 * @param con - SQL connection
		 * @return list of gender values
		 * @throws SQLException
		 */
		public String convertIDtoMood(Connection con, String moodID) throws SQLException 
		{
	    	String query = "select name from mood WHERE supported is TRUE and mood_id = " + moodID;
	    	ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
			resultSet.next();
			try {
				return(resultSet.getString(1));
			} catch (Exception e) {
				// query return no result
			}
			
			return null;
		}
		
		/**
		 * Convert IDs to Gestures
		 * @param con - SQL connection
		 * @return list of gender values
		 * @throws SQLException
		 */
		public String convertIDtoGestures(Connection con, String gestureID) throws SQLException 
		{
	    	String query = "select name from gesture WHERE supported is TRUE and gesture_id = " + gestureID;
	    	ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
			resultSet.next();
			try {
				return(resultSet.getString(1));
			} catch (Exception e) {
				// query return no result
			}
			
			return null;
		}
		
//		/**
//		 * Get the Mood values from the database
//		 * @param con - SQL connection
//		 * @return list of gender values
//		 * @throws SQLException
//		 */
//		public List<String> getMoodValuesFromDB(Connection con) throws SQLException 
//		{
//			// Query to get the Mood values
//			String query = "select name from mood WHERE supported is TRUE";
//			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
//
//			// Save the Mood values in a list
//			List<String> rowsValues = new ArrayList<String>();
//		    while (resultSet.next()) {
//		    	rowsValues.add(resultSet.getString(1));
//		    }
//			return rowsValues;
//		}

		/**
		 * Get the Gesture values from the database
		 * @param con - SQL connection
		 * @return list of gender values
		 * @throws SQLException
		 */
		public List<String> getGestureValuesFromDB(Connection con, String chosenDomain) throws SQLException 
		{
			// Remove private mark from domain name:  (P)
			if (chosenDomain.contains(" (P)"))
				chosenDomain = chosenDomain.replace(" (P)", "");
			
			if(chosenDomain.contains("'"))
				chosenDomain = chosenDomain.replace("'", "''");
			
			// Get allowed domains
			List<String> allowedDomains = getDomainsInHomepageDropdown(con);
						
			// Updated version - need to get gestures for the chosen domain
			List<String> chosenGesturesId = new ArrayList<String>();
			
			boolean hasParent = true;
			String DBDomainID = null;
			List<String> DBDomainIDs = new ArrayList<String>();
			
			while(hasParent)
			{
				// Get the id of the chosen domain
				
				String query = "select smi_domain_id from smi_domain WHERE TRIM(domain_name) iLIKE '" + chosenDomain + "'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				// boolean thereIsData = resultSet.next();
//				if (thereIsData)
				while (resultSet.next())
					DBDomainIDs.add(resultSet.getString(1));
				
				// Remove domains that are also projects
//				List<String> rowsValuesNew = new ArrayList<String>();
				for (String value : DBDomainIDs) {
					String query_parent_project_id = "SELECT parent_project_id from smi_domain where smi_domain_id = " + value;
					System.out.println("query_parent_project_id: "+ query_parent_project_id);
					resultSet = DatadUltilities.ExecuteQuery(query_parent_project_id, con);
					resultSet.next();
					String parent_project_id = resultSet.getString(1);

					if (!value.equals(parent_project_id))
						DBDomainID = value.replace("''", "'");
					else continue;
					
					// Check if the chosen domain is an allowed domain
					if (!allowedDomains.contains(DBDomainID))
						continue;
					
//					rowsValuesNew.add(value.replace("''", "'"));
//					DBDomainID = rowsValuesNew.get(0);
	
					// Get the available gestures ids for the retrieved domain id
					query = "select gesture_id from smi_domain_allowed_gestures WHERE smi_domain_id = " + DBDomainID;
					resultSet = DatadUltilities.ExecuteQuery(query, con);
					
				    while (resultSet.next()) {
				    	if (!chosenGesturesId.contains(resultSet.getString(1)))
				    		chosenGesturesId.add(resultSet.getString(1));  }
			}
				
				// Check if the domain has a parent domain
				query = "select parent_id from smi_domain WHERE domain_name iLike '" + chosenDomain + "'";
				resultSet = DatadUltilities.ExecuteQuery(query, con);
				boolean thereIsData = resultSet.next();
				String res = resultSet.getString(1);
				if (thereIsData && res!=null)
				{
					DBDomainID = resultSet.getString(1);	
					// Get the parent domain name
					query = "SELECT domain_name FROM smi_domain WHERE smi_domain_id="+DBDomainID;
					resultSet = DatadUltilities.ExecuteQuery(query, con);
					if(resultSet.next())
						chosenDomain = resultSet.getString(1);
				}
				else
					hasParent = false;
		
				}
		
  		// Get the gestures names of the gestures ids
		    List<String> chosenGestures = new ArrayList<String>();
		    for (String gestureID : chosenGesturesId) {
		    	String query = "select name from gesture WHERE supported is TRUE and gesture_id = " + gestureID;
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				resultSet.next();
				try {
					chosenGestures.add(resultSet.getString(1));
				} catch (Exception e) {
					// query return no result
				}
				
			}
		    
			return chosenGestures;
		}


//		/**
//		 * Get the Gesture values from the database
//		 * @param con - SQL connection
//		 * @return list of gender values
//		 * @throws SQLException
//		 */
//		public List<String> getGestureValuesFromDB(Connection con) throws SQLException 
//		{
//			// Query to get the Gesture values
//			String query = "select name from gesture WHERE supported is TRUE";
//			ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
//
//			// Save the Gesture values in a list
//			List<String> rowsValues = new ArrayList<String>();
//		    while (resultSet.next()) {
//		    	rowsValues.add(resultSet.getString(1));
//		    }
//			return rowsValues;
//		}
		
		/**
		 * Get random symbols
		 * @param length
		 * @return
		 */
		public String randomSymbols(int length) {
			char[] chars = "!@#$%^&*()~{}[]=+-_.?';:".toCharArray();
			StringBuilder sb = new StringBuilder();
			Random random = new Random();
			for (int i = 0; i < length; i++) {
			    char c = chars[random.nextInt(chars.length)];
			    sb.append(c);
			}
			String output = sb.toString();
			return output;
		}
		
		/**
		 * Get random numbers
		 * @param length
		 * @return
		 */
		public String randomNumbers(int length) {
			char[] chars = "0123456789".toCharArray();
			StringBuilder sb = new StringBuilder();
			Random random = new Random();
			for (int i = 0; i < length; i++) {
			    char c = chars[random.nextInt(chars.length)];
			    sb.append(c);
			}
			String output = sb.toString();
			return output;
		}
		
		/**
		 * Check the confirmation message
		 * @throws InterruptedException 
		 */
		public void fadedPopup(Logger logger, String expectedMessage) throws InterruptedException{
			int timesToTry = 10;
			CurrentPage = GetInstance(HomePage.class);
			String message = "";
			while (timesToTry != 0)
			{
				try {
					System.out.println("fade popup" + timesToTry);
					message = CurrentPage.As(HomePage.class).displayedMessage.getText();
					if (!message.isEmpty() && message.equals(expectedMessage)) {
						logger.info("Verify the message displayed: " + message);
						timesToTry = 0;
					}else if (!message.isEmpty() && message != expectedMessage) timesToTry--;
				} catch (Exception e) {
					timesToTry--;
					Thread.sleep(100);
				}
			}
			
			// Check the message input
			logger.info("message: " + message + ", expected Message: " + expectedMessage);
			assertEquals(message, expectedMessage);
		}
		
		
		//----------------------------------------------------------
		//----------------------------------------------------------
		/**
		 * Select a random Gesture
		 * @return the chosen gesture
		 * @throws AWTException 
		 */
		public String chooseRandomGesture() throws AWTException {
			// Get the available gestures
			List<String> gesturesTypes = this.getGestureValues();
			
			// Choose a random gesture
			Random randomizer = new Random();
			String randomGesture = gesturesTypes.get(randomizer.nextInt(gesturesTypes.size()));
			
			// Select the random gesture
			String Chosen = this.SelectFromGestures(randomGesture);
//			Robot robot = new Robot();
//			Point point = CurrentPage.As(HomePage.class).GestureValues.getLocation();
//			robot.mouseMove(point.getX()-50,point.getY()-50);
			return Chosen;
		}

		public ArrayList<String> getGestureValues() throws AWTException {
			    
					// old
//			      Point point = CurrentPage.As(HomePage.class).GestureValues.getLocation();
//		          Robot robot = new Robot();
//		          robot.mouseMove(point.getX()+20,point.getY()+107);
		    	
//			  Point point = CurrentPage.As(HomePage.class).SpeedBar.getLocation();
//	          Robot robot = new Robot();
//	          robot.mouseMove(point.getX()+20,point.getY()+50);
	          
	            Actions builder = new Actions(DriverContext._Driver);
		  		
		  		try {
		  			builder.moveToElement(CurrentPage.As(HomePage.class).GestureValues).perform();
				} catch (Exception e) {
					System.out.println(e);
				}
		  		
		        // Get all of the options
		        List<WebElement> options = GestureValues.findElements(By.xpath("//*[@id='gestureList']/li"));
		          
		        ArrayList<String> gesturesTypes= new ArrayList<String>();
		        for (WebElement opt : options) {
		        	gesturesTypes.add(opt.getText());
		        }
		        
		        
		        
		        return gesturesTypes;
		}
		
		public String SelectFromGestures(String option) {
			 // Get all of the options
				List<WebElement> options = this.GestureValues.findElements(By.xpath("//*[@id='gestureList']/li"));

			// Loop through the options and select the one that matches
			    for (WebElement opt : options) {
			        if (opt.getText().equals(option)) {
			            opt.click();
			            return option;
	        }
	    }
				return null;
		}
		
		/**
		 * Select a random Mood
		 * @return the picked choice
		 * @throws AWTException 
		 */
		public String chooseRandomMood() throws AWTException {
			// Get the available moods
			List<String> moodTypes = this.getMoodValues();
			moodTypes.remove("Neutral");
			// Choose a random mood
			Random randomizer = new Random();
			String randomMood = moodTypes.get(randomizer.nextInt(moodTypes.size()));
			
			// Select the random Mood
			String Chosen = this.SelectFromMoods(randomMood);
			
			return Chosen;
		}

		public ArrayList<String> getMoodValues() throws AWTException {
				
//				 Point point = CurrentPage.As(HomePage.class).hsbPitch.getLocation();
//		          Robot robot = new Robot();
//		          robot.mouseMove(point.getX()+20,point.getY()+50);
		         // robot.mouseMove(point.getX(),point.getY());
		          
		        Actions builder = new Actions(DriverContext._Driver);
		  		builder.moveToElement(CurrentPage.As(HomePage.class).MoodValues).perform();
		  		
		        // Get all of the options
		        List<WebElement> options = MoodValues.findElements(By.xpath("//*[@id='moodList']/li"));
		        
		        ArrayList<String> moodTypes= new ArrayList<String>();
		        for (WebElement opt : options) {
		        	if(!opt.getText().equals(""))
		        		moodTypes.add(opt.getText());
		        }
//		        moodTypes.remove("Neutral");
		        return moodTypes;
		}
		
		public String SelectFromMoods(String option) {
			 // Get all of the options
				List<WebElement> options = this.MoodValues.findElements(By.xpath("//*[@id='moodList']/li"));		
//				this.btnMood.click();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			// Loop through the options and select the one that matches
					    for (WebElement opt : options) {
					    	int attempts = 3;
					    	while(attempts>0)
					    	{
					    		try {
					    			 if (opt.getText().equals(option)) {
								            opt.click();
								            return option;}
						    			
					        	} catch (Exception staleelementrException) {
									// TODO: handle exception
				    			 }
					    	attempts--;
					    	}
			    }
						return null;
		}
		
		/**
		 * Select a random value from the voice dropdown
		 * @return the picked choice
		 * @throws InterruptedException 
		 */
			public String filterWithRandomDomain() throws InterruptedException 
			{
				// Get the available voices
				List<String> domainValues = this.getDomainValues();
				// If the domain dropdown is open then try again
				if (!(domainValues.size()>1 && domainValues.get(1)!="")) domainValues = this.getDomainValues();
					
				// Delete first empty value
				domainValues.remove(0);
				
				// Choose a random domain
				Random randomizer = new Random();
				String randomDomain = domainValues.get(randomizer.nextInt(domainValues.size()));
				
				// Type and filter by the random picked domain
				this.txtDomainSearch.sendKeys(randomDomain);
				Thread.sleep(1000);
				CurrentPage.As(HomePage.class).cmbDomain.click();
				this.btnDomainSearch.click();
				Thread.sleep(1000);
				
				return randomDomain;
			}
						
			/**
			 * Check if finished synthesizing the text
			 * @throws InterruptedException 
			 */
			public double finishedSynthsize(ExtentTest test, Logger logger) throws InterruptedException{
				int timesToTry = 120;
				CurrentPage = GetInstance(HomePage.class);
				boolean isDisplayed = true;
					try {
						while (isDisplayed == true && timesToTry != 0){
							System.out.println("finishedSynthsize - timesToTry: " + timesToTry);
							isDisplayed = CurrentPage.As(HomePage.class).PlayButtonLoading.isDisplayed();
							timesToTry--;
							Thread.sleep(100);
							}
					} catch (Exception e) {
						logger.info("Finished synthesizing the text");
					}
					Thread.sleep(100);
					// Check if Pause button is displayed in order to make sure that clip is finished loading 
					//this.checkPauseAfterSynthesizing();
					return ((timesToTry)*0.1);
			}
			
			/**
			 * Check if the text editor scroll is visible
			 * @return
			 * @throws InterruptedException 
			 */
			public boolean isScrollVisible() throws InterruptedException {
				Thread.sleep(2000);
				JavascriptExecutor js = (JavascriptExecutor) DriverContext._Driver;
				boolean scrollBarPresentHeight = (boolean) js.executeScript(
				        "var element = document.querySelector('#innerText'); " + 
				        "return element.scrollHeight > element.clientHeight;");
				
				boolean scrollBarPresentWidth = (boolean) js.executeScript(
				        "var element = document.querySelector('#innerText'); " + 
				        "return element.scrollWidth > element.clientWidth;");
				
				if (scrollBarPresentHeight == true || scrollBarPresentWidth == true)
					return true;
				
				return false;
			}
			
			//----------------------------------------------------------
			//----------------------------------------------------------
			/**
			 * Select a random Style
			 * @return the chosen Style
			 * @throws AWTException 
			 * @throws InterruptedException 
			 */
			public String chooseRandomStyle() throws AWTException, InterruptedException {
				// Get the available Styles
				List<String> StylesTypes = this.getStyleValues();
				
				// Choose a random Style
				Random randomizer = new Random();
				String randomStyle = StylesTypes.get(randomizer.nextInt(StylesTypes.size()));
				
				// Select the random Style
				String ChosenStle = this.SelectFromStyles(randomStyle);
				
				return ChosenStle;
			}

			public ArrayList<String> getStyleValues() throws AWTException, InterruptedException {

		        	Actions builder = new Actions(DriverContext._Driver);
		  			builder.moveToElement(CurrentPage.As(HomePage.class).StyleValues).perform();
		  		
		  		
			        // Get all of the options
			        List<WebElement> options = this.StyleValues.findElements(By.xpath("//*[@id='stylesBarSubMenu']/ul/li"));
			        
			        //System.out.println(StyleValues.getAttribute("innerHTML"));
			        
			        ArrayList<String> StylesTypes= new ArrayList<String>();
			        for (WebElement opt : options) {
			        	StylesTypes.add(opt.getText());
			        }
			        return StylesTypes;
			}
			
			/**
			 * Verify that the element displayed in the page
			 * @param element - element name
			 * @return
			 */
			public boolean FindIpaPopup(String element) {
			    try {
			     switch (element) 
			     {
			             case "IpaPopupTitle":
			            	 return this.IpaPopupTitle.isDisplayed();
			     }
			         } catch (NoSuchElementException e) {
			             System.out.println(e);
			             return false;
			         }
			    return false;
			   }
			
			
			
			/**
			 * Select from Style
			 * @param option
			 * @return
			 */
			
			public String SelectFromStyles(String option) {
				 // Get all of the options
					List<WebElement> options = this.StyleValues.findElements(By.xpath("//*[@id='stylesBarSubMenu']/ul/li"));

				// Loop through the options and select the one that matches
				    for (WebElement opt : options) {
				        if (opt.getText().equals(option)) {
				            opt.click();
				            return option;
		        }
		    }
					return null;
			}
			
			/**
			 * Check if the region block do exist in the wave bar
			 */
			public boolean doRegionBlockExist() {
				try {
					String regionClass = this.region.getTagName();
					return true;
				} catch (Exception e) {
					// Is not found
					return false;
				}
			
			}
			
			/**
			 * Is homepage is ready and available
			 * @return
			 * @throws InterruptedException 
			 */
			public int isHomepageReady() throws InterruptedException {
				
//				boolean f = ((JavascriptExecutor) DriverContext._Driver).executeScript("return document.readyState").toString().equals("complete");
//				if(f)
//					{System.out.println("home page is ready");return 1;}
//				return 0;
				
				int timesToTry = 10;
				CurrentPage = GetInstance(HomePage.class);
				while (timesToTry != 0)
				{
					System.out.println("isHomepageReady - times To Try: " + timesToTry);
					try {
						
						List<String> list = getDomainValues();
//						List<String> list = getVoiceValues();
						// Check that language dropdown is loaded too
						String languageDropdownValue = cmbLanguage.getText();
						String languageId = languageDropdownValue.equals("English") ? "6" : "10";
						
						if (list.size()>1 && list.get(1)!="" && languageId.equals(Setting.Language) ) {
							lblDomain.click();
//							VoiceLabel.click();
							return timesToTry;
						}		
						else {timesToTry--;
							  Thread.sleep(100);}
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				return timesToTry;
				}			
			
			/**
			 * Do mood exist, this is in order to bypass a current bug
			 * @return list of Voice values
			 * @throws InterruptedException 
			 * @throws AWTException 
			 */
			public void doMoodExist() throws InterruptedException, AWTException {
				int timesToTry = 5;
				CurrentPage = GetInstance(HomePage.class);
				System.out.println("Mood intro");
				while (timesToTry != 0)
				{
//					  Point point = CurrentPage.As(HomePage.class).MoodValues.getLocation();
//			          Robot robot = new Robot();
//			          robot.mouseMove(point.getX()-50,point.getY()-50);
			          System.out.println("timesToTry: " + timesToTry);
					try {
						List<String> list = getMoodValues();
						System.out.println("list: " + list);
						if (list.size()>1 && list.get(1)!="") {
//							lblDomain.click(); 
//						    point = CurrentPage.As(HomePage.class).MoodValues.getLocation();
//				            robot.mouseMove(point.getX()-50,point.getY()-50);
							return;
						}
						else {timesToTry--;
							 DriverContext._Driver.navigate().refresh();
							 isHomepageReady();
							  }
					} catch (Exception e) {
						timesToTry--;
						//Thread.sleep(100);
					}
				}
				}
			
		
			/**
			 * Check for the pause button after finishing synthesizing
			 * @return
			 * @throws InterruptedException 
			 */
			public void checkPauseAfterSynthesizing() throws InterruptedException {
				int timesToTry = 3;
				CurrentPage = GetInstance(HomePage.class);
				boolean buttonStatus;
				while (timesToTry != 0)
				{
					try {
						System.out.println("checkPauseAfterSynthesizing - timesToTry: " + timesToTry);
						buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPause.isDisplayed();
						if (buttonStatus == true) {
							return;
						}
						else {timesToTry--;
							  Thread.sleep(100);}
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				}			
			
			/**
			 * Check profile title
			 * @return
			 * @throws InterruptedException 
			 */
			public void checkProfileTitle() throws InterruptedException {
				int timesToTry = 50;
				CurrentPage = GetInstance(HomePage.class);
				while (timesToTry != 0)
				{
					try {
						String voiceTitle = CurrentPage.As(HomePage.class).textVoiceTitle.getText();
						if (!voiceTitle.isEmpty()) return;
						else {timesToTry--;
							  Thread.sleep(100);}
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				}
			
			/**
			 * Check if the voice dropdown is empty with timeiout 5 seconds
			 * @throws InterruptedException 
			 */
			public boolean isVoiceListEmpty(ExtentTest test, Logger logger) throws InterruptedException{
				int timesToTry = 5;
				CurrentPage = GetInstance(HomePage.class);
				List<String> voicesInDropdown;
				while (timesToTry != 0)
				{
					try {
						voicesInDropdown = CurrentPage.As(HomePage.class).getVoiceValues();
						if (voicesInDropdown.size() == 0 )
						{
							LogUltility.log(test, logger, "Values in voice dropdown: " + voicesInDropdown);
							return true;
						}	else timesToTry--;
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				return false;
			}
			
			/**
			 * Is voice list ready
			 * @return
			 * @throws InterruptedException 
			 */
			public List<String> areVoicesReady() throws InterruptedException {
				int timesToTry = 50;
				CurrentPage = GetInstance(HomePage.class);
				List<String> list = null;
				while (timesToTry != 0)
				{
					try {
						list = getVoiceValues();
						if (list.size()>1 && list.get(1)!="") {
							lblDomain.click();
							return list;
						}
						else {timesToTry--;
							  Thread.sleep(100);}
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				return list;
				}

			/**
			 * Check that we got a new list that includes the searched voice and this list
			 *	is not equal to the initialize voices list
			 * @return
			 * @throws InterruptedException 
			 */
			public boolean checkFilteredVoice(ExtentTest test, Logger logger, List<String> initialList, String filteredByVoice) throws InterruptedException {
				int timesToTry = 50;
				CurrentPage = GetInstance(HomePage.class);
				List<String> voiceInDropdown = null;
				while (timesToTry != 0)
				{
					try {
						voiceInDropdown = getVoiceValues();
						Collections.sort(initialList);
						Collections.sort(voiceInDropdown);
						boolean containResult = false;
						//boolean containResult = voiceInDropdown.contains(filteredByVoice);
						for(String str : voiceInDropdown)
						    if(str.contains(filteredByVoice)){
						    	containResult = true;
								break;}
						
						
						System.out.println("containResult: " + containResult);
						System.out.println(containResult == true);
						System.out.println("equal: " + !voiceInDropdown.equals(initialList));
						if (containResult == true && !voiceInDropdown.equals(initialList)) {
							LogUltility.log(test, logger, "After filtering - Values in voice dropdown: " + voiceInDropdown);
							lblDomain.click();
							return true;
						}
						else {timesToTry--;
							  Thread.sleep(100);}
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				return false;
				}
			
			/**
			 * Check if the domain dropdown is empty with timeiout 5 seconds
			 * @throws InterruptedException 
			 */
			public boolean isDomainListEmpty(ExtentTest test, Logger logger) throws InterruptedException{
				int timesToTry = 50;
				CurrentPage = GetInstance(HomePage.class);
				List<String> domainsInDropdown;
				while (timesToTry != 0)
				{
					try {
						domainsInDropdown = CurrentPage.As(HomePage.class).getDomainValues();
//						if (domainsInDropdown.size() == 1 && domainsInDropdown.get(0).length() == 0)
						if (domainsInDropdown.size() == 0)
						{
							LogUltility.log(test, logger, "Values in domain dropdown: " + domainsInDropdown);
							return true;
						}	else timesToTry--;
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				return false;
			}
			
			/**
			 * Is Wave Bar Displayed
			 * @return
			 * @throws InterruptedException 
			 */
			public boolean isWaveBarDisplayed() throws InterruptedException {
				int timesToTry = 50;
				CurrentPage = GetInstance(HomePage.class);
				boolean WaveBar;
				while (timesToTry != 0)
				{
					try {
						WaveBar = CurrentPage.As(HomePage.class).barWave.isDisplayed();
						String width = CurrentPage.As(HomePage.class).barWaveCanvas.getAttribute("width");
						if (WaveBar == true) {
							return true;
						}
						else {timesToTry--;
							  Thread.sleep(100);}
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				return false;
				}

			/**
			 * Check the span contain
			 * @return
			 * @throws InterruptedException 
			 */
			public boolean checkSpan() throws InterruptedException {
				int timesToTry = 50;
				while (timesToTry != 0)
				{
					try {
						System.out.println(timesToTry);
						String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
						if (applied == "speed") {
							return true;
						}
						else {timesToTry--;
							  Thread.sleep(100);}
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				return false;
				}
			
			/**
			 * scroll down, this is good if tests run on small screen
			 * @throws InterruptedException
			 */
			public void scrollDown() throws InterruptedException {
			   	JavascriptExecutor jse = (JavascriptExecutor)DriverContext._Driver;
			   	jse.executeScript("scroll(0, 250);");
			}

			/**
			 * Get two domains, the first one does have a gesture that
			 * does not included in the second
			 * @param con - SQL connection
			 * @return list of gender values
			 * @throws SQLException
			 */
			public List<String> getTwoDomainsDifferentGesturesFromDB(Connection con) throws SQLException 
			{
				
				// Fetch all the available supported and public domains
				List<String> getDomainValues = CurrentPage.As(HomePage.class).getVDomainValuesFromDB(con);
				
				 // Go on the domain result list and if one of them is project then remove it
				List<String> assignedDomainValues = new ArrayList<String>();
				for (String value : getDomainValues) {
//					value = value.replace("'", "''");
					// Get ID
					int count = StringUtils.countMatches(value, "'");
					if (count == 1)
						value = value.replace("'", "''");
					String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where domain_name ILike '" + value + "'";
					System.out.println("query_smi_domain_id: "+ query_smi_domain_id);
					ResultSet resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
					resultSet.next();
					String smi_domain_id = resultSet.getString(1);
					String query_parent_project_id = "SELECT parent_project_id from smi_domain where domain_name ILike '" + value + "'";
					System.out.println("query_parent_project_id: "+ query_parent_project_id);
					resultSet = DatadUltilities.ExecuteQuery(query_parent_project_id, con);
					resultSet.next();
					String parent_project_id = resultSet.getString(1);
					if (!smi_domain_id.equals(parent_project_id))
						assignedDomainValues.add(value.replace("''", "'"));
						
				}
				
				ArrayList<List<String>> domainsGestures = new ArrayList<List<String>>(); 
				
				// Fetch all the supported and public gestures for the above domains
				for (String domain : assignedDomainValues) {

					// Get the id of the chosen domain
					domain = domain.replace("'", "''");
					System.out.println("domain name: " + domain);
					String query = "SELECT smi_domain_id FROM smi_domain WHERE language_id="+Setting.Language+" AND domain_name iLike '" + domain + "'";
					System.out.println("domain query: " + query);
					ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
					
					resultSet.next();
					String DBDomainID = resultSet.getString(1);
					
					// Get the available gestures ids for the retrieved domain id
					query = "select gesture_id from smi_domain_allowed_gestures WHERE smi_domain_id = " + DBDomainID;
					System.out.println("gestureid query: " + query);
					resultSet = DatadUltilities.ExecuteQuery(query, con);
					
					List<String> chosenGestuersIds = new ArrayList<String>();
				    while (resultSet.next()) {
				    	chosenGestuersIds.add(resultSet.getString(1));
				    }
					
				    // Get the gestures names of the gestures ids
				    List<String> chosenGestures = new ArrayList<String>();
				    for (String gestureID : chosenGestuersIds) {
				    	query = "select name from gesture WHERE supported is TRUE and gesture_id = " + gestureID;
				    	System.out.println("gesturename query: " + query);
						resultSet = DatadUltilities.ExecuteQuery(query, con);
						try {
							resultSet.next();
							//System.out.println(resultSet.getString(1));
							chosenGestures.add(resultSet.getString(1));
						} catch (Exception e) {
							System.out.println("don't add - gestures is disabled");
						}
						
					}
				domainsGestures.add(chosenGestures);   
				}
				
				// Check in all the lists in domainsGestures arraylist
				// for a gestures that exist and domain and not in another

				// get a gesture from the domain's gestures list
				for (int i = 0; i < domainsGestures.size(); i++) {
					List<String> list = domainsGestures.get(i);
					// get a gesture from a gesture list from the selected domain
					for (String gesture : list) {
						// search that selected gesture is not included in
						// any of the next domains
						float domainToCompare = this.domainNotContainThatGesture(domainsGestures, gesture, i);
						if  (domainToCompare != -1) {
							List<String> result = Arrays.asList(assignedDomainValues.get(i), assignedDomainValues.get((int) domainToCompare), gesture);
							return result;
//							String firstDomain = getVDomainWithIndexFromDB(con, i);
//							String secondDomain = getVDomainWithIndexFromDB(con, domainToCompare);
//							List<String> result = Arrays.asList(firstDomain, secondDomain);
//							
						}
						}
					}	
				List<String> nothingfound = Arrays.asList("nothing");
				return nothingfound;	
			}
			
			// search for a domain that does not have the chosen gesture
			float domainNotContainThatGesture(ArrayList<List<String>> domainsGestures, String gesture, int dontCheckLine) {
				
				for (int i = 0; i < domainsGestures.size(); i++) {
					// don't check the same line that gesture was taken from
					if (dontCheckLine == i) continue;
					List<String> list = domainsGestures.get(i);
					// get a gesture from a gesture list from the selected domain
					boolean gestureFound = false;
					for (String gestureToCompare : list) {
						// search that selected gesture is not included in
						// any of the next domains
						if (gestureToCompare.equals(gesture)) gestureFound = true;
					}
					if (gestureFound == false) return i;
				}
				
				return -1;
			}
			
			/**
			 * Get two domains, the first one does have a mood that
			 * is not included in the second
			 * @param con - SQL connection
			 * @return list of gender values
			 * @throws SQLException
			 */
			public List<String> getTwoDomainsDifferentMoodsFromDB(Connection con) throws SQLException 
			{
				
				// Fetch all the available supported and public domains
				List<String> getDomainValues = CurrentPage.As(HomePage.class).getVDomainValuesFromDB(con);
				
				 // Go on the domain result list and if one of them is project then remove it
				List<String> assignedDomainValues = new ArrayList<String>();
				for (String value : getDomainValues) {
//					value = value.replace("'", "''");
					// Get ID
					int count = StringUtils.countMatches(value, "'");
					if (count == 1)
						value = value.replace("'", "''");
					String query_smi_domain_id = "SELECT smi_domain_id from smi_domain where domain_name ILike '" + value + "'";
					System.out.println("query_smi_domain_id: "+ query_smi_domain_id);
					ResultSet resultSet = DatadUltilities.ExecuteQuery(query_smi_domain_id, con);
					resultSet.next();
					String smi_domain_id = resultSet.getString(1);
					String query_parent_project_id = "SELECT parent_project_id from smi_domain where domain_name ILike '" + value + "'";
					System.out.println("query_parent_project_id: "+ query_parent_project_id);
					resultSet = DatadUltilities.ExecuteQuery(query_parent_project_id, con);
					resultSet.next();
					String parent_project_id = resultSet.getString(1);
					if (!smi_domain_id.equals(parent_project_id))
						assignedDomainValues.add(value.replace("''", "'"));
						
				}
				
				ArrayList<List<String>> domainsMoods = new ArrayList<List<String>>(); 
				
				// Fetch all the supported and public moods for the above domains
				for (String domain : assignedDomainValues) {

					// Get the id of the chosen domain
					domain = domain.replace("'", "''");
					System.out.println("domain name: " + domain);
					String query = "select smi_domain_id from smi_domain WHERE language_id="+Setting.Language+" AND domain_name iLike '" + domain + "'";
					System.out.println("domain query: " + query);
					ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
					
					resultSet.next();
					String DBDomainID = resultSet.getString(1);
					
					// Get the available moods ids for the retrieved domain id
					query = "select mood_id from smi_domain_allowed_moods WHERE smi_domain_id = " + DBDomainID;
					System.out.println("moodid query: " + query);
					resultSet = DatadUltilities.ExecuteQuery(query, con);
					
					List<String> chosenMoodsIds = new ArrayList<String>();
				    while (resultSet.next()) {
				    	chosenMoodsIds.add(resultSet.getString(1));
				    }
					
				    // Get the moods names of the moods ids
				    List<String> chosenMoods = new ArrayList<String>();
				    for (String moodID : chosenMoodsIds) {
				    	query = "select name from mood WHERE supported is TRUE and mood_id = " + moodID;
				    	System.out.println("moodname query: " + query);
						resultSet = DatadUltilities.ExecuteQuery(query, con);
						try {
							resultSet.next();
							//System.out.println(resultSet.getString(1));
							if (!resultSet.getString(1).equals("Neutral")) chosenMoods.add(resultSet.getString(1));
						} catch (Exception e) {
							System.out.println("don't add - mood is disabled");
						}
						
					}
				domainsMoods.add(chosenMoods);   
				}
				
				// Check in all the lists in domainsMoods arraylist
				// for a moods that exist in domain and not in another

				// get a mood from the domain's mood list
				for (int i = 0; i < domainsMoods.size(); i++) {
					List<String> list = domainsMoods.get(i);
					// get a mood from a mood list from the selected domain
					for (String mood : list) {
						// search that selected mood is not included in
						// one of the next domains
						float domainToCompare = this.domainNotContainThatMood(domainsMoods, mood, i);
						if  (domainToCompare != -1) {
							List<String> result = Arrays.asList(assignedDomainValues.get(i), assignedDomainValues.get((int) domainToCompare), mood);
							return result;
//							String firstDomain = getVDomainWithIndexFromDB(con, i);
//							String secondDomain = getVDomainWithIndexFromDB(con, domainToCompare);
//							List<String> result = Arrays.asList(firstDomain, secondDomain);
//							
						}
						}
					}	
				List<String> nothingfound = Arrays.asList("nothing");
				return nothingfound;	
			}
			
			// search for a domain that does not have the chosen mood
			float domainNotContainThatMood(ArrayList<List<String>> domainsMoods, String mood, int dontCheckLine) {
				
				for (int i = 0; i < domainsMoods.size(); i++) {
					// don't check the same line that gesture was taken from
					if (dontCheckLine == i) continue;
					List<String> list = domainsMoods.get(i);
					// get a gesture from a mood list from the selected domain
					boolean moodFound = false;
					for (String moodToCompare : list) {
						// search that selected mood is not included in
						// any of the next domains
						if (moodToCompare.equals(mood)) moodFound = true;
					}
					if (moodFound == false) return i;
				}
				
				return -1;
			}
			
			/**
			 * Select a random value from the voice dropdown THAT IS ONLY ENGLISH
			 * @return the picked choice
			 */
			public String chooseRandomVoiceOnlyEnglish() {
				// Get the available voices
				 List<String> voiceValues = this.getVoiceValues();
				// Delete first empty value
				voiceValues.remove(0);
				
				int i = 0;
				ArrayList<String> VoicesToDelete = new ArrayList<String>();
				for (String voice : voiceValues) {
					char c= voice.charAt(0);
					if (!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))) {
						VoicesToDelete.add(voice);
					}
					i++;
				}
				
				// Remove the non English voices
				for (String voice : VoicesToDelete) {
					voiceValues.remove(voice);
				}
				
				// Choose a random voice
				Random randomizer = new Random();
				String randomVoice = voiceValues.get(randomizer.nextInt(voiceValues.size()));
				
				// Select the random voice
				this.Select_Item_In_ListBox(cmbVoice, randomVoice);
				
				return randomVoice;
			}
			
			/**
			 * Select a random value from the voice dropdown THAT IS HEBREW
			 * @return the picked choice
			 */
			public String chooseRandomVoiceNotEnglish() {
				// Get the available voices
				List<String> voiceValues = this.getVoiceValues();
				// Delete first empty value
				voiceValues.remove(0);
				
				// Remove any voice that is English
				int i = 0;
				ArrayList<String> VoicesToDelete = new ArrayList<String>();
				for (String voice : voiceValues) {
					char c= voice.charAt(0);
					if (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))) {
						VoicesToDelete.add(voice);
					}
					i++;
				}
				
				// Remove the non English voices
				for (String voice : VoicesToDelete) {
					voiceValues.remove(voice);
				}
				
				// Choose a random voice
				Random randomizer = new Random();
				String randomVoice = voiceValues.get(randomizer.nextInt(voiceValues.size()));
				
				// Select the random voice
				this.Select_Item_In_ListBox(cmbVoice, randomVoice);
				
				return randomVoice;
			}
			
			/**
			 * Choose random domain with templates
			 * @param con
			 * @param email
			 * @return
			 * @throws SQLException
			 */
			public String chooseRandomDomainWithTemplates(Connection con, String projectName) throws SQLException {

				// Click on the domain dropdown
				cmbDomain.click();
				
//				// Get Domains that have values in styles and gestures and moods
//				String query = "select DISTINCT smi_domain_id from renderer_text where open_to_public is TRUE "
//						+ "and text_owner_project_id iLike '" + getProjectId(con,projectName) + "' ORDER BY smi_domain_id";
//				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
//				
//				// Get the moods names of the moods ids
//				List<String> domains = new ArrayList<String>();
//				ResultSet resultSetIn;
//			    while (resultSet.next()) {
//			    	query = "select domain_name from smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE and smi_domain_id = " + resultSet.getString(1);
//			    	resultSetIn = DatadUltilities.ExecuteQuery(query, con);
//			    	resultSetIn.next();
//			    	try {
//			    		domains.add(resultSetIn.getString(1));
//					} catch (Exception e) {
//						System.out.println("Domain is not supported, don't add: " + e);
//					}
//			    }
			    
				String query = 	"SELECT * FROM ( WITH editable_templates AS ( SELECT DISTINCT r1.name,r1.smi_domain_id FROM renderer_text r1 "
						+ "JOIN template_words t1 ON r1.template_id = t1.template_id) SELECT  DISTINCT s1.domain_name,t1.name,s1.language_id,s1.open_to_public FROM smi_domain s1 JOIN editable_templates t1 ON t1.smi_domain_id = s1.smi_domain_id "
						+ "WHERE s1.supported is TRUE AND s1.smi_domain_owner_project_id ='" + getProjectId(con,projectName) + "' UNION (WITH editable_templates AS ( SELECT DISTINCT r1.name,r1.smi_domain_id FROM renderer_text r1 JOIN template_words t1 ON r1.template_id = t1.template_id "
								+ " WHERE open_to_public is TRUE) SELECT DISTINCT s1.domain_name,t1.name,s1.language_id,s1.open_to_public FROM smi_domain s1 JOIN editable_templates t1 ON t1.smi_domain_id = s1.smi_domain_id "
								+ "WHERE s1.supported is TRUE  AND s1.smi_domain_owner_project_id='S4398046511105-4398046511106')) as tableC WHERE tableC.language_id="+Setting.Language+" order by tableC.domain_name";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				// Get domains names
				List<String> domains = new ArrayList<String>();
				while(resultSet.next())
				{
					if(!domains.contains(resultSet.getString(1)))
						if(resultSet.getString(4).equals("f"))
							domains.add(resultSet.getString(1)+" (P)");
						else
							domains.add(resultSet.getString(1));
				}
				
				// Choose a random domain
				Random randomizer = new Random();
				String randomDomain = domains.get(randomizer.nextInt(domains.size()));
				
				// Select the random domain
				randomDomain = selectDomainFromDropdown(randomDomain);

				return randomDomain;
			}
			
			/**
			 * Choose random domain with files
			 * @param con
			 * @param email
			 * @return
			 * @throws SQLException
			 */
			public String chooseRandomDomainWithFiles(Connection con, String projectName,String usernameLogin) throws SQLException {

				// Click on the domain dropdown
				cmbDomain.click();
				
				// Get available domains for the used user and project
//				String query = "SELECT smi_domain_id,domain_name,smi_domain_owner_project_id FROM smi_domain WHERE language_id="+Setting.Language+
//						" AND smi_domain_id!=parent_project_id AND supported is TRUE AND smi_domain_owner_project_id="+"'"+getProjectId(con, projectName)+"'";		
				String query= "SELECT * FROM (SELECT smi_domain_id,domain_name,smi_domain_owner_project_id,language_id "
						+ "FROM smi_domain WHERE smi_domain_id!=parent_project_id AND supported is TRUE AND smi_domain_owner_project_id='"+ getProjectId(con, projectName)+"' union "  
						+ "SELECT smi_domain_id,domain_name,smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id!=parent_project_id AND supported is TRUE "
						+ "AND smi_domain_owner_project_id='S4398046511105-4398046511106')as tableA WHERE tableA.language_id="+Setting.Language;
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				// Get each domain's files
				List<String> domains = new ArrayList<String>();
				while(resultSet.next())
				{
					List<String> files =getDomainFiles(con,getProjectId(con, projectName),usernameLogin,resultSet.getString(1));
					if(!files.isEmpty())
						domains.add(resultSet.getString(2));
				}
				
				// Choose a random domain
				Random randomizer = new Random();
				String randomDomain = domains.get(randomizer.nextInt(domains.size()));
				
				// Select the random domain
				randomDomain = selectDomainFromDropdown(randomDomain);

				return randomDomain;
			}
			
			
			/**
			 * 
			 * Get the Voice id from DB
			 * @param con - SQL connection
			 * @return list of Voice values
			 * @throws SQLException
			 */
			public String getVoiceID(Connection con,String voiceName) throws SQLException 
			{
				// Query to get the active Voices values
				// Set id = 10 for Hebrew language
				String query = "select smi_voice_id from smi_voice WHERE name iLike" +"'" + voiceName +"'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

				if(resultSet.next())
					return resultSet.getString(1);
				return null;
			}
			
			
			/**
			 * 
			 * Get available Hebrew voices that have more than one mood (neutral mood)
			 * @param con - SQL connection
			 * @return list of Voice values
			 * @throws SQLException
			 */
			public String getVoiceWithMoods(Connection con) throws SQLException 
			{
				// Query to get the voices that have multiple moods (not only neutral)
				// Set id = 10 for Hebrew language
				String query = "WITH voice_with_moods AS(SELECT  smi_voice_id,COUNT (mood_id) FROM smi_voice_moods GROUP BY smi_voice_id HAVING COUNT(mood_id) >1)"
						+ "SELECT s1.name FROM smi_voice s1"
						+ " JOIN voice_with_moods s2 ON (s1.smi_voice_id = s2.smi_voice_id)"
						+ "WHERE s1.supported IS TRUE AND s1.language_id="+Setting.Language;
				
			   ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
			   
			   // Insert the available voices to list
			   ArrayList<String> voicesList = new ArrayList<String>();
			   
				while(resultSet.next())
					voicesList.add(resultSet.getString(1));
				
				// Choose random voice to return
				Random random = new Random();
				String chosenVoice = voicesList.get(random.nextInt(voicesList.size()));
				
				// Select from dropdown
				this.select_voice(chosenVoice);
				
				return chosenVoice;
			
			}
			
			
			/**
			 * 
			 * Get the Voice moods from DB
			 * @param con - SQL connection
			 * @return list of Voice values
			 * @throws SQLException
			 */
			public ArrayList<String> getVoiceMoodsFromDB(Connection con,String voiceID) throws SQLException 
			{
				// Query to get the moods id for voice
				String query = "SELECT DISTINCT mood_id from smi_voice_moods WHERE smi_voice_id="+voiceID+" AND supported is TRUE";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				ArrayList<String> moods = new ArrayList<String>();
				while(resultSet.next())
				{
					String mood_id = resultSet.getString(1);
					String query2 = "SELECT name from mood WHERE mood_id="+mood_id;
					ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query2, con);
					resultSet2.next();
					moods.add(resultSet2.getString(1));
				}
			
				
				return moods;
			}
			
			

			
			/**
			 * 
			 * Get domain with editable template
			 * @param con - SQL connection
			 * @return list of Voice values
			 * @throws SQLException
			 */
			public HashMap<String,List<String>> getDomainWithEditableTemplate(Connection con,String projectName) throws SQLException 
			{
				// Query to get the active domain names
//				String query="WITH editable_templates AS"
//						+ "( SELECT DISTINCT r1.name,r1.smi_domain_id "
//						+ "FROM renderer_text r1 "
//						+ "JOIN template_words t1 ON r1.template_id = t1.template_id "
//						+ "WHERE t1.template_field_type_id!=0 AND t1.template_field_type_id!=10 ) "
//						+ "SELECT  DISTINCT s1.domain_name,t1.name "
//						+ "FROM smi_domain s1 "
//						+ "JOIN editable_templates t1 ON t1.smi_domain_id = s1.smi_domain_id "
//						+ "WHERE s1.supported is TRUE AND language_id= "+Setting.Language+" ORDER BY s1.domain_name";
				
				// Get all editable templates names with domain's names for the used project
				// The second part of the query (after union) gets all the opened to public templates 
				String project_id  = getProjectId(con, projectName);
				String query =
						"SELECT * FROM ( WITH editable_templates AS ( SELECT DISTINCT r1.name,r1.smi_domain_id FROM renderer_text r1 "
						+ "JOIN template_words t1 ON r1.template_id = t1.template_id WHERE t1.template_field_type_id!=0 AND t1.template_field_type_id!=10) "
						+ "SELECT  DISTINCT s1.domain_name,t1.name,s1.language_id,s1.open_to_public FROM smi_domain s1 JOIN editable_templates t1 ON t1.smi_domain_id = s1.smi_domain_id "
						+ "WHERE s1.supported is TRUE AND s1.smi_domain_owner_project_id ='"+project_id+"' "
						+ "UNION (WITH editable_templates AS ( SELECT DISTINCT r1.name,r1.smi_domain_id FROM renderer_text r1 JOIN template_words t1 ON r1.template_id = t1.template_id "
						+ "WHERE t1.template_field_type_id!=0 AND t1.template_field_type_id!=10 AND open_to_public is TRUE) SELECT DISTINCT s1.domain_name,t1.name,s1.language_id,s1.open_to_public "
						+ "FROM smi_domain s1 JOIN editable_templates t1 ON t1.smi_domain_id = s1.smi_domain_id "
						+ "WHERE s1.supported is TRUE  AND s1.smi_domain_owner_project_id ='"+project_id+"' OR s1.smi_domain_owner_project_id='S4398046511105-4398046511106')) as tableC "
						+ "WHERE tableC.language_id="+Setting.Language+" order by tableC.domain_name";
				// 
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				HashMap<String,List<String>> domainEditTemps=new HashMap<String,List<String>>();    
				 
				while(resultSet.next())
					{
						String nextDomain = resultSet.getString(1);
						String prevDomain = nextDomain;
						if(resultSet.getString(4).equals("f"))
							prevDomain+=" (P)";
						
						ArrayList<String> editableTemplates = new ArrayList<String>();
						editableTemplates.add(resultSet.getString(2));
						
						if(resultSet.next())
						while(resultSet.getString(1).equals(nextDomain))
						{
							prevDomain = resultSet.getString(1);
							editableTemplates.add(resultSet.getString(2));
						if(!resultSet.next())
							break;
						}
					    	domainEditTemps.put(prevDomain, editableTemplates);
					}
			
				return domainEditTemps;
				
			}
			
			
			/**
			 * Get the Hebrew instructions 
			 * @return
			 * @throws FileNotFoundException 
			 */
			public String getHebrewInstruction() throws FileNotFoundException {
			
				Scanner in = null ;

			in = new Scanner(new File("hebrew_help_file.txt"), "UTF-8");

		   		List<String> lines = new ArrayList<>();
		   		while(in.hasNextLine()) {
		   		    String line = in.nextLine().trim();
		   		    lines.add(line);
		   		}
		   		in.close();

				return lines.get(0);
			}
			
			
			/**
			 * Get the domain language
			 * @param con - SQL connection
			 * @return list of gender values
			 * @throws SQLException
			 */
			public String getDomainLanguage(Connection con,String domain) throws SQLException 
			{
				// Remove so it will suite DB values
				if (domain.contains(" (P)"))
					domain = domain.replace(" (P)", "");
				
				if(domain.contains("'"))
					domain = domain.replace("'", "''");
				
				// Query to get the Gender values
				String query = "SELECT language_id FROM smi_domain WHERE domain_name iLike '"+domain+"'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				String language = null;
				String languageId = null;
				if(resultSet.next())
					languageId=resultSet.getString(1);
				
				if(languageId.equals("10"))
					language = "עברית";
				if(languageId.equals("6"))
					language = "English";
				
				return language;
			}
			
			
			/**
			 * 
			 * Get mood id number
			 * @param con - SQL connection
			 * @return Mood's id
			 * @throws SQLException
			 */
			public String getMoodId(Connection con,String mood) throws SQLException 
			{
				// Query to get the mood id 
				String query = "select mood_id from mood WHERE name iLike '"+mood+"'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				String moodID ="";
				if(resultSet.next())
					moodID = resultSet.getString(1);
				return moodID;
			}
	
			 
			/**
			 * Click on editable field in templates
			 * @param lstBox - dropdown element
			 * @param ItemName - value to choose
			 */
			public WebElement click_on_editable_field(WebElement textArea)
			{
				List<WebElement> itemlist= textArea.findElements(By.tagName("span"));
				Random random = new Random();
				WebElement chosenField = itemlist.get(random.nextInt(itemlist.size()));
				chosenField.click();
				return chosenField;
			}
			
			
			/**
			 * 
			 * Get domain with gestures and with editable template 
			 * @param con - SQL connection
			 * @return list of Voice values
			 * @throws SQLException
			 */
			public HashMap<String,List<String>> getDomainWithGestureAndEditableTemplate(Connection con) throws SQLException 
			{
				// Query to get the active domain names that contains templates with editable fields and the templates name, also the domains are with gestures	
			String query = "WITH editable_templates AS (SELECT DISTINCT r1.name,r1.smi_domain_id "
					+"FROM renderer_text r1 "
					+"JOIN template_words t1 ON r1.template_id = t1.template_id "
					+"WHERE t1.template_field_type_id!=0 AND open_to_public is TRUE) "
					+"SELECT DISTINCT s1.domain_name,t1.name  FROM (editable_templates t1 LEFT JOIN smi_domain s1 ON t1.smi_domain_id = s1.smi_domain_id) "
					+"LEFT JOIN smi_domain_allowed_gestures g1 ON s1.smi_domain_id = g1.smi_domain_id "
					+"WHERE s1.supported is TRUE AND language_id="+Setting.Language
					+" AND g1.gesture_id in (select gesture_id from gesture where supported is TRUE)";
					
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				HashMap<String,List<String>> domainEditTemps=new HashMap<String,List<String>>();    
				 
				while(resultSet.next())
					{
						String nextDomain = resultSet.getString(1);
						String prevDomain =nextDomain;
						
						ArrayList<String> editableTemplates = new ArrayList<String>();
						editableTemplates.add(resultSet.getString(2));
						
						if(resultSet.next())
						while(resultSet.getString(1).equals(nextDomain))
						{
							prevDomain = resultSet.getString(1);
							editableTemplates.add(resultSet.getString(2));
						if(!resultSet.next())
							break;
						}
						
						domainEditTemps.put(prevDomain, editableTemplates);
						
					}
			
				return domainEditTemps;
			}
			
			
			/**
			 * 
			 * Get domain that is accessible for two different users
			 * @param con - SQL connection
			 * @return domain name
			 * @throws SQLException
			 */
			public List<ArrayList<String>> getAccessibleDomainForTwoUsers(Connection con,String firstUser,String secondUser) throws SQLException 
			{
//					String query = "SELECT smi_domain_id FROM smi_domain_permissions WHERE user_email='"+firstUser+"' "
//							+ "intersect "
//							+ "SELECT smi_domain_id FROM smi_domain_permissions WHERE user_email='"+secondUser+"'"
//							+ " intersect SELECT smi_domain_id FROM smi_domain WHERE language_id="+Setting.Language;
					
//					ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
//					while(resultSet.next())
//					{
//						// Check if it is a project,then get the child domains
//						String query2= "SELECT domain_name FROM smi_domain WHERE smi_domain_id="+resultSet.getString(1)+" AND parent_project_id="+resultSet.getString(1);
//						ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query2,con);
//						if(resultSet2.next())
//						{
//							// Get the project domains
//							String query3= "SELECT smi_domain_id FROM smi_domain WHERE supported is TRUE AND parent_project_id="+resultSet.getString(1) +" AND smi_domain_id!="+resultSet.getString(1);
//							ResultSet resultSet3 = DatadUltilities.ExecuteQuery(query3,con);
//							
//							// Iterate over all the domains and check if users can access same private domain
//							while(resultSet3.next())
//							{
//								String query4="SELECT smi_domain_id FROM smi_domain_permissions WHERE smi_domain_id="+resultSet3.getString(1)+" AND user_email='"+firstUser+"' "
//										+ "intersect "
//										+ "SELECT smi_domain_id FROM smi_domain_permissions WHERE smi_domain_id="+resultSet3.getString(1)+" AND user_email='"+secondUser+"'";
//								ResultSet resultSet4 = DatadUltilities.ExecuteQuery(query4,con);
//								
//								if(resultSet4.next())
//									if(!mutualDomain.contains(getDomainNameById(con,resultSet4.getString(1))))
//									mutualDomain.add(getDomainNameById(con,resultSet4.getString(1)));
//										
//							}
//						}
//						else
//							if(!mutualDomain.contains(getDomainNameById(con,resultSet.getString(1))))
//								mutualDomain.add(getDomainNameById(con,resultSet.getString(1)));
//					}
//					
//					// Add all supported and open to public domains to the list
//					String query5="SELECT * FROM smi_domain WHERE  supported AND open_to_public And smi_domain_id!=parent_project_id AND language_id="+Setting.Language;
//					ResultSet resultSet5 = DatadUltilities.ExecuteQuery(query5,con);
//					while(resultSet5.next())
//						if(!mutualDomain.contains(getDomainNameById(con,resultSet5.getString(1))))
//							mutualDomain.add(getDomainNameById(con,resultSet5.getString(1)));
				
					List<ArrayList<String>> firstUserDomains = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, firstUser);
					List<ArrayList<String>> secondUserDomains = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, secondUser);
					
					// Get common accounts/projects/domains
					 firstUserDomains.retainAll(secondUserDomains);
					 return firstUserDomains;
			}
	
			/**
			 * Intersection between two lists
			 * @param list1
			 * @param list2
			 * @return
			 */
			public <T> List<T> intersection(List<T> list1, List<T> list2) {
		        List<T> list = new ArrayList<T>();

		        for (T t : list1) {
		            if(list2.contains(t)) {
		                list.add(t);
		            }
		        }

		        return list;
		    }
			
			
			/**
			 * get the domain language id
			 * @param con
			 * @param domainName
			 * @return
			 * @throws SQLException
			 */
			public String getDomainLanguageId(Connection con, String domainName) throws SQLException
			{
            	String query= "SELECT language_id from smi_domain where domain_name iLike '"+domainName+"'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				// Get the domain language id
				if(resultSet.next())
					return resultSet.getString(1);
				// domain not found
				return "";
			}
			
			/**
			 * get the domain name from DB by ID
			 * @param con
			 * @param domain_id
			 * @return
			 * @throws SQLException
			 */
			public String getDomainNameById(Connection con, String domain_id) throws SQLException
			{
            	String query= "SELECT domain_name from smi_domain where smi_domain_id="+domain_id;
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				// Get the domain name
				if(resultSet.next())
					return resultSet.getString(1);
				// domain not found
				return "";
			}
			
			
			/**
			 * get the domain project name
			 * @param con
			 * @param domainName
			 * @return
			 * @throws SQLException
			 */
			public String getProjectforDomain(Connection con, String domainName) throws SQLException
			{
				// Remove so it will suite DB values
				if (domainName.contains(" (P)"))
					domainName = domainName.replace(" (P)", "");
				
				if(domainName.contains("'"))
					domainName = domainName.replace("'", "''");
				
            	String query= "SELECT parent_project_id from smi_domain where domain_name iLike '"+domainName+"'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				if(resultSet.next())
				{
					 query="SELECT domain_name from smi_domain where smi_domain_id="+resultSet.getString(1);
					 resultSet = DatadUltilities.ExecuteQuery(query, con);
					 if(resultSet.next())
						 return resultSet.getString(1);
				}
				
				// Not found
				return null;
			}
			
			
			/**
			 * Get project id
			 * @param con
			 * @param projectName
			 * @return
			 * @throws SQLException
			 */
			public String getProjectId(Connection con,String projectName) throws SQLException
			{
				if(projectName.contains("'"))
					projectName = projectName.replace("'", "''");
				
				String query = "SELECT project_id FROM project WHERE project_name="+"'"+projectName+"'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				resultSet.next();
				String project_id = resultSet.getString(1);
				return project_id;
			}
			
			/**
			 * Get the specific domain files
			 * @param con
			 * @param projectId
			 * @param email
			 * @param domainId
			 * @return
			 * @throws SQLException
			 */
			public List<String> getDomainFiles(Connection con, String projectId,String email,String domainId) throws SQLException
			{
				// Get all info
				String query= "SELECT DISTINCT renderer_text_id, name, text_ssml, smi_domain_id, text_owner_project_id,template_id, is_template_org, open_to_public,"
						+ "CASE WHEN open_to_public THEN FALSE WHEN trim(text_owner_project_id) ILIKE "+"'"+projectId+"' THEN TRUE WHEN is_write THEN TRUE ELSE FALSE END edit_permissions "
								+ "FROM renderer_text LEFT OUTER JOIN renderer_text_permissions USING (renderer_text_id) "
								+ "WHERE (open_to_public OR trim(text_owner_project_id) ILIKE "+"'"+projectId+"' OR trim(user_email) ILIKE "+"'"+email+"') and name NOT IN (SELECT name FROM renderer_text where name ilike '%_quickrender%') "
										+ "and is_deleted is distinct from true and smi_domain_id = "+domainId +" and template_id IS NULL ORDER BY name";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				// Get the files names
				List<String> names = new ArrayList<String>();
				while(resultSet.next())
					names.add(resultSet.getString(2));
				
				return names;	
			}
			
			/**
			 * Retrieve the account id by name
			 * @param con
			 * @param accountName
			 * @return
			 * @throws SQLException 
			 */
			public String getAccountId(Connection con, String accountName) throws SQLException
			{
				// Remove so it will suite DB values
				if(accountName.contains("'"))
					accountName = accountName.replace("'", "''");
				String query = "SELECT account_id FROM account WHERE account_name iLike "+"'"+accountName+"'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				resultSet.next();
				String accountId = resultSet.getString(1);
				return accountId;
			}
			
			/**
			 * retrieve active domains in homepage for current project/account
			 * @param con
			 * @param projectName
			 * @return
			 * @throws SQLException
			 */
			public List<String> getDomainNamesFromDB(Connection con,String projectName) throws SQLException
			{
				List<String> domains = new ArrayList<String>();
				String project_id = getProjectId(con, projectName);
				String query = "SELECT domain_name FROM smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE AND smi_domain_owner_project_id='"+project_id+"'";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				while(resultSet.next())
					domains.add(resultSet.getString(1));
				
				// Get default open to public domains
				if(domains.size() == 0)
				{
					 query = "SELECT domain_name,open_to_public FROM smi_domain WHERE language_id="+Setting.Language+" AND supported is TRUE AND smi_domain_owner_project_id='S4398046511105-4398046511106'";
					 resultSet = DatadUltilities.ExecuteQuery(query, con);
						while(resultSet.next())
							if(resultSet.getString(2).equals("t"))
								domains.add(resultSet.getString(1));
				}
				return domains;

			}
			
			/**
			 * 
			 * Get the Voice id moods from DB
			 * @param con - SQL connection
			 * @return list of mood ids values
			 * @throws SQLException
			 */
			public ArrayList<String> getVoiceMoodsIdsFromDB(Connection con,String voiceID) throws SQLException 
			{
				// Query to get the moods id for voice
				String query = "SELECT mood_id from smi_voice_moods WHERE smi_voice_id="+voiceID+" AND supported is TRUE";
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				
				ArrayList<String> moods = new ArrayList<String>();
				while(resultSet.next())
					moods.add(resultSet.getString(1));
			
				return moods;
			}
				
}