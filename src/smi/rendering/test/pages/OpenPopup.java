package smi.rendering.test.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.smi.framework.base.BasePage;

/**
 * - Defined the elements and methods for the open popup
 *
 */
public class OpenPopup extends BasePage {
	
	
	// Main open popup
	@FindBy(how=How.ID,using ="openPopup")
	public WebElement openMainPopup;
	
	// Open button on the home page
	@FindBy(how=How.ID,using ="openButton")
	public WebElement btnOpen;
	
	// Text dropdown
	@FindBy(how=How.XPATH,using ="//*[@id='openPopupFile_line']/div[1]/div/div[2]/p")
	public WebElement cmbText;
	
	// Open button on the popup
	//@FindBy(how=How.XPATH,using ="//*[@id='openPopup']/div[3]/a[2]")  popupOpen
	@FindBy(how=How.CLASS_NAME,using ="popupOpen")
	public WebElement btnOpenInPopup;
	
	// Cancel button on the popup
	@FindBy(how=How.CLASS_NAME,using ="popupCancel")
	public WebElement btnCancelInPopup;
	
	// Open popup title
	//@FindBy(how=How.XPATH,using ="//*[@id='openPopup']/div[1]")  popupHead
	@FindBy(how=How.ID,using ="openPopup")
	public WebElement titlePopup;
	
	// Template radio button  : .//input[@value="openPopupTemplate" and @name="openPopupType"]
	@FindBy(how=How.XPATH,using ="//*[@id='openPopup']/div[2]/div[1]/input[2]")
	public WebElement templateRBtn;
	
	// Template dropdown  //*[@id='openPopupTemplate_line']/div[1]/div/div[2]/p
//	@FindBy(how=How.XPATH,using ="//*[@id=\"openPopupTemplate_line\"]/div[1]/div/div[2]")
	@FindBy(how=How.XPATH,using="//*[@id=\"openPopupTemplate_line\"]/div[1]/div")
	public WebElement cmbTemplateDropdown;
	
	@FindBy(how=How.XPATH,using ="//*[@id=\"openPopupTemplate_line\"]/div[1]/div/div[2]")
	public WebElement cmbTemplate;
	
	// Domain field
	@FindBy(how=How.XPATH,using ="//*[@id='openPopupDomain']")
	public WebElement domainField;
	
	// Files dropdown element
	@FindBy(how=How.XPATH,using="//*[@id=\"openPopupFile_line\"]/div[1]/div")
	public WebElement cmbFileDropdown;
	
	/**
	 * Select a random file from the files dropdown
	 * @return the picked choice
	 * @throws InterruptedException 
	 */
	public String chooseRandomFile() throws InterruptedException {
		// Get the available voices
		List<String> fileNames = this.getFilesValues();
		// Delete first empty value
		fileNames.remove(0);
		
		// Choose a random file name
		Random randomizer = new Random();
		String randomFile = fileNames.get(randomizer.nextInt(fileNames.size()));
		
		// Select the random voice
		String Chosen = this.SelectFromFileDropdown(randomFile);
		
		return Chosen;
	}
	
	public ArrayList<String> getFilesValues() throws InterruptedException {
		
			int timesToTry = 5;
			CurrentPage = GetInstance(OpenPopup.class);
			List<WebElement> options = null;
			while (timesToTry != 0)
			{
				try {
					//If the dropdown isn't open
					if(!cmbFileDropdown.getAttribute("class").contains("selectricOpen"))
						this.cmbFileDropdown.click();
					
//					this.cmbText.click();
					
			        // Get all of the options
			        options = cmbText.findElements(By.xpath("//*[@id='openPopupFile_line']/div[1]/div/div[3]/div/ul/li"));
//			        System.out.println("options.size(): " + options.size());
//			        System.out.println("options.get(1).getText(): " + options.get(1).getText());
					if (options.size()>1 && options.get(1).getText()!="") {
						break;
					}
					else {timesToTry--;
						  Thread.sleep(100);}
				} catch (Exception e) {
					timesToTry--;
					Thread.sleep(100);
				}
			}
						
	        ArrayList<String> filesNames= new ArrayList<String>();
	        for (WebElement opt : options) {
	       	if(!opt.getText().equals(""))
	       	{
	       		int attemps = 3;
	       		while(attemps>0)
	       		{
	       			try {
	    	        	filesNames.add(opt.getText());
	    	        	break;
					} catch (Exception e) {
						// TODO: handle exception
					}
	       			attemps--;
	       		}

	       	}
	        }
	        return filesNames;
	        
	        
//			this.cmbText.click();
//	
//	        // Get all of the options
//	        List<WebElement> options = cmbText.findElements(By.xpath("//*[@id='openPopupFile_line']/div[1]/div/div[3]/div/ul/li"));
//	        		
//	        ArrayList<String> filesNames= new ArrayList<String>();
//	        for (WebElement opt : options) {
//	        	filesNames.add(opt.getText());
//	        }
//	        return filesNames;
	}
	
	public String SelectFromFileDropdown(String option) {
		
		//If the dropdown isn't open
		if(!cmbFileDropdown.getAttribute("class").contains("selectricOpen"))
			this.cmbFileDropdown.click();
		
		 // Get all of the options
			List<WebElement> options = this.cmbText.findElements(By.xpath("//*[@id='openPopupFile_line']/div[1]/div/div[3]/div/ul/li"));        

		// Loop through the options and select the one that matches
		    for (WebElement opt : options) {
		    	String getOption = opt.getText();
		        if (getOption.equals(option)) {
		            opt.click();
		            return option;
        }
    }
			return null;
	}		
			
			/**
			 * Select a random Template from the Template dropdown
			 * @return the picked choice
			 * @throws InterruptedException 
			 */
			public String chooseRandomTemplate() throws InterruptedException {
				// Get the available Templates
				Thread.sleep(4000);
				List<String> templateNames = this.getTemplateValues();
				// Delete first empty value
				templateNames.remove("Select Template");
				
				// Choose a random file name
				Random randomizer = new Random();
				String randomTemplate = templateNames.get(randomizer.nextInt(templateNames.size()));
				
				// Select the random template
				String Chosen = this.SelectFromtemplateDropdown(randomTemplate);
				
				return Chosen;
			}
			
			public ArrayList<String> getTemplateValues() {
				//If the dropdown isn't open
					if(!cmbTemplateDropdown.getAttribute("class").contains("selectricOpen"))
						this.cmbTemplateDropdown.click();
				
			        // Get all of the options                                      
			        List<WebElement> options = cmbTemplate.findElements(By.xpath("//*[@id='openPopupTemplate_line']/div[1]/div/div[3]/div/ul/li"));
			        
			        ArrayList<String> templateNames= new ArrayList<String>();
			        for (WebElement opt : options) {
			        	if(!opt.getText().equals(""))
				        	templateNames.add(opt.getText());	        	
			        }
			        return templateNames;
			}
			
			public String SelectFromtemplateDropdown(String option) {
				 // Get all of the options
				//If the dropdown isn't open
				if(!cmbTemplateDropdown.getAttribute("class").contains("selectricOpen"))
					this.cmbTemplateDropdown.click();
				
		        List<WebElement> options = cmbTemplate.findElements(By.xpath("//*[@id='openPopupTemplate_line']/div[1]/div/div[3]/div/ul/li"));
				// Loop through the options and select the one that matches
				    for (WebElement opt : options) {
//				    	String getOption = 	opt.getAttribute("innerText");
				        if (opt.getText().equals(option)) {
				            opt.click();
				            return option;
		        }
		    }
					return null;
	}
	
	/**
	 * timeout 5 sec for the text values in open popup to be available
	 * @throws InterruptedException 
	 */
	public void openFileWait() throws InterruptedException{
		int timesToTry = 50;
		CurrentPage = GetInstance(OpenPopup.class);
		while (timesToTry != 0)
		{
			try {
				List<String> filesValues = this.getFilesValues();
				if (filesValues.size()>1 && filesValues.get(1)!="") {
					titlePopup.click();
					return;
				}
				else {timesToTry--;
					  Thread.sleep(100);}
			} catch (Exception e) {
				timesToTry--;
				Thread.sleep(100);
			}
		}
	}
	
	/**
	 * timeout 5 sec for the text values in open popup to be available
	 * @throws InterruptedException 
	 */
	public void openTemplateWait() throws InterruptedException{
		int timesToTry = 50;
		CurrentPage = GetInstance(OpenPopup.class);
		while (timesToTry != 0)
		{
			try {
				List<String> templateNames = this.getTemplateValues();
				if (templateNames.size()>1 && templateNames.get(1)!="") {
					titlePopup.click();
					return;
				}
				else {timesToTry--;
					  Thread.sleep(100);}
			} catch (Exception e) {
				timesToTry--;
				Thread.sleep(100);
			}
		}
	}
}
