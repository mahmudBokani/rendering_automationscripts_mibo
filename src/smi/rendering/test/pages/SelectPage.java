package smi.rendering.test.pages;	


import static org.testng.Assert.assertEquals;
import java.awt.AWTException;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.ExtentTest;
import com.mongodb.connection.ServerSettings;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


/**
 * Define elements and methods for the Select project domain Page
 *
 */
public class SelectPage extends BasePage {
	
	// sub title text
//	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[1]/div[1]/h1[1]")
	@FindBy(how=How.XPATH,using="/html/body/div[2]/div[1]/h1")
	public WebElement subTitleText;
	
	// Project dropdown
	//@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/p[1]")
	@FindBy(how=How.XPATH,using ="/html/body/div[2]/div[3]/div[2]/div[1]")
	public WebElement cmbProjectSelect;
	
	// Project text
	@FindBy(how=How.XPATH,using ="//label[@for='projectSelect']")
	public WebElement lblProject;
	
	// domain dropdown
	@FindBy(how=How.XPATH,using ="//p[contains(text(),'Select Domain')]")
//	@FindBy(how=How.XPATH,using ="//div[contains(@class,'left_selection')]//div[4]//div[1]//div[1]//div[2]")
	public WebElement cmbDomainSelect;
	
	// domain text
	@FindBy(how=How.XPATH,using ="//label[@for='domainSelect']")
	public WebElement lblDomain;
	
	// logout link
	@FindBy(how=How.XPATH,using ="//span[@class='logout_a']")
	public WebElement lnkLogout;
	
	// Account dropdown
	@FindBy(how=How.XPATH,using="//body//div[1]//div[1]//div[1]//div[2]")
	public WebElement cmbAccount;
	
	// account text
	@FindBy(how=How.XPATH,using ="//label[contains(@for,'accountSelect')]")
	public WebElement lblAccount;
	
	/**
	 * Is selectPage ready and available
	 * @return
	 * @throws InterruptedException 
	 */
	public int isSelectPageReady() throws InterruptedException {
		int timesToTry = 10;
		while (timesToTry != 0)
		{
			System.out.println("isSelectPageReady: times To Try: " + timesToTry);
			try {
				List<String> list = getAccountsValues();
//				List<String> list = Get_List_Items_Value(cmbAccount);
//				System.out.println("getProjectValues: " + list);
				if (list.size()>=1) {
					lblAccount.click();
					return timesToTry;
				}
				else {timesToTry--;
					  Thread.sleep(100);}
			
			} catch (Exception e) {
				timesToTry--;
				Thread.sleep(100);
//				// Check if we already in the homepage
//				System.out.println("Are we in the Homepage?");
//				int homepageTry = GetInstance(HomePage.class).As(HomePage.class).isHomepageReady();
//				System.out.println("homepageTry: " + homepageTry);
//				if (homepageTry != 0) return homepageTry; 
			}
		}
		return timesToTry;
		}
	
	
	/**
	 * Get the domain dropdown values
	 * @return List
	 * @throws InterruptedException 
	 */
	public List<String> getDomainValues() throws InterruptedException {
		List<String> domains = null;
		
		int timesToTry = 50;
		while (timesToTry != 0)
		{
			System.out.println("times To Try: " + timesToTry);
			try {
				domains = getDomainValuesMain();
				if (domains.size()>=1) {
					return domains;
				}
				else {timesToTry--;
					  Thread.sleep(100);}
			} catch (Exception e) {
				timesToTry--;
				Thread.sleep(100);
			}
		}
		return domains;
	}
	
	public List<String> getDomainValuesMain() {
		cmbDomainSelect.click();
		return Get_List_Items_Value(cmbDomainSelect);
	} 
	
	/**
	 * Get the project dropdown values
	 * @return List
	 * @throws InterruptedException 
	 */
	public List<String> getProjectValues() throws InterruptedException {
		List<String> projects = null;
		
		int timesToTry = 5;
		while (timesToTry != 0)
		{
			System.out.println("times To Try: " + timesToTry);
			try {
				projects = getProjectValuesMain();
				// TILL GET AN ANSEWR FOR THIS, MEANWHILE NO USE OF SYSTEM PROJECT BECAUSE IT HAVE ENGLISH AND HEBREW DOMAINS
				if (projects.size()>=1) {
//					projects.remove("System");
					return projects;
				}
				else {timesToTry--;
					  Thread.sleep(100);}
			} catch (Exception e) {
				timesToTry--;
				Thread.sleep(100);
			}
		}
		return projects;
	}

	public List<String> getProjectValuesMain() {
		cmbProjectSelect.click();
		return Get_List_Items_Value(cmbProjectSelect);
	}

	/**
	 * Get the values list from a dropdown
	 * @param lstBox - dropdown element
	 * @return dropdown
	 */
	private List<String> Get_List_Items_Value(WebElement lstBox)
	{
		//get all children
		List<WebElement> itemlist= lstBox.findElements(By.xpath("//div[@class='selectricScroll']/ul/li"));
		//List<WebElement> itemlist= lstBox.findElements(By.cssSelector("div.selectricScroll ul>*"));
		ArrayList<String> itemNames= new ArrayList<String>();
		for(WebElement e : itemlist)
		{
		if(!e.getText().equals(""))
		    itemNames.add(e.getText());	
		}
		return itemNames;
	}
	
	/**
	 * Select an item from a listbox
	 * @param lstBox - dropdown element
	 * @param ItemName - value to choose
	 */
	public void Select_Item_In_ListBox(WebElement lstBox,String ItemName)
	{
		//get all childs
		List<WebElement> itemlist= lstBox.findElements(By.xpath("//div[@class='selectricScroll']/ul/li"));
		//List<WebElement> itemlist= lstBox.findElements(By.cssSelector("div.selectricScroll ul>*"));
		for(WebElement e : itemlist)
		{
//			System.out.println(e.getText());
			if (e.getText().equals(ItemName))
					{
					  e.click();
					  return;
					}
		}
		
	}
	
	/**
	 * Select a random value from the project dropdown
	 * @return the picked choice
	 * @throws InterruptedException 
	 * @throws SQLException 
	 */
	public String chooseRandomProject(Connection con) throws InterruptedException, SQLException {
	
		
		// Get the chosen language in the config
		String chosenLanguage = Setting.Language;
		
		// Get the available project from the dropdown
		List<String> projectValues = this.getProjectValues();
		// Delete first empty value
		//projectValues.remove(0);
		
		// Temporary because there is a problem in choosing project with one domain 
//		projectValues.remove("Bank Hapoalim PoC Project");
		
		// Keep only project with chosenLanguage
		List<String> projectValuesLanguage = new ArrayList<String>();
		for (String value : projectValues) {
			String projectToAdd = value;
			value = value.replace("'", "''");
			String query = "select language_id from smi_domain where domain_name iLike '" + value + "'";
			
			ResultSet resultSetIn = DatadUltilities.ExecuteQuery(query, con);
			resultSetIn.next();
			//if ((chosenLanguage.equals("HE") && resultSetIn.getString(1).equals("10")) || (chosenLanguage.equals("EN") && resultSetIn.getString(1).equals("6")))
			if(resultSetIn.getString(1).equals(chosenLanguage))
				projectValuesLanguage.add(projectToAdd);
		}
		
		// Choose a random project
		Random randomizer = new Random();
		String randomProj = projectValuesLanguage.get(randomizer.nextInt(projectValuesLanguage.size()));
		
		// Select the random project
		this.Select_Item_In_ListBox(cmbProjectSelect, randomProj);


		return randomProj;
	}
	
	
	/**
	 * Select a random value from the domain dropdown
	 * @return the picked choice
	 * @throws InterruptedException 
	 */
	public String chooseRandomDomain() throws InterruptedException {
		// Get the available project
		List<String> domainValues = this.getDomainValues();
		// Delete first empty value
		//	domainValues.remove(0);
		
		// Choose a random project
		Random randomizer = new Random();
		String randomDom = domainValues.get(randomizer.nextInt(domainValues.size()));
		
		// Select the random project
		this.Select_Item_In_ListBox(cmbDomainSelect, randomDom);
		
		return randomDom;
	}
	
	
	
	/**
	 * 
	 * get projects id with editable templates
	 * @param con - SQL connection
	 * @return list of Voice values
	 * @throws SQLException
	 */
	public List<String> getProjectIDsWithEditableTemplates(Connection con) throws SQLException 
	{
		// Query to get the active domain names
		String query="WITH editable_templates AS("
				+ "SELECT DISTINCT t1.smi_domain_id "
				+ "FROM template t1 "
				+ "JOIN template_words t2 ON t1.template_id = t2.template_id "
				+ "WHERE t2.template_field_type_id!=0) "
				+ "SELECT DISTINCT s1.parent_project_id "
				+ "FROM smi_domain s1 "
				+ "JOIN editable_templates t1 ON t1.smi_domain_id = s1.smi_domain_id "
				+ "WHERE s1.supported is TRUE";
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		
		List<String> projectsIds = new ArrayList<String>();
		
		while(resultSet.next())
			projectsIds.add(resultSet.getString(1));
		
		// Remove Ashraf's Project
		projectsIds.remove("1099511627882");
		return projectsIds;
		
	}
	
	
	/**
	 * 
	 * Get the language_id of project
	 * @param con - SQL connection
	 * @return list of Voice values
	 * @throws SQLException
	 */
	public String getProjecNameByID(Connection con,String projID) throws SQLException 
	{
		// Query to get the project id language
		String query = "SELECT domain_name FROM smi_domain WHERE smi_domain_id="+projID;
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
	
		// Save the language id 
		resultSet.next();
		String name = resultSet.getString(1);
		
		return name;
	}
	
	
	/**
	 * Select project from the project dropdown
	 */
	public void chooseProject(String project) {
		
		// Select the project
		cmbProjectSelect.click();
		this.Select_Item_In_ListBox(cmbProjectSelect, project);	
	}
	
	
	/**
	 * Select domain from the domains dropdown
	 */
	public void chooseDomain(String domain) {
		
		// Select the domain
		cmbDomainSelect.click();
		this.Select_Item_In_ListBox(cmbDomainSelect, domain);	
	}
	
	
	/**
	 * Select account from the accounts dropdown
	 */
	public void chooseAccount(String account) {
		
		// Select the domain
		cmbAccount.click();
		this.Select_Item_In_ListBox(cmbAccount, account);	
	}
	
	
	/**
	 * 
	 * get domain name,project id and editable template name
	 * @param con - SQL connection
	 * @return list of Voice values
	 * @throws SQLException
	 */
	public List<ArrayList<String>> getDomainProjectEditableTemplateName(Connection con) throws SQLException 
	{
		// Query to get the active domain names
		String query="WITH editable_templates AS"
				+ "( SELECT DISTINCT r1.name,r1.smi_domain_id "
				+ "FROM renderer_text r1 "
				+ "JOIN template_words t1 ON r1.template_id = t1.template_id "
				+ "WHERE t1.template_field_type_id!=0 AND open_to_public is TRUE) "
				+ "SELECT  DISTINCT s1.domain_name,s1.parent_project_id,t1.name "
				+ "FROM smi_domain s1 "
				+ "JOIN editable_templates t1 ON t1.smi_domain_id = s1.smi_domain_id "
				+ "WHERE s1.supported is TRUE AND language_id= "+Setting.Language;
		// 
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		
		List<ArrayList<String>> info = new ArrayList<ArrayList<String>>();
		while(resultSet.next())
			{
			ArrayList<String> row = new ArrayList<String>();
			//domain name
			row.add(resultSet.getString(1));
			//parent project id
			row.add(resultSet.getString(2));
			//editable template name
			row.add(resultSet.getString(3));
			info.add(row);
			}
		return info;
		
	}
	
	/**
	 * implemented to be used in SelectPage
	 * Get available domains for user for specific project
	 * @param con - SQL connection
	 * @return list of Voice values
	 * @throws SQLException
	 */
	public ArrayList<String> getDomainsProjectForUser(Connection con,String projectName,String userEmail) throws SQLException 
	{
		
		// Get the project id
		if(projectName.contains("'"))
			projectName = projectName.replace("'", "''");
		String query = "SELECT smi_domain_id FROM smi_domain WHERE domain_name iLike '"+projectName+"'";
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		resultSet.next();
		String projectID = resultSet.getString(1);
		
		// Get all the domains under one project
		query="SELECT smi_domain_id,domain_name,open_to_public FROM smi_domain WHERE parent_project_id ="+projectID+" AND supported is true AND smi_domain_id!="+projectID;
		resultSet = DatadUltilities.ExecuteQuery(query, con);
		
		
		ArrayList<String> availableDomains = new ArrayList<String>();
		while(resultSet.next())
		{
			// if the domain is  public then add its name to list
			if(resultSet.getBoolean(3))
				availableDomains.add(resultSet.getString(2));
			else
			{
				// the domain is private, check if the user is the owner of the domain
				String query2="SELECT domain_name FROM smi_domain WHERE smi_domain_id ="+resultSet.getString(1)+" AND smi_domain_owner_email iLike'"+userEmail+"'";	
				ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query2, con);
				// Add the domain name to the list
				if(resultSet2.next())
					availableDomains.add(resultSet2.getString(1));
				else
				{
					// The user isn't the owner of the domain, but he could have permissions to use it, so check smi_domain_permissions
					String query3 = "SELECT * FROM smi_domain_permissions WHERE smi_domain_id =" +resultSet.getString(1)+" AND user_email iLike '"+userEmail+"'";
					ResultSet resultSet3 = DatadUltilities.ExecuteQuery(query3, con);
					
					// if it has permissions
					if(resultSet3.next())
						availableDomains.add(resultSet.getString(2));
				}
			}	
		}
		
		return availableDomains;
	
	}
	
	
	/**
	 * Get available private domains for user for specific project
	 * @param con - SQL connection
	 * @return list of Voice values
	 * @throws SQLException
	 */
	public ArrayList<String> getProjectPrivateDomainsForUser(Connection con,String projectName,String userEmail) throws SQLException 
	{
		
		// Get the project id
		if(projectName.contains("'"))
			projectName = projectName.replace("'", "''");
		String query = "SELECT smi_domain_id FROM smi_domain WHERE domain_name iLike '"+projectName+"'";
		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
		resultSet.next();
		String projectID = resultSet.getString(1);
		
		// Get all the domains under one project
		query="SELECT smi_domain_id,domain_name,open_to_public FROM smi_domain WHERE parent_project_id ="+projectID+" AND supported is true AND smi_domain_id!="+projectID;
		resultSet = DatadUltilities.ExecuteQuery(query, con);
		
		
		ArrayList<String> availableDomains = new ArrayList<String>();
		while(resultSet.next())
		{
			// if the domain is  public DO NOT add to the list
			if(resultSet.getBoolean(3))
				;
			else
			{
				// the domain is private, check if the user is the owner of the domain
				String query2="SELECT domain_name FROM smi_domain WHERE smi_domain_id ="+resultSet.getString(1)+" AND smi_domain_owner_email iLike'"+userEmail+"'";	
				ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query2, con);
				// Add the domain name to the list
				if(resultSet2.next())
					availableDomains.add(resultSet2.getString(1));
				else
				{
					// The user isn't the owner of the domain, but he could have permissions to use it, so check smi_domain_permissions
					String query3 = "SELECT * FROM smi_domain_permissions WHERE smi_domain_id =" +resultSet.getString(1)+" AND user_email iLike '"+userEmail+"'";
					ResultSet resultSet3 = DatadUltilities.ExecuteQuery(query3, con);
					
					// if it has permissions
					if(resultSet3.next())
						availableDomains.add(resultSet.getString(2));
				}
			}	
		}
		
		return availableDomains;
	
	}
	
			/**
			 * Get the accounts dropdown values
			 * @return List
			 * @throws InterruptedException 
			 */
			public List<String> getAccountsValues() throws InterruptedException {
				List<String> accounts = null;
				
				int timesToTry = 50;
				while (timesToTry != 0)
				{
					System.out.println("times To Try: " + timesToTry);
					try {
						accounts = getAccountValuesMain();
						if (accounts.size()>=1) {
							return accounts;
						}
						else {timesToTry--;
							  Thread.sleep(100);}
					} catch (Exception e) {
						timesToTry--;
						Thread.sleep(100);
					}
				}
				return accounts;
			}
			
			public List<String> getAccountValuesMain() {
				cmbAccount.click();
				return Get_List_Items_Value(cmbAccount);
			}
			

			/**
			 * Get all available account,project and domain combinations to login with email
			 * @param con
			 * @param email
			 * @return
			 * @throws SQLException
			 */
			public List<ArrayList<String>> getAccountProjectDomainComb(Connection con,String email) throws SQLException
			{
				List<ArrayList<String>> account_project_domain = new ArrayList<ArrayList<String>>();
				// Get email accounts and projects
				String query = "SELECT account_name,project_id,project_name FROM user_project_permissions upp "
						+ "INNER JOIN user_permissions pp ON(upp.user_permission_id = pp.user_permission_id) "
						+ "INNER JOIN project p USING(project_id) "
						+ "INNER JOIN user_account_permissions uap USING(account_id, user_email) "
						+ "INNER JOIN user_permissions ap ON(uap.user_permission_id = ap.user_permission_id) "
						+ "INNER JOIN account a USING(account_id) "
						+ "WHERE(upp.user_email ILIKE '"+email+"') AND project_name  NOT ILIKE '% - Management Project'";
				
				ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
				while(resultSet.next())
				{
						
					// Get the project domains
					String query2 =" SELECT domain_name,open_to_public FROM smi_domain s1 WHERE s1.supported is TRUE and smi_domain_owner_project_id='"+resultSet.getString(2)+"' and language_id="+Setting.Language;
					ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query2, con);

					int count = 0;
					while(resultSet2.next())
					{
						String domain_name =resultSet2.getString(1);
						// If it is private domain add "(P)"
						if(resultSet2.getString(2).equals("f"))
							domain_name = resultSet2.getString(1)+" (P)";
						
						ArrayList<String> values = new ArrayList<String>();
						// Add account
						values.add(resultSet.getString(1));		
						// Add project
						values.add(resultSet.getString(3));
						// Add domain
						values.add(domain_name);
						// don't add the api test accounts
						if(!values.get(0).contains("testAC") && !values.get(0).contains("Test"))
							account_project_domain.add(values);
						count++;
					}
					
					// If empty, add the default domains to the list
					if(count==0)
					{
						List<String> defaultDomains = getDefaultDomains();
						for(int i=0;i<defaultDomains.size();i++)
						{
							ArrayList<String> values = new ArrayList<String>();
							// Add account
							values.add(resultSet.getString(1));		
							// Add project
							values.add(resultSet.getString(3));
							// Add domain
							values.add(defaultDomains.get(i));
							
							// don't add the api test accounts
							if(!values.get(0).contains("testAC")  && !values.get(0).contains("Test"))
								account_project_domain.add(values);
						}
					}
				}
				
				return account_project_domain;
			}
			
			List<String> getDefaultDomains()
			{
				List<String> domains = new ArrayList<String>();
				switch(Setting.Language)
				{
				case "6":
					domains.add("US Default");
					domains.add("Call Center");
					domains.add("Banking and Financing");
					domains.add("Formal");
					domains.add("Informal");
					domains.add("Literature");
					domains.add("Customer Care");break;
				case "10":
					domains.add("עברית");
					domains.add("בנקאות");break;	
					
				}
				return domains;
			}
						
}
