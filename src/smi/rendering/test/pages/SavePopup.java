package smi.rendering.test.pages;


import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import com.smi.framework.base.BasePage;
import com.smi.framework.uiltilies.DatadUltilities;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 *  Defined the elements and methods for the save popup
 *
 */
public class SavePopup extends BasePage {
	

    // save header                //*[@id='savePopup']/div[1]
	@FindBy(how=How.XPATH,using ="//*[@id='savePopup']/div[1]")
	public WebElement Saveheader;
	
	// Save popup
	@FindBy(how=How.XPATH,using ="//*[@id='savePopup']")
	public WebElement SavePopup;
	
	// File name textbox
	@FindBy(how=How.XPATH,using ="//*[@id='saveFilename']") 
	public WebElement FileName;
	
	// Cancel button
	@FindBy(how=How.XPATH,using ="//*[@id='savePopup']/div[3]/a[1]")
	public WebElement CancelBtn;
	
	//Save button
	//@FindBy(how=How.XPATH,using ="//*[@id='savePopup']/div[3]/a[2]")
	@FindBy(how=How.CLASS_NAME,using ="popupSave")
	public WebElement SaveBtn;
	
	// Save & Share button
	//@FindBy(how=How.XPATH,using ="//*[@id='savePopup']/div[3]/a[3]")
	@FindBy(how=How.XPATH,using ="//*[@id='savePopup']/div[3]/a[3]")
	public WebElement SaveShareBtn;
	
    // Message when save blank text : There is no text to save     X   
	@FindBy(how=How.XPATH,using ="html/body/div[7]/div/div[2]/div/span")
	public WebElement BlankTextMsg;
	
	// Message when save blank text : There is no text to save     X   
	@FindBy(how=How.XPATH,using ="//*[@id='general_confirm_content']")
	public WebElement OverWritePopup;

	// Yes button   
	@FindBy(how=How.XPATH,using ="//*[@id='general_confirm_button_yes']")
	public WebElement Yes;
	
	// No button   
	@FindBy(how=How.XPATH,using ="//*[@id='general_confirm_button_no']")
	public WebElement No;
	
	// Open to public checkbox
	@FindBy(how=How.ID,using="open_to_public")
	public WebElement openToPublic;
	/**
	 * save the file 
	 * @param language
	 */
    public void Save_File(String language){
	
    	SaveBtn.click();
    }
         
         /**
          * Get the saved text info from the database
          * @param con
          * @param emails
          * @param notes
          * @return
          * @throws SQLException
          */
         public List<List<String>> getFileName(Connection con, String fileName) throws SQLException {
         	   // Get the id of the selected age
         	   String query = "select name from renderer_text Where name = '" + fileName.replace("'", "''") + "'" ;
         	   ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

         	      // Use metdata in order to fetch columns names
         	      ResultSetMetaData rsmd = resultSet.getMetaData();

         	      // Number of columns
         	      int columnsNumber = rsmd.getColumnCount();

         	      // To save the fetched rows
         	      List<List<String>> table = new ArrayList<List<String>>();
         	      List<String> columnNames = new ArrayList<String>();
         	      List<String> namesRow = new ArrayList<String>();
         	      
         	      // Get the columns names
         	         for (int i = 1; i <= columnsNumber; i++) {
         	          columnNames.add(rsmd.getColumnName(i));
         	         }

         	      // Get the distinct age values
         	      while (resultSet.next()) {
         	          for (int i = 1; i <= columnsNumber; i++) {
         	           namesRow.add(resultSet.getString(i));
         	          }
         	      }

         	      table.add(columnNames);
         	      table.add(namesRow);
         	      
         	   return table;
 }

         /**
          * Get the saved text info from the database
          * @param con
          * @param emails
          * @param notes
          * @return
          * @throws SQLException
          */
         public List<List<String>> getSavedText(Connection con, String fileName,String chosenDomain) throws SQLException {
         	  
        	// Remove private mark from domain name:  (P)
 			if (chosenDomain.contains(" (P)"))
 				chosenDomain = chosenDomain.replace(" (P)", "");
 			
 			if(chosenDomain.contains("'"))
 				chosenDomain = chosenDomain.replace("'", "''");
 			
        	 // get the domain id
 			String query = "select smi_domain_id FROM smi_domain WHERE TRIM(domain_name) iLike '" + chosenDomain + "'";
        	 ResultSet resultSet2 = DatadUltilities.ExecuteQuery(query, con);
        	String domain_id="";
        	 while (resultSet2.next())
        		 domain_id = resultSet2.getString(1);
        	 
//        	 // Get allowed domains
// 			List<String> allowedDomains = GetInstance(HomePage.class).As(HomePage.class).getDomainsInHomepageDropdown(con);
//        	 // Choose the domain that is in the allowed domains list
//			for(String id : domainIDs) {
//				if (allowedDomains.contains(id)) {
//					domain_id = id;
//					break;
//				}
//			}
        	 
//         	   String query = "select text_ssml from renderer_text Where name iLike '"+ fileName.replace("'", "''") +"' AND text_ssml iLike '%"+ randomText + "%'";
        	    query = "select text_ssml from renderer_text Where name iLike '" + fileName.replace("'", "''") +"' AND smi_domain_id=" + domain_id;
         	   ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
         	      
         	      // Use metdata in order to fetch columns names
         	      ResultSetMetaData rsmd = resultSet.getMetaData();

         	      // Number of columns
         	      int columnsNumber = rsmd.getColumnCount();

         	      // To save the fetched rows
         	      List<List<String>> table = new ArrayList<List<String>>();
         	      List<String> columnNames = new ArrayList<String>();
         	      List<String> namesRow = new ArrayList<String>();
         	      
         	      // Get the columns names
         	         for (int i = 1; i <= columnsNumber; i++) {
         	          columnNames.add(rsmd.getColumnName(i));
         	         }

         	      // Get the distinct age values
         	      while (resultSet.next()) {
         	          for (int i = 1; i <= columnsNumber; i++) {
         	           namesRow.add(resultSet.getString(i));
         	          }
         	      }

         	      table.add(columnNames);
         	      table.add(namesRow);
         	      
         	   return table;
     }
         
       
         /**
          * Check if file saved and open to public is set to true
          * @param con
          * @param file name
          * @return
          * @throws SQLException
          */
         public boolean checkSavedFileWithOTP(Connection con, String fileName) throws SQLException {
         	   // Get the id of the selected age
        	 if(fileName.contains("'"))
        		 fileName= fileName.replace("'","''");
         	   String query = "select name from renderer_text Where name = '" + fileName.replace("'", "''") + "'" +" AND open_to_public is TRUE" ;
         	   ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
         	   resultSet.next();
         	  if(resultSet.getString(1).equals(fileName))
         		   return true;
         	   return false;
         	}
         
     }

