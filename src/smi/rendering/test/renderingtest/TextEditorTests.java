package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import jxl.read.biff.BiffException;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SavePopup;
import smi.rendering.test.pages.Say_As_Popup;
import smi.rendering.test.pages.SelectPage;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;


public class TextEditorTests extends FrameworkInitialize {
	private static Logger logger = null;
	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	private String usernameLogin = "";
	private String chosenProject = "";
	public boolean isThereMoreThanOneAssignedDomain;
	
		 
    /**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
     * @throws ClassNotFoundException 
     * @throws SQLException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Save",Setting.Browser);
	
	// Logger
	logger = LogManager.getLogger(TextEditorTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite = TextEditorTests ");
	logger.info("Save Tests - FrameworkInitilize");
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	
	// Get account,project and domain with the requested language and logged-in email
	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
	chosenProject = account_project_domain.get(index).get(1);
	
	DriverContext.browser.Maximize();
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		CurrentPage= GetInstance(HomePage.class);
		//Thread.sleep(10000);
		CurrentPage.As(HomePage.class).isHomepageReady();
	
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}
	
	/**
	 *  SRA-31:Check the “Style, Gesture and Mood” can be changed after selecting voice
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA31_Check_the_Style_Gesture_and_Mood_can_be_changed_after_selecting_voice() throws InterruptedException, SQLException, AWTException
	{
		test = extent.createTest("TextEditorTests-SRA-31", " SRA-31:Check the “Style, Gesture and Mood” can be changed after selecting voice");
			
//		// Check if the mood exist due a bug, if not then refresh once
//		try {
//			CurrentPage.As(HomePage.class).getMoodValues();
//		} catch (Exception e) {
//			LogUltility.log(test, logger, "Page was refreshed once due missing Moods");
//			DriverContext._Driver.navigate().refresh();
//			//Thread.sleep(10000);
//		}

		//CurrentPage.As(HomePage.class).doMoodExist();

		
		// Select a Domain // now need a domain in order to show the gestures...
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Select a Voice
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "select random voice: " + randomVoice);
		
		//Get the mood Values and compare the Dropdown with the DB
		CurrentPage = GetInstance(HomePage.class);
		// Get moods value from the DB
		
		// Get the mood list from dropdown
		ArrayList<String> MoodValues = null;
		do {
			try {
				MoodValues= CurrentPage.As(HomePage.class).getMoodValues();
				LogUltility.log(test, logger, "The Moods list returned from Dropdown: " + MoodValues);
			} catch (Exception e) {
				System.out.println("mood exception: " + e);
			}
		} while (MoodValues == null);
			
		// Get voice id 
		String voiceId = CurrentPage.As(HomePage.class).getVoiceID(con,randomVoice);
//		List<String> DBMood = CurrentPage.As(HomePage.class).getMoodValuesFromDB(con, randomDomain);
		ArrayList<String> DBMoodsVoice = CurrentPage.As(HomePage.class).getVoiceMoodsFromDB(con, voiceId);
		LogUltility.log(test, logger, "The Voice Moods list returned from DB: " + DBMoodsVoice);
		
		// Get domain DB moods
		List<String> DBMoodsDomain = CurrentPage.As(HomePage.class).getMoodValuesFromDB(con, randomDomain);
		LogUltility.log(test, logger, "The Domain Moods list returned from DB: " + DBMoodsDomain);

		// Get the intersection 
		DBMoodsVoice.retainAll(DBMoodsDomain);
		LogUltility.log(test, logger, "The Voice X Domain common moods: " + DBMoodsVoice);
		
		// Sort the two lists before comparing
	    Collections.sort(DBMoodsVoice);
	    LogUltility.log(test, logger, "DBMood after sort: " + DBMoodsVoice);
	    Collections.sort(MoodValues);
	    LogUltility.log(test, logger, "MoodValues after sort: " + MoodValues);
	  
	    // Compare if the Mood dropdown values are the same in the database
	    boolean compareMoodResult = MoodValues.equals(DBMoodsVoice);
	    LogUltility.log(test, logger, "Compare if the Mood dropdown values :"+ MoodValues +" are the same in the database: " + DBMoodsVoice+ " and value should be true " + compareMoodResult);
    	assertTrue(compareMoodResult);
				
		//Get a style Values and compare the Dropdown with the DB
		ArrayList<String> StyleValues= CurrentPage.As(HomePage.class).getStyleValues();
		List<String> DBStyle = CurrentPage.As(HomePage.class).getStylesValuesFromDB(con, randomDomain);
		
		// Sort the two lists before comparing
		Collections.sort(StyleValues);
		LogUltility.log(test, logger, "StyleValues after sort: " + StyleValues);
	    Collections.sort(DBStyle);
	    LogUltility.log(test, logger, "DBStyle after sort: " + DBStyle);
		  
	    // Compare if the style dropdown values are the same in the database
	    boolean compareStyleResult = StyleValues.equals(DBStyle);
        LogUltility.log(test, logger, "Compare if the Style dropdown values are the same in the database: " + compareStyleResult);
	    assertTrue(compareStyleResult);
		
	    //Get a Gesture Values and compare the Dropdown with the DB
		//Thread.sleep(5000);
		CurrentPage = GetInstance(HomePage.class);
		ArrayList<String> GestureValues= CurrentPage.As(HomePage.class).getGestureValues();
		List<String> DBGesture = CurrentPage.As(HomePage.class).getGestureValuesFromDB(con, randomDomain);
		
		// Sort the two lists before comparing
	    Collections.sort(GestureValues);
        LogUltility.log(test, logger, "GestureValues after sort: " + GestureValues);
	    Collections.sort(DBGesture);
	    LogUltility.log(test, logger, "DBGesture after sort: " + DBGesture);
		  
	    // Compare if the Gesture dropdown values are the same in the database
	    boolean compareGestureResult = GestureValues.equals(DBGesture);
	    LogUltility.log(test, logger, "Compare if the Style dropdown values are the same in the database: " + compareGestureResult);
	    assertTrue(compareGestureResult);
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-32:Check the Style dropdown
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA32_Check_the_Style_dropdown() throws InterruptedException, SQLException, AWTException
	{
		test = extent.createTest("TextEditorTests- SRA-32", "Check the Style dropdown");
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		CurrentPage.As(HomePage.class).cmbDomain.click();
		 CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		//Get a style Values and compare the Dropdown with the DB
		//Thread.sleep(5000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		ArrayList<String> StyleValues= CurrentPage.As(HomePage.class).getStyleValues();
		List<String> DBStyle = CurrentPage.As(HomePage.class).getStylesValuesFromDB(con, randomDomain);
		
		// Sort the two lists before comparing
	    Collections.sort(StyleValues);
	    LogUltility.log(test, logger, "StyleValues after sort: " + StyleValues);
	    Collections.sort(DBStyle);
	    LogUltility.log(test, logger, "DBStyle after sort: " + DBStyle);
	   
	    // Compare if the style dropdown values are the same in the database
	    boolean compareStyleResult = StyleValues.equals(DBStyle);
	    LogUltility.log(test, logger, "Compare if the Style dropdown values are the same in the database: " + compareStyleResult);
	    assertTrue(compareStyleResult);
	  
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/* SRA-56:Check the remove Mood feature
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA56_Check_the_remove_Mood_feature() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests- SRA-56", "Check the remove Mood feature");
				
//		// Check if the mood exist due a bug, if not then refresh once
//		try {
//			CurrentPage.As(HomePage.class).getMoodValues();
//		} catch (Exception e) {
//			LogUltility.log(test, logger, "Page was refreshed once due missing Moods");
//			DriverContext._Driver.navigate().refresh();
//			//Thread.sleep(10000);
//		}
		
		//CurrentPage.As(HomePage.class).doMoodExist();
		
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMood(con, chosenProject, usernameLogin);
		  LogUltility.log(test, logger, "select random domain: " + randomDomain);

	    // Type in the textbox
		  //Thread.sleep(5000);
//		  String randomText = generateInput.RandomString(1, "words");
		  String randomText = RandomText.getSentencesFromFile();
		  LogUltility.log(test, logger, "input text to the text area: " + randomText);
		  CurrentPage.As(HomePage.class).Textinput.click();
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		    
		// Mark the text in the text area
		  LogUltility.log(test, logger, "highlight the text in the textbox");
		  CurrentPage.As(HomePage.class).Textinput.click();
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		  Thread.sleep(1000);
		  
		  Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
	        Robot robot = new Robot();
	        robot.mouseMove(point.getX(),point.getY());
		  
		// press the mood button and select one of the moods 
		  String choice = CurrentPage.As(HomePage.class).chooseRandomMood();
		  LogUltility.log(test, logger, "Chosen Mood: " + choice);
		  //Thread.sleep(5000);data-mood
		  
		// Verify that mood was selected for the highlighted text
		  String MoodType= CurrentPage.As(HomePage.class).txtMood.getAttribute("data-mood");
		  String MoodTypeName = CurrentPage.As(HomePage.class).convertIDtoMood(con, MoodType);
		  LogUltility.log(test, logger, "MoodType: " + MoodType + ", MoodTypeName: " + MoodTypeName);
		  assertEquals(MoodTypeName, choice);
		  //Thread.sleep(5000);
        
//		  Point point = CurrentPage.As(HomePage.class).MoodValues.getLocation();
//          Robot robot = new Robot();
//          robot.mouseMove(point.getX()-20,point.getY()-107);
//          robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//  	      robot.delay(1000);
//  	      robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
          
		// Mark again the text in the text area to remove the mood
		  LogUltility.log(test, logger, "highlight the text in the textbox");
		  CurrentPage.As(HomePage.class).Textinput.click();
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		  //Thread.sleep(5000);
		  
		// press the mood button and select the neutral mood
		  
		  Actions builder = new Actions(DriverContext._Driver);
	  	  builder.moveToElement(CurrentPage.As(HomePage.class).MoodValues).perform();
	  		
		  String choiceNeutral  = CurrentPage.As(HomePage.class).SelectFromMoods("Neutral");
		  LogUltility.log(test, logger, "Second Chosen Mood should be Neutral: " + choiceNeutral);
		  
        // check that the text without mood
		  try {
			  String MoodTypeNeutral= CurrentPage.As(HomePage.class).txtMood.getAttribute("data-mood");
			  String MoodTypeName2 = CurrentPage.As(HomePage.class).convertIDtoMood(con, MoodTypeNeutral);
			  LogUltility.log(test, logger, "Mood still exist: " + MoodTypeNeutral);
			  LogUltility.log(test, logger, "Mood still exist: " + MoodTypeName2);
			  boolean samemood= MoodTypeName2.equals("Neutral");
			  LogUltility.log(test, logger, "Mood should be: " + MoodTypeName2+ " and it should be equal to Neutral, and the value should be true: "+samemood );
			  assertTrue(samemood);
		} catch (Exception e) {
			LogUltility.log(test, logger, "No mood was found");
		}
		  
		  LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA-45:Verify user can add Gestures to the text
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class) 
	public void SRA45_Verify_user_can_add_Gestures_to_the_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-45", "Verify user can add Gestures to the text");
		
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Choose random gesture
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		//Thread.sleep(4000);
		CurrentPage.As(HomePage.class).btnGesture.click();
		String chosenGesture = CurrentPage.As(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Chosen Gesture: " + chosenGesture);

		// Type in the textbox
		//Thread.sleep(5000);
//	    String randomText = generateInput.RandomString(1, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Choose random gesture 1
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
		//Thread.sleep(4000);
		//CurrentPage.As(HomePage.class).btnGesture.click();
		String chosenGesture1 = CurrentPage.As(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Chosen Gesture: " + chosenGesture1);
		
		// Type in the textbox
		//Thread.sleep(4000);
//	    String randomText1 = generateInput.RandomString(1, "words");
		String randomText1 = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText1);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText1);
		
		// Choose random gesture 2
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
		//Thread.sleep(4000);
		//CurrentPage.As(HomePage.class).btnGesture.click();
		String chosenGesture2 = CurrentPage.As(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Chosen Gesture: " + chosenGesture2);
		
		// Get the text from the text input
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Get the text from the text editor");
		CurrentPage = GetInstance(HomePage.class);
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		
		
		
		
		
		LogUltility.log(test, logger, "Verify the text contain all the gestures");
		
		boolean containgesture= InnerText.contains(chosenGesture);
		LogUltility.log(test, logger, "text editor contain containgesture1 should be True :" + containgesture  );
		assertTrue(containgesture);
		boolean containgesture1= InnerText.contains(chosenGesture1);
		LogUltility.log(test, logger, "text editor contain containgesture1 should be True :" + containgesture1 );
		assertTrue(containgesture1);
		boolean containgesture2= InnerText.contains(chosenGesture2);
		LogUltility.log(test, logger, "text editor contain containgesture1 should be True :" + containgesture2);
		assertTrue(containgesture2);
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-57:Verify adding Emphasis to the text
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA57_Verify_adding_Emphasis_to_the_text() throws InterruptedException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-57", "Verify adding Emphasis to the text");
	
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(1, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// Click the bold button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Click the bold button");
		CurrentPage.As(HomePage.class).btnBold.click();
		
		// Check that the text is bolded
		//Thread.sleep(2000);
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);
		
//		boolean doContain = source.contains("data-type=\"emphasis\"");
		boolean doContain = source.contains("data-emphasis=");
		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
		assertTrue(doContain);
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	 
	/**
	 * SRA-82:Check the wrong popup message displays when user click the play message without selecting a voice
	 * @throws InterruptedException
	 * @throws SQLException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA82_Check_the_wrong_popup_message_displays_when_user_click_the_play_message_without_selecting_a_voice() throws InterruptedException, SQLException 
	{
		test = extent.createTest("TextEditorTests-SRA-82", "Check the wrong popup message displays when user click the play message without selecting a voice");
				
		//click the play button
		CurrentPage.As(HomePage.class).btnplay.click();
		
		//check popup displayed "select a voice"
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please select Voice     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-83:Check the wrong popup message displays when the user click the play message without selecting Domain
	 * @throws InterruptedException
	 * @throws SQLException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA83_Check_the_wrong_popup_message_displays_when_the_user_click_the_play_message_without_selecting_Domain() throws InterruptedException, SQLException 
	{
		test = extent.createTest("TextEditorTests-SRA-83", "Check the wrong popup message displays when the user click the play message without selecting Domain");
	
		// Select a domain
//		//String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		String randomDomain = "Bank Hapoalim PoC Domain";
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		//click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).btnplay.click();
		
		//check popup displayed "select a voice"
		CurrentPage.As(HomePage.class).fadedPopup(logger, "There is no text to process     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-84:Check the wrong popup message displays when the user click the play without inputting text
	 * @throws InterruptedException
	 * @throws SQLException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA84_Check_the_wrong_popup_message_displays_when_the_user_click_the_play_without_inputting_text() throws InterruptedException, SQLException 
	{
		test = extent.createTest("TextEditorTests-SRA-84", "Check the wrong popup message displays when the user click the play without inputting text");
	
		// Select a voice
		//Thread.sleep(2000);
//		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select a domain
	//	String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		String randomDomain = "Bank Hapoalim PoC Domain";
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		//click the play button
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).btnplay.click();
		
		//check popup displayed "select a voice"
		CurrentPage.As(HomePage.class).fadedPopup(logger, "There is no text to process     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	 }	
	
	/**
	 * SRA-85:Check the reaction of the app when the user inputs NOT English words
	 * @throws InterruptedException
	 * @throws SQLException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA85_Check_the_reaction_of_the_app_when_the_user_inputs_NOT_English_words() throws InterruptedException, SQLException 
	{
		test = extent.createTest("TextEditorTests-SRA-85", "Check the reaction of the app when the user inputs NOT English words");
	
		// Select a voice
		//Thread.sleep(2000);
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
		
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Type in the textbox
		//Thread.sleep(2000);
	    String randomText = "של";
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		//click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		//check popup displayed "select a voice"
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Error loading clip     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-88:Check the limitation of the text box
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA88_Check_the_limitation_of_the_text_box() throws InterruptedException, SQLException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-88", "Check the limitation of the text box");
			
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(50, "words");
		String randomText = "";
		for (int i = 0; i < 10; i++) {
			randomText += RandomText.getSentencesFromFile();
		}
				
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		//Press Enter to start a new line 
		CurrentPage.As(HomePage.class).Textinput.click();
		LogUltility.log(test, logger, "Press several time the 'Enter' button");
		for(int i=1; i<17; i++){
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.ENTER);
		}
		
		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText2 = generateInput.RandomString(15, "words");
		String randomText2 = "";
		for (int i = 0; i < 5; i++) {
			randomText2 += RandomText.getSentencesFromFile();
		}
		LogUltility.log(test, logger, "input text to the text area: " + randomText2);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText2);
		
		// Check that scrolls are visible
		LogUltility.log(test, logger, "Check that the 2 scroll bars are visible");
		boolean isScrollVisible = CurrentPage.As(HomePage.class).isScrollVisible();
		LogUltility.log(test, logger, "is scroll visible: " + isScrollVisible);
		assertTrue(isScrollVisible);
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA-115:Check changing voices does not resetting text box
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA115_Check_changing_voices_does_not_resetting_text_box() throws InterruptedException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-115", "Check changing voices does not resetting text box");
	
		// Select a voice
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
	
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
					
		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Change the voice
		Thread.sleep(1000);
		randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
		
		// Get the text from the text editor and check that the same text still exist
		String getText = CurrentPage.As(HomePage.class).Textinput.getText();
		
		boolean isItEqual = getText.equals(randomText);
		LogUltility.log(test, logger, "Is it equal: " + isItEqual);
		assertTrue(isItEqual);
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-58:Check the remove Emphasis feature
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA58_Check_the_remove_Emphasis_feature() throws InterruptedException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-58", "Check the remove Emphasis feature");
	
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(1, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// Click the bold button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Click the bold button");
		CurrentPage.As(HomePage.class).btnBold.click();
		
		// Check that the text is bolded
		//Thread.sleep(2000);
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);
		
		//boolean doContain = source.contains("data-type=\"emphasis\"");
		boolean doContain = source.contains("data-emphasis=");
		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
		assertTrue(doContain);
		
		// Click the bold button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Click the bold button");
		CurrentPage.As(HomePage.class).btnBold.click();
		
		// Check that the text is not bold, emphasis is removed
		//Thread.sleep(2000);
		source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);
		
		//doContain = source.contains("data-type=\"emphasis\"");
		doContain = source.contains("data-emphasis=");
		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
		assertFalse(doContain);
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-130:Check the Mood dropdown
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 */
	@Test(groups= "Full Refression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA130_Check_the_Mood_dropdown() throws InterruptedException, SQLException, AWTException 
	{
		test = extent.createTest("TextEditorTests-SRA-130", "Check the Mood dropdown");
		
		//Actions builder = new Actions(DriverContext._Driver);
		//builder.moveToElement(CurrentPage.As(HomePage.class).MoodValues).perform();
//		By locator = By.id("clickElementID");
//		driver.click(locator);
		
//		// Check if the mood exist due a bug, if not then refresh once
//		try {
//			CurrentPage.As(HomePage.class).getMoodValues();
//		} catch (Exception e) {
//			LogUltility.log(test, logger, "Page was refreshed once due missing Moods");
//			DriverContext._Driver.navigate().refresh();
//			//Thread.sleep(10000);
//		}
	
	    //CurrentPage.As(HomePage.class).doMoodExist();
		
		// Select a Domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMood(con, chosenProject, usernameLogin);
//		String randomDomain = "Banking and Financing";
//		 CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		
		//Get the mood Values and compare the Dropdown with the DB
		//Thread.sleep(5000);
		//CurrentPage = GetInstance(HomePage.class);
		List<String> DBMood = CurrentPage.As(HomePage.class).getMoodValuesFromDB(con, randomDomain);
		ArrayList<String> MoodValues= CurrentPage.As(HomePage.class).getMoodValues();
//		MoodValues.add("Neutral");
	
		// Sort the two lists before comparing
	    Collections.sort(DBMood);
	    LogUltility.log(test, logger, "DBMood after sort: " + DBMood);
	    Collections.sort(MoodValues);
	    LogUltility.log(test, logger, "MoodValues after sort: " + MoodValues);
		  
	    // Compare if the Mood dropdown values are the same in the database
	    boolean compareMoodResult = MoodValues.equals(DBMood);
	    LogUltility.log(test, logger, "Compare if the Mood dropdown values are the same in the database: " + compareMoodResult);
	    assertTrue(compareMoodResult);
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-129:check Gestures dropdown
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 */
	@Test(groups= "Full Refression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA129_Check_the_Gesture_dropdown() throws InterruptedException, SQLException, AWTException 
	{
		test = extent.createTest("TextEditorTests-SRA-129", "Check the Gesture dropdown");
		 
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		//Get a Gesture Values and compare the Dropdown with the DB
		//Thread.sleep(5000);
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		ArrayList<String> GestureValues= CurrentPage.As(HomePage.class).getGestureValues();
		//List<String> DBGesture = CurrentPage.As(HomePage.class).getGestureValuesFromDB(con, randomDomain);
		List<String> DBGesture = CurrentPage.As(HomePage.class).getGestureValuesFromDB(con, randomDomain);

	    // Sort the two lists before comparing
  	    Collections.sort(GestureValues);
	    LogUltility.log(test, logger, "GestureValues after sort: " + GestureValues);
        Collections.sort(DBGesture);
	    LogUltility.log(test, logger, "DBGesture after sort: " + DBGesture);
	  
	    // Compare if the Gesture dropdown values are the same in the database
	    boolean compareGestureResult = GestureValues.equals(DBGesture);
	    LogUltility.log(test, logger, "Compare if the Gesture dropdown values are the same as in the database: " + compareGestureResult);
	    assertTrue(compareGestureResult);
	    LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-59:Verify adding International Phonetic Alphabet to the text
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA59_Verify_adding_International_Phonetic_Alphabet_to_the_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-59", "Verify adding International Phonetic Alphabet to the text");
		
		DriverContext.browser.Maximize();
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(5000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
	    
		// Mark the text in the text area
		//Thread.sleep(2000);
        //CurrentPage.As(HomePage.class).Textinput.click();
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
	
//		 point = CurrentPage.As(HomePage.class).barWave.getLocation();
//		 robot.mouseMove(point.getX(),point.getY());
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
        robot.mouseMove(point.getX()+60,point.getY()+105);
        //robot.mouseMove(point.getX()+60,point.getY()+108);
//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(1, "words");
		String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-60:Verify adding Pronunciation Symbol to the text
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA60_Verify_adding_Pronunciation_Symbol_to_the_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-60", "Verify adding Pronunciation Symbol to the text");
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
        robot.mouseMove(point.getX()+60,point.getY()+105);
//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
	    // Click the punctuation radio button
	    CurrentPage = GetInstance(Say_As_Popup.class);
	    //Thread.sleep(1000);
	    CurrentPage.As(Say_As_Popup.class).rdoPunctiation.click();
	    
	    // Choose random vowel, consonant, stress and lenghening
	    String vowel = CurrentPage.As(Say_As_Popup.class).chooseRandomVowel();
	    
	    String consonant = CurrentPage.As(Say_As_Popup.class).chooseRandomConsonant();
	    
	    String stress = CurrentPage.As(Say_As_Popup.class).chooseRandomStress();
	    
	    String lenghening = CurrentPage.As(Say_As_Popup.class).chooseRandomLenghening();
	    	    
	    // Scroll down to the ok button
	    Point place = CurrentPage.As(Say_As_Popup.class).btnOK.getLocation();
	    JavascriptExecutor jse = (JavascriptExecutor)DriverContext._Driver;
	    jse.executeScript("window.scrollBy("+place.getX()+","+place.getY()+")", "");
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(vowel+consonant+stress+lenghening);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-137:Verify adding International Phonetic Alphabet with Pronunciaton symbols to the text
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA137_Verify_adding_International_Phonetic_Alphabet_with_Pronunciaton_symbols_to_the_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-137", "Verify adding International Phonetic Alphabet with Pronunciaton symbols to the text");
		
		// Select a Domain
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
				
		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
  		robot.mouseMove(point.getX()+60,point.getY()+105);
//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
	    // Click the punctuation radio button
	    CurrentPage = GetInstance(Say_As_Popup.class);
	    //Thread.sleep(1000);
	    CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
	    CurrentPage.As(Say_As_Popup.class).rdoPunctiation.click();
	    
	    // Write the say as text
 		//Thread.sleep(2000);
 		CurrentPage = GetInstance(Say_As_Popup.class);
 		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
// 		String randomItalicText = generateInput.RandomString(1, "words");
 		String randomItalicText = RandomText.getSentencesFromFile();
 		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
 		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
	 		
	    // Choose random vowel, consonant, stress and lenghening
	    String vowel = CurrentPage.As(Say_As_Popup.class).chooseRandomVowel();
	    
	    String consonant = CurrentPage.As(Say_As_Popup.class).chooseRandomConsonant();
	    
	    String stress = CurrentPage.As(Say_As_Popup.class).chooseRandomStress();
	    
	    String lenghening = CurrentPage.As(Say_As_Popup.class).chooseRandomLenghening();
	    
	    // Scroll down to the ok button
	    Point place = CurrentPage.As(Say_As_Popup.class).btnOK.getLocation();
	    JavascriptExecutor jse = (JavascriptExecutor)DriverContext._Driver;
	    jse.executeScript("window.scrollBy("+place.getX()+","+place.getY()+")", "");
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getText.equals(randomItalicText+vowel+consonant+stress+lenghening);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-139:Verify User can replace few words with IPA phrase
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA139_Verify_User_can_replace_few_words_with_IPA_phrase() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-139", "Verify User can replace few words with IPA phrase");
		
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(5, "letters") + " " + generateInput.RandomString(5, "letters");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);
//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(5, "words");
		String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-142:Verify User can replace a phrase with one IPA word
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA142_Verify_User_can_replace_a_phrase_with_one_IPA_word() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-142", "Verify User can replace a phrase with one IPA word");
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
	    String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);
//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
	   // String randomItalicText = generateInput.RandomString(5, "letters") + " " + generateInput.RandomString(5, "letters");
		String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-140:Verify Message appear when try to add IPA when no text was selected
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA140_Verify_Message_appear_when_try_to_add_IPA_when_no_text_was_selected() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-140", "Verify Message appear when try to add IPA when no text was selected");
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
	    String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Click the italic button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);
//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		boolean answer = CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpenWithAnswer();
		LogUltility.log(test, logger, "Did IPA window open: " + answer);
		assertFalse(answer);
		
//	    String randomItalicText = CurrentPage.As(Say_As_Popup.class).RandomString(5);
//		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
//		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
//		
//		// Click the OK button
//		//Thread.sleep(2000);
//		CurrentPage.As(Say_As_Popup.class).btnOK.click();
//		
//		// Check the Save confirmation message
//		CurrentPage = GetInstance(HomePage.class);
//		CurrentPage.As(HomePage.class).fadedPopup(logger, "No text was selected     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-141:Verify user can modify the exist IPA and Pronunciation symbols
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA141_Verify_user_can_modify_the_exist_IPA_and_Pronunciation_symbols() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-141", "Verify user can modify the exist IPA and Pronunciation symbols");
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//		        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);
//			    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(1, "words");
	    String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		//-------------------------
		// SECOND PART - modifying
		// Now try to modify the added IPA and check the result
        robot.mouseMove(point.getX(),point.getY());
        
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);

		// Write the say as text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    randomItalicText = generateInput.RandomString(1, "words");
	    randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.clear();
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);

		LogUltility.log(test, logger, "Test Case PASSED");

	 }
	
	/**
	 * SRA-124:Verify the user can save multiple lines in the text box
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA124_Verify_the_user_can_save_multiple_lines_in_the_text_box() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("TextEditorTests-SRA-124", "Verify the user can save multiple lines in the text box");
		
//		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		
		// Enter a text
		//Thread.sleep(2000);
		String allEnteredText ="";
//		String text = generateInput.RandomString(1, "words");
		String text = RandomText.getSentencesFromFile();
		allEnteredText = text;
		LogUltility.log(test, logger, "Text to enter: " + text);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		
		LogUltility.log(test, logger, "Click enter");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.ENTER);
		allEnteredText += "\n";
		
//		text = generateInput.RandomString(1, "words");
		text = RandomText.getSentencesFromFile();
		allEnteredText += text;
		LogUltility.log(test, logger, "Text to enter: " + text);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Clear the text box editor
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.clear();
		
	    // Click the open button and open the saved file
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).openFileWait();
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		// Click Open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box and equal to the entered text (with line between)
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String savedText = CurrentPage.As(HomePage.class).Textinput.getText();
		LogUltility.log(test, logger, "text in textbox: " + savedText);
		assertEquals(savedText, allEnteredText);
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-75:Verify user can add numbers and symbols
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA75_Verify_user_can_add_numbers_and_symbols() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("TextEditorTests-SRA_75", "Verify user can add numbers and symbols");
		
//		// Check if the mood exist due a bug, if not then refresh once
//		try {
//			CurrentPage.As(HomePage.class).getMoodValues();
//		} catch (Exception e) {
//			LogUltility.log(test, logger, "Page was refreshed once due missing Moods");
//			DriverContext._Driver.navigate().refresh();
//			//Thread.sleep(10000);
//		}
		
		//CurrentPage.As(HomePage.class).doMoodExist();
		
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
//		String randomDomain = "Bank Hapoalim PoC Domain";
//		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		
		// Create new file with text that includes numbers and specific  symbols : ^ and & 
		//Thread.sleep(2000);
		String text = CurrentPage.As(HomePage.class).randomSymbols(25) + CurrentPage.As(HomePage.class).randomNumbers(10);
		LogUltility.log(test, logger, "Text to enter: " + text);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		
		// Choose random gesture
		  //Thread.sleep(5000);
		  LogUltility.log(test, logger, "Press into the textbox");
		  CurrentPage = GetInstance(HomePage.class);
		  CurrentPage.As(HomePage.class).Textinput.click();
		  String chosenGesture = CurrentPage.As(HomePage.class).chooseRandomGesture();
		  LogUltility.log(test, logger, "Chosen Gesture: " + chosenGesture);
		    		    
		  //Choose random style
		  //Thread.sleep(5000);
		  CurrentPage = GetInstance(HomePage.class);
		  //LogUltility.log(test, logger, "Press into the textbox");
		  //CurrentPage.As(HomePage.class).Textinput.click();
		  String chosenStyle = CurrentPage.As(HomePage.class).chooseRandomStyle();
		  LogUltility.log(test, logger, "Chosen style:" + chosenStyle );
		  
		// Mark the text in the text area
		  LogUltility.log(test, logger, "highlight the text in the textbox");
		  CurrentPage.As(HomePage.class).Textinput.click();
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		  //Thread.sleep(5000);
		  
		  // press the mood button and select one of the moods 
		  String choice = CurrentPage.As(HomePage.class).chooseRandomMood();
		  LogUltility.log(test, logger, "Chosen Mood: " + choice);
		  
		
		// Click the save button
		  //Thread.sleep(5000);
		LogUltility.log(test, logger, "Click the save button");
		//Thread.sleep(4000);
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		//Thread.sleep(2000);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
//		//Refresh the home page
//		DriverContext._Driver.navigate().refresh();
//		//Thread.sleep(6000);
		
		
//		// Select the same domain that was selected before
//		CurrentPage = GetInstance(HomePage.class);
//		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		
	    // Click the open button and open the saved files with the specific symbols 
		Thread.sleep(5000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).openFileWait();
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
	    // Choose the created file and click open
		////Thread.sleep(3000);
		//CurrentPage.As(OpenPopup.class).cmbText.click();
		//String Chosen = CurrentPage.As(OpenPopup.class).SelectFromFileDropdown(fileName);
		//LogUltility.log(test, logger, "Select the created file: " + Chosen);
		// Click Open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		//CurrentPage.As(OpenPopup.class).btnOpen.click();
		
	    // Check that there is text displayed in the text box and equal to the entered text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		String savedText = CurrentPage.As(HomePage.class).Textinput.getText().trim();
		LogUltility.log(test, logger, "text in textbox: " + text + ", and text that saved: " + savedText);
		assertEquals(savedText, text);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-160:Try text with non regular quotes/double quotes
	 * @throws InterruptedException 
	 * @throws SQLException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA160_Try_text_with_non_regular_quotes_double_quotes() throws InterruptedException, SQLException
	{		
		test = extent.createTest("TextEditorTests-SRA_160", "Try text with non regular quotes/double quotes");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Enter text in the text box
		//Thread.sleep(3000);
		String Text = "I said, ‘This isn’t going to happen.’ I ended up passing all the ambulances because they would be actively triaging people.”";
		LogUltility.log(test, logger, "Text to enter: " + Text);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Text);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				
		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-159:Check the download button for the synthesized text
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA159_Check_the_download_button_for_the_synthesized_text() throws InterruptedException, SQLException, FileNotFoundException
	{		
		test = extent.createTest("TextEditorTests-SRA_159", "Check the download button for the synthesized text");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				
		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		
		Thread.sleep(5000);
		// Check if download button is displayed
		boolean isDownloadDisplayed = CurrentPage.As(HomePage.class).btnDownload.isDisplayed();
		LogUltility.log(test, logger, "Is download button displayed: " + isDownloadDisplayed);
		assertTrue(isDownloadDisplayed);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-162:Mouse click on text editor after rendering
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA162_Mouse_click_on_text_editor_after_rendering() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{		
		test = extent.createTest("TextEditorTests-SRA_162", "Mouse click on text editor after rendering");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice );	
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				
		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);

		// Mouse click in the text editor
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
	    robot.delay(1000);
	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
        
		// Check that the wave bar still exist and was not reseted
		isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-168:Text Verify user can clear the text editor
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 * @throws ScriptException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA168_text_Verify_user_can_clear_the_text_editor() throws InterruptedException, SQLException, FileNotFoundException, ScriptException 
	{
		test = extent.createTest("TextEditorTests-SRA-168", "Text Verify user can clear the text editor");

					
		// Type in the textbox
//	    String randomText = generateInput.RandomString(1, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		
		// Get the text from the text editor and check that the same text still exist
		String getText = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean isEmpty = getText.isEmpty();
		LogUltility.log(test, logger, "Is the text editor empty: " + isEmpty);
		assertFalse(isEmpty);
		
		// Click the clear text button
		LogUltility.log(test, logger, "Click the clear text button");
		CurrentPage.As(HomePage.class).clearText.click();
		
		// Get the text from the text editor and check that the same text still exist
		getText = CurrentPage.As(HomePage.class).Textinput.getText();
		isEmpty = getText.isEmpty();
		LogUltility.log(test, logger, "Is the text editor empty: " + isEmpty);
		assertTrue(isEmpty);

		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-168:Template Verify user can clear the text editor
	 * @throws InterruptedException
	 * @throws SQLException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA168_Template_Verify_user_can_clear_the_text_editor() throws InterruptedException, SQLException 
	{
		test = extent.createTest("TextEditorTests-SRA-168", "Template Verify user can clear the text editor");
	
		// Select a domain
		//String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
		//LogUltility.log(test, logger, "select random domain: " + randomDomain);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, "Banking and Financing");
		
		// Click the open button and check for open popup
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		//Choose the Template radio button
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		
	    // Choose random Template and click open
		//Thread.sleep(1000);
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		//CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected template: " + chosenTemplate);
		// Click Open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box
		Thread.sleep(1000);
		CurrentPage = GetInstance(HomePage.class);
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean containsText = text.isEmpty();
		assertFalse(containsText);
		LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + containsText);
		
		// Click the clear text button
		LogUltility.log(test, logger, "Click the clear text button");
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).clearText.click();
		
		// Get the text from the text editor and check that the same text still exist
		text = CurrentPage.As(HomePage.class).Textinput.getText();
		containsText = text.isEmpty();
		LogUltility.log(test, logger, "Is the text editor empty: " + containsText);
		assertTrue(containsText);

		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-174:Verify that the Say-as popup doesn`t open if no text was highlighted
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA174_Verify_that_the_Say_as_popup_doesn_t_open_if_no_text_was_highlighted() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-174", "Verify that the Say-as popup doesn`t open if no text was highlighted");
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
			
		// Click the italic button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Try to open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
 
  		// Write the say as text
		Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
//		CurrentPagentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = CurrentPage.As(Say_As_Popup.class).RandomString(5) + " " + CurrentPage.As(Say_As_Popup.class).RandomString(5);
//		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
//		CurrentPage.As(Say_As_Popup.class).txtInput.isDisplayed();
		
		try {
			String title = CurrentPage.As(Say_As_Popup.class).titleSayAs.getText();
			//if (title != "") assertTrue(false);
			boolean result = title.contains("Say-As");
			assertFalse(result);
		} catch (Exception e) {
			
		}
		LogUltility.log(test, logger, "Could not find IPA window");
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-163:Check restore previous domain option with gestures
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA163_Check_restore_previous_domain_option_with_gestures() throws InterruptedException, SQLException, AWTException
	{
		test = extent.createTest("TextEditorTests-SRA-163", "Check restore previous domain option with gestures");
		
		List<String> twoDomains = CurrentPage.As(HomePage.class).getTwoDomainsDifferentGesturesFromDB(con);
		
		// check if two domains with different gestures found
		if (twoDomains.get(0).equals("nothing")) {
			LogUltility.log(test, logger, "Stopping this test, Could not find a gesture in a domain"
					+ "that is not included in another domian");
			assertTrue(false);
		}
		
		String firstDomain = twoDomains.get(0);
		String secondDomain = twoDomains.get(1);
		String gestureToUse = twoDomains.get(2);
		LogUltility.log(test, logger, "First domain: " + firstDomain + 
				", Second domain: " + secondDomain +
				", Gesture to use: " + gestureToUse);
		
		// Choose a voice
		CurrentPage.As(HomePage.class).isHomepageReady();
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
		
		// Select the first domain
//		CurrentPage.As(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, firstDomain);
		CurrentPage.As(HomePage.class).select_Domain(firstDomain);
		LogUltility.log(test, logger, "selected first domain: " + firstDomain);
		
		// Choose random gesture
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		//Thread.sleep(4000);
		CurrentPage.As(HomePage.class).btnGesture.click();
		CurrentPage.As(HomePage.class).SelectFromGestures(gestureToUse);
		LogUltility.log(test, logger, "Chosen Gesture: " + gestureToUse);

		// Select the second domain
//		CurrentPage.As(HomePage.class).isHomepageReady();
//		CurrentPage.As(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, secondDomain);
		CurrentPage.As(HomePage.class).select_Domain(secondDomain);
		LogUltility.log(test, logger, "selected second domain: " + secondDomain);		
		
		// click the restore previous domain button
		Thread.sleep(4000);
		CurrentPage.As(HomePage.class).restoreDomain.click();
		LogUltility.log(test, logger, "click the restore previous domain button");
		
		// check that the current selected domain is the first one 
		String getCurrentDomain = CurrentPage.As(HomePage.class).cmbDomain.getText();
		boolean result = getCurrentDomain.contains(firstDomain);
		LogUltility.log(test, logger, "Is domain reverted: " + result);
		assertTrue(result);
		
		// check also that the gesture is still kept in the old domain
		// Get the text from the text editor
		LogUltility.log(test, logger, "Get the text from the text editor");
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		
		boolean containgesture= InnerText.contains(gestureToUse);
		LogUltility.log(test, logger, "text editor contain gesture should be True :" + containgesture  );
		assertTrue(containgesture);
				
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-164:Check keep new domain option with gestures
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA164_Check_keep_new_domain_option_with_gestures() throws InterruptedException, SQLException, AWTException
	{
		test = extent.createTest("TextEditorTests-SRA-164", "Check keep new domain option with gestures");
		
		List<String> twoDomains = CurrentPage.As(HomePage.class).getTwoDomainsDifferentGesturesFromDB(con);
		
		// check if two domains with different gestures found
		if (twoDomains.get(0).equals("nothing")) {
			LogUltility.log(test, logger, "Stopping this test, Could not find a gesture in a domain"
					+ "that is not included in another domian");
			assertTrue(false);
		}
		
		String firstDomain = twoDomains.get(0);
		String secondDomain = twoDomains.get(1);
		String gestureToUse = twoDomains.get(2);
		LogUltility.log(test, logger, "First domain: " + firstDomain + 
				", Second domain: " + secondDomain +
				", Gesture to use: " + gestureToUse);
		
		// Choose a voice
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
		
		// Select the first domain
//		CurrentPage.As(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, firstDomain);
		CurrentPage.As(HomePage.class).select_Domain(firstDomain);
		LogUltility.log(test, logger, "selected first domain: " + firstDomain);
		
		// apply the chosen gesture
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		//Thread.sleep(4000);
		CurrentPage.As(HomePage.class).btnGesture.click();
		CurrentPage.As(HomePage.class).SelectFromGestures(gestureToUse);
		LogUltility.log(test, logger, "Chosen Gesture: " + gestureToUse);

		// Select the second domain
//		CurrentPage.As(HomePage.class).isHomepageReady();
//		CurrentPage.As(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, secondDomain);
		CurrentPage.As(HomePage.class).select_Domain(secondDomain);
		LogUltility.log(test, logger, "selected second domain: " + secondDomain);		
		
		// click the keep new domain button
		Thread.sleep(4000);
		CurrentPage.As(HomePage.class).keepNewDomain.click();
		LogUltility.log(test, logger, "click the keep new domain button");
		
		// check that the current selected domain is the second one 
		String getCurrentDomain = CurrentPage.As(HomePage.class).cmbDomain.getText();
		boolean result = getCurrentDomain.contains(secondDomain);
		LogUltility.log(test, logger, "Is domain changed: " + result);
		assertTrue(result);
		
		// check also that the gesture is removed with the second domain
		// Get the text from the text editor
		Thread.sleep(2000);
		LogUltility.log(test, logger, "Get the text from the text editor");
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		
		boolean containgesture= InnerText.contains(gestureToUse);
		LogUltility.log(test, logger, "text editor contain gesture should be False :" + containgesture  );
		assertFalse(containgesture);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }

	/**
	 * SRA-165:Check restore previous domain option with moods
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA165_Check_restore_previous_domain_option_with_moods() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-165", "Check restore previous domain option with moods");
		
		// Choose a voice
		CurrentPage.As(HomePage.class).isHomepageReady();
//		String chosenVoice = CurrentPage.As(HomePage.class).getVoiceWithMoods(con);
//		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
//		String voiceID = CurrentPage.As(HomePage.class).getVoiceID(con,chosenVoice);
		
		// Get two domains with uncommon moods
		List<String> twoDomains = CurrentPage.As(HomePage.class).getTwoDomainsDifferentMoodsFromDB(con);
		
		// check if two domains with different gestures found
		if (twoDomains.get(0).equals("nothing")) {
			LogUltility.log(test, logger, "Stopping this test, Could not find a gesture in a domain"
					+ "that is not included in another domian");
			assertTrue(false);
		}
		
		String firstDomain = twoDomains.get(0);
		String secondDomain = twoDomains.get(1);
		String moodToUse = twoDomains.get(2);
		LogUltility.log(test, logger, "First domain: " + firstDomain + 
				", Second domain: " + secondDomain +
				", Mood to use: " + moodToUse);
		
		
		// Select the first domain
//		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(firstDomain);
		LogUltility.log(test, logger, "selected first domain: " + firstDomain);
		
		// apply the chosen mood
	    // Type in the textbox
//		  String randomText = generateInput.RandomString(1, "words");
		  String randomText = RandomText.getSentencesFromFile();
		  LogUltility.log(test, logger, "input text to the text area: " + randomText);
		  CurrentPage.As(HomePage.class).Textinput.click();
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		  
		  Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
	        Robot robot = new Robot();
	        robot.mouseMove(point.getX(),point.getY());
	        
		// Mark the text in the text area
		  LogUltility.log(test, logger, "highlight the text in the textbox");
		  CurrentPage.As(HomePage.class).Textinput.click();
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		  Thread.sleep(1000);
		  
		// press the mood button and select one of the moods 
		  Actions builder = new Actions(DriverContext._Driver);
	  	  builder.moveToElement(CurrentPage.As(HomePage.class).MoodValues).perform();
	  	  CurrentPage.As(HomePage.class).SelectFromMoods(moodToUse);
		  LogUltility.log(test, logger, "Chosen Mood: " + moodToUse);
		  
		// Select the second domain
//		CurrentPage.As(HomePage.class).isHomepageReady();
//		CurrentPage.As(HomePage.class).cmbDomain.click();
			CurrentPage.As(HomePage.class).select_Domain(secondDomain);
		LogUltility.log(test, logger, "selected second domain: " + secondDomain);		
		
		// click the restore previous domain button
		Thread.sleep(2000);
		CurrentPage.As(HomePage.class).restoreDomain.click();
		LogUltility.log(test, logger, "click the restore previous domain button");
		
		// check that the current selected domain is the first one 
		String getCurrentDomain = CurrentPage.As(HomePage.class).cmbDomain.getText();
		boolean result = getCurrentDomain.contains(firstDomain);
		LogUltility.log(test, logger, "Is domain reverted: " + result);
		assertTrue(result);
		
		// check also that the mood is still kept in the old domain
		String currentDomain = CurrentPage.As(HomePage.class).cmbDomain.getText();
		LogUltility.log(test, logger, "The current chosen domain is: " + currentDomain);
		assertEquals(firstDomain, currentDomain);
		
		// Verify that mood was selected for the highlighted text
		LogUltility.log(test, logger, "Get the text from the text editor");
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		
		// Check chosen  Mood is included
		String MoodType= CurrentPage.As(HomePage.class).txtMood.getAttribute("data-mood");
		String MoodTypeName = CurrentPage.As(HomePage.class).convertIDtoMood(con, MoodType);
		LogUltility.log(test, logger, "MoodType: " + MoodType + ", MoodTypeName: " + MoodTypeName);
		assertEquals(MoodTypeName, moodToUse);
		  
//		boolean containmood= InnerText.contains(moodToUse);
//		LogUltility.log(test, logger, "text editor contain mood:" + containmood);
//		assertTrue(containmood);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-166:Check keep new domain option with moods
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA166_Check_keep_new_domain_option_with_moods() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-166", "Check keep new domain option with moods");
		
		List<String> twoDomains = CurrentPage.As(HomePage.class).getTwoDomainsDifferentMoodsFromDB(con);
		
		// check if two domains with different gestures found
		if (twoDomains.get(0).equals("nothing")) {
			LogUltility.log(test, logger, "Stopping this test, Could not find a gesture in a domain"
					+ "that is not included in another domian");
			assertTrue(false);
		}
		
		String firstDomain = twoDomains.get(0);
		String secondDomain = twoDomains.get(1);
		String moodToUse = twoDomains.get(2);
		LogUltility.log(test, logger, "First domain: " + firstDomain + 
				", Second domain: " + secondDomain +
				", Mood to use: " + moodToUse);
		
		// Choose a voice
//		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
		
		// Select the first domain
		 CurrentPage.As(HomePage.class).isHomepageReady();
//		CurrentPage.As(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, firstDomain);
		CurrentPage.As(HomePage.class).select_Domain(firstDomain);
		LogUltility.log(test, logger, "selected first domain: " + firstDomain);
		Thread.sleep(2000);
		
		// apply the chosen mood
	    // Type in the textbox
//		  String randomText = generateInput.RandomString(1, "words");
	      String randomText = RandomText.getSentencesFromFile();
		  LogUltility.log(test, logger, "input text to the text area: " + randomText);
		  CurrentPage.As(HomePage.class).Textinput.click();
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		  
		  Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
	        Robot robot = new Robot();
	        robot.mouseMove(point.getX(),point.getY());
	        
		// Mark the text in the text area
		  LogUltility.log(test, logger, "highlight the text in the textbox");
		  CurrentPage.As(HomePage.class).Textinput.click();
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));

		// press the mood button and select one of the moods 
		  Actions builder = new Actions(DriverContext._Driver);
	  	  builder.moveToElement(CurrentPage.As(HomePage.class).MoodValues).perform();
	  	  CurrentPage.As(HomePage.class).SelectFromMoods(moodToUse);
		  LogUltility.log(test, logger, "Chosen Mood: " + moodToUse);
		  
		// Select the second domain
		 CurrentPage.As(HomePage.class).isHomepageReady();
//		CurrentPage.As(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, secondDomain);
		CurrentPage.As(HomePage.class).select_Domain(secondDomain);
		LogUltility.log(test, logger, "selected second domain: " + secondDomain);		
		
		// click the keep new domain button
		Thread.sleep(3000);
		CurrentPage.As(HomePage.class).keepNewDomain.click();
		LogUltility.log(test, logger, "click the keep new domain button");
		
		// check that the current selected domain is the second one 
		String getCurrentDomain = CurrentPage.As(HomePage.class).cmbDomain.getText();
		boolean result = getCurrentDomain.contains(secondDomain);
		LogUltility.log(test, logger, "Is domain changed: " + result);
		assertTrue(result);
		
		// check also that the mood is removed with the second domain
		// Get the text from the text editor
		LogUltility.log(test, logger, "Get the text from the text editor");
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		
		boolean containmood= InnerText.contains(moodToUse);
		LogUltility.log(test, logger, "text editor contain mood should be False :" + containmood);
		assertFalse(containmood);
						
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-172: Text, Verify that text shift when change voices for Hebrew and English
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA172_Text_Verify_that_text_shift_when_change_voices_for_Hebrew_and_English() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-172", "Check the “Text_Verify that text shift when change voices for Hebrew and English");
		// Modified on 27-11-2018, language value decides if text goes RTL or LTR not voices
		
		// Select a Voice that is only english
//		String randomVoiceEnglish = CurrentPage.As(HomePage.class).chooseRandomVoiceOnlyEnglish();
//		LogUltility.log(test, logger, "select random voice: " + randomVoiceEnglish);
		
		// Set language dropdown to English
		 CurrentPage.As(HomePage.class).select_language("English");
		 LogUltility.log(test, logger, "Set Language to English ");
		   
		// Select a Domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Type in the textbox
//		String randomText = generateInput.RandomString(1, "words");
		String randomText = RandomText.getSentencesFromFile();
	    LogUltility.log(test, logger, "input text to the text area: " + randomText);
	    CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Check that the text editor is left to right
		LogUltility.log(test, logger, "Check that the text editor is left to right");
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("style");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		boolean doesExist = InnerText.contains("ltr");
		assertTrue(doesExist);
		
		// Select a Voice that is hebrew
//		String randomVoiceHebrew = CurrentPage.As(HomePage.class).chooseRandomVoiceNotEnglish();
//		LogUltility.log(test, logger, "select random domain: " + randomVoiceHebrew);
		
		 CurrentPage.As(HomePage.class).select_language("עברית");
		 LogUltility.log(test, logger, "Set Language to עברית ");
		 
		// Check that the text editor is left to right
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Check that the text editor is left to right");
		InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("style");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		doesExist = InnerText.contains("rtl");
		
		assertTrue(doesExist);
		
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-172: Template, Verify that text shift when change voices for Hebrew and English
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA172_Template_Verify_that_text_shift_when_change_voices_for_Hebrew_and_English() throws InterruptedException, SQLException, AWTException
	{
		test = extent.createTest("TextEditorTests-SRA-172", " SRA-172:Check the “Template_Verify that text shift when change voices for Hebrew and English");
		
		// Select a Voice that is only english
//		String randomVoiceEnglish = CurrentPage.As(HomePage.class).chooseRandomVoiceOnlyEnglish();
//		LogUltility.log(test, logger, "select random voice: " + randomVoiceEnglish);
		// Set language dropdown to English
		CurrentPage.As(HomePage.class).select_language("English");
		 LogUltility.log(test, logger, "Set Language to English ");
		
		 // Store the language value
		 String temp = Setting.Language;
		 // Change for DB check
		 Setting.Language = "6";

		// Select a Domain
		//String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
		 CurrentPage.As(HomePage.class).isHomepageReady();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Click the open button and check for open popup
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		//Choose the Template radio button
		//Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).openFileWait();
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		LogUltility.log(test, logger, "Select the Template radio button");
		
	    // Choose random Template and click open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		//CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected file: " + chosenTemplate);
		// Click Open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
//	    // Check that there is text displayed in the text box
//		//Thread.sleep(3000);
//		CurrentPage = GetInstance(HomePage.class);
//		String text = CurrentPage.As(HomePage.class).Textinput.getText();
//		boolean containsText = text.isEmpty();
//		assertFalse(containsText);
//		LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + containsText);
//		
//		
			
		// Check that the text editor is left to right
		CurrentPage = GetInstance(HomePage.class);
		LogUltility.log(test, logger, "Check that the text editor is left to right");
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("style");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		boolean doesExist = InnerText.contains("ltr");
		assertTrue(doesExist);
		
		// Select a Voice that is hebrew
		// Set language to Hebrew
		 CurrentPage.As(HomePage.class).isHomepageReady();
		 CurrentPage.As(HomePage.class).select_language("עברית");
		 LogUltility.log(test, logger, "Set Language to עברית ");
		 
		 // Change for DB check
		 Setting.Language = "10";
		 
		// Choose random domain
//		 randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		 
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		CurrentPage.As(OpenPopup.class).openFileWait();
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		LogUltility.log(test, logger, "Select the Template radio button");
		
	    // Choose random Template and click open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		//CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected file: " + chosenTemplate);
		// Click Open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
//		String randomVoiceHebrew = CurrentPage.As(HomePage.class).chooseRandomVoiceNotEnglish();
//		LogUltility.log(test, logger, "select random domain: " + randomVoiceHebrew);
		
		// Check that the text editor is left to right
		CurrentPage = GetInstance(HomePage.class);
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Check that the text editor is left to right");
		InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("style");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		doesExist = InnerText.contains("rtl");
		
		assertTrue(doesExist);
		
		 // Restore the original value
		 Setting.Language = temp;
		
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-173:Verify the Play button become disable when the confirm message displayed
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA173_Verify_the_Play_button_become_disable_when_the_confirm_message_displayed() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-173", "Verify the Play button become disable when the confirm message displayed");
		
		List<String> twoDomains = CurrentPage.As(HomePage.class).getTwoDomainsDifferentGesturesFromDB(con);
		
		// check if two domains with different gestures found
		if (twoDomains.get(0).equals("nothing")) {
			LogUltility.log(test, logger, "Stopping this test, Could not find a gesture in a domain"
					+ "that is not included in another domian");
			assertTrue(false);
		}
		
		String firstDomain = twoDomains.get(0);
		String secondDomain = twoDomains.get(1);
		String gestureToUse = twoDomains.get(2);
		LogUltility.log(test, logger, "First domain: " + firstDomain + 
				", Second domain: " + secondDomain +
				", Gesture to use: " + gestureToUse);
		
		// Change the voice
		Thread.sleep(1000);
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select the first domain
//		CurrentPage.As(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, firstDomain);
		CurrentPage.As(HomePage.class).select_Domain(firstDomain);
		LogUltility.log(test, logger, "selected first domain: " + firstDomain);
		
		// Choose random gesture
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		// Enter text in the text box
//		String randomText = generateInput.RandomString(40, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		//Thread.sleep(4000);
		CurrentPage.As(HomePage.class).btnGesture.click();
		CurrentPage.As(HomePage.class).SelectFromGestures(gestureToUse);
		LogUltility.log(test, logger, "Chosen Gesture: " + gestureToUse);
		
		// Do check that the play button is disabled
		//------------------------------------------
		// Click the play button
		CurrentPage.As(HomePage.class).btnplay.click();

		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
		Thread.sleep(1000);
		//Click the play button and Check that there is now that sound is pausing and there is a play icon
		CurrentPage.As(HomePage.class).btnplay.click();
		//Thread.sleep(1000);
		boolean buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPlay.isDisplayed();
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
		
		// Select the second domain
//		CurrentPage.As(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, secondDomain);
		CurrentPage.As(HomePage.class).select_Domain(secondDomain);
		LogUltility.log(test, logger, "selected second domain: " + secondDomain);	
				
		// Click the play button again and Check that we still have the pausing button
		Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		//Thread.sleep(1000);
		buttonStatus = CurrentPage.As(HomePage.class).PlayButton2.isDisplayed();
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-169:LAST WORD Verify user can edit and change any IPA value in the Say-as Popup
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA169_LAST_WORD_Verify_user_can_edit_and_change_any_IPA_value_in_the_Say_as_Popup() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-169", "LAST WORD Verify user can edit and change any IPA value in the Say-as Popup");
		
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//		        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);
//			    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = CurrentPage.As(Say_As_Popup.class).RandomString(5)
//	    		+ " " + CurrentPage.As(Say_As_Popup.class).RandomString(5)
//	    		+ " " + CurrentPage.As(Say_As_Popup.class).RandomString(5);
//		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
//		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
//	    String randomItalicText1 = generateInput.RandomString(1, "words");
	    String randomItalicText1 = RandomText.getSentencesFromFile();
//	    String randomItalicText2 = generateInput.RandomString(1, "words");
	    String randomItalicText2 = RandomText.getSentencesFromFile();
//	    String randomItalicText3 = generateInput.RandomString(1, "words");
	    String randomItalicText3 = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);

		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		//-------------------------
		// SECOND PART - modifying
		// Now try to modify the added IPA and check the result
        robot.mouseMove(point.getX(),point.getY());
        
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);

		// Write the say as text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.END);
		
		// highlight the last IPA word
		// FOR HEBREW changed Keys,Arrow_left to Arrow_right
		//CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		if(Setting.Language.equals("10"))
		for (int i=0; i<randomItalicText3.length(); i++)
			CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_RIGHT));
		else
			if(Setting.Language.equals("6"))
			for (int i=0; i<randomItalicText3.length(); i++)
				CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
		
//	    randomItalicText3 = generateInput.RandomString(1, "words");
		randomItalicText3 = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText3);
		//CurrentPage.As(Say_As_Popup.class).txtInput.clear();
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText3);
		
		// Click the OK button
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		String s =randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3;
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);

		LogUltility.log(test, logger, "Test Case PASSED");

	 }
	
	/**
	 * SRA-169:MIDDLE WORD Verify user can edit and change any IPA value in the Say-as Popup
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA169_MIDDLE_WORD_Verify_user_can_edit_and_change_any_IPA_value_in_the_Say_as_Popup() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-169", "MIDDLE WORD Verify user can edit and change any IPA value in the Say-as Popup");
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//		        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);
//			    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = CurrentPage.As(Say_As_Popup.class).RandomString(5)
//	    		+ " " + CurrentPage.As(Say_As_Popup.class).RandomString(5)
//	    		+ " " + CurrentPage.As(Say_As_Popup.class).RandomString(5);
//		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
//		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
//	    String randomItalicText1 = generateInput.RandomString(1, "words");
	    String randomItalicText1 = RandomText.getSentencesFromFile();
//	    String randomItalicText2 = generateInput.RandomString(1, "words");
	    String randomItalicText2 = RandomText.getSentencesFromFile();
//	    String randomItalicText3 = generateInput.RandomString(1, "words");
	    String randomItalicText3 = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);

		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		//-------------------------
		// SECOND PART - modifying
		// Now try to modify the added IPA and check the result
        robot.mouseMove(point.getX(),point.getY());
        
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);

		// Write the say as text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.END);
		
		//*********************
		// FOR HEBREW TESTING replaced ARROW_LEFT with ARROW_RIGHT
		// bypass the last word
		if(Setting.Language.equals("10"))
		for (int i=0; i<randomItalicText3.length()+1; i++)
			CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.ARROW_RIGHT);
		else
			if(Setting.Language.equals("6"))
				for (int i=0; i<randomItalicText3.length()+1; i++)
					CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.ARROW_LEFT);
		
		// highlight the middle IPA word
		//CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		if(Setting.Language.equals("10"))
		for (int i=0; i<randomItalicText2.length(); i++)
			CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_RIGHT));
		else
			if(Setting.Language.equals("6"))
			for (int i=0; i<randomItalicText2.length(); i++)
				CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
		
//	    randomItalicText2 = generateInput.RandomString(1, "words");
		randomItalicText2 = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText2);
		//CurrentPage.As(Say_As_Popup.class).txtInput.clear();
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText2);
		
		// Click the OK button
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);

		LogUltility.log(test, logger, "Test Case PASSED");

	 }
	
	/**
	 * SRA-169:FIRST WORD Verify user can edit and change any IPA value in the Say-as Popup
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA169_FIRST_WORD_Verify_user_can_edit_and_change_any_IPA_value_in_the_Say_as_Popup() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-169", "FIRST WORD Verify user can edit and change any IPA value in the Say-as Popup");
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(1, "words");
	    String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//		        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);
//			    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = CurrentPage.As(Say_As_Popup.class).RandomString(5)
//	    		+ " " + CurrentPage.As(Say_As_Popup.class).RandomString(5)
//	    		+ " " + CurrentPage.As(Say_As_Popup.class).RandomString(5);
//		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
//		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
//	    String randomItalicText1 = generateInput.RandomString(1, "words");
	    String randomItalicText1 = RandomText.getSentencesFromFile();
//	    String randomItalicText2 = generateInput.RandomString(1, "words");
	    String randomItalicText2 = RandomText.getSentencesFromFile();
//	    String randomItalicText3 = generateInput.RandomString(1, "words");
	    String randomItalicText3 = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);

		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		String allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);
		
		//-------------------------
		// SECOND PART - modifying
		// Now try to modify the added IPA and check the result
        robot.mouseMove(point.getX(),point.getY());
        
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
		robot.mouseMove(point.getX()+60,point.getY()+105);

		// Write the say as text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.END);
		
		//***************************************
		// FOR HEBREW TESTING REPLACED ARROW_LEFT WITH ARROW_RIGHT
		// bypass the last word
		if(Setting.Language.equals("10"))
		for (int i=0; i<randomItalicText2.length()+randomItalicText3.length()+2; i++)
			CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.ARROW_RIGHT);
		else
			if(Setting.Language.equals("6"))
				for (int i=0; i<randomItalicText2.length()+randomItalicText3.length()+2; i++)
					CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.ARROW_LEFT);
		
		// highlight the middle IPA word
		//CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		if(Setting.Language.equals("10"))
		for (int i=0; i<randomItalicText1.length(); i++)
			CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_RIGHT));
		else
			if(Setting.Language.equals("6"))
				for (int i=0; i<randomItalicText1.length(); i++)
					CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
		
//	    randomItalicText1 = generateInput.RandomString(1, "words");
	    randomItalicText1 = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText1);
		//CurrentPage.As(Say_As_Popup.class).txtInput.clear();
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText1);
		
		// Click the OK button
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Check if the entered IPA text do exist as italic in the text editor
		CurrentPage= GetInstance(HomePage.class);
		allText = CurrentPage.As(HomePage.class).EnteredText.getText();
		getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText1 + " " + randomItalicText2 + " " + randomItalicText3);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		exist = allText.contains(randomAllText);
		LogUltility.log(test, logger, "Main entered text exist: " + exist);
		assertTrue(exist);

		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-133:Check playing a sentence that contains gesture and marked with mood
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class) 
	public void SRA133_Check_playing_a_sentence_that_contains_gesture_and_marked_with_mood() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-133", "Check playing a sentence that contains gesture and marked with mood");
		
		// Select a domain
//		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();		
		String chosenVoice = CurrentPage.As(HomePage.class).getVoiceWithMoods(con);	
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Select a Domain
//		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
//		Random random = new Random();
//		String chosenDomain = domains.get(random.nextInt(domains.size()));
		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
//		GetInstance(HomePage.class).cmbDomain.click();
//		CurrentPage.As(HomePage.class).select_Domain(chosenDomain);
		CurrentPage.As(HomePage.class).select_voice(chosenDomain);
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);
//		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);
		
		// Type in the textbox
		//Thread.sleep(5000);
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Choose random gesture
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
		//Thread.sleep(4000);
		//CurrentPage.As(HomePage.class).btnGesture.click();
		String chosenGesture = CurrentPage.As(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Chosen Gesture: " + chosenGesture);
		
		// Get the text from the text input
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Get the text from the text editor");
		CurrentPage = GetInstance(HomePage.class);
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		
		
		// Mood add
		// Mark the text in the text area
		  LogUltility.log(test, logger, "highlight the text in the textbox");
		  CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		  
		  Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
	        Robot robot = new Robot();
	        robot.mouseMove(point.getX(),point.getY());
		  
		// press the mood button and select one of the moods 
	      CurrentPage.As(HomePage.class).isHomepageReady();
		  String choice = CurrentPage.As(HomePage.class).chooseRandomMood();
		  LogUltility.log(test, logger, "Chosen Mood: " + choice);
		  //Thread.sleep(5000);data-mood
		  
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				
		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		  
		  
		// Verify that mood was selected for the highlighted text
		  String MoodType= CurrentPage.As(HomePage.class).txtMood.getAttribute("data-mood");
		  String MoodTypeName = CurrentPage.As(HomePage.class).convertIDtoMood(con, MoodType);
		  LogUltility.log(test, logger, "MoodType: " + MoodType + ", MoodTypeName: " + MoodTypeName);
		  assertEquals(MoodTypeName, choice);

		LogUltility.log(test, logger, "Verify the text contains the gesture");
		
		boolean containgesture= InnerText.contains(chosenGesture);
		LogUltility.log(test, logger, "text editor contain containgesture1 should be True :" + containgesture);
		assertTrue(containgesture);		

		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-183:Pressing clear button after applying "say as" shouldn’t remove the word from text editor – bug 914
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA183_Pressing_clear_button_after_applying_say_as_shouldn_t_removethe_word_from_text_editor_bug_914() throws InterruptedException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-183", "Pressing clear button after applying \"say as\" shouldn’t remove the word from text editor – bug 914");
	
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
					
		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(1, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Get the text from the text editor and check that the same text still exist
		String getText = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean isEmpty = getText.isEmpty();
		LogUltility.log(test, logger, "Is the text editor empty: " + isEmpty);
		assertFalse(isEmpty);
		
		// Click the clear text button
		LogUltility.log(test, logger, "Click the clear text button");
		CurrentPage.As(HomePage.class).clearText.click();
		
		// Get the text from the text editor and check that the same text still exist
		getText = CurrentPage.As(HomePage.class).Textinput.getText();
		isEmpty = getText.isEmpty();
		LogUltility.log(test, logger, "Is the text editor empty: " + isEmpty);
		assertTrue(isEmpty);

		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-117:Add tags to copy paste text
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA117_Add_tags_to_copy_paste_text() throws InterruptedException, SQLException, AWTException, IOException, UnsupportedFlavorException
	{
		test = extent.createTest("TextEditorTests- SRA-117", "Add tags to copy paste text");
		
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMood(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Set path
		String path = null;
		if (Setting.Language.equals("6"))
				path = System.getProperty("user.dir") + "\\htmlDemoPageEN.html";
		else if (Setting.Language.equals("10"))
				path = System.getProperty("user.dir") + "\\htmlDemoPageHE.html";

		// Open new window
		((JavascriptExecutor)DriverContext._Driver).executeScript("window.open();");
		
		// Focus on last opened window
		for (String handle : DriverContext._Driver.getWindowHandles()) {
			DriverContext._Driver.switchTo().window(handle);
			}
		System.out.println("path: " + path);
		// Open local HTML page
		DriverContext.browser.GoToUrl(path);
		LogUltility.log(test, logger, "Open local HTML page");
		
		// Copy text
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyPress(KeyEvent.VK_C);
		robot.keyRelease(KeyEvent.VK_C);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		LogUltility.log(test, logger, "Copy ALL text");
		
		// Focus on first opened window
		ArrayList<String> tabs = new ArrayList<String> (DriverContext._Driver.getWindowHandles());
		DriverContext._Driver.switchTo().window(tabs.get(0));
		LogUltility.log(test, logger, "Focus on the first tab");
		
		// Get data stored in the clipboard as string
		Clipboard c=Toolkit.getDefaultToolkit().getSystemClipboard();
        String clipboardData = (String) c.getData(DataFlavor.stringFlavor);
        LogUltility.log(test, logger, "Retrieve the copied sentence from the page: "+clipboardData);
	        
		// Insert text to text area
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(clipboardData);
		LogUltility.log(test, logger, "Insert the copied text to the text area");
		
		// Highlight all text
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
	
		// Choose mood for the highlighted text
		CurrentPage.As(HomePage.class).chooseRandomMood();
		LogUltility.log(test, logger, "Choose mood");
		
		// Highlight all text
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// emphasize the highlighted text
		CurrentPage.As(HomePage.class).Boldbtn.click();
		LogUltility.log(test, logger, "Click the bold button");
		
		// Click the save button
		CurrentPage.As(HomePage.class).SaveButton.click();
		LogUltility.log(test, logger, "Click the save button");
		
		// Insert random file name
		CurrentPage= GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		LogUltility.log(test, logger, "Insert file name: "+fileName);
		
		// Click save button
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		LogUltility.log(test, logger, "Click save button");
		
		// Refresh the page
		DriverContext._Driver.navigate().refresh();
		LogUltility.log(test, logger, "Refresh the page");
		
		// Select a domain
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select domain again: " + randomDomain);
		
		// Click open button
		CurrentPage.As(HomePage.class).Openbtn.click();
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).openFileWait();
//		CurrentPage.As(OpenPopup.class).btnOpen.click();
	    LogUltility.log(test, logger, "Click the open button");
	    
	    // Select the saved file
	    Thread.sleep(1000);
//	    CurrentPage.As(OpenPopup.class).cmbText.click();
	    CurrentPage.As(OpenPopup.class).SelectFromFileDropdown(fileName);
	    CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
	    LogUltility.log(test, logger, "Select the saved file: "+fileName);
	    
	    // Check if the saved file is both with mood and emphasized
	    CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
	    String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);
		
		boolean isBold = source.contains("data-emphasis=");
		LogUltility.log(test, logger, "Is text emphasized: " + isBold);
		assertTrue(isBold);
		
		boolean isMood = source.contains("data-mood=");
		LogUltility.log(test, logger, "Mood applied to text: " + isMood);
		assertTrue(isMood);
	   
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-187:Say-as - Verify that Pronunciation symbols doesn`t appear when adding text and symbols
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA187_Say_as_Verify_that_Pronunciation_symbols_doesnt_appear_when_adding_text_and_symbols() throws InterruptedException, SQLException, FileNotFoundException, AWTException 
	{
		test = extent.createTest("TextEditorTests-SRA-187", "Say-as - Verify that Pronunciation symbols doesn`t appear when adding text and symbols then open the popup again bug 655");
	
		// Select a voice
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Generate random text
//		String text = generateInput.RandomString(10, "words");
		String text = RandomText.getSentencesFromFile();
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		LogUltility.log(test, logger, "Insert text to the text area");
		
		// Highlight all text
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
	
		// Click Say-as button
		CurrentPage.As(HomePage.class).Italicbtn.click();
		LogUltility.log(test, logger, "Click Say-As button");
		
		// Insert Say-As text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(2, "words");
	    String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
	
		// Click ok
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Press Say-as button again
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).Italicbtn.click();
		LogUltility.log(test, logger, "Click Say-As button");
		
		// Select pronunciation symbols radio button
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		CurrentPage.As(Say_As_Popup.class).rdoPunctiation.click();
		LogUltility.log(test, logger, "Click Punctiation Symbol radio button");
		
		// Add random vowels and consonants
		Random random = new Random();
		int len = random.nextInt(5);
		for(int i=0;i<len;i++)
		{
			CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
			if(i%2==1)
				CurrentPage.As(Say_As_Popup.class).chooseRandomVowel();
			else
				CurrentPage.As(Say_As_Popup.class).chooseRandomConsonant();
		}
		
		// Say-As text with symbols
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		String italicTextWithSymbols = CurrentPage.As(Say_As_Popup.class).txtInput.getAttribute("value");
		LogUltility.log(test, logger, "Say-as text with symbols before re-opening: "+italicTextWithSymbols);
			
	    // Scroll down to the ok button
		Point place = CurrentPage.As(Say_As_Popup.class).btnOK.getLocation();
		JavascriptExecutor jse = (JavascriptExecutor)DriverContext._Driver;
		jse.executeScript("window.scrollBy("+place.getX()+","+place.getY()+")", "");
		
		// Click ok
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Press Say-as button again
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		CurrentPage.As(HomePage.class).Italicbtn.click();
		LogUltility.log(test, logger, "Click Say-As button");
		
		// Check that same text still exists
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		String italicTextWithSymbolsAfterReopening = CurrentPage.As(Say_As_Popup.class).txtInput.getAttribute("value");
		LogUltility.log(test, logger, "Say-as text with symbols after re-opening: "+italicTextWithSymbolsAfterReopening);
		boolean isSame = italicTextWithSymbolsAfterReopening.equals(italicTextWithSymbols);
		LogUltility.log(test, logger, "text didn't change: "+isSame);
		assertTrue(isSame);
		
		 LogUltility.log(test, logger, "Test Case PASSED");
	 }	
	
	/**
	 * SRA-188:Verify that bars doesnâ€™t change first when applying say-as then change bars for first word â€“ bug 646 -part1 change bar
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA188_Verify_that_bars_doesnt_change_first_when_applying_say_as_then_change_bars_for_first_word_part1_change_bar() throws InterruptedException, SQLException, FileNotFoundException, AWTException 
	{
		test = extent.createTest("TextEditorTests-SRA-188", "Verify that bars doesnâ€™t change first when applying say-as then change bars for first word â€“ bug 646-part1 change bar");
	
		// Select a voice
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Generate random text
//		String text = generateInput.RandomString(10, "words");
		String text = RandomText.getSentencesFromFile();
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		LogUltility.log(test, logger, "Insert text to the text area");
		
		//**************
		// change for hebrew -->ARROW_RIGHT instead of ARROW_LEFT
		// Highlight last word
		if(Setting.Language.equals("10"))
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT, Keys.ARROW_RIGHT));
		else
			if(Setting.Language.equals("6"))
				CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT, Keys.ARROW_LEFT));
		
		Robot robot = new Robot();
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
		robot.mouseMove(point.getX(),point.getY());
	    robot.mouseMove(point.getX()-10,point.getY()-10);
	    
		// change the volume range bar
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
		
		// Get the volume value
		String data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();
		LogUltility.log(test, logger, "Get the volume value after changing of the highlighted word: "+data_volume);
		
		// Click on the default text
		CurrentPage.As(HomePage.class).Textinput.click();
		String default_data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();
		LogUltility.log(test, logger, "Get the volume value of default text: "+default_data_volume);
		
		boolean areDifferent = !default_data_volume.equals(data_volume);
		LogUltility.log(test, logger, "Changed volume values only for the highlighted text: "+areDifferent);
		assertTrue(areDifferent);
		
		 LogUltility.log(test, logger, "Test Case PASSED");
	 }	
	
	
	/**
	 * SRA-188:Verify that bars doesnâ€™t change first when applying say-as then change bars for first word â€“ bug 646 -part2 emphasis
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA188_Verify_that_bars_doesnt_change_first_when_applying_say_as_then_change_bars_for_first_word_part2_emphasis() throws InterruptedException, SQLException, FileNotFoundException, AWTException 
	{
		test = extent.createTest("TextEditorTests-SRA-188", "Verify that bars doesnâ€™t change first when applying say-as then change bars for first word â€“ bug 646-part2 emphasis");
	
		// Select a voice
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Generate random text
//		String text = generateInput.RandomString(10, "words");
		String text = RandomText.getSentencesFromFile();
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		LogUltility.log(test, logger, "Insert text to the text area");
		
		//**************
		// change for hebrew -->ARROW_RIGHT instead of ARROW_LEFT
		// Highlight last word
		if(Setting.Language.equals("10"))
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT, Keys.ARROW_RIGHT));
		else
			if(Setting.Language.equals("6"))
				CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT, Keys.ARROW_LEFT));
		// Move mouse
		Robot robot = new Robot();
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
		robot.mouseMove(point.getX(),point.getY());
	    robot.mouseMove(point.getX()-10,point.getY()-10);
	    
		// Emphasize the highlighted text
		CurrentPage.As(HomePage.class).Boldbtn.click();
		LogUltility.log(test, logger, "Click the bold button");
		
		// Get source data of the highlighted word
	    String highlightedSource = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor of highlighted word: " + highlightedSource);
		
		boolean isBold = highlightedSource.contains("data-emphasis=");
		LogUltility.log(test, logger, "Highlighted text emphasized: " + isBold);
		assertTrue(isBold);
		
		// Get the emphasized text
		String emphisizedText = CurrentPage.As(HomePage.class).txtSpan.getText();
		 LogUltility.log(test, logger, "Emphisized text: "+emphisizedText);
		 
		 // Check that not all text got emphasized
		 boolean onlyHighlighted =	!(emphisizedText.length() == text.length()) ;
		 LogUltility.log(test, logger, "Only highlighted text got emphasized: "+onlyHighlighted);
		 assertTrue(onlyHighlighted);
		 
		 LogUltility.log(test, logger, "Test Case PASSED");
	 }	
	
	
	/**
	 * SRA-188:Verify that bars doesnâ€™t change first when applying say-as then change bars for first word â€“ bug 646 -part3 Say-as
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA188_Verify_that_bars_doesnt_change_first_when_applying_say_as_then_change_bars_for_first_word_part3_SayAs() throws InterruptedException, SQLException, FileNotFoundException, AWTException 
	{
		test = extent.createTest("TextEditorTests-SRA-188", "Verify that bars doesnâ€™t change first when applying say-as then change bars for first word â€“ bug 646-part3 Say-as");
	
		// Select a voice
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
//		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Generate random text
//		String text = generateInput.RandomString(10, "words");
		String text = RandomText.getSentencesFromFile();
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		LogUltility.log(test, logger, "Insert text to the text area");
		
		//**************
		// change for hebrew -->ARROW_RIGHT instead of ARROW_LEFT
		// Highlight last word
		if(Setting.Language.equals("10"))
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT, Keys.ARROW_RIGHT));
		else
			if(Setting.Language.equals("6"))
				CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT, Keys.ARROW_LEFT));
		
		// Move mouse
		Robot robot = new Robot();
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
		robot.mouseMove(point.getX(),point.getY());
	    robot.mouseMove(point.getX()-10,point.getY()-10);
	    
		// Click Say-As for highlighted text
		CurrentPage.As(HomePage.class).Italicbtn.click();
		LogUltility.log(test, logger, "Click the Italic/Say-As button");
		
		// Insert Say-As text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(2, "words");
		String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		// Click ok
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Get source data of the highlighted word
		CurrentPage = GetInstance(HomePage.class);
	    String highlightedSource = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor of highlighted word: " + highlightedSource);
		
		boolean isItalic = highlightedSource.contains("data-sub=");
		LogUltility.log(test, logger, "Highlighted text got subtituted: " + isItalic);
		assertTrue(isItalic);
		
		// Get the highlighted text
		String italicText = CurrentPage.As(HomePage.class).txtSpan.getText();
		LogUltility.log(test, logger, "Italic text: "+italicText);
		 
		 // Check that not all text got emphasized
		 boolean onlyHighlighted =	!italicText.contains(text) ;
		 LogUltility.log(test, logger, "Only highlighted text subtituted: "+onlyHighlighted);
		 assertTrue(onlyHighlighted);
		 
		 LogUltility.log(test, logger, "Test Case PASSED");
	 }	
	
	/**
	 * SRA-192:Say-as â€“ Verify that no empty field appear for words with IPA input and bars values - bug 630
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA192_Say_as_Verify_that_no_empty_field_appear_for_words_with_IPA_input_and_bars_values() throws InterruptedException, SQLException, AWTException, IOException, UnsupportedFlavorException
	{	
		test = extent.createTest("TextEditorTests- SRA-192", "Say-as â€“ Verify that no empty field appear for words with IPA input and bars values - bug 630");
		
//		// Select a voice
//		CurrentPage = GetInstance(HomePage.class);
//		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
	        
		// Generate random text
//		String text = generateInput.RandomString(10, "words");
		String text = RandomText.getSentencesFromFile();
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		LogUltility.log(test, logger, "Insert text to the text area");
	
		// Highlight last word
		//CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT, Keys.ARROW_LEFT));
		 CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		 
		Robot robot = new Robot();
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
		robot.mouseMove(point.getX(),point.getY());
	    robot.mouseMove(point.getX()-10,point.getY()-10);
	    
		// change the volume range bar
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
	
		// Click Say-As for highlighted text
		CurrentPage.As(HomePage.class).Italicbtn.click();
		LogUltility.log(test, logger, "Click the Italic/Say-As button");
		
		// Insert Say-As text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(2, "words");
	    String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "insert Say-As text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		// Click ok
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
		// Click the save button
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).SaveButton.click();
		LogUltility.log(test, logger, "Click the save button");
		
		// Insert random file name
		CurrentPage= GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		LogUltility.log(test, logger, "Insert file name: "+fileName);
		
		// Click save button
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		LogUltility.log(test, logger, "Click save button");
		
		// Click clear text button
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).clearText.click();
		LogUltility.log(test, logger, "Click clear text button");
		
		// Click "X" button
//		CurrentPage.As(HomePage.class).xButton.click();
//		LogUltility.log(test, logger, "Click X button");
		
		// Click open button
//		CurrentPage.As(HomePage.class).Openbtn.click();
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		CurrentPage.As(OpenPopup.class).openFileWait();
	    LogUltility.log(test, logger, "Click the open button");
	    
	    // Select the saved file
	    CurrentPage.As(OpenPopup.class).cmbText.click();
	    CurrentPage.As(OpenPopup.class).SelectFromFileDropdown(fileName);
	    CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
	    LogUltility.log(test, logger, "Select the saved file");
	    
		// Click on the input text 
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).Textinput.click();
		
		// Click on the modified Say-As text
		CurrentPage.As(HomePage.class).txtSpan.click();
		LogUltility.log(test, logger, "Click on the modified Say-As text");
		
		// Click Say-As for highlighted text
		CurrentPage.As(HomePage.class).Italicbtn.click();
		LogUltility.log(test, logger, "Click the Italic/Say-As button");
		
		// Get Say-As text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
	    String getItalicTextAfterOpen = CurrentPage.As(Say_As_Popup.class).txtInput.getAttribute("value");
	    LogUltility.log(test, logger, "Get Say-As text after re-opening: " + getItalicTextAfterOpen);
	    // Click ok
	 	CurrentPage.As(Say_As_Popup.class).btnOK.click();
	 		
	    boolean isSame = getItalicTextAfterOpen.equals(randomItalicText);
	    LogUltility.log(test, logger, "Say-As was applied successfully: "+isSame);
	    assertTrue(isSame);
	    
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-194:Verify that user Can erase or make changes for the first word in each row - 617
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA194_Verify_that_user_Can_erase_or_make_changes_for_the_first_word_in_each_row() throws InterruptedException, SQLException, AWTException, IOException, UnsupportedFlavorException
	{	
		test = extent.createTest("TextEditorTests- SRA-194", "Verify that user Can erase or make changes for the first word in each row - 617");
		
//		// Select a voice
//		CurrentPage = GetInstance(HomePage.class);
//		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select a Domain
//		List<String> domains =
//		Random random = new Random();
		String randomDomain =  CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
	    
		Robot robot = new Robot();
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
		
		for(int i=0;i<4;i++)
		{
//			String text = generateInput.RandomString(1, "words");
			String text = RandomText.getSentencesFromFile();
			text = text.split(" ",2)[0];
			CurrentPage.As(HomePage.class).Textinput.click();
			CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
			LogUltility.log(test, logger, "Insert text: " + text);
			// Highlight word
			//CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT, Keys.ARROW_LEFT));
			 CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
				LogUltility.log(test, logger, "Highlight all");
			 
			// For sync
			robot.mouseMove(point.getX(),point.getY());
		    robot.mouseMove(point.getX()-10,point.getY()-10);
		    
		    switch(i) {
		    case 1:
				// Emphasize the highlighted text
		    	CurrentPage.As(HomePage.class).isHomepageReady();
				CurrentPage.As(HomePage.class).Boldbtn.click();
				LogUltility.log(test, logger, "Click the bold button");
				break;
		    case 2:
		    	// change the volume range bar
		    	CurrentPage.As(HomePage.class).isHomepageReady();
				CurrentPage.As(HomePage.class).hsbVolume.click();
				for (int j=0; j<100; j++)
					CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
				break;
		    case 3:
				// Choose mood for the highlighted text
		    	CurrentPage.As(HomePage.class).isHomepageReady();
				CurrentPage.As(HomePage.class).chooseRandomMood();
				LogUltility.log(test, logger, "Choose mood");
				robot.mouseMove(point.getX(),point.getY());
			    robot.mouseMove(point.getX()-10,point.getY()-100);
			    robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			    robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
			    break;
		    case 0:
		    	// Click Say-As for highlighted text
		    	CurrentPage.As(HomePage.class).isHomepageReady();
				CurrentPage.As(HomePage.class).Italicbtn.click();
				LogUltility.log(test, logger, "Click the Italic/Say-As button");
				// Insert Say-As text
				CurrentPage = GetInstance(Say_As_Popup.class);
				CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//			    String randomItalicText = generateInput.RandomString(2, "words");
				String randomItalicText = RandomText.getSentencesFromFile();
				randomItalicText = randomItalicText.split(" ",2)[0];
				LogUltility.log(test, logger, "insert Say-As text to the text area: " + randomItalicText);
				CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
				// Click ok
				CurrentPage.As(Say_As_Popup.class).btnOK.click();
				robot.mouseMove(point.getX(),point.getY());
			    robot.mouseMove(point.getX()-10,point.getY()-100);
			    robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			    robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
		    	break;
		    }
		    
		    if(i<3)
		    {
			    // change for hebrew --> to left
			    CurrentPage = GetInstance(HomePage.class);
			    if(Setting.Language.equals("10"))
				CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.ARROW_RIGHT,Keys.SPACE));
			    else
			    	if(Setting.Language.equals("6"))
			    		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.ARROW_LEFT,Keys.SPACE));
	//			text = generateInput.RandomString(2, "words");
				text = RandomText.getSentencesFromFile();
				text = text.split(" ",2)[0];
				CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
				// New row
				CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord("\n"));
		    }
		}
		
	    // Change to default mood
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).spanTC194.click();
		robot.mouseMove(point.getX(),point.getY());
	    robot.mouseMove(point.getX()-10,point.getY()-10);
	    // Click "X" button
//	    CurrentPage.As(HomePage.class).isHomepageReady();
	 	CurrentPage.As(HomePage.class).xButtonMood.click();
	 	LogUltility.log(test, logger, "Click Mood X button");
	 	
		// change the volume range bar to default
		CurrentPage.As(HomePage.class).spanTC194.click();
		robot.mouseMove(point.getX(),point.getY());
	    robot.mouseMove(point.getX()-10,point.getY()-10);
	    // Click "X" button
	 	CurrentPage.As(HomePage.class).xButtonVolume.click();
	 	LogUltility.log(test, logger, "Click Volume X button");
	 	
		// Check effects can be re-edited
		// deemphasize the text
//		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).spanTC194.click();
		robot.mouseMove(point.getX(),point.getY());
	    robot.mouseMove(point.getX()-10,point.getY()-10);
	    CurrentPage.As(HomePage.class).Boldbtn.click();
		LogUltility.log(test, logger, "Click the bold button to deemphasize");
		
	    // Remove IPA value
  		CurrentPage.As(HomePage.class).isHomepageReady();
  		CurrentPage.As(HomePage.class).spanTC194.click();
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
  		CurrentPage.As(HomePage.class).xButtonIPA.click();
  		LogUltility.log(test, logger, "Click IPA X button");
	    
		// Check that source doesn't contain IPA/Mood/Emphasize/Changed bar value
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		String htmlSource = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		
		boolean allReset = !(htmlSource.contains("data-emphasis") && htmlSource.contains("data-volume") && htmlSource.contains("data-mood") && htmlSource.contains("data-sub"));
		LogUltility.log(test, logger, "Reset everything successfully: "+allReset);
		assertTrue(allReset);
		
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-193:Say-as- Verify that popup opens for IPA input in the saved files- 629
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA193_Say_as_Verify_that_popup_opens_for_IPA_input_in_the_saved_files_629() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-193", "Say-as- Verify that popup opens for IPA input in the saved files- 629");
		
		DriverContext.browser.Maximize();
		
		// Select a Domain
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		GetInstance(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(5000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
	    
     // Send the mouse cursor to the end 
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
        
     // Highlight  the last word
        if(Setting.Language.equals("6"))
        	CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.LEFT));
        else
        	if(Setting.Language.equals("10"))
        		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.RIGHT));
        

		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
        robot.mouseMove(point.getX()+60,point.getY()+105);
        //robot.mouseMove(point.getX()+60,point.getY()+108);
//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(1, "words");
		String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		LogUltility.log(test, logger, "Click the ok button ");
		
		// Save the text as file
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).SaveButton.click();
		LogUltility.log(test, logger, "Press the save button");
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ",2)[0];
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Clear the text box editor
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).clearText.click();
		LogUltility.log(test, logger, "Clear text area ");
		
	    // Click the open button and open the saved file
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage.As(HomePage.class).Openbtn.click();
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).openFileWait();
		
		// Click Open
	    CurrentPage.As(OpenPopup.class).SelectFromFileDropdown(fileName);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "File text displayed");

		CurrentPage = GetInstance(HomePage.class);
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute exist: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(randomItalicText);
		LogUltility.log(test, logger, "getText exist: " + exist);
		assertTrue(exist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-206:Check that the mood label displayed as “Tone / Mood”
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA206_Check_that_the_mood_label_displayed_as_Tone_Mood() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-206", "Check that the mood label displayed as “Tone / Mood”");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);
		
		homePage.isHomepageReady();
		
		// Get tone/mood button name
		String actual = homePage.btnMood.getText().trim();
		String expected = "Tone/Mood";
		LogUltility.log(test, logger, "Tone/Mood text: "+actual);
		
		// Check if its written as expected
		boolean same = actual.equals(expected);
		LogUltility.log(test, logger, "Written as expected: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 * SRA-207:Verify that resetting the volume bar values works successfully
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA207_Verify_that_resetting_the_volume_bar_values_works_successfully() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-207", "Verify that resetting the volume bar values works successfully");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);	
		homePage.isHomepageReady();
		
		// Insert text to the text area
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		homePage.Textinput.click();
		homePage.Textinput.sendKeys(randomAllText);
		
		// Highlight all text
		homePage.Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
		
		// Get the default volume bar value before changing
		String default_volume = homePage.volumeVal.getText();
		LogUltility.log(test, logger, "Get the volume value before changing: "+default_volume);
		
		//Change the volume bar values
		homePage.hsbVolume.click();
		for (int j=0; j<50; j++)
			homePage.hsbVolume.sendKeys(Keys.RIGHT);
		LogUltility.log(test, logger, "Change the volume bar values to: "+homePage.volumeVal.getText());
		
		// Check that the remove red icon is displayed
		boolean removeButton = homePage.xButtonVolume.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		homePage.xButtonVolume.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
		
		// Check that the remove red icon is not displayed
		 removeButton = !homePage.xButtonVolume.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
		assertTrue(removeButton);
		
		// Get the value after resetting to default
		String volume_after_reset = homePage.volumeVal.getText();
		LogUltility.log(test, logger, "Get the volume value after restoring: "+volume_after_reset);
		
		// Check that values are equal
		boolean same = volume_after_reset.equals(default_volume);
		LogUltility.log(test, logger, "The default value was restored successfully: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-208:Verify that resetting the speed bar values works successfully
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA208_Verify_that_resetting_the_speed_bar_values_works_successfully() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-208", "Verify that resetting the speed bar values works successfully");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);	
		homePage.isHomepageReady();
		
		// Insert text to the text area
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		homePage.Textinput.click();
		homePage.Textinput.sendKeys(randomAllText);
		
		// Highlight all text
		homePage.Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
		
		// Get the default speed bar value before changing
		String default_speed = homePage.speedVal.getText();
		LogUltility.log(test, logger, "Get the speed value before changing: "+default_speed);
		
		//Change the speed bar values
		homePage.hsbSpeed.click();
		for (int j=0; j<50; j++)
			homePage.hsbSpeed.sendKeys(Keys.RIGHT);
		LogUltility.log(test, logger, "Change the speed bar values to: "+homePage.speedVal.getText());
		
		// Check that the remove red icon is displayed
		boolean removeButton = homePage.xButtonSpeed.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		homePage.xButtonSpeed.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
		
		// Check that the remove red icon is not displayed
		 removeButton = !homePage.xButtonSpeed.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
		assertTrue(removeButton);
		
		// Get the value after resetting to default
		String speed_after_reset = homePage.speedVal.getText();
		LogUltility.log(test, logger, "Get the speed value after restoring: "+speed_after_reset);
		
		// Check that values are equal
		boolean same = speed_after_reset.equals(default_speed);
		LogUltility.log(test, logger, "The default value was restored successfully: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-209:Verify that resetting the pitch bar values works successfully
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA209_Verify_that_resetting_the_pitch_bar_values_works_successfully() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-209", "Verify that resetting the pitch bar values works successfully");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);	
		homePage.isHomepageReady();
		
		// Insert text to the text area
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		homePage.Textinput.click();
		homePage.Textinput.sendKeys(randomAllText);
		
		// Highlight all text
		homePage.Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
		
		// Get the default pitch bar value before changing
		String default_pitch = homePage.pitchVal.getText();
		LogUltility.log(test, logger, "Get the pitch value before changing: "+default_pitch);
		
		//Change the pitch bar values
		homePage.hsbPitch.click();
		for (int j=0; j<50; j++)
			homePage.hsbPitch.sendKeys(Keys.RIGHT);
		LogUltility.log(test, logger, "Change the pitch bar values to: "+homePage.pitchVal.getText());
		
		// Check that the remove red icon is displayed
		boolean removeButton = homePage.xButtonPitch.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		homePage.xButtonPitch.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
		
		// Check that the remove red icon is not displayed
		 removeButton = !homePage.xButtonPitch.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
		assertTrue(removeButton);
		
		// Get the value after resetting to default
		String pitch_after_reset = homePage.pitchVal.getText();
		LogUltility.log(test, logger, "Get the pitch value after restoring: "+pitch_after_reset);
		
		// Check that values are equal
		boolean same = pitch_after_reset.equals(default_pitch);
		LogUltility.log(test, logger, "The default value was restored successfully: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 * SRA-210:Verify that Say-As feature can be removed successfully
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA210_Verify_that_Say_As_feature_can_be_removed_successfully() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-210", "Verify that Say-As feature can be removed successfully");
		
		DriverContext.browser.Maximize();

		// Type in the textbox
		//Thread.sleep(5000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
	    
  
		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
        
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
        robot.mouseMove(point.getX()+60,point.getY()+105);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(1, "words");
		String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		LogUltility.log(test, logger, "Click the ok button ");
		
		// Load homepage and check if its ready to use
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Get the attribute and check that there is say-as
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		LogUltility.log(test, logger, "getAttribute: " + getAttribute);
		
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "There is say-as: " + exist);
		assertTrue(exist);
	
		// Check that the remove red icon is displayed
		boolean removeButton = CurrentPage.As(HomePage.class).xButtonIPA.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after applying Say-As: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		CurrentPage.As(HomePage.class).xButtonIPA.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to remove the Say-As");
		
		// Check that the remove red icon is not displayed
		 removeButton = !CurrentPage.As(HomePage.class).xButtonIPA.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed: "+removeButton);
		assertTrue(removeButton);
		

		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 * SRA-211:Verify that emphasize can be removed successfully
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA211_Verify_that_emphasize_can_be_removed_successfully() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-211", "Verify that emphasize can be removed successfully");
		
		DriverContext.browser.Maximize();

		// Type in the textbox
		//Thread.sleep(5000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Boldbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
	    
  
		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
        
		// Click the bold button
		 CurrentPage.As(HomePage.class).Boldbtn.click();
		 LogUltility.log(test, logger, "Click bold button");
		
		// Get the html attribute and check if the text was emphasized
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-emphasis=");
		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
		assertTrue(doContain);
			
		// Check that the remove red icon is displayed
		boolean removeButton = CurrentPage.As(HomePage.class).xButtonBold.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after applying emphasize: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		CurrentPage.As(HomePage.class).xButtonBold.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to remove the emphasize");
		
		// Check that the remove red icon is not displayed
		 removeButton = !CurrentPage.As(HomePage.class).xButtonBold.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed: "+removeButton);
		assertTrue(removeButton);
		
		// Get the html attribute and check if the text was emphasized
		 source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		doContain = !source.contains("data-emphasis=");
		LogUltility.log(test, logger, "text is NOT emphasized: " + doContain);
		assertTrue(doContain);
		
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-212:Verify that mood can be removed successfully
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA212_Verify_that_mood_can_be_removed_successfully() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-212", "Verify that mood can be removed successfully");
		
		DriverContext.browser.Maximize();

		// Select a domain with moods
		CurrentPage.As(HomePage.class).isHomepageReady();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMood(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		 
		// Insert text
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
			
		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
//		Thread.sleep(1000);
		
		// Choose mood
		Actions builder = new Actions(DriverContext._Driver);
  	  	builder.moveToElement(CurrentPage.As(HomePage.class).MoodValues).perform();
		String mood = CurrentPage.As(HomePage.class).chooseRandomMood();
		LogUltility.log(test, logger, "Choose mood: "+mood);
		
		// Get the html attribute and check if the text was emphasized
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-mood=");
		LogUltility.log(test, logger, "Is text with mood: " + doContain);
		assertTrue(doContain);
		
		// Check that the remove red icon is displayed
		boolean removeButton = CurrentPage.As(HomePage.class).xButtonMood.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after applying mood: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		CurrentPage.As(HomePage.class).xButtonMood.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to remove the mood");
		
		// Check that the remove red icon is not displayed
		 removeButton = !CurrentPage.As(HomePage.class).xButtonMood.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed: "+removeButton);
		assertTrue(removeButton);
		
		// Get the html attribute and check if the text was emphasized
		 source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		doContain = !source.contains("data-mood=");
		LogUltility.log(test, logger, "text is NOT with mood: " + doContain);
		assertTrue(doContain);
				
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-213:Verify that user can’t play the “instructions text” before clicking “Clear Text”
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA213_Verify_that_user_cant_play_the_instructions_text_before_clicking_Clear_Text() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-213", "Verify that user can’t play the “instructions text” before clicking “Clear Text”");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);
		homePage.isHomepageReady();
		
		// Choose voice
		String voice = homePage.chooseRandomVoice();
		LogUltility.log(test, logger, "Choose voice: "+voice);
		
		// Press play button
		homePage.PlayButton.click();
		LogUltility.log(test, logger, "Press the play button");
		
		// Check getting the warning message
		String expectedMessage = "Please clear text and type new text     X";
		homePage.fadedPopup(logger, expectedMessage);
		
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 * SRA-214:Verify that alphabet entry in “say as” set according to the suitable language
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA214_Verify_that_alphabet_entry_in_say_as_set_according_to_the_suitable_language() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-214", "Verify that alphabet entry in “say as” set according to the suitable language");
		
		DriverContext.browser.Maximize();

		// Set language dropdown to English
		 CurrentPage.As(HomePage.class).select_language("English");
		 LogUltility.log(test, logger, "Set Language to English ");
				 
		// Type in the textbox
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());

		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
        
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
        robot.mouseMove(point.getX()+60,point.getY()+105);
		
        // Check that the say-as input is left to right
        CurrentPage = GetInstance(Say_As_Popup.class);
        LogUltility.log(test, logger, "Check that the Say-As input field is left to right");
		String innerHTML = CurrentPage.As(Say_As_Popup.class).txtInput.getAttribute("style");
		LogUltility.log(test, logger, "Get the intterHTML from say-as input field :" + innerHTML);
		boolean doesExist = innerHTML.contains("ltr");
		assertTrue(doesExist);
		
		// Close the say-as poup
		 CurrentPage.As(Say_As_Popup.class).btnCancel.click();
		 LogUltility.log(test, logger, "Close the Say-As popup ");
		 	 
		// Select Hebrew
		 CurrentPage = GetInstance(HomePage.class);
		 CurrentPage.As(HomePage.class).select_language("עברית");
		 LogUltility.log(test, logger, "Set Language to עברית ");
				 
		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
        
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
		builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
        robot.mouseMove(point.getX()+60,point.getY()+105);
	         
		// Check that the say-as input is right to left  
		  CurrentPage = GetInstance(Say_As_Popup.class);
		LogUltility.log(test, logger, "Check that the Say-As input field is right to left");
		innerHTML = CurrentPage.As(Say_As_Popup.class).txtInput.getAttribute("style");
		LogUltility.log(test, logger, "Get the intterHTML from say-as input field  :" + innerHTML);
		doesExist = innerHTML.contains("rtl");
		assertTrue(doesExist);
     		
		// Close the say-as poup
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).btnCancel.click();
		LogUltility.log(test, logger, "Close the Say-As popup ");

		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-215:Verify that alphabet entry in “say as” supports Nikud
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA215_Verify_that_alphabet_entry_in_say_as_supports_Nikud() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-215", "Verify that alphabet entry in “say as” supports Nikud");
		
		DriverContext.browser.Maximize();
			 
		// Type in the textbox
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());

		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
        
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
        robot.mouseMove(point.getX()+60,point.getY()+105);
		
       // Insert text with nikud
        String textWithNikud = "תִּשְׁלַח אֶת הַבַּקָּשָׁה בַּדֹּאַר";
        CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
	    LogUltility.log(test, logger, "Insert text with Nikud: "+textWithNikud);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(textWithNikud);
		
		// Press ok
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		LogUltility.log(test, logger, "Press Ok button in say-as popup");
		
		// Retrive and check that italic/say-as text was applied successfully
		CurrentPage = GetInstance(HomePage.class);
		String getAttribute = CurrentPage.As(HomePage.class).txtIPA.getAttribute("style");
		String getItalicText = CurrentPage.As(HomePage.class).txtIPA.getText().trim();
		
		LogUltility.log(test, logger, "getAttribute: " + getAttribute + " getText: " + getItalicText);
		boolean exist = getAttribute.equals("font-style: italic;");
		LogUltility.log(test, logger, "getAttribute - Say-as exists: " + exist);
		assertTrue(exist);
		
		exist = getItalicText.equals(textWithNikud);
		LogUltility.log(test, logger, "getText - Text is displayed as expected: " + exist);
		assertTrue(exist);

		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 * SRA-216:Verify that gestures are inserted in the correct order according to language
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA216_Verify_that_gestures_are_inserted_in_the_correct_order_according_to_language() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-216", "Check the “Verify that gestures are inserted in the correct order according to language");
		// Modified on 27-11-2018, language value decides if text goes RTL or LTR not voices

		// Choose domain with gestures
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
			
		// Insert gestures
		 CurrentPage.As(HomePage.class).Textinput.click();
		 String gesture = CurrentPage.As(HomePage.class).chooseRandomGesture();
		 LogUltility.log(test, logger, "Insert gesture: "+gesture);
		 
		// Check that the text editor is left to right in English
		 if(Setting.Language.equals("6"))
		 {
			LogUltility.log(test, logger, "Check that the text editor is left to right");
			String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("style");
			LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
			boolean doesExist = InnerText.contains("ltr");
			assertTrue(doesExist);
		 }
		else 
		 if(Setting.Language.equals("10"))
		 {
			// Check that the text editor is right to left in Hebrew
			LogUltility.log(test, logger, "Check that the text editor is left to right");
			String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("style");
			LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
			boolean doesExist = InnerText.contains("rtl");	
			assertTrue(doesExist);
		 }
		 // language not supported
		 else
			 assertTrue(false);
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-221:Verify that the remove red icon is not displayed after un-emphasizing text
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA221_Verify_that_the_remove_red_icon_is_not_displayed_after_unemphasizing_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-221", "Verify that the remove red icon is not displayed after un-emphasizing text");
		
		DriverContext.browser.Maximize();

		// Type in the textbox
		//Thread.sleep(5000);
//	    String randomAllText = generateInput.RandomString(1, "words");
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Boldbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
	    
		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
        
		// Click the bold button
		 CurrentPage.As(HomePage.class).Boldbtn.click();
		 LogUltility.log(test, logger, "Click bold button");
		
		// Get the html attribute and check if the text was emphasized
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-emphasis=");
		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
		assertTrue(doContain);
			
		// Check that the remove red icon is displayed
		boolean removeButton = CurrentPage.As(HomePage.class).xButtonBold.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after applying emphasize: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		 CurrentPage.As(HomePage.class).Boldbtn.click();
		 LogUltility.log(test, logger, "Click bold button again to emphesize");
		
		// Check that the remove red icon is not displayed
		 removeButton = !CurrentPage.As(HomePage.class).xButtonBold.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed: "+removeButton);
		assertTrue(removeButton);
		
		// Get the html attribute and check if the text was emphasized
		 source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		doContain = !source.contains("data-emphasis=");
		LogUltility.log(test, logger, "text is NOT emphasized: " + doContain);
		assertTrue(doContain);
		
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	
	/**
	 * SRA-222:Verify that user can apply mood to already mooded text
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA222_Verify_that_user_can_apply_mood_to_already_mooded_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-222", "Verify that user can apply mood to already mooded text");
		
		DriverContext.browser.Maximize();

		// Select a domain with moods
		 CurrentPage.As(HomePage.class).isHomepageReady();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMood(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Insert text
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
			
	 // Send the mouse cursor to the end 
       CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
        
     // Highlight  the last word
        if(Setting.Language.equals("6"))
        	CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.LEFT));
        else
        	if(Setting.Language.equals("10"))
        		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.RIGHT));
        
		// Choose mood
		String firstMood = CurrentPage.As(HomePage.class).chooseRandomMood();
		LogUltility.log(test, logger, "Choose mood: "+firstMood);
		
		// Get the chosen mood id
		String firstMoodId = CurrentPage.As(HomePage.class).getMoodId(con,firstMood);
        
		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
		
		// Choose mood
		 String secondMood = CurrentPage.As(HomePage.class).chooseRandomMood();
		LogUltility.log(test, logger, "Choose mood: "+secondMood);
		
		// Get the chosen mood id
		String secondMoodId = CurrentPage.As(HomePage.class).getMoodId(con,firstMood);
		
		// Get the html attribute and check if the text was emphasized
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-mood="+"\""+firstMoodId+"\"") &&source.contains("data-mood="+"\""+secondMoodId+"\"") ;
		LogUltility.log(test, logger, "Is text with two moods: " + doContain);
		assertTrue(doContain);
		
		// Check if the second mood is the one that affects the text
		String lastMood = source.split("data-mood=",2)[1];
		boolean lastMoodEffect = lastMood.contains(secondMoodId);
		LogUltility.log(test, logger, "The last mood that was added is the one that affects the text: "+lastMoodEffect);
		assertTrue(lastMoodEffect);
				
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	
	/**
	 * SRA-224:Verify that user can reopen say-as popup after applying say-as to multiple lines
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA224_Verify_that_user_can_reopen_say_as_popup_after_applying_say_as_to_multiple_lines() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-224", "Verify that user can reopen say-as popup after applying say-as to multiple lines");
		
		DriverContext.browser.Maximize();
		

		// Type in the textbox
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
	    
        // Send the mouse cursor to the end 
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
        
        
        //Press Enter to start a new line 
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.ENTER);
        LogUltility.log(test, logger, "Press Enter to start a new line");
        
        
		// Add more text
		 randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);

		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
		
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
        robot.mouseMove(point.getX()+60,point.getY()+105);
        //robot.mouseMove(point.getX()+60,point.getY()+108);
//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//	    String randomItalicText = generateInput.RandomString(1, "words");
		String randomItalicText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the say-as text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		LogUltility.log(test, logger, "Click the ok button ");
		
		
		// Click the italic button again to open the say-as popup
		LogUltility.log(test, logger, "Open the Say As window again");
		builder = new Actions(DriverContext._Driver);
		CurrentPage = GetInstance(HomePage.class);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
		
  		// Is the say-as opened 
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		String italicTextAfter = CurrentPage.As(Say_As_Popup.class).txtInput.getAttribute("value");
		LogUltility.log(test, logger, "the text in the say-as input field is: " + italicTextAfter);
		
		// Check that the entered text still exists after reopening
		boolean same = italicTextAfter.equals(randomItalicText);
		LogUltility.log(test, logger, "Same text after reopening: " + same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	
	/**
	 * SRA-228:Verify that mood isn't changed after opening template and selecting voice
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA228_Verify_that_mood_isnt_changed_after_opening_template_and_selecting_voice() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("OpenOptionsTests-SRA-228", "Verify that mood isn't changed after opening template and selecting voice");

		// Check if homepage is ready to use
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Choose domain with templates
		String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
		LogUltility.log(test, logger, "Choose domain with templates: "+randomDomain);
		
		// Choose voice
		String firstVoice = GetInstance(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Choose voice before opening template: "+firstVoice);
		
		// Get the default tone/mood value
		String moodB4Open = GetInstance(HomePage.class).moodBar.getAttribute("data-moodname");
		LogUltility.log(test, logger, "Mood before opening template: "+moodB4Open);
		
		// Click the open button
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		//Choose the Template radio button
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		LogUltility.log(test, logger, "Click template radio button");
		
	    // Choose random Template and click open
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected Template: " + chosenTemplate);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "Click the open button");
		
	 
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Choose different voice
		List<String> voices = GetInstance(HomePage.class).getVoiceValues();
		voices.remove(firstVoice);
		voices.remove("Select Voice");
		Random random = new Random();
		String secondVoice = voices.get(random.nextInt(voices.size()));
		GetInstance(HomePage.class).select_voice(secondVoice);
		LogUltility.log(test, logger, "Choose different voice: "+secondVoice);
	
		// Verify that the mood didn't change
		String moodAfterOpen = GetInstance(HomePage.class).moodBar.getAttribute("data-moodname");
		LogUltility.log(test, logger, "Mood after opening template: "+moodAfterOpen);
		boolean same = moodAfterOpen.equals(moodB4Open);
		assertTrue(same);
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-230:Verify that text area is cleared when choosing new domain while template is opened
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA230_Verify_that_text_area_is_cleared_when_choosing_new_domain_while_template_is_opened() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("TextEditorTests-SRA-230", "Verify that text area is cleared when choosing new domain while template is opened");

		// Check if homepage is ready to use
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Choose domain with templates
		String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
		LogUltility.log(test, logger, "Choose domain with templates: "+randomDomain);
		
		// Click the open button
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		//Choose the Template radio button
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		LogUltility.log(test, logger, "Click template radio button");
		
	    // Choose random Template and click open
//		CurrentPage.As(OpenPopup.class).openTemplateWait();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected Template: " + chosenTemplate);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "Click the open button");
		
	 
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Choose different domain
		List<String> domains = GetInstance(HomePage.class).getDomainValues();
		domains.remove(randomDomain);
		domains.remove("Select Domain");
		Random random = new Random();
		String differentDomain = domains.get(random.nextInt(domains.size()));
		GetInstance(HomePage.class).cmbDomain.click();
		GetInstance(HomePage.class).select_Domain(differentDomain);
		LogUltility.log(test, logger, "Choose new domain: "+differentDomain);
		
		// Verify that the text area is empty
		String textArea = GetInstance(HomePage.class).Textinput.getText();
		boolean empty = textArea.equals("");
		LogUltility.log(test, logger, "The text area is empty: "+empty);
		assertTrue(empty);
		
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-231:Verify that language dropdown isn't changed after opening template
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA231_Verify_that_language_dropdown_isnt_changed_after_opening_template() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("TextEditorTests-SRA-231", "Verify that language dropdown isn't changed after opening template");

		// Check if homepage is ready to use
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Choose domain with templates
		String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
		LogUltility.log(test, logger, "Choose domain with templates: "+randomDomain);
		
		// Get the language dropdown value before opening template
		String languageBeforeOpen = GetInstance(HomePage.class).cmbLanguage.getText();
		LogUltility.log(test, logger, "Language before opening the template: "+languageBeforeOpen);
		
		// Click the open button
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		//Choose the Template radio button
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		LogUltility.log(test, logger, "Click template radio button");
		
	    // Choose random Template and click open
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected Template: " + chosenTemplate);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "Click the open button");
		
	 
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Get the language dropdown value after opening template
		String languageAfterOpen = GetInstance(HomePage.class).cmbLanguage.getText();
		LogUltility.log(test, logger, "Language before opening the template: "+languageAfterOpen);
		
		// Check if the dropdown value didn't change
		boolean same = languageAfterOpen.equals(languageBeforeOpen);
		LogUltility.log(test, logger, "Language didn't change: "+same);
		assertTrue(same);
		
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	

	/**
	 * SRA-238:Verify that user can't apply Say-as to gestures
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA238_Verify_that_user_cant_apply_Say_as_to_gestures() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("TextEditorTests-SRA-238", "Verify that user can't apply Say-as to gestures");

		// Check if homepage is ready to use
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Choose domain with gestures
		String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "Choose domain: "+randomDomain);
		
		// Press clear text button
		GetInstance(HomePage.class).clearText.click();
		LogUltility.log(test, logger, "Press clear text");
		  
		// Insert gestures 
		GetInstance(HomePage.class).Textinput.click();
		String gesture = GetInstance(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Insert gesture: "+gesture);
		
		// Highlight all text
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight all text");
		
		// Click the italic button
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
  		// Check that the popup did NOT open
		CurrentPage = GetInstance(Say_As_Popup.class);
		boolean noPopup = !CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpenWithAnswer();
		LogUltility.log(test, logger, "Say as did NOT open " + noPopup);
		assertTrue(noPopup);
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-229:Verify that IPA/Say-as button works after copying template with editable fields
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA229_Verify_that_IPA_Say_as_button_works_after_copying_template_with_editable_fields() throws InterruptedException, SQLException, AWTException, UnsupportedFlavorException, IOException {
	
	test = extent.createTest("TextEditorTests-SRA-229", "Verify that IPA/Say-as button works after copying template with editable fields");

	String chosenTemplate = "";
	List <String> allEditTemplates = new ArrayList<String>();

	// Get domains with editable templates
	CurrentPage = GetInstance(HomePage.class); 
	HashMap<String,List<String>> domainsEditTemps = GetInstance(HomePage.class).getDomainWithEditableTemplate(con,chosenProject);
	
	// Get the name of the domains and choose one
	List<String> domains = new ArrayList<String>();
	for ( String key : domainsEditTemps.keySet() ) 
			domains.add(key);
		
	// Get app dropdown values
	 GetInstance(HomePage.class).isHomepageReady();
	List<String> smorphDomains = GetInstance(HomePage.class).getDomainValues();
	
	//*************************
	// Available domains for the user to use
	domains.retainAll(smorphDomains);

//	// Choose random domain
	Random random = new Random();
	String randomDomain = domains.get(random.nextInt(domains.size()));
//	String dropdownDomain ="";
//	// To get private domains correct name, since they are not stored with "(P)" in the DB
//	for(int i =0;i<smorphDomains.size();i++)
//		if(smorphDomains.get(i).contains(randomDomain))
//			dropdownDomain = smorphDomains.get(i);
//	
	GetInstance(HomePage.class).isHomepageReady();
	GetInstance(HomePage.class).select_Domain(randomDomain);
	LogUltility.log(test, logger, "Select random domain: " + randomDomain);
	
	// Click the open button and check for open popup
	LogUltility.log(test, logger, "Click the open button");
	CurrentPage = GetInstance(OpenPopup.class);
	CurrentPage.As(OpenPopup.class).btnOpen.click();
	
	//Choose the Template radio button
	CurrentPage = GetInstance(OpenPopup.class);
	CurrentPage.As(OpenPopup.class).templateRBtn.click();
	
    // Choose random editable Template and click open
	CurrentPage.As(OpenPopup.class).openTemplateWait();
	CurrentPage.As(OpenPopup.class).cmbTemplate.click();
	
	// Choose random editable template
	allEditTemplates = domainsEditTemps.get(randomDomain);
	random = new Random();
	chosenTemplate = allEditTemplates.get(random.nextInt(allEditTemplates.size()));
	LogUltility.log(test, logger, "Selected file: " + chosenTemplate);

	// Select from templates dropdown
	CurrentPage = GetInstance(OpenPopup.class);
	CurrentPage.As(OpenPopup.class).SelectFromtemplateDropdown(chosenTemplate);
	
	// Click Open
	CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
	LogUltility.log(test, logger, "Open template");
	
    // Get the text area position
	CurrentPage = GetInstance(HomePage.class);
	GetInstance(HomePage.class).isHomepageReady();

	Point point = CurrentPage.As(HomePage.class).Textinput.getLocation();
//	Point point = CurrentPage.As(HomePage.class).SaveButton.getLocation();
    Robot robot = new Robot();
    robot.mouseMove(point.getX()+961,point.getY()+115);
//    robot.mouseMove(point.getX(),point.getY());
    
    // Press the mouse left click and drag to highlight the text
    robot.mousePress(java.awt.event.InputEvent.BUTTON1_DOWN_MASK);
    point = CurrentPage.As(HomePage.class).btnStyle.getLocation();
//    point = CurrentPage.As(HomePage.class).cmbLanguage.getLocation();
    robot.mouseMove(point.getX(),point.getY());
    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_DOWN_MASK);

	// Copy the text
	robot.keyPress(KeyEvent.VK_CONTROL);
	robot.keyPress(KeyEvent.VK_C);
	robot.keyRelease(KeyEvent.VK_C);
	robot.keyRelease(KeyEvent.VK_CONTROL);
	LogUltility.log(test, logger, "Copy ALL text");
		
	 // Press clear text button
	 CurrentPage.As(HomePage.class).clearText.click();
	 LogUltility.log(test, logger, "Press clear text button");
	 
	// Get data stored in the clipboard as string
	Clipboard c=Toolkit.getDefaultToolkit().getSystemClipboard();
    String clipboardData = (String) c.getData(DataFlavor.stringFlavor);
    LogUltility.log(test, logger, "Retrieve the copied sentence from the page: "+clipboardData);
        
	// Insert text to text area
	CurrentPage= GetInstance(HomePage.class);
	CurrentPage.As(HomePage.class).Textinput.click();
	CurrentPage.As(HomePage.class).Textinput.sendKeys(clipboardData);
	LogUltility.log(test, logger, "Insert the copied text to the text area");
	
	// Highlight all text
    CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
	LogUltility.log(test, logger, "Highlight all text");
	
	// Click the italic button
	LogUltility.log(test, logger, "Open the Say As window");
	Actions builder = new Actions(DriverContext._Driver);
	builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
		
	// Check that the popup did  open
	CurrentPage = GetInstance(Say_As_Popup.class);
	boolean Popup = CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpenWithAnswer();
	LogUltility.log(test, logger, "Say as did open " + Popup);
	assertTrue(Popup);
	
	// Close the say-as popup
	CurrentPage.As(Say_As_Popup.class).btnCancel.click();
	LogUltility.log(test, logger, "Close the Say-as popup");
	
	LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-239:Verify that saving new template and opening will not change the language
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA239_Verify_that_saving_new_template_and_opening_will_not_change_the_language() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("TextEditorTests-SRA-239", "Verify that saving new template and opening will not change the language");

		// Check if homepage is ready to use
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Choose domain with templates
		String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
		LogUltility.log(test, logger, "Choose domain with templates: "+randomDomain);
		
		// Get the language dropdown value before opening template
		String languageBeforeOpen = GetInstance(HomePage.class).cmbLanguage.getText();
		LogUltility.log(test, logger, "Language before opening the template: "+languageBeforeOpen);
		
		// Click the open button
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		//Choose the Template radio button
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		LogUltility.log(test, logger, "Click template radio button");
		
	    // Choose random Template and click open
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected Template: " + chosenTemplate);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "Click the open button");
		
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		
		// Press save button
		GetInstance(HomePage.class).SaveButton.click();
		LogUltility.log(test, logger, "Click the save button");
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ",2)[0];
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Click the open button
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		//Choose the Template radio button
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		LogUltility.log(test, logger, "Click template radio button");
		
		// Choose the created template
		CurrentPage.As(OpenPopup.class).openTemplateWait();
//		CurrentPage.As(OpenPopup.class).SelectFromtemplateDropdown(fileName);
		LogUltility.log(test, logger, "Choose template: "+fileName);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "Click the open button");
		
		// Get the language dropdown value after opening template
		String languageAfterOpen = GetInstance(HomePage.class).cmbLanguage.getText();
		LogUltility.log(test, logger, "Language before opening the template: "+languageAfterOpen);
		
		// Check if the dropdown value didn't change
		boolean same = languageAfterOpen.equals(languageBeforeOpen);
		LogUltility.log(test, logger, "Language didn't change: "+same);
		assertTrue(same);
		
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-240:Verify that clip doesn't pause after clicking between the download button and wav bar 
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA240_Verify_that_clip_doesnt_pause_after_clicking_between_the_download_button_and_wav_bar() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-240", "Verify that clip doesn't pause after clicking between the download button and wav bar");
		
        
		// Choose voice
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Insert text
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		CurrentPage.As(HomePage.class).btnplay.click();
		LogUltility.log(test, logger, "Press the play button");
		
		// Wait for the test to start playing
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
		
		// Click between the download button and wav bar
//		CurrentPage.As(HomePage.class).btnplay.click();
		Point point = 	CurrentPage.As(HomePage.class).btnDownload.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX()+40,point.getY()+120);
		LogUltility.log(test, logger, "Play clip again and move the mouse to the area between the download button and wav bar");
		
        // Press the mouse left click 
        robot.mousePress(java.awt.event.InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_DOWN_MASK); 
		LogUltility.log(test, logger, "Click the mouse left click");
		
		//Check that the pause icon is displayed after clicking on the beginning
		boolean buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPause.isDisplayed();
		LogUltility.log(test, logger, "Pause button is displayed and the clip didn't pause: " + buttonStatus);
		assertTrue(buttonStatus);
	
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 *  SRA-241:Check that choosing gesture without clicking on the text area doesn't case error in console
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA241_Check_that_choosing_gesture_without_clicking_on_the_text_area_doesnt_case_error_in_console() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("TextEditorTests-SRA-241", "Check that choosing gesture without clicking on the text area doesn't case error in console");
			
		
		// Refresh the page
		DriverContext._Driver.navigate().refresh();
		LogUltility.log(test, logger, "Refresh the page");
		
		// Select a Domain with gestures 
		CurrentPage.As(HomePage.class).isHomepageReady();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
		CurrentPage.As(HomePage.class).cmbDomain.click();
		 CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);

		// Choose random gesture while focus is outside text area 
		String gesture = CurrentPage.As(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Choose gesture: " + gesture);
		
		// Check that the text editor is disabled and still contains the instructions text
			String currentText = CurrentPage.As(HomePage.class).Textinput.getText();
			LogUltility.log(test, logger, "Get text from the textbox:" + currentText);
			boolean containText = false;
			
			if(Setting.Language.equals("6"))
			 containText= currentText.contains("Select a voice and a domain from the menu on the left, and then either open an existing voice text file, or just type some text right into this box");
			if(Setting.Language.equals("10"))
				{
					String hebrew_inst = CurrentPage.As(HomePage.class).getHebrewInstruction();
					containText= currentText.contains(hebrew_inst);
				}	
	        LogUltility.log(test, logger, "containText should be true : " + containText);
	        assertTrue(containText);

	    LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, InterruptedException
    {	
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Text Editor",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
    
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
		
    	}
	
	/**
	 * Closing the browser after running all the TCs
	 */
	@AfterClass(alwaysRun = true) 
	public void CloseBrowser() {
		 DriverContext._Driver.quit();
    }
}
