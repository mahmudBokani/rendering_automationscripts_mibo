package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import jxl.read.biff.BiffException;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SelectPage;

/**
 * All tests in Domain folder
 *
 */
public class DomainTests extends FrameworkInitialize {
	
	private static Logger logger = null;
	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	private String usernameLogin = "";
	private String chosenProject = "";
	public boolean isThereMoreThanOneAssignedDomain;
	
	/**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	@Parameters({ "testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
		
	// Initialize the report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Domain", Setting.Browser);
	
	// Logger
	logger = LogManager.getLogger(DomainTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite= Domain Tests ");
	logger.info("Domain Tests - FrameworkInitilize");
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	
	
	// Get account,project and domain with the requested language and logged-in email
	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
	chosenProject = account_project_domain.get(index).get(1);
	
//	// Check if we have only one domain within one project
//	isThereMoreThanOneAssignedDomain =   GetInstance(HomePage.class).As(HomePage.class).isThereMoreThanOneAssignedDomain(con);
//	
//	// Login only once then run the following tests
//	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
//	

	usernameLogin = ExcelUltility.ReadCell("Email",1);
//	
//	if (isThereMoreThanOneAssignedDomain) {
//		int timesTried = 0;
//		do {
//			try {
//				// Select random project and domain according to the selected language
//	//			LoginUtilities.selectSpecificProjectDomain(GetInstance(SelectPage.class), logger, "Bank Hapoalim Project", "Bank Hapoalim Domain");
//				chosenProject = LoginUtilities.selectRandomProjectDomain(GetInstance(SelectPage.class), logger, con);
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//			
//			CurrentPage= GetInstance(HomePage.class);
//			timesTried =  CurrentPage.As(HomePage.class).isHomepageReady();
//			
//			// Wait for the page to load
//			System.out.println("timesTried: " + timesTried);
//			if(timesTried == 0) {
//				DriverContext._Driver.navigate().refresh();
//				Thread.sleep(2000);
//			}
//
//		} while (timesTried == 0);
//	}
//	else {
//		// If we have only one domain and one project then get this project name 
//	    try {
//			List<String> assignedProjects = GetInstance(HomePage.class).As(HomePage.class).getTheOnlyProjectAssignedToUser(con, usernameLogin);
//			chosenProject = assignedProjects.get(0);
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	//	CurrentPage= GetInstance(HomePage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	generateInput = new RandomText();
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		//Thread.sleep(10000);
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}
	
	/**
	 * SRA-132:Verify user can search domain in the search field
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA132_Verify_user_can_search_domain_in_the_search_field() throws InterruptedException 
	{
		test = extent.createTest("DomainTests-SRA-132", "Verify user can search domain in the search field");
		
		// Filter by entering random domain nameF
		String filteredByDomain = CurrentPage.As(HomePage.class).filterWithRandomDomain();
		LogUltility.log(test, logger, "Filter by entering random domain: " + filteredByDomain);
		
		// Click to close the domain dropdown
		//Thread.sleep(3000);
		//CurrentPage.As(HomePage.class).cmbDomain.click();
		
		// Check that the filtered by domain does exist in the domain dropdown values
		//Thread.sleep(3000);
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> domainInDropdown = CurrentPage.As(HomePage.class).getDomainValues();
		LogUltility.log(test, logger, "Domain In Dropdown: " + domainInDropdown);
		
		boolean containResult = domainInDropdown.contains(filteredByDomain);
		LogUltility.log(test, logger, "Check that the filtered by domain does exist in the domain dropdown values: " + containResult);
		assertTrue(containResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-30:Check the domain dropdown
	 * @throws InterruptedException 
	 * @throws SQLException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA30_Check_the_domain_dropdown() throws InterruptedException, SQLException {
		test = extent.createTest("DomainTests-SRA-30", "Check the domain dropdown");
		
		// Get values from Domain dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> domainValuespre = CurrentPage.As(HomePage.class).getDomainValues();
//		domainValuespre.remove(0);
		
		// remove (P) from domains
		List<String> domainValues = new ArrayList<>(); 
		for (String domain : domainValuespre) {
			domainValues.add(domain.replace(" (P)", ""));
		}
		
		// Fetch the Domain values from the database
		List<String> assignedDomainValues = CurrentPage.As(HomePage.class).getDomainNamesFromDB(con,chosenProject);
		
		// Sort the two lists before comparing
		domainValues.sort(String::compareToIgnoreCase);
		assignedDomainValues.sort(String::compareToIgnoreCase);
//		Collections.sort(domainValues);
//		Collections.sort(assignedDomainValues);
		
		// Compare if the Domain dropdown values are the same in the database
		boolean compareResult = domainValues.equals(assignedDomainValues);
		LogUltility.log(test, logger, "Get values from Domain dropdown: " + domainValues);
		LogUltility.log(test, logger, "Fetch the Domain values from the database: " + assignedDomainValues);
		logger.info("Compare if the Domain dropdown values are the same in the database: " + compareResult);
		assertTrue(compareResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}

//	MOVED TO OBSOLETE
//	/**
//	 *  SRA-46:Verify the text editor enabled after selecting a domain
//	 * @throws InterruptedException 
//	 * @throws SQLException 
//	 * @throws FileNotFoundException 
//	 */
//	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
//	public void SRA46_Verify_the_text_editor_enabled_after_selecting_a_domain() throws InterruptedException, SQLException, FileNotFoundException {
//		test = extent.createTest("DomainTests-SRA-46", "Verify the text editor enabled after selecting a domain");
//
//		// Check that the text editor is disabled , if the default text appear in the textbox
//		String currentText = CurrentPage.As(HomePage.class).Textinput.getText();
//		LogUltility.log(test, logger, "Get text from the textbox:" + currentText);
//		boolean containText = false;
//		
//		if(Setting.Language.equals("6"))
//		 containText= currentText.contains("Select a voice and a domain from the menu on the left, and then either open an existing voice text file, or just type some text right into this box");
//		if(Setting.Language.equals("10"))
//			{
//				String hebrew_inst = CurrentPage.As(HomePage.class).getHebrewInstruction();
//				containText= currentText.contains(hebrew_inst);
//			}
//		
//        LogUltility.log(test, logger, "containText should be true : " + containText);
//        assertTrue(containText);
//        
//		// Select a domain
////        String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
////		//CurrentPage.As(HomePage.class).select_Domain(randomDomain);
////		LogUltility.log(test, logger, "select random domain: " + randomDomain);
//		
//		// Click on the text box
//		CurrentPage.As(HomePage.class).Textinput.click();
//		
//	    // Now the text editor should be clean and empty
//		//Thread.sleep(3000);
//		currentText = CurrentPage.As(HomePage.class).Textinput.getText();
//        boolean isEmpty= currentText.isEmpty();
//        LogUltility.log(test, logger, "isEmpty should be true : " + isEmpty);
//        assertTrue(isEmpty);
//        LogUltility.log(test, logger, "Test Case PASSED");
//	}
	
	/**
	 *  SRA-51:Verify the domain can be selected before selecting a voice
	 * @throws InterruptedException 
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA51_Verify_the_domain_can_be_selected_before_selecting_a_voice() throws InterruptedException, SQLException {
		test = extent.createTest("DomainTests-SRA-51", "Verify the domain can be selected before selecting a voice");
        
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		
		// Click the open button
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		// Wait for the open popup to be available
		CurrentPage.As(OpenPopup.class).openFileWait();
		
		// Choose random file and click open
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
			
		// Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		
		//Thread.sleep(5000);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		LogUltility.log(test, logger, "text in textbox: " + text);
		boolean isEmpty = text.isEmpty();
		LogUltility.log(test, logger, "Is textbox empty: " + isEmpty);
		assertFalse(isEmpty);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-123:Verify the system does not returns value when searching with invalid keyword
	 * @throws InterruptedException 
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA123_Verify_the_system_does_not_returns_value_when_searching_with_invalid_keyword() throws InterruptedException, SQLException {
		test = extent.createTest("DomainTests-SRA-123", "Verify the system does not returns value when searching with invalid keyword");

		// Input invalid symbols string and click the filter button
		String randomSymbols = CurrentPage.As(HomePage.class).randomSymbols(5);
		LogUltility.log(test, logger, "Random symbols to enter: " + randomSymbols);
		
		CurrentPage.As(HomePage.class).txtDomainSearch.sendKeys(randomSymbols);
		CurrentPage.As(HomePage.class).btnDomainSearch.click();
		
		// Check that the domain dropdown is empty
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> domainsInDropdown = CurrentPage.As(HomePage.class).getDomainValues();
		LogUltility.log(test, logger, "Values in domain dropdown: " + domainsInDropdown);
		
		boolean isItEmpty = CurrentPage.As(HomePage.class).isDomainListEmpty(test, logger);
		
//		if (domainsInDropdown.size() == 1 && domainsInDropdown.get(0).length() == 0)
//			isItEmpty = true;
//		else isItEmpty = false;
		
		LogUltility.log(test, logger, "Is the domain dropdown empty: " + isItEmpty);
		assertTrue(isItEmpty);
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA-157:Check the supported Mood/style/ Gestures for different domains - Gestures - Check all domains - Part 1
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 */
	@Test(groups= "Full Refression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA157_Check_the_supported_Mood_style_Gestures_for_different_domains_Gestures_Check_all_domains_Part_1() throws InterruptedException, SQLException, AWTException 
	{
		test = extent.createTest("DomainTests-SRA-157", "Check the supported Mood/style/ Gestures for different domains - Gestures - Check all domains - Part 1");
		
		// Get all values from Domain dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).lblDomain.click();
		List<String> domainValues = CurrentPage.As(HomePage.class).getDomainValues();
//		domainValues.remove(0);
		System.out.println("Get values from Domain dropdown: " + domainValues);
		CurrentPage.As(HomePage.class).lblDomain.click();
		
		// Select each one of the domains
		for (String domain : domainValues) {
			LogUltility.log(test, logger, "--------------------");
			
			CurrentPage.As(HomePage.class).lblDomain.click();
			
			LogUltility.log(test, logger, "Chosen domain: " + domain);
//			CurrentPage.As(HomePage.class).cmbDomain.click();
//			Thread.sleep(1000);
//			CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, domain);
			CurrentPage.As(HomePage.class).select_Domain(domain);
			
			// Get the Gestures for the selected domain in the web page
			Thread.sleep(2000);
			System.out.println("Check gestures");
			ArrayList<String> GestureValues= CurrentPage.As(HomePage.class).getGestureValues();
			System.out.println("done get gestures");
			
			// Get the Gestures for the selected domain in the DB
			List<String> DBGesture = CurrentPage.As(HomePage.class).getGestureValuesFromDB(con, domain);
			
			// Sort the two lists before comparing
	  	    Collections.sort(GestureValues);
		    LogUltility.log(test, logger, "GestureValues after sort: " + GestureValues);
	        Collections.sort(DBGesture);
		    LogUltility.log(test, logger, "DBGesture after sort: " + DBGesture);
		    
		    // Compare if the Gesture dropdown values are the same in the database
		    boolean compareGestureResult = GestureValues.equals(DBGesture);
		    LogUltility.log(test, logger, "Compare if the Gesture dropdown values are the same as in the database: " + compareGestureResult);
		    assertTrue(compareGestureResult);
		}
	 }
	
	/**
	 * SRA-157:Check the supported Mood/style/ Gestures for different domains - Moods - Check all domains - Part 2
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 */
	@Test(groups= "Full Refression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA157_Check_the_supported_Mood_style_Gestures_for_different_domains_Moods_Check_all_domains_Part_2() throws InterruptedException, SQLException, AWTException 
	{
		test = extent.createTest("DomainTests-SRA-157", "Check the supported Mood/style/ Gestures for different domains - Moods - Check all domains - Part 2");
		
		// Get all values from Domain dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).lblDomain.click();
		List<String> domainValues = CurrentPage.As(HomePage.class).getDomainValues();
		
		// Select voice
		String selectedVoice = CurrentPage.As(HomePage.class).chooseRandomVoice() ;
		LogUltility.log(test, logger, "Chosen voice: " + selectedVoice);
		
		// Get the moods for the selected voice in the DB 
		String voice_id = CurrentPage.As(HomePage.class).getVoiceID(con,selectedVoice);
		List<String> voiceMoodIds = CurrentPage.As(HomePage.class).getVoiceMoodsIdsFromDB(con,voice_id);

//		domainValues.remove(0);
		System.out.println("Get values from Domain dropdown: " + domainValues);
		CurrentPage.As(HomePage.class).lblDomain.click();

		// Select each one of the domains
		for (String domain : domainValues) {
			LogUltility.log(test, logger, "--------------------");

			LogUltility.log(test, logger, "Chosen domain: " + domain);
//			CurrentPage.As(HomePage.class).cmbDomain.click();
//			Thread.sleep(1000);
//			CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, domain);
			CurrentPage.As(HomePage.class).select_Domain(domain);
			
			// Get the Moods for the selected domain in the web page
			Thread.sleep(2000);
			ArrayList<String> MoodValues= CurrentPage.As(HomePage.class).getMoodValues();
			
			if (domain.contains("Genesys"))
				System.out.println();
			
			// Get the Moods for the selected domain in the DB
			List<String> DBMood = CurrentPage.As(HomePage.class).getMoodValuesFromDB(con, domain);
			DBMood.retainAll(voiceMoodIds);
			
			// Sort the two lists before comparing
	  	    Collections.sort(MoodValues);
		    LogUltility.log(test, logger, "MoodValues after sort: " + MoodValues);
	        Collections.sort(DBMood);
		    LogUltility.log(test, logger, "DBMood after sort: " + DBMood);
		    
		    // Compare if the Mood dropdown values are the same in the database
		    boolean compareMoodResult = MoodValues.equals(DBMood);
		    LogUltility.log(test, logger, "Compare if the Mood dropdown values are the same as in the database: " + compareMoodResult);
		    assertTrue(compareMoodResult);
		}
	 }
	
	/**
	 * SRA-157:Check the supported Mood/style/Gestures for different domains - Styles - Check all domains - Part 3
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 */
	@Test(groups= "Full Refression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA157_Check_the_supported_Mood_style_Gestures_for_different_domains_Styles_Check_all_domains_Part_3() throws InterruptedException, SQLException, AWTException 
	{
		test = extent.createTest("DomainTests-SRA-157", "Check the supported Mood/style/ Gestures for different domains - Styles - Check all domains - Part 3");
		 
		// Get all values from Domain dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).lblDomain.click();
		List<String> domainValues = CurrentPage.As(HomePage.class).getDomainValues();
//		domainValues.remove(0);
		System.out.println("Get values from Domain dropdown: " + domainValues);
		CurrentPage.As(HomePage.class).lblDomain.click();

		// Select each one of the domains
		for (String domain : domainValues) {
			LogUltility.log(test, logger, "--------------------");

			LogUltility.log(test, logger, "Chosen domain: " + domain);
//			CurrentPage.As(HomePage.class).cmbDomain.click();
//			Thread.sleep(1000);
//			CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, domain);
			CurrentPage.As(HomePage.class).select_Domain(domain);

			// Get the Styles for the selected domain in the web page
			Thread.sleep(2000);
			ArrayList<String> StyleValues= CurrentPage.As(HomePage.class).getStyleValues();
			
			// Get the Styles for the selected domain in the DB
			List<String> DBStyle = CurrentPage.As(HomePage.class).getStylesValuesFromDB(con, domain);
			
			// Sort the two lists before comparing
	  	    Collections.sort(StyleValues);
		    LogUltility.log(test, logger, "StyleValues after sort: " + StyleValues);
	        Collections.sort(DBStyle);
		    LogUltility.log(test, logger, "DBStyle after sort: " + DBStyle);
		    
		    // Compare if the Mood dropdown values are the same in the database
		    boolean compareStyleResult = StyleValues.equals(DBStyle);
		    LogUltility.log(test, logger, "Compare if the Style dropdown values are the same as in the database: " + compareStyleResult);
		    assertTrue(compareStyleResult);
		}
	 }
	
	/**
	 * SRA-186:Verify that bars values still appear after changing the domain to one that doesnï¿½t contain-bug658
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA186_Verify_that_bars_values_still_appear_after_changing_the_domain_to_one_that_doesn_t_contain_bug658() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("DomainTests-SRA-186", "Verify that bars values still appear after changing the domain to one that doesnï¿½t contain-bug658");
		
		List<String> twoDomains = CurrentPage.As(HomePage.class).getTwoDomainsDifferentGesturesFromDB(con);
		
		// check if two domains with different gestures found
		if (twoDomains.get(0).equals("nothing")) {
			LogUltility.log(test, logger, "Stopping this test, Could not find a gesture in a domain"
					+ "that is not included in another domian");
			assertTrue(false);
		}
		
		String firstDomain = twoDomains.get(0);
		String secondDomain = twoDomains.get(1);
		String gestureToUse = twoDomains.get(2);
		LogUltility.log(test, logger, "First domain: " + firstDomain + 
				", Second domain: " + secondDomain +
				", Gesture to use: " + gestureToUse);
		
		// Choose a voice
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
		
		// Select the first domain
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, firstDomain);
		LogUltility.log(test, logger, "selected first domain: " + firstDomain);
		
		// apply the chosen gesture
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		//Thread.sleep(4000);
		
		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "letters") +
//	    		" " + generateInput.RandomString(5, "letters");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		CurrentPage.As(HomePage.class).btnGesture.click();
		CurrentPage.As(HomePage.class).SelectFromGestures(gestureToUse);
		LogUltility.log(test, logger, "Chosen Gesture: " + gestureToUse);

		//-----------------------------------
		// Select values in the bars
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		// Click the bold button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Click the bold button");
		CurrentPage.As(HomePage.class).btnBold.click();
		
		// change values for volume, speed and pitch
		// change the pitch range bar
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
		// change the volume range bar
		//Thread.sleep(1000);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
		// change the speed range bar
		//Thread.sleep(1000);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		CurrentPage.As(HomePage.class).SpeedBar.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
		
		//-----------------------------------
		
		// Select the second domain
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, secondDomain);
		LogUltility.log(test, logger, "selected second domain: " + secondDomain);		
		
		// click the keep new domain button
		Thread.sleep(2000);
		CurrentPage.As(HomePage.class).keepNewDomain.click();
		LogUltility.log(test, logger, "click the keep new domain button");
		
		// check that the current selected domain is the second one 
		String getCurrentDomain = CurrentPage.As(HomePage.class).cmbDomain.getText();
		boolean result = getCurrentDomain.contains(secondDomain);
		LogUltility.log(test, logger, "Is domain changed: " + result);
		assertTrue(result);
		
		// check also that the gesture is removed with the second domain
		// Get the text from the text editor
		Thread.sleep(2000);
		LogUltility.log(test, logger, "Get the text from the text editor");
		String InnerText = CurrentPage.As(HomePage.class).EnteredText.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Get the text from the text editor :" + InnerText);
		
		
		//----------------------------------
		
		// Check for results:
		boolean containgesture= InnerText.contains(gestureToUse);
		LogUltility.log(test, logger, "text editor contain gesture should be False :" + containgesture  );
		assertFalse(containgesture);
						
		// Check that the text is bolded
		//Thread.sleep(2000);
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);
		//boolean doContain = source.contains("data-type=\"emphasis\"");
		boolean doContain = source.contains("data-emphasis=");
		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
		assertTrue(doContain);

		// Check that the volume and pitch and speed are also set
		doContain = source.contains("data-pitch=");
		LogUltility.log(test, logger, "Is pitch found: " + doContain);
		assertTrue(doContain);
		doContain = source.contains("data-volume=");
		LogUltility.log(test, logger, "Is volume found: " + doContain);
		assertTrue(doContain);
		doContain = source.contains("data-speed=");
		LogUltility.log(test, logger, "Is speed found: " + doContain);
		assertTrue(doContain);
		LogUltility.log(test, logger, "Test Case PASSED");
		
		
	 }
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Domain Selection",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
			}
}
