package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SavePopup;
import smi.rendering.test.pages.Say_As_Popup;
import smi.rendering.test.pages.SelectPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * All tests in Open options folder
 *
 */
public class OpenOptionsTests extends FrameworkInitialize {

	private static Logger logger = null;
	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	private String usernameLogin = "";
	private String chosenProject = "";
	public boolean isThereMoreThanOneAssignedDomain;
	
	/**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("OpenOptions", Setting.Browser);
		
	// Logger
	logger = LogManager.getLogger(OpenOptionsTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite = OpenOptionsTests ");
	logger.info("Open options Tests - FrameworkInitilize");
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	// Get account,project and domain with the requested language and logged-in email
	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
	chosenProject = account_project_domain.get(index).get(1);

	// Check if we have only one domain within one project
//	isThereMoreThanOneAssignedDomain =   GetInstance(HomePage.class).As(HomePage.class).isThereMoreThanOneAssignedDomain(con);
		
	
	CurrentPage= GetInstance(HomePage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());

		CurrentPage = GetInstance(HomePage.class);
		//Thread.sleep(10000);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}
	
	/**
	 *  SRA-103:Verify user can open TXT file to the rendering app successfully
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA103_Verify_user_can_open_TXT_file_to_the_rendering_app_successfully() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("OpenOptionsTests-SRA-103", "Verify user can open TXT file to the rendering app successfully");
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
	
	    // Click the open button and check for open popup
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
	    // Choose random file and click open
		CurrentPage.As(OpenPopup.class).openFileWait();
		//Thread.sleep(3000);
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		// Click Open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean containsText = text.isEmpty();
		assertFalse(containsText);
		LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + containsText);
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		CurrentPage.As(SavePopup.class).FileName.clear();
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[0];
		LogUltility.log(test, logger, "Enter a file name : "  + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
        
        // Verify that the filename saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> File_name =CurrentPage.As(SavePopup.class).getFileName(con,fileName);
        LogUltility.log(test, logger, "results from DB" + File_name);
        assertEquals(fileName, File_name.get(1).get(0));
        LogUltility.log(test, logger, "Test Case PASSED");
	}
    
	/**
	 * SRA-104:Check the open button disabled when the domain is not selected
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA104_Check_the_open_button_disabled_when_the_domain_is_not_selected() throws InterruptedException, AWTException, SQLException {
			
		test = extent.createTest("OpenOptionsTests-SRA-104" , "Check the open button disabled when the domain is not selected");
		
		//Check that the open button is disabled , if the default text appear in the textbox
		CurrentPage = GetInstance(HomePage.class);
		String currentText = CurrentPage.As(HomePage.class).InformationText.getText();
		LogUltility.log(test, logger, "Get text from the textbox:" + currentText);
        boolean containText= currentText.contains("Select a voice and a domain from the menu on the left, and then either open an existing voice text file, or just type some text right into this box");
        LogUltility.log(test, logger, "containText should be true : " + containText);
        assertTrue(containText);
        
		// Select a domain
        String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		
		// Check that the open button becomes Enabled
		//Thread.sleep(2000);
		currentText = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean result = currentText.isEmpty();
		LogUltility.log(test, logger, "containText should be empty : " + result);
		assertTrue(result);
		
	    // Click the open button and check for open popup
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button and choose random file");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		CurrentPage.As(OpenPopup.class).openFileWait();
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
  
	/**
	 *  SRA-106:Verify user can edit opened TXT
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA106_Verify_user_can_edit_opened_TXT() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("OpenOptionsTests-SRA-106" ,"Verify_user_can_edit_opened_TXT");
		
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
	
	    // Click the open button and check for open popup
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
	    // Choose random file and click open
		CurrentPage.As(OpenPopup.class).openFileWait();
		//Thread.sleep(3000);
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		// Click Open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean notEmpty = text.isEmpty();
		LogUltility.log(test, logger, "textbox is empty: " + notEmpty);
		assertFalse(notEmpty);
		
		// Delete the text and add new text  in the textbox
		CurrentPage.As(HomePage.class).Textinput.clear();
//		String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Check text do exist in the text editor
		//Thread.sleep(3000);
		String newText = CurrentPage.As(HomePage.class).Textinput.getText();
		notEmpty = newText.isEmpty();
		LogUltility.log(test, logger, "textbox is empty: " + notEmpty);
		assertFalse(notEmpty);
		
		// Verify that the text from the TXT file not the same after edit it 
		LogUltility.log(test, logger, "Text in textbox: " + newText + " Not equal to " + text);
		assertNotEquals(newText, text, "Not equal");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-108:Verify user can open text with numbers and symbols
	 * @throws InterruptedException 
	 * @throws AWTException 
     * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA108_Verify_user_can_open_text_with_numbers_and_symbols() throws InterruptedException, AWTException, SQLException, FileNotFoundException {

		test = extent.createTest("OpenOptionsTests-SRA_108", "Verify_user_can_open_text_with_numbers_and_symbols");

		// Create new file with text that includes numbers and symbols 
		//Thread.sleep(2000);
//		String text = generateInput.RandomString(5, "words") + CurrentPage.As(HomePage.class).randomNumbers(5) + " "	
//				+ CurrentPage.As(HomePage.class).randomSymbols(5);
		String text = RandomText.getSentencesFromFile() + CurrentPage.As(HomePage.class).randomNumbers(5) + " "	
				+ CurrentPage.As(HomePage.class).randomSymbols(5);
		LogUltility.log(test, logger, "Text to enter: " + text);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);

		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[0];
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Clear the text box editor
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).Textinput.clear();
		
	    // Click the open button and open the saved files with the symbols and numbers text
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		// Click Open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).openFileWait();
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box and equal to the entered text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String savedText = CurrentPage.As(HomePage.class).Textinput.getText();
		LogUltility.log(test, logger, "text in textbox: " + savedText);
		assertEquals(savedText, text);
	
//		// Enter file name in order to save the text in text editor again
//		CurrentPage = GetInstance(SavePopup.class);
//		CurrentPage.As(SavePopup.class).FileName.clear();
//		String NewFileName = CurrentPage.As(SavePopup.class).RandomString(5);
//		LogUltility.log(test, logger, "Enter a file name : "  + NewFileName);
//		CurrentPage.As(SavePopup.class).FileName.sendKeys(NewFileName);
//		CurrentPage.As(SavePopup.class).SaveBtn.click();
//		
//		// Check the Save confirmation message
//		CurrentPage = GetInstance(HomePage.class);
//		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
//        
//        // Verify that the filename saved to DB
//		CurrentPage = GetInstance(SavePopup.class);
//    	List<List<String>> File_name =CurrentPage.As(SavePopup.class).getFileName(con,NewFileName);
//        LogUltility.log(test, logger, "results from DB" + File_name);
//        assertEquals(NewFileName, File_name.get(1).get(0));
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
		
		/**
		 * SRA-144: Verify user can open Template successfully
		 * @throws InterruptedException
		 * @throws SQLException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA144_Verify_user_can_open_Template_successfully() throws InterruptedException, SQLException {
		
		test = extent.createTest("OpenOptions-SRA-144", "Verify user can open Template successfully");		
		// Select a domain	
		CurrentPage.As(HomePage.class).isHomepageReady();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithTemplates(con,chosenProject);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		//Thread.sleep(2000);
	
		// Click the open button and check for open popup
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		//Choose the Template radio button
		//Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		
	    // Choose random Template and click open
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		//CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected file: " + chosenTemplate);
		// Click Open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		Thread.sleep(1000);
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean containsText = text.isEmpty();
		assertFalse(containsText);
		LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + containsText);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * SRA-145:Verify user can open Template and edit the editable words and save successfully
		 * @throws InterruptedException
		 * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA145_Verify_user_can_open_Template_and_edit_the_editable_words_and_save_successfully() throws InterruptedException, SQLException, FileNotFoundException {
			test = extent.createTest("OpenOptions-SRA-145", "Verify user can open Template and edit the editable words and save successfully");
			
//		// Select a domain	
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		//Thread.sleep(2000);


		String chosenTemplate = "";
		List <String> allEditTemplates = new ArrayList<String>();

		// Get domains with editable templates
		CurrentPage = GetInstance(HomePage.class);
		HashMap<String,List<String>> domainsEditTemps = GetInstance(HomePage.class).getDomainWithEditableTemplate(con,chosenProject);
		
		// Get the name of the domains and choose one
		List<String> domains = new ArrayList<String>();
		for ( String key : domainsEditTemps.keySet() ) 
				domains.add(key);
			
		// Get app dropdown values
		 GetInstance(HomePage.class).isHomepageReady();
		List<String> smorphDomains = GetInstance(HomePage.class).getDomainValues();
		smorphDomains.remove("Select Domain");
		
		//*************************
		// Available domains for the user to use
		domains.retainAll(smorphDomains);
	
//		// Choose random domain
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
//		String dropdownDomain ="";
//		// To get private domains correct name, since they are not stored with "(P)" in the DB
//		for(int i =0;i<smorphDomains.size();i++)
//			if(smorphDomains.get(i).contains(randomDomain))
//				dropdownDomain = smorphDomains.get(i);
//		
		GetInstance(HomePage.class).isHomepageReady();
		GetInstance(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		
		// Click the open button and check for open popup
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		//Choose the Template radio button
		//Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		
	    // Choose random editable Template and click open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		
		// Choose random editable template
		allEditTemplates = domainsEditTemps.get(randomDomain);
		random = new Random();
		chosenTemplate = allEditTemplates.get(random.nextInt(allEditTemplates.size()));
		LogUltility.log(test, logger, "Selected file: " + chosenTemplate);

		// Select from templates dropdown
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).SelectFromtemplateDropdown(chosenTemplate);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean containsText = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + "text area is empty: " + containsText);
		assertFalse(containsText);
		
		// Add text in the editable text 
		//CurrentPage.As(HomePage.class).editableText.click();
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).editableText.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		//Thread.sleep(2000);
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
//		String fileName = generateInput.RandomString(5, "words");
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[0];
		LogUltility.log(test, logger, "Enter a file name" + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
		LogUltility.log(test, logger, "Test Case PASSED");
		}
	       
		/**
		 * SRA-146:Verify user can`t save template with the same name
		 * @throws InterruptedException
		 * @throws SQLException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA146_Verify_user_cant_save_template_with_the_same_name() throws InterruptedException, SQLException {
			test = extent.createTest("OpenOptions-SRA-146", "Verify user cant save Template with the same name");
			
			// Select a domain	
//			String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//			String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithTemplates(con, usernameLogin);
//			LogUltility.log(test, logger, "Select random domain: " + randomDomain);
			//Thread.sleep(2000);

			// Click the open button and check for open popup
			//Thread.sleep(3000);
			LogUltility.log(test, logger, "Click the open button");
			CurrentPage = GetInstance(OpenPopup.class);
			CurrentPage.As(OpenPopup.class).btnOpen.click();
			
			//Choose the Template radio button
			//Thread.sleep(3000);
			CurrentPage = GetInstance(OpenPopup.class);
			CurrentPage.As(OpenPopup.class).templateRBtn.click();
			
		    // Choose random Template and click open
			//Thread.sleep(3000);
			CurrentPage.As(OpenPopup.class).openTemplateWait();
			//CurrentPage.As(OpenPopup.class).cmbTemplate.click();
			String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
			LogUltility.log(test, logger, "Selected Template: " + chosenTemplate);
			// Click Open
			//Thread.sleep(3000);
			CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
	
		    // Check that there is text displayed in the text box
			//Thread.sleep(3000);
			CurrentPage = GetInstance(HomePage.class);
			String text = CurrentPage.As(HomePage.class).Textinput.getText();
			
			boolean containsText = text.isEmpty();
			LogUltility.log(test, logger, "text in textbox: " + text + "empty text: " + containsText);
			assertFalse(containsText);
			
//			// Add text in the editable text 
//			//CurrentPage.As(HomePage.class).editableText.click();
//			//Thread.sleep(2000);
//		    String randomText = CurrentPage.As(HomePage.class).RandomString(5);
//			LogUltility.log(test, logger, "input text to the text area: " + randomText);
//			CurrentPage.As(HomePage.class).editableText.sendKeys(randomText);
//			
			// Click the save button
			CurrentPage.As(HomePage.class).isHomepageReady();
			LogUltility.log(test, logger, "Click the save button");
			CurrentPage.As(HomePage.class).SaveButton.click();
			
			// Enter the same template file name
			CurrentPage = GetInstance(SavePopup.class);
			LogUltility.log(test, logger, "Enter the same template name: " + chosenTemplate);
			CurrentPage.As(SavePopup.class).FileName.sendKeys(chosenTemplate);
			CurrentPage.As(SavePopup.class).SaveBtn.click();
			LogUltility.log(test, logger, "Click the save button on the save popup");
			
			// Check the Save confirmation message
			CurrentPage = GetInstance(HomePage.class);
			CurrentPage.As(HomePage.class).fadedPopup(logger, "You cannot use an existing template name     X");
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * SRA-182:domain field shouldn�t be editable after playing text � bug 918
		 * @throws InterruptedException 
		 * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
		public void SRA182_domain_field_shouldn_t_be_editable_after_playing_text_bug_918() throws InterruptedException, SQLException, FileNotFoundException
		{		
			test = extent.createTest("OpenOptions-SRA-182", "domain field shouldn�t be editable after playing text � bug 918");
			
			// Select random voice
			String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
			LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice );	
			
			// Enter text in the text box
			//Thread.sleep(3000);
//			String randomText = generateInput.RandomString(5, "words");
			String randomText = RandomText.getSentencesFromFile();
			LogUltility.log(test, logger, "Text to enter: " + randomText);
			CurrentPage.As(HomePage.class).Textinput.click();
			CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
			
			// Click the play button
			//Thread.sleep(2000);
			CurrentPage.As(HomePage.class).btnplay.click();
			
			// Wait for the test to start playing
			//Thread.sleep(2000);
			CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
					
			// Check if the wave bar for clip is displayed
			//Thread.sleep(10000);
			boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
			LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
			assertTrue(isWaveBarDisplayed);
			
			// Open the Open window
			LogUltility.log(test, logger, "Click the open button");
			CurrentPage = GetInstance(OpenPopup.class);
			CurrentPage.As(OpenPopup.class).btnOpen.click();
			
			// Check can't modify the domain field
//			String mibo = CurrentPage.As(OpenPopup.class).domainField.getText();
//			System.out.println("mibo: " + mibo);
			String isItDisabled = CurrentPage.As(OpenPopup.class).domainField.getAttribute("disabled");
			System.out.println("isItDisabled: " + isItDisabled);
			assertEquals(isItDisabled, "true");
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 *  SRA-226:Verify that "clear text" button doesn't disappear after opening file
		 * @throws InterruptedException 
		 * @throws AWTException 
	     * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
		public void  SRA226_Verify_that_clear_text_button_doesnt_disappear_after_opening_file() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
				
			test = extent.createTest("OpenOptionsTests-SRA-226", "Verify that clear text button doesn't disappear after opening file");

			// Check if homepage is ready to use
			CurrentPage = GetInstance(HomePage.class);
			GetInstance(HomePage.class).isHomepageReady();
			
			// Choose domain with files
			String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
			LogUltility.log(test, logger, "Choose domain with files: "+randomDomain);
			
			// Is clear text button displayed
			boolean clearBtnBeforeOpen = GetInstance(HomePage.class).clearText.isDisplayed();
			LogUltility.log(test, logger, "Is clear text button displayed before opening file: "+clearBtnBeforeOpen);
			assertTrue(clearBtnBeforeOpen);
			
			// Click the open button and check for open popup
			LogUltility.log(test, logger, "Click the open button");
			CurrentPage = GetInstance(OpenPopup.class);
			CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		    // Choose random file and click open
			CurrentPage.As(OpenPopup.class).openFileWait();
			//Thread.sleep(3000);
			String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
			LogUltility.log(test, logger, "Selected file: " + chosenFile);
			
			// Click Open
			CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
			
		    // Check that there is text displayed in the text box
			//Thread.sleep(3000);
			CurrentPage = GetInstance(HomePage.class);
			CurrentPage.As(HomePage.class).isHomepageReady();

			// Is clear text button displayed after opening file
			boolean clearBtnAfterOpen = GetInstance(HomePage.class).clearText.isDisplayed();
			LogUltility.log(test, logger, "Is clear text button displayed after opening file: "+clearBtnAfterOpen);
			assertTrue(clearBtnAfterOpen);
			
			
	        LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 *   SRA-227:Verify that red remove icons are not displayed after opening file - part 1 files
		 * @throws InterruptedException 
		 * @throws AWTException 
	     * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
		public void  SRA227_Verify_that_red_remove_icons_are_not_displayed_after_opening_file_part1_files() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
				
			test = extent.createTest("OpenOptionsTests-SRA-227", "Verify that red remove icons are not displayed after opening file - part 1 files");

			// Check if homepage is ready to use
			CurrentPage = GetInstance(HomePage.class);
			GetInstance(HomePage.class).isHomepageReady();
			
			// Choose domain with files
			String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
			LogUltility.log(test, logger, "Choose domain with files: "+randomDomain);
			
			// Insert text to the text area
			String randomText = RandomText.getSentencesFromFile();
			LogUltility.log(test, logger, "Text to enter: " + randomText);
			CurrentPage.As(HomePage.class).Textinput.click();
			CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
			
			// Highlight all text
	        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			LogUltility.log(test, logger, "Highlight all text");
			
			// Click the open button and check for open popup
			LogUltility.log(test, logger, "Click the open button");
			CurrentPage = GetInstance(OpenPopup.class);
			CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		    // Choose random file and click open
			CurrentPage.As(OpenPopup.class).openFileWait();
			//Thread.sleep(3000);
			String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
			LogUltility.log(test, logger, "Selected file: " + chosenFile);
			
			// Click Open
			CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
			
		    // Check that the red remove icons are not displayed
			CurrentPage = GetInstance(HomePage.class);
			CurrentPage.As(HomePage.class).isHomepageReady();

			// Is VSP 'X' remove button displayed
			boolean redRemoveIcons =! (CurrentPage.As(HomePage.class).xButtonPitch.isDisplayed() && CurrentPage.As(HomePage.class).xButtonSpeed.isDisplayed() 
					&& CurrentPage.As(HomePage.class).xButtonVolume.isDisplayed());
			LogUltility.log(test, logger, "The remove red icons are NOT displayed on the VSP bars: "+redRemoveIcons);
			assertTrue(redRemoveIcons);
			
			
	        LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 *   SRA-227:Verify that red remove icons are not displayed after opening file - part 2 templates
		 * @throws InterruptedException 
		 * @throws AWTException 
	     * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
		public void  SRA227_Verify_that_red_remove_icons_are_not_displayed_after_opening_file_part2_templates() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
				
			test = extent.createTest("OpenOptionsTests-SRA-227", "Verify that red remove icons are not displayed after opening - part 2 templates");

			// Check if homepage is ready to use
			CurrentPage = GetInstance(HomePage.class);
			GetInstance(HomePage.class).isHomepageReady();
			
			// Choose domain with files
			String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
			LogUltility.log(test, logger, "Choose domain with templates: "+randomDomain);
			
			// Insert text to the text area
			String randomText = RandomText.getSentencesFromFile();
			LogUltility.log(test, logger, "Text to enter: " + randomText);
			CurrentPage.As(HomePage.class).Textinput.click();
			CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
			
			// Highlight all text
	        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			LogUltility.log(test, logger, "Highlight all text");
			
			// Click the open button
			LogUltility.log(test, logger, "Click the open button");
			CurrentPage = GetInstance(OpenPopup.class);
			CurrentPage.As(OpenPopup.class).btnOpen.click();
		
			//Choose the Template radio button
			//Thread.sleep(3000);
			CurrentPage = GetInstance(OpenPopup.class);
			CurrentPage.As(OpenPopup.class).templateRBtn.click();
			LogUltility.log(test, logger, "Click template radio button");
			
		    // Choose random Template and click open
			//Thread.sleep(3000);
			CurrentPage.As(OpenPopup.class).openTemplateWait();
			//CurrentPage.As(OpenPopup.class).cmbTemplate.click();
			String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
			LogUltility.log(test, logger, "Selected Template: " + chosenTemplate);
			
			// Click Open
			CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
			LogUltility.log(test, logger, "Click the open button");
			
		    // Check that the red remove icons are not displayed
			CurrentPage = GetInstance(HomePage.class);
			CurrentPage.As(HomePage.class).isHomepageReady();

			// Is VSP 'X' remove button displayed
			boolean redRemoveIcons =! (CurrentPage.As(HomePage.class).xButtonPitch.isDisplayed() && CurrentPage.As(HomePage.class).xButtonSpeed.isDisplayed() 
					&& CurrentPage.As(HomePage.class).xButtonVolume.isDisplayed());
			LogUltility.log(test, logger, "The remove red icons are NOT displayed on the VSP bars: "+redRemoveIcons);
			assertTrue(redRemoveIcons);
			
			
	        LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * SRA-234:Verify that IPA is disabled for template fields
		 * @throws InterruptedException
		 * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA234_Verify_that_IPA_is_disabled_for_template_fields() throws InterruptedException, SQLException, FileNotFoundException {
		
		test = extent.createTest("OpenOptions-SRA-234", "Verify that IPA is disabled for template fields");

		String chosenTemplate = "";
		List <String> allEditTemplates = new ArrayList<String>();

		// Get domains with editable templates
		CurrentPage = GetInstance(HomePage.class);
		HashMap<String,List<String>> domainsEditTemps = GetInstance(HomePage.class).getDomainWithEditableTemplate(con,chosenProject);
		
		// Get the name of the domains and choose one
		List<String> domains = new ArrayList<String>();
		for ( String key : domainsEditTemps.keySet() ) 
				domains.add(key);
			
		// Get app dropdown values
		 GetInstance(HomePage.class).isHomepageReady();
		List<String> smorphDomains = GetInstance(HomePage.class).getDomainValues();
		
		//*************************
		// Available domains for the user to use
		domains.retainAll(smorphDomains);
	
//		// Choose random domain
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
//		String dropdownDomain ="";
//		// To get private domains correct name, since they are not stored with "(P)" in the DB
//		for(int i =0;i<smorphDomains.size();i++)
//			if(smorphDomains.get(i).contains(randomDomain))
//				dropdownDomain = smorphDomains.get(i);
//		
		GetInstance(HomePage.class).isHomepageReady();
		GetInstance(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		
		// Click the open button and check for open popup
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		//Choose the Template radio button
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		
	    // Choose random editable Template and click open
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		
		// Choose random editable template
		allEditTemplates = domainsEditTemps.get(randomDomain);
		random = new Random();
		chosenTemplate = allEditTemplates.get(random.nextInt(allEditTemplates.size()));
		LogUltility.log(test, logger, "Selected file: " + chosenTemplate);

		// Select from templates dropdown
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).SelectFromtemplateDropdown(chosenTemplate);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "Open template");
		
	    // Check that there is text displayed in the text box
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean containsText = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + "text area is empty: " + containsText);
		assertFalse(containsText);
		
		// Highlight text in editable field
		WebElement chosenField =CurrentPage.As(HomePage.class).click_on_editable_field(CurrentPage.As(HomePage.class).Textinput);
		chosenField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight text in the editable field: "+chosenField.getText());
		
		// Press the italic/say-as button
		GetInstance(HomePage.class).Italicbtn.click();
		LogUltility.log(test, logger, "Click the italic/say-as button");
		
		// Check that the popup is NOT displayed
		CurrentPage = GetInstance(Say_As_Popup.class);
		boolean noPopup= !CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpenWithAnswer();
		LogUltility.log(test, logger, "Say-as popup is NOT displayed: "+noPopup);
		assertTrue(noPopup);
	
		LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * SRA-235:Verify User can't insert gesture to editable template fields
		 * @throws InterruptedException
		 * @throws SQLException 
		 * @throws FileNotFoundException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA235_Verify_User_cant_insert_gesture_to_editable_template_fields() throws InterruptedException, SQLException, FileNotFoundException, AWTException {
		
		test = extent.createTest("OpenOptions-SRA-235", "Verify User can't insert gesture to editable template fields");

		String chosenTemplate = "";
		List <String> allEditTemplates = new ArrayList<String>();

		// Get domains with editable templates
		CurrentPage = GetInstance(HomePage.class);
		HashMap<String,List<String>> domainsEditTemps = GetInstance(HomePage.class).getDomainWithGestureAndEditableTemplate(con);
		
		// Get the name of the domains and choose one
		List<String> domains = new ArrayList<String>();
		for ( String key : domainsEditTemps.keySet() ) 
				domains.add(key);	

		// Get app dropdown values
		 GetInstance(HomePage.class).isHomepageReady();
		List<String> smorphDomains = GetInstance(HomePage.class).getDomainValues();
		smorphDomains.remove("Select Domain");
		
		//*************************
		// Available domains for the user to use
		domains.retainAll(smorphDomains);
	
//		// Choose random domain
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size()));
//		String dropdownDomain ="";
//		// To get private domains correct name, since they are not stored with "(P)" in the DB
//		for(int i =0;i<smorphDomains.size();i++)
//			if(smorphDomains.get(i).contains(randomDomain))
//				dropdownDomain = smorphDomains.get(i);
//		
		GetInstance(HomePage.class).isHomepageReady();
		GetInstance(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "Select random domain: " + randomDomain);
		
		// Click the open button and check for open popup
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		CurrentPage.As(OpenPopup.class).openFileWait();
		
		//Choose the Template radio button
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		
	    // Choose random editable Template and click open
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		
		// Choose random editable template
		allEditTemplates = domainsEditTemps.get(randomDomain);
		random = new Random();
		chosenTemplate = allEditTemplates.get(random.nextInt(allEditTemplates.size()));
		LogUltility.log(test, logger, "Selected file: " + chosenTemplate);

		// Select from templates dropdown
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).SelectFromtemplateDropdown(chosenTemplate);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "Open template");
		
	    // Check that there is text displayed in the text box
		CurrentPage = GetInstance(HomePage.class);
		GetInstance(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean containsText = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + "text area is empty: " + containsText);
		assertFalse(containsText);
		
		// Highlight text in editable field
		WebElement chosenField =CurrentPage.As(HomePage.class).click_on_editable_field(CurrentPage.As(HomePage.class).Textinput);
		chosenField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		LogUltility.log(test, logger, "Highlight text in the editable field: "+chosenField.getText());
		
		// Click on gesture
		String gesture = GetInstance(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Click on gesture: "+gesture);
		
		// Get the text after trying to choose gesture
		String textAfterClick = CurrentPage.As(HomePage.class).Textinput.getText();
		LogUltility.log(test, logger, "Text after trying to insert a gesture: "+textAfterClick);
	
		// Verify that the gesture were not inserted
		boolean same = textAfterClick.equals(text);
		LogUltility.log(test, logger, "There is no gesture in the text: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * SRA-236:Check that user can't save empty text
		 * @throws InterruptedException 
		 * @throws AWTException 
	     * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
		public void  SRA236_Check_that_user_cant_save_empty_text() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
				
			test = extent.createTest("OpenOptionsTests-SRA-236", "Check that user can't save empty text");

			// Check if homepage is ready to use
			CurrentPage = GetInstance(HomePage.class);
			GetInstance(HomePage.class).isHomepageReady();
			
			// Press clear text button
			GetInstance(HomePage.class).clearText.click();
			LogUltility.log(test, logger, "Press clear text");
			  
			// Press the save button
			GetInstance(HomePage.class).SaveButton.click();
			LogUltility.log(test, logger, "Press the save button");
			
			// Check for warning message
			String expectedMessage = "There is no text to save     X";
			GetInstance(HomePage.class).fadedPopup(logger, expectedMessage);
	
			
	        LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * SRA-237:Verify that user can't save text that contains only gestures
		 * @throws InterruptedException 
		 * @throws AWTException 
	     * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
		public void  SRA237_Verify_that_user_cant_save_text_that_contains_only_gestures() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
				
			test = extent.createTest("OpenOptionsTests-SRA-237", "Verify that user can't save text that contains only gestures");

			// Check if homepage is ready to use
			CurrentPage = GetInstance(HomePage.class);
			GetInstance(HomePage.class).isHomepageReady();
			
			// Choose domain with gestures
			String randomDomain = GetInstance(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
			LogUltility.log(test, logger, "Choose domain: "+randomDomain);
			
			// Press clear text button
			GetInstance(HomePage.class).clearText.click();
			LogUltility.log(test, logger, "Press clear text");
			  
			// Insert gestures 
			GetInstance(HomePage.class).Textinput.click();
			String gesture = GetInstance(HomePage.class).chooseRandomGesture();
			LogUltility.log(test, logger, "Insert gesture: "+gesture);
			
			// Press the save button
			GetInstance(HomePage.class).SaveButton.click();
			LogUltility.log(test, logger, "Press the save button");
			
			// Check for warning message
			String expectedMessage = "There is no text to save     X";
			GetInstance(HomePage.class).fadedPopup(logger, expectedMessage);
	
			
	        LogUltility.log(test, logger, "Test Case PASSED");
		}
		@AfterMethod(alwaysRun = true)
	    public void getResult(ITestResult result, Method method) throws IOException
	    {	
			int testcaseID = 0;
			boolean writeToReport = false;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Open option",method.getName());
			System.out.println("tryCount: " + tryCount);
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
				writeToReport = true;
			}
			else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
				extent.removeTest(test);
				}
				else {
					writeToReport = true;
					}
			
			// Write to report
			if (writeToReport) {
				tryCount = 0;

				 // Write to report
				 if(result.getStatus() == ITestResult.FAILURE)
				    {
				        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
				        test.fail(result.getThrowable());
				        String screenShotPath = Screenshot.captureScreenShot();
				        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
				        // Send result to testlink
				        if (enableReportTestlink == true){
				        	try {
				        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
				            	LogUltility.log(test, logger, "Report to testtlink: " + response);
							} catch (Exception e) {
								LogUltility.log(test, logger, "Testlink Error: " + e);
							}
				        	}
				    }
				    else if(result.getStatus() == ITestResult.SUCCESS)
				    {
				        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
				        // Send result to testlink
				        if (enableReportTestlink == true){
				        	try {
				        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
				            	LogUltility.log(test, logger, "Report to testtlink: " + response);
				    		} catch (Exception e) {
				    			LogUltility.log(test, logger, "Testlink Error: " + e);
				    		}
				        	
				        	}
				    }
				    else
				    {
				        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
				        test.skip(result.getThrowable());
				    }
			        // Get console report
			        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
				    extent.flush();
			}	
			// Refresh the website for the new test
			DriverContext._Driver.navigate().refresh();
	    	}
	
			/**
			 * Closing the browser after running all the TCs
			 */
			@AfterClass(alwaysRun = true) 
			public void CloseBrowser() {
				 DriverContext._Driver.quit();
			}
}