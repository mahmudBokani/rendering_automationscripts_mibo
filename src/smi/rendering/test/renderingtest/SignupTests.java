package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.Mailinator;
import smi.rendering.test.pages.SelectPage;
import smi.rendering.test.pages.SignupPage;
import smi.rendering.test.pages.UMSPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

/**
 * All tests in Share folder
 *
 */
public class SignupTests extends FrameworkInitialize {

	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	
    /**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
     * @throws ClassNotFoundException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
		
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Signup", Setting.Browser);
		
	// Logger
	logger = LogManager.getLogger(SignupTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite = SignupTests ");
	logger.info("Signup Tests - FrameworkInitilize");
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL+"signup.html");
	DriverContext.browser.Maximize();

	CurrentPage= GetInstance(SignupPage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		DriverContext.browser.GoToUrl(Setting.AUT_URL+"signup.html");
		CurrentPage= GetInstance(SignupPage.class);

		//Thread.sleep(10000);
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}

	/**
	 * SRA 8: Verify user can login after signing up successfully
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA8_Verify_user_can_login_after_signing_up_successfully() throws InterruptedException, FileNotFoundException, SQLException
	{
		test = extent.createTest("SignupTests-SRA-8", "Verify user can login after signing up successfully");
		
		DriverContext.browser.GoToUrl(Setting.AUT_URL+"signup.html");
		DriverContext.browser.Maximize();
		
		// Fill the fields with correct info
		String lang = Setting.Language;
		Setting.Language = "6";
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@mailinator.com";
		String password = generateInput.RandomString(6, "letters");
		Setting.Language = lang;
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage = GetInstance(SignupPage.class);
		Thread.sleep(6000);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
		
		//Thread.sleep(10000);
		
		CurrentPage.As(SignupPage.class).checkSignupSuccessMessage(test, logger);
		
		// Check the signup confirmation message : Thank you for signing up! A confirmation email was sent to your email address. Please check your inbox
//		String confirmationMessage = CurrentPage.As(SignupPage.class).msgSignupConfirmation.getText();
//		LogUltility.log(test, logger, "Check the confirmation message: " + confirmationMessage);
//		boolean SignUpSuccessfully = confirmationMessage.contains("Thank you for signing up!");
//		LogUltility.log(test, logger, "Check the SignUpSuccessfully is True: " + SignUpSuccessfully);
//		assertTrue(SignUpSuccessfully);
		
		// Navigate to mailinator website
		CurrentPage = GetInstance(Mailinator.class);
		LogUltility.log(test, logger, "Navigate to mailinator");
		DriverContext.browser.GoToUrl("http://www.mailinator.com/");
		
		// Fill the email field and click Go
		Thread.sleep(5000);
		LogUltility.log(test, logger, "Fill the registered email: " + email + ",and click the activation message");
		CurrentPage.As(Mailinator.class).FillInboxField(email);
		
		// Click the received message
		//Thread.sleep(10000);
		LogUltility.log(test, logger, "Click the received confirmation email");
		CurrentPage.As(Mailinator.class).lnkEmailTitle.click();

		// Click the activate link
		//Thread.sleep(5000);
		DriverContext._Driver.switchTo().frame("msg_body");
		CurrentPage.As(Mailinator.class).lnkActivate.click();
		
		// Check the activation success message
		//Thread.sleep(3000);
		CurrentPage = GetInstance(UMSPage.class);
		CurrentPage.As(UMSPage.class).focusOnLastOpenedTab();
//		CurrentPage.As(UMSPage.class).reloadUMS();
		String SuccessMessage = CurrentPage.As(UMSPage.class).msgActivate.getText();
		LogUltility.log(test, logger, "Success message: " + SuccessMessage);
		Assert.assertEquals(SuccessMessage, "Congratulations, Your account is activated.");
//		Assert.assertEquals(SuccessMessage, "Your account was already activated!");
		
		// Login with the activated account
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Login with the activated account");
		DriverContext.browser.GoToUrl(Setting.AUT_URL); 
		
		// Fill the login fields and click the submit button
		//Thread.sleep(3000);
		CurrentPage = GetInstance(LoginPage.class);
		CurrentPage = CurrentPage.As(LoginPage.class).Login(email, password);
		
		// Wait for the select page to load and check if user logged in 
		CurrentPage = GetInstance(SelectPage.class);
		String projectLabel = CurrentPage.As(SelectPage.class).lblProject.getText();
		String domainLabel = CurrentPage.As(SelectPage.class).lblDomain.getText();
		assertEquals(projectLabel,"Project");
		assertEquals(domainLabel,"Domain");
		
		
		String LogoutText = CurrentPage.As(SelectPage.class).lnkLogout.getText();
		LogUltility.log(test, logger, "Check if logged in successfully: " + LogoutText);
		Assert.assertEquals(LogoutText, "Logout");
		
		LogUltility.log(test, logger, "Test Case PASSED");
}
	
	/**
	 * SRA-143:Check message when reactivating an account
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA143_Check_message_when_reactivating_an_account() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-143", "Check message when reactivating an account");
		
		DriverContext.browser.GoToUrl(Setting.AUT_URL+"/signup.html");
		//Thread.sleep(5000);
		
		// Fill the fields with correct info
		CurrentPage = GetInstance(SignupPage.class);
		String lang = Setting.Language;
		Setting.Language = "6"; 
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@mailinator.com";
		String password = generateInput.RandomString(6, "letters");
		Setting.Language = lang;
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
		
		CurrentPage.As(SignupPage.class).checkSignupSuccessMessage(test, logger);
		
		//Thread.sleep(10000);
//		
//		// Check the signup confirmation message : Thank you for signing up! A confirmation email was sent to your email address. Please check your inbox
//		String confirmationMessage = CurrentPage.As(SignupPage.class).msgSignupConfirmation.getText();
//		LogUltility.log(test, logger, "Check the confirmation message: " + confirmationMessage);
//		boolean SignUpSuccessfully = confirmationMessage.contains("Thank you for signing up!");
//		LogUltility.log(test, logger, "Check the SignUpSuccessfully is True: " + SignUpSuccessfully);
//		assertTrue(SignUpSuccessfully);
		
		// Navigate to mailinator website
		CurrentPage = GetInstance(Mailinator.class);
		LogUltility.log(test, logger, "Navigate to mailinator");
		DriverContext.browser.GoToUrl("http://www.mailinator.com/");
		
		// Fill the email field and click Go
		LogUltility.log(test, logger, "Fill the registered email: " + email + ",and click the activation message");
		CurrentPage.As(Mailinator.class).FillInboxField(email);
		
		// Click the received message
		//Thread.sleep(10000);
		LogUltility.log(test, logger, "Click the received confirmation email");
		CurrentPage.As(Mailinator.class).lnkEmailTitle.click();

		// Click the activate link
		//Thread.sleep(5000);
		DriverContext._Driver.switchTo().frame("msg_body");
		CurrentPage.As(Mailinator.class).lnkActivate.click();
		
		// Check the activation success message
		//Thread.sleep(3000);
		CurrentPage = GetInstance(UMSPage.class);
		CurrentPage.As(UMSPage.class).focusOnLastOpenedTab();
//		CurrentPage.As(UMSPage.class).reloadUMS();
		String SuccessMessage = CurrentPage.As(UMSPage.class).msgActivate.getText();
		LogUltility.log(test, logger, "Success message: " + SuccessMessage);
		Assert.assertEquals(SuccessMessage, "Congratulations, Your account is activated.");
//		Assert.assertEquals(SuccessMessage, "Your account was already activated!");
		
		// GO AND CLICK ON THE ACTIVATIN LINK AGAIN
		// Navigate to mailinator website
		CurrentPage = GetInstance(Mailinator.class);
		LogUltility.log(test, logger, "Navigate to mailinator");
		DriverContext.browser.GoToUrl("http://www.mailinator.com/");
		
		// Fill the email field and click Go
		LogUltility.log(test, logger, "Fill the registered email: " + email + ",and click the activation message");
		CurrentPage.As(Mailinator.class).FillInboxField(email);
		
		// Click the received message
		//Thread.sleep(10000);
		LogUltility.log(test, logger, "Click the received confirmation email");
		CurrentPage.As(Mailinator.class).lnkEmailTitle.click();

		// Click the activate link
		//Thread.sleep(5000);
		DriverContext._Driver.switchTo().frame("msg_body");
		CurrentPage.As(Mailinator.class).lnkActivate.click();
		
		// Check the already activated message
		//Thread.sleep(3000);
		CurrentPage = GetInstance(UMSPage.class);
		CurrentPage.As(UMSPage.class).focusOnLastOpenedTab();
		DriverContext._Driver.switchTo().defaultContent();
		
		String errorMessage = CurrentPage.As(UMSPage.class).msgActivate.getText();
		LogUltility.log(test, logger, "Activation message: " + errorMessage);
		Assert.assertEquals(errorMessage, "Your account was already activated!");
		LogUltility.log(test, logger, "Test Case PASSED");
}
	
	/**
	 * SRA 7: Verify user can sign up successfully to the App
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA7_Verify_user_can_sign_up_successfully_to_the_App() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-7", "Verify user can sign up successfully to the App");
		
		DriverContext.browser.GoToUrl(Setting.AUT_URL+"/signup.html");
		//Thread.sleep(5000);
		
		// Fill the fields with correct info
		CurrentPage = GetInstance(SignupPage.class);
		String lang = Setting.Language;
		Setting.Language = "6";
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@mailinator.com";
		String password = generateInput.RandomString(6, "letters");
		Setting.Language = lang;
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
		
		// Check the signup confirmation message
		CurrentPage.As(SignupPage.class).checkSignupSuccessMessage(test, logger);
		
		//Thread.sleep(10000);
//		String confirmationMessage = CurrentPage.As(SignupPage.class).msgSignupConfirmation.getText();
//		LogUltility.log(test, logger, "Check the confirmation message: " + confirmationMessage);
//		boolean messageFound = confirmationMessage.contains("Thank you for signing up!") &&
//				confirmationMessage.contains("A confirmation email was sent to your email address. Please check your inbox");
//		LogUltility.log(test, logger, "Success message is displayed: " + messageFound);
//		assertTrue(messageFound);
		
		// Click back to login link
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Go to the login page and fill with registered info - email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).lnkBackToLogin.click();
		
		// Fill the login fields and click the submit button
		//Thread.sleep(5000);
		CurrentPage= GetInstance(LoginPage.class);
		CurrentPage.As(LoginPage.class).Login(email, password);
		
		// Check the system message
		//Thread.sleep(3000);
		String ErrorMessage = CurrentPage.As(LoginPage.class).ActivationMessage.getText();
		LogUltility.log(test, logger, "Error message: " + ErrorMessage);
//		Assert.assertEquals(ErrorMessage, "Account is not yet active. Check your email for activation instructions.");
		Assert.assertEquals(ErrorMessage, "Error:please activate your account.");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA 9: Verify user can't sign up with blank first name
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA9_Verify_user_cant_sign_up_with_blank_first_name() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-9", "Verify user can't sign up with blank first name");
		
		// Fill the fields
		CurrentPage= GetInstance(SignupPage.class);
		String fname = "";
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@gmail.com";
		String password = generateInput.RandomString(6, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage= GetInstance(SignupPage.class);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
    	
		// Get the error message
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgFnameError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have not answered all required fields");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 10: Verify user can't sign up with blank last name
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA10_Verify_user_cant_sign_up_with_blank_last_name() throws InterruptedException, FileNotFoundException 
	{
		test = extent.createTest("SignupTests-SRA-10", "Verify user can't sign up with blank last name");
		
		// Fill the fields
		String fname = generateInput.RandomString(5, "letters");
		String lname = "";
		String email = generateInput.RandomString(5, "letters") + "@gmail.com";
		String password = generateInput.RandomString(6, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);	
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
    	
		// Get the error message
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgLnameError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have not answered all required fields");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 11: Verify user can't sign up with blank Email
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA11_Verify_user_cant_sign_up_with_blank_Email() throws InterruptedException, FileNotFoundException
	{	
		test = extent.createTest("SignupTests-SRA-11", "Verify user can't sign up with blank Email");
		
		// Fill the fields
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = "";
		String password = generateInput.RandomString(6, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
    	
		// Get the error message
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgEmailError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have not given a correct e-mail address");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 12: Verify user can't sign up with exist email in SRA system
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA12_Verify_user_cant_sign_up_with_exist_email_in_SRA_system() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-12", "Verify user can't sign up with exist email in SRA system");
		
		// Fill the fields
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = "qwer@mailinator.com";
		String password = generateInput.RandomString(6, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
    	
		// Get the error message
//		Thread.sleep(3000);
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgEmailError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "user already registered");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 13: Verify user can't sign up with invalid email
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA13_Verify_user_cant_sign_up_with_invalid_email() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-13", "Verify user can't sign up with invalid email");
		
		// Fill the fields
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@gmail";
		String password = generateInput.RandomString(6, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
    	
		// Get the error message
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgEmailError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have not given a correct e-mail address");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 14: Verify user can't sign up with blank password
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA14_Verify_user_cant_sign_up_with_blank_password() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-14", "Verify user can't sign up with blank password");
		
		// Fill the fields
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@gmail.com";
		String password = generateInput.RandomString(6, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, "", password);
    	
		// Get the error message
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgPasswordError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have given an answer shorter than 6 characters");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 15: Verify user can't sign up with too short password
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA15_Verify_user_cant_sign_up_with_too_short_password() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-15", "Verify user can't sign up with too short password");
		
		// Fill the fields
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@gmail.com";
		String password = generateInput.RandomString(3, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
    	
		// Get the error message
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgPasswordError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have given an answer shorter than 6 characters");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 16: Verify user can't sign up with blank confirmation password
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA16_Verify_user_cant_sign_up_with_blank_conformation_password() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-16", "Verify user can't sign up with blank confirmation password");
		
		// Fill the fields
		String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@gmail.com";
		String password = generateInput.RandomString(6, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, "");
    	
		// Get the error message
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgPasswordConfirmError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "The two passwords don't match");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 17: Verify user can't sign up with invalid confirmation password
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA17_Verify_user_cant_sign_up_with_invalid_conformation_password() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-17", "Verify user can't sign up with invalid confirmation password");
		
		// Fill the fields
		String fname = "";
		String lname = "";
		String email = generateInput.RandomString(5, "letters");
		String password = generateInput.RandomString(3, "letters");
		String confirmationPass = generateInput.RandomString(3, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, confirmationPass);
    	
		// Get the error message
		String ErrorMessage = CurrentPage.As(SignupPage.class).msgPasswordConfirmError.getText();
		LogUltility.log(test, logger, "Get the error message: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "The two passwords don't match");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 126: Verify user can't sign up with invalid blank fields
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA126_Verify_user_cant_sign_up_with_invalid_blank_fields() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("SignupTests-SRA-126", "Verify user can't sign up with invalid blank fields");
		
		// Fill the fields
		String fname = "";
		String lname = "";
		String email = generateInput.RandomString(5, "letters");
		String password = generateInput.RandomString(3, "letters");
		String confirmationPass = generateInput.RandomString(3, "letters");
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, confirmationPass);
    	
		String ErrorMessage = "";
		// Check fname field
		ErrorMessage = CurrentPage.As(SignupPage.class).msgFnameError.getText();
		LogUltility.log(test, logger, "Error message for fname: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have not answered all required fields");
		
		// Check lname field
		ErrorMessage = CurrentPage.As(SignupPage.class).msgLnameError.getText();
		LogUltility.log(test, logger, "Error message for lname: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have not answered all required fields");
		
		// Check email field
		ErrorMessage = CurrentPage.As(SignupPage.class).msgEmailError.getText();
		LogUltility.log(test, logger, "Error message for email: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have not given a correct e-mail address");
		
		// Check password field
		ErrorMessage = CurrentPage.As(SignupPage.class).msgPasswordError.getText();
		LogUltility.log(test, logger, "Error message for password: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "You have given an answer shorter than 6 characters");
		
		// Check password confirmation field
		ErrorMessage = CurrentPage.As(SignupPage.class).msgPasswordConfirmError.getText();
		LogUltility.log(test, logger, "Error message for password confirmation: " + ErrorMessage);
		Assert.assertEquals(ErrorMessage, "The two passwords don't match");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
		
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {				
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Sign up",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		     
			  // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 DriverContext._Driver.quit();
	    }
}