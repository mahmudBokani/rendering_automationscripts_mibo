package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import jxl.read.biff.BiffException;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SelectPage;

/**
 * All tests in Domain folder
 *
 */
public class ProjectDomainSelectTests extends FrameworkInitialize {
	
	private static Logger logger = null;
	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	private String usernameLogin = "";
	private String chosenProject = "";
	public boolean isThereMoreThanOneAssignedDomain;
	
	/**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();

	// Initialize the report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Domain", Setting.Browser);
	
	// Logger
	logger = LogManager.getLogger(DomainTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite= Project Domain Select Tests ");
	logger.info("Domain Tests - FrameworkInitilize");
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	// Check if we have only one domain within one project
	isThereMoreThanOneAssignedDomain =   GetInstance(HomePage.class).As(HomePage.class).isThereMoreThanOneAssignedDomain(con);
		
	// Login only once then run the following tests
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	generateInput = new RandomText();
	
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}
	
	
	
	/**
	 * SRA-199:Verify that selecting a default domain will set the main page domain dropdown to same value
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA199_Verify_that_selecting_a_default_domain_will_set_the_main_page_domain_dropdown_to_same_value() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("ProjectDomainSelection-SRA-199", "Verify that selecting a default domain will set the main page domain dropdown to same value");
		
		if (!isThereMoreThanOneAssignedDomain) {
			LogUltility.log(test, logger, "Skip this test, can't stop at select page");
			return;
		}
		
		// Initialize select page
		CurrentPage= GetInstance(SelectPage.class);
		SelectPage selectPage = CurrentPage.As(SelectPage.class);
		
		// Select random project
		String project = selectPage.chooseRandomProject(con);
		logger.info("Choose Project: " + project);
		
		// Get the available domains under the project
		ArrayList<String> availableDomains = selectPage.getDomainsProjectForUser(con, project, usernameLogin);
		
		// If it has one domain then it will be chosen automatically else choose one
		String selectPageDomain ="";
		if(availableDomains.size()>1)
		{
			// Select random domain
			selectPageDomain = selectPage.chooseRandomDomain();

		}
		else
			selectPageDomain = availableDomains.get(0);
		
		logger.info("Choose Domain in Select Page: " + selectPageDomain);
		logger.info("Navigating to the main page");
		
		// Home page displayed
		CurrentPage= GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);
		homePage.isHomepageReady();
		
		// Get the home page selected domain name
		String homePageDomain = homePage.cmbDomain.getText();
		boolean sameValue = homePageDomain.equals(selectPageDomain);
		logger.info("Domain name in Home Page: "+homePageDomain);
		logger.info("Chosen domain in select page set as default in home page: "+sameValue);
		assertTrue(sameValue);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
		
	 }
	
	/**
	 * SRA-200:verify that chosen domain choses the related language in the next page
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA200_verify_that_chosen_domain_choses_the_related_language_in_the_next_page() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("ProjectDomainSelection-SRA-200", "Verify that chosen domain choses the related language in the next page");
		
		if (!isThereMoreThanOneAssignedDomain) {
			LogUltility.log(test, logger, "Skip this test, can't stop at select page");
			return;
		}
		
		// Initialize select page
		CurrentPage= GetInstance(SelectPage.class);
		SelectPage selectPage = CurrentPage.As(SelectPage.class);
		
		// Select random project
		String project = selectPage.chooseRandomProject(con);
		logger.info("Choose Project: " + project);
		
		// Get the available domains under the project
		ArrayList<String> availableDomains = selectPage.getDomainsProjectForUser(con, project, usernameLogin);
		
		// If it has one domain then it will be chosen automatically else choose one
		String selectPageDomain ="";
		if(availableDomains.size()>1)
		{
			// Select random domain
			selectPageDomain = selectPage.chooseRandomDomain();

		}
		else
			selectPageDomain = availableDomains.get(0);
		
		logger.info("Choose Domain in Select Page: " + selectPageDomain);
		logger.info("Navigating to the main page");
		

		// Home page displayed
		CurrentPage= GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);
		homePage.isHomepageReady();
		// Get the domain language
		String domainLanguageDB = homePage.getDomainLanguage(con, selectPageDomain);
		logger.info("The domain language is from DB: "+domainLanguageDB);
		
		// Get the home page selected language 
		String homePageLanguage = homePage.cmbLanguage.getText();
		boolean sameValue = homePageLanguage.equals(domainLanguageDB);
		logger.info("Domain language in Home Page: "+homePageLanguage);
		logger.info("Is the chosen language in dropdown equals the domain language DB value: "+sameValue);
		assertTrue(sameValue);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-201:verify that chosen domain chooses the related instructions
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA201_verify_that_chosen_domain_chooses_the_related_instructions() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("ProjectDomainSelection-SRA-201", "Verify that chosen domain chooses the related instructions");
		
		if (!isThereMoreThanOneAssignedDomain) {
			LogUltility.log(test, logger, "Skip this test, can't stop at select page");
			return;
		}
		
		// Initialize select page
		CurrentPage= GetInstance(SelectPage.class);
		SelectPage selectPage = CurrentPage.As(SelectPage.class);
		
		// Select random project
		String project = selectPage.chooseRandomProject(con);
		logger.info("Choose Project: " + project);
		
		// Get the available domains under the project
		ArrayList<String> availableDomains = selectPage.getDomainsProjectForUser(con, project, usernameLogin);
		
		// If it has one domain then it will be chosen automatically else choose one
		String selectPageDomain ="";
		if(availableDomains.size()>1)
		{
			// Select random domain
			selectPageDomain = selectPage.chooseRandomDomain();

		}
		else
			selectPageDomain = availableDomains.get(0);
		
		logger.info("Choose Domain in Select Page: " + selectPageDomain);
		logger.info("Navigating to the main page");
		

		// Home page displayed
		CurrentPage= GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);
		homePage.isHomepageReady();
		
		// Get the domain language
		String domainLanguageDB = homePage.getDomainLanguage(con, selectPageDomain);
		logger.info("The domain language is: "+domainLanguageDB);

		String currentText = homePage.Textinput.getText();
		boolean containText = false;
		if(domainLanguageDB.equals("English"))
			 containText = currentText.contains("Select a voice and a domain from the menu on the left, and then either open an existing voice text file, or just type some text right into this box");
			if(domainLanguageDB.equals("עברית"))
				{
					String hebrew_inst = CurrentPage.As(HomePage.class).getHebrewInstruction();
					containText= currentText.contains(hebrew_inst);
				}
			
			LogUltility.log(test, logger, "The instructions: " + currentText);  
	        LogUltility.log(test, logger, "Insructions are displayed in the correct language : " + containText);
	        assertTrue(containText);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-203:verify that chosen project displays the related domains in the domain dropdown
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA203_verify_that_chosen_project_displays_the_related_domains_in_the_domain_dropdown() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("ProjectDomainSelection-SRA-203", "verify that chosen project displays the related domains in the domain dropdown");
		
		if (!isThereMoreThanOneAssignedDomain) {
			LogUltility.log(test, logger, "Skip this test, can't stop at select page");
			return;
		}
		
		// Initialize select page
		CurrentPage= GetInstance(SelectPage.class);
		SelectPage selectPage = CurrentPage.As(SelectPage.class);
		
		// Select random project
		String project = selectPage.chooseRandomProject(con);
		logger.info("Choose Project: " + project);
		
		// Get the available domains under the project from DB
		ArrayList<String> availableDbDomains = selectPage.getDomainsProjectForUser(con, project, usernameLogin);
		Collections.sort(availableDbDomains);
		logger.info("DB domains: " + availableDbDomains);
		
		boolean same = false ;
		ArrayList<String> availableDropdownDomains = new ArrayList<String>();
		
		//There is only one domain, it chooses the domain automatically 
		if(availableDbDomains.size() == 1)
		{
			// Check if the chosen domain in dropdown equals the one in DB
			CurrentPage= GetInstance(HomePage.class);
			HomePage homePage = CurrentPage.As(HomePage.class);
			homePage.isHomepageReady();
			String chosenDom = homePage.cmbDomain.getText().replace(" (P)", "");
			logger.info("Dropdown domains: " + chosenDom);
			same = chosenDom.equals(availableDbDomains.get(0));
		}
		
		else {
		
		// Get domains from domain dropdown
		availableDropdownDomains = (ArrayList<String>) selectPage.getDomainValues();
		for(int i=0;i<availableDropdownDomains.size();i++)
			availableDropdownDomains.set(i, availableDropdownDomains.get(i).replace(" (P)", ""));
		Collections.sort(availableDropdownDomains);
		logger.info("Dropdown domains: " + availableDropdownDomains);
		same = availableDbDomains.equals(availableDropdownDomains);
		
		}

		logger.info("Dropdown values matches DB values: " + same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 * SRA-217:Verify that private domains only end with “(P)”
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class) 
	public void SRA217_Verify_that_private_domains_only_end_with_P() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{
		test = extent.createTest("ProjectDomainSelection-SRA-217", "Verify that private domains only end with “(P)”");
		
		if (!isThereMoreThanOneAssignedDomain) {
			LogUltility.log(test, logger, "Skip this test, can't stop at select page");
			return;
		}
		
		// Initialize select page
		CurrentPage= GetInstance(SelectPage.class);
		SelectPage selectPage = CurrentPage.As(SelectPage.class);
		
		// Select random project
		String project = selectPage.chooseRandomProject(con);
		logger.info("Choose Project: " + project);
		
		// Get the available domains under the project
		ArrayList<String> availableDomains = selectPage.getDomainsProjectForUser(con, project, usernameLogin);		
				
		// Get the available private domains under the project from DB
		ArrayList<String> availablePrivateDomains = selectPage.getProjectPrivateDomainsForUser(con, project, usernameLogin);
		
		// Add (P) to the Database private domains
		for(int i=0;i<availablePrivateDomains.size();i++)
			availablePrivateDomains.set(i, availablePrivateDomains.get(i).concat(" (P)"));
		
		logger.info("DB private domains: " + availablePrivateDomains);
		
		boolean contains = false ;
		ArrayList<String> availableDropdownDomains = new ArrayList<String>();
		
		//There is only one domain, it chooses the domain automatically 
		if(availableDomains.size() == 1)
		{
			// Check if the chosen domain in dropdown equals the one in DB
			CurrentPage= GetInstance(HomePage.class);
			HomePage homePage = CurrentPage.As(HomePage.class);
			homePage.isHomepageReady();
			String chosenDom = homePage.cmbDomain.getText();
			logger.info("Dropdown private domain: " + chosenDom);
			contains = chosenDom.equals(availablePrivateDomains.get(0));
		}
		
		else {
		
		// Get domains from domain dropdown
		availableDropdownDomains = (ArrayList<String>) selectPage.getDomainValues();
		logger.info("Dropdown domains: " + availableDropdownDomains);
		
		for(int i=0;i<availablePrivateDomains.size();i++)
			if(availableDropdownDomains.contains(availablePrivateDomains.get(i)))
				contains = true;
		}

		logger.info("Dropdown private domains matches DB private domains: " + contains);
		assertTrue(contains);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {			
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Project/Domain page",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
			}
}
