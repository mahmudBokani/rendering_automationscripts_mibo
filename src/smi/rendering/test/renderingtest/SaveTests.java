package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SavePopup;
import smi.rendering.test.pages.SelectPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * All tests in Share folder
 *
 */
public class SaveTests extends FrameworkInitialize {

	private static Logger logger = null;
	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	private String usernameLogin = "";
	private String chosenProject = "";	
	private String chosenAccount = "";
	public boolean isThereMoreThanOneAssignedDomain;
	
    /**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
     * @throws ClassNotFoundException 
     * @throws SQLException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, SQLException {
	freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
	ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Save", Setting.Browser);
	
	// Logger
	logger = LogManager.getLogger(SaveTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite = SaveTests ");
	logger.info("Save Tests - FrameworkInitilize");
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	
	// Get account,project and domain with the requested language and logged-in email
	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
	chosenProject = account_project_domain.get(index).get(1);
	chosenAccount = account_project_domain.get(index).get(0);
	CurrentPage= GetInstance(HomePage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		CurrentPage= GetInstance(HomePage.class);
		//Thread.sleep(10000);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}

	/**
	 * SRA-34:Check the cancel option
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA34_check_the_cancel_option() throws InterruptedException, SQLException, FileNotFoundException 
	{     
		test = extent.createTest("SaveTests-SRA-34", "Check the cancel option");
		
		// Select a voice
		//Thread.sleep(2000);
		String randomVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Random voice: " + randomVoice);
				
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Random domain: " + randomDomain);
        		
		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
//		// Press the play button
//		//Thread.sleep(2000);
//		LogUltility.log(test, logger, "Click the Play button");
//		CurrentPage.As(HomePage.class).PlayButton.click();
//		//Thread.sleep(20000);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Check that save window is displayed
		Thread.sleep(4000);
    	LogUltility.log(test, logger, "verify that the save title appear in the Save popup"); 
    	CurrentPage = GetInstance(SavePopup.class);
    	String saveTitle = CurrentPage.As(SavePopup.class).Saveheader.getText();
    	LogUltility.log(test, logger, "Save popup title: " + saveTitle);
    	assertEquals(saveTitle, "Save");
		
		// Enter file name and press the Cancel button
    	//Thread.sleep(2000);
//		String fileName = generateInput.RandomString(5, "letters");
//    	String fileName = RandomText.getSentencesFromFile();
    	String fileName = generateInput.RandomString(5, "letters");
		LogUltility.log(test, logger, "Enter a file name" + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).CancelBtn.click();
		//Thread.sleep(2000);
		
		// Check save popup is closed
		boolean isDisplayed = CurrentPage.As(SavePopup.class).Saveheader.isDisplayed();
		LogUltility.log(test, logger, "isDisplayed: " + isDisplayed);
		assertFalse(isDisplayed);
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA-67:Check the save option
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA67_check_the_save_option() throws InterruptedException, SQLException, FileNotFoundException 
	{	
		test = extent.createTest("SaveTests-SRA-67", "Check the save option");

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name" + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-68:Check the saved text stored in the DB
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA68_Check_the_saved_text_stored_in_the_DB() throws InterruptedException, SQLException, FileNotFoundException 
	{	
		test = extent.createTest("SaveTests-SRA-68", "Check the saved text stored in the DB");

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name : " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
		
		// Verify that the filename saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> File_name =CurrentPage.As(SavePopup.class).getFileName(con,fileName);
        LogUltility.log(test, logger, "results from DB" + File_name);
        assertEquals(fileName, File_name.get(1).get(0));
        LogUltility.log(test, logger, "Test Case PASSED");
	}
			
	/**
	 * SRA-69:Check the texts are saved in SSML format in the DB
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 */
	 @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA69_Check_the_texts_saved_in_SSML_format_in_the_DB() throws InterruptedException, SQLException, FileNotFoundException 
	 {	 
		 test = extent.createTest("SaveTests-SRA-69", "Check the texts are saved in SSML format in the DB");

		// Choose domain
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "Choose domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name : "  + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");

		// Verify that the filename saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, fileName,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);
        
        // Check that the saved text contain the text entered by the user
        String listResults= Saved_text.get(1).get(0);
        LogUltility.log(test, logger, "list of results from DB : " + listResults);
        boolean containText= listResults.contains(randomText);
        LogUltility.log(test, logger, "containText should be true : " + containText);
        assertTrue(containText);
        boolean containSsml= listResults.contains("</speak>");
        LogUltility.log(test, logger, "containSsml should be true : " + containSsml);
        assertTrue(containSsml); 
        LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 * SRA-70:Check the SSML tags of the Mood in DB
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA70_Check_the_SSML_tags_of_the_mood_in_DB() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("SaveTests-SRA-70", "Check the SSML tags of the Mood in DB");
	
		// Check if the mood exist due a bug, if not then refresh once
//		try {
//			CurrentPage.As(HomePage.class).getMoodValues();
//		} catch (Exception e) {
//			LogUltility.log(test, logger, "Page was refreshed once due missing Moods");
//			DriverContext._Driver.navigate().refresh();
//			Thread.sleep(10000);
//		}
		
		CurrentPage = GetInstance(HomePage.class);
		//CurrentPage.As(HomePage.class).doMoodExist();

		// Select a domain
		CurrentPage.As(HomePage.class).isHomepageReady();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMood(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// press the mood button and select one of the moods 
		Thread.sleep(2000);
		String choice = CurrentPage.As(HomePage.class).chooseRandomMood();
		LogUltility.log(test, logger, "Chosen Mood: " + choice);
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name : "  + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();

		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");

		// Verify that the filename saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, fileName,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);
        
        // Check that the saved text contain the mood entered by the user
        String listResults= Saved_text.get(1).get(0);
        LogUltility.log(test, logger, "list of results from DB: " + listResults);
        boolean containText= listResults.contains(randomText);
        LogUltility.log(test, logger, "containText should be true: " + containText);
        assertTrue(containText);
//        boolean containSsml= listResults.contains(choice);
//        LogUltility.log(test, logger, "containSsml should be true: " + containSsml);
//        assertTrue(containSsml);
//        CurrentPage = GetInstance(HomePage.class);
//        String MoodType= CurrentPage.As(HomePage.class).txtMood.getAttribute("data-mood");
//        String MoodTypeName = CurrentPage.As(HomePage.class).convertIDtoMood(con, MoodType);
//		LogUltility.log(test, logger, "MoodType: " + MoodType + ", MoodTypeName: " + MoodTypeName);
//		assertEquals(MoodTypeName, choice);
		
    
        String []fetchMoodsRight = listResults.split("detail=\"");
        String []fetchMoodsLeft = fetchMoodsRight[1].split("\">");
   //   String []fetchMoodsRight = listResults.split("<mood value=\"");  
       // String []fetchMoodsLeft = fetchMoodsRight[1].split("\">");
        String MoodType = fetchMoodsLeft[0];
        CurrentPage = GetInstance(HomePage.class);
		String MoodTypeName = CurrentPage.As(HomePage.class).convertIDtoMood(con, MoodType);
		LogUltility.log(test, logger, "MoodType: " + MoodType + "MoodTypeName: " + MoodTypeName);
		assertEquals(MoodTypeName, choice);
		  
        LogUltility.log(test, logger, "Test Case PASSED");
	 }

	/**
	 * SRA-71:Check the SSML tags of the Emphasis in DB
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA71_Check_the_SSML_tags_of_the_Emphasis_in_DB() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("SaveTests-SRA_71", "Check the SSML tags of the Emphasis in DB");

		// Choose domain
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
//		String randomDomain = "System Project (P)";
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		
		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		LogUltility.log(test, logger, "highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Press the bold button and select one of the moods
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Press the bold button ");
		CurrentPage.As(HomePage.class).Boldbtn.click();
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name : "  + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");

		// Verify that the filename saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, fileName,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);
       
        // Check that the saved text contain the mood entered by the user
        String listResults= Saved_text.get(1).get(0);
        LogUltility.log(test, logger, "list of results from DB : " + listResults);
        boolean containText= listResults.contains(randomText);
        LogUltility.log(test, logger, "containText should be true : " + containText);
        assertTrue(containText);
        boolean containSsml= listResults.contains("</emphasis>");
        LogUltility.log(test, logger, "containSsml should be true : " + containSsml);
        assertTrue(containSsml);
        LogUltility.log(test, logger, "Test Case PASSED");
	 }
	  
	/**
	 * SRA-72:Check the SSML tags of the Gesture in DB
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	 @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA72_Check_the_SSML_tags_of_the_Gesture_in_DB() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	 {
		 test = extent.createTest("SaveTests-SRA-72", "Check the SSML tags of the Gesture in DB");
		 
		// Select a domain
		 String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Choose random gesture
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
		//Thread.sleep(1000);
		String chosenGesture = CurrentPage.As(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Chosen Gesture: " + chosenGesture);
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name : "  + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		//Thread.sleep(2000);
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");

		// Verify that the filename saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, fileName,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);
        
        // Check that the saved text contain the Gesture entered by the user
        Thread.sleep(2000);
        String listResults= Saved_text.get(1).get(0);
        LogUltility.log(test, logger, "list of results from DB : " + listResults);
        boolean containText= listResults.contains(randomText);
        LogUltility.log(test, logger, "containText should be true : " + containText);
        assertTrue(containText);
//        boolean containSsml= listResults.contains(chosenGesture);
//        LogUltility.log(test, logger, "containSsml should be true : " + containSsml);
//        assertTrue(containSsml);

        String []fetchGesturesRight = listResults.split("detail=\"");
        String []fetchGesturesLeft = fetchGesturesRight[1].split("\">");
//        String []fetchGesturesRight = listResults.split("<gesture value=\"");
//        String []fetchGesturesLeft = fetchGesturesRight[1].split("\"></gesture>");
        String GestureType = fetchGesturesLeft[0];
        CurrentPage = GetInstance(HomePage.class);
		String GestureTypeName = CurrentPage.As(HomePage.class).convertIDtoGestures(con, GestureType);
		LogUltility.log(test, logger, "GestureType: " + GestureType + "GestureTypeName: " + GestureTypeName);
		assertEquals(GestureTypeName, chosenGesture);
        LogUltility.log(test, logger, "Test Case PASSED");
	 }

	 /**
	 *  SRA-107:Verify user can save imported text
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA107_Verify_user_can_save_imported_text() throws InterruptedException, AWTException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("SaveTests-SRA-107", "Verify user can save imported text");
		
		// Select random domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
		LogUltility.log(test, logger, "Choose random domain: " + randomDomain);
		
		// Click the open button and check for open popup
		Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();

		// Choose random file and click open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).openFileWait();
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		// Click Open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
		// Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean isEmpty = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + isEmpty);
		assertFalse(isEmpty);

		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();

		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		CurrentPage.As(SavePopup.class).FileName.clear();
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
//		String fileName = RandomText.getSentencesFromFile().split(" ")[0];
		LogUltility.log(test, logger, "Enter a file name : "  + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();

		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
        
        // Verify that the filename saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> File_name =CurrentPage.As(SavePopup.class).getFileName(con,fileName);
        LogUltility.log(test, logger, "results from DB" + File_name);
        assertEquals(fileName, File_name.get(1).get(0));
        
		// Verify that the text saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, fileName,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);
        boolean doExist = Saved_text.get(1).get(0).contains(text);
        LogUltility.log(test, logger, "Is text found in DB: " + doExist);
        assertTrue(doExist);
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	 
 	/**
 	 * SRA-113:Verify saving blank text box displays popup
 	 * @throws InterruptedException
 	 * @throws SQLException 
 	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA113_Verify_saving_blank_text_box_displays_popup() throws InterruptedException, SQLException 
	{
		test = extent.createTest("SaveTests-SRA-113", "Verify saving blank text box displays popup");
		
		// Select a domain
		CurrentPage = GetInstance(HomePage.class);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click on the text area");
		CurrentPage.As(HomePage.class).Textinput.click();
		
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();

		// Check the "There is no text to save" message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "There is no text to save     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}	
    	
    	
	/**
 	 * SRA-118:Check the save button is activate only after selecting domain
 	 * @throws InterruptedException
 	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA118_Check_the_save_button_activate_only_after_selecting_domain() throws InterruptedException 
	{
		test = extent.createTest("SaveTests-SRA-118", "Check the save button is activate only after selecting domain");
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// // Check the "Please select a domain and open/enter text     X" message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please select a domain and open/enter text     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	  * SRA-136:Check saving only one letter
	  * @throws InterruptedException
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	  */
	 @Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	 public void SRA136_Check_saving_only_one_letter() throws InterruptedException, SQLException, FileNotFoundException 
	 { 
	  test = extent.createTest("SaveTests-SRA-67", "Check saving only one letter");
	 
	  // Select a domain
//	  String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//	  LogUltility.log(test, logger, "select random domain: " + randomDomain);

	  // Type in the textbox
	  //Thread.sleep(2000);
//	  String randomText = generateInput.RandomString(1, "letters");
	  String randomText = RandomText.getSentencesFromFile().split(" ")[2].substring(0,1);
	  LogUltility.log(test, logger, "input text to the text area: " + randomText);
	  CurrentPage.As(HomePage.class).Textinput.click();
	  CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
	  
	  // Click the save button
	  LogUltility.log(test, logger, "Click the save button");
	  CurrentPage.As(HomePage.class).SaveButton.click();
	 
	  // Check the Save confirmation message
	  CurrentPage.As(HomePage.class).fadedPopup(logger, "Text is too short. Type at least 2 characters     X");
	  LogUltility.log(test, logger, "Test Case PASSED");
	  
	 }
	 
	/**
	 *  SRA-138:Check saving a text that includes specific symbols
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA138_Check_saving_a_text_that_includes_specific_symbols() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
			
		test = extent.createTest("SaveTests-SRA_138", "Check saving a text that includes specific symbols");
		
		// Create new file with text that includes numbers and specific  symbols : ^ and & 
		//Thread.sleep(2000);
		String text = "We are testing the numbers and the symbols, so we can try 2^2 or Me&you not &";
		LogUltility.log(test, logger, "Text to enter: " + text);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		//Thread.sleep(4000);
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
//		String fileName = generateInput.RandomString(5, "words");
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		//Thread.sleep(2000);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Clear the text box editor
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.clear();

	    // Click the open button and open the saved files with the specific symbols 
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).openFileWait();
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		// Click Open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box and equal to the entered text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).Textinput.click();
		String savedText = CurrentPage.As(HomePage.class).Textinput.getText();
		LogUltility.log(test, logger, "text in textbox: " + text + ", and text that saved: " + savedText);
		assertEquals(savedText, text);
		LogUltility.log(test, logger, "Test Case PASSED");
}

	/**
	 * SRA-171:Verify a proper message appear when save template with the same name
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA171_Verify_a_proper_message_appear_when_save_template_with_the_same_name() throws InterruptedException, SQLException {
		test = extent.createTest("SaveTests-SRA_171", "Verify a proper message appear when save template with the same name");
		
	// Select a domain
//	String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithTemplates(con, usernameLogin);
//	LogUltility.log(test, logger, "Select random domain: " + randomDomain);
	//Thread.sleep(2000);

	// Click the open button and check for open popup
	//Thread.sleep(3000);
	LogUltility.log(test, logger, "Click the open button");
	CurrentPage = GetInstance(OpenPopup.class);
	CurrentPage.As(OpenPopup.class).btnOpen.click();
	
	//Choose the Template radio button
	//Thread.sleep(3000);
	CurrentPage = GetInstance(OpenPopup.class);
	CurrentPage.As(OpenPopup.class).templateRBtn.click();
	
    // Choose random Template and click open
	//Thread.sleep(1000);
	CurrentPage.As(OpenPopup.class).openTemplateWait();
	//CurrentPage.As(OpenPopup.class).cmbTemplate.click();
	String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
	LogUltility.log(test, logger, "Selected file: " + chosenTemplate);
	// Click Open
	//Thread.sleep(3000);
	CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
	
    // Check that there is text displayed in the text box
	//Thread.sleep(3000);
	CurrentPage = GetInstance(HomePage.class);
	Thread.sleep(1000);
	String text = CurrentPage.As(HomePage.class).Textinput.getText();
	boolean containsText = text.isEmpty();
	assertFalse(containsText);
	LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + containsText);
	
	// Save the template with the same name
	LogUltility.log(test, logger, "Click the save button");
	CurrentPage.As(HomePage.class).SaveButton.click();
	
	// Enter the same opened template name
	CurrentPage = GetInstance(SavePopup.class);
	CurrentPage.As(SavePopup.class).FileName.sendKeys(chosenTemplate);
	
	CurrentPage.As(SavePopup.class).SaveBtn.click();
	
	// Check the Save confirmation message
	CurrentPage = GetInstance(HomePage.class);
	CurrentPage.As(HomePage.class).fadedPopup(logger, "You cannot use an existing template name     X");
	
	LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-176:Verify a proper message appear when save text file with the same name
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA176_Verify_a_proper_message_appear_when_save_text_file_with_the_same_name() throws InterruptedException, SQLException {
		test = extent.createTest("SaveTests-SRA_176", "Verify a proper message appear when save text file with the same name");

	// Select a domain
//	String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, usernameLogin);
//	LogUltility.log(test, logger, "Select random domain: " + randomDomain);
	//Thread.sleep(2000);

	CurrentPage = GetInstance(HomePage.class);
	CurrentPage.As(HomePage.class).isHomepageReady();
	 // Click the open button and check for open popup
	//Thread.sleep(3000);
	LogUltility.log(test, logger, "Click the open button");
	CurrentPage = GetInstance(OpenPopup.class);
	CurrentPage.As(OpenPopup.class).btnOpen.click();

    // Choose random file and click open
	CurrentPage.As(OpenPopup.class).openFileWait();
	//Thread.sleep(3000);
	String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
	LogUltility.log(test, logger, "Selected file: " + chosenFile);
	// Click Open
	//Thread.sleep(3000);
	CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
	
    // Check that there is text displayed in the text box
	//Thread.sleep(3000);
	CurrentPage = GetInstance(HomePage.class);
	CurrentPage.As(HomePage.class).isHomepageReady();
	String text = CurrentPage.As(HomePage.class).Textinput.getText();
	boolean containsText = text.isEmpty();
	assertFalse(containsText);
	LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + containsText);
	
	// Save the file with the same name
	LogUltility.log(test, logger, "Click the save button");
	CurrentPage.As(HomePage.class).SaveButton.click();
	//Thread.sleep(2000);
	CurrentPage = GetInstance(SavePopup.class);
	CurrentPage.As(SavePopup.class).SaveBtn.click();
	
	// Check the Save confirmation message
	CurrentPage = GetInstance(HomePage.class);
	CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
	
	LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-185:Verify that text files list doesnï¿½t change after saving files - not stable behavior ï¿½ bug 709
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA185_Verify_that_text_files_list_doesn_t_change_after_saving_files_not_stable_behavior_bug_709() throws InterruptedException, AWTException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("SaveTests-SRA-185", "Verify that text files list doesn't change after saving files - not stable behavior bug 709");
		
		// Select random domain
		CurrentPage.As(HomePage.class).isHomepageReady();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
		LogUltility.log(test, logger, "Choose random domain: " + randomDomain);

		// Click the open button and check for open popup
		//Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();

		// Choose random file and click open
		//Thread.sleep(2000); 
		CurrentPage.As(OpenPopup.class).openFileWait();

		// Get files amount
		List<String> fileNames = CurrentPage.As(OpenPopup.class).getFilesValues();
		fileNames.remove("Select Text");
		// Delete first empty value
//		fileNames.remove(0);
		int FilesAmountBefore = fileNames.size();
		LogUltility.log(test, logger, "Files Amount Before: " + FilesAmountBefore);
			
		CurrentPage.As(OpenPopup.class).cmbText.click();
		CurrentPage.As(OpenPopup.class).openFileWait();
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		// Click Open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
		// Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean isEmpty = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + isEmpty);
		assertFalse(isEmpty);
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		CurrentPage.As(SavePopup.class).FileName.clear();
		String fileName = chosenFile;
		LogUltility.log(test, logger, "Enter a file name : "  + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
        
        // Verify that the filename saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> File_name =CurrentPage.As(SavePopup.class).getFileName(con,fileName);
        LogUltility.log(test, logger, "results from DB" + File_name);
        assertEquals(fileName, File_name.get(1).get(0));
        
		// Verify that the text saved to DB
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, fileName,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);

        boolean doExist = Saved_text.get(1).get(0).contains(text);
        LogUltility.log(test, logger, "Is text found in DB: " + doExist);
        assertTrue(doExist);
        
        // Now get the files amount after
        // Click the open button and check for open popup
		//Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		// Choose random file and click open
		//Thread.sleep(2000); 
		CurrentPage.As(OpenPopup.class).openFileWait();
		
		// Get files amount
		List<String> fileNamesAfter = CurrentPage.As(OpenPopup.class).getFilesValues();
		fileNamesAfter.remove("Select Text");
		// Delete first empty value
//		fileNamesAfter.remove(0);
		int FilesAmountAfter = fileNamesAfter.size();
		LogUltility.log(test, logger, "Files Amount After: " + FilesAmountAfter);
		Collections.sort(fileNames);
		Collections.sort(fileNamesAfter);
		LogUltility.log(test, logger, "Files list Before: " +fileNames );
		LogUltility.log(test, logger, "Files list After: " +fileNamesAfter );

		boolean amountEqual = false;
		if (FilesAmountAfter == FilesAmountBefore)
			amountEqual = true;
		
		LogUltility.log(test, logger, "Are files amount before equal to after: " + amountEqual);
		assertTrue(amountEqual);
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-198:Verify a overwrite/cancel popup appear when save text file with existing file name - overwrite - part 1
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA198_Verify_a_overwrite_cancel_popup_appear_when_save_text_file_with_existing_file_name_overwrite_part_1() throws InterruptedException, AWTException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("SaveTests-SRA-198", "Verify a overwrite/cancel popup appear when save text file with existing file name - overwrite - part 1");
		
		// Select random domain
		CurrentPage.As(HomePage.class).isHomepageReady();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
		LogUltility.log(test, logger, "Choose random domain: " + randomDomain);

		// Click the open button and check for open popup
		Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		// Choose random file and click open
		//Thread.sleep(2000); 
		CurrentPage.As(OpenPopup.class).openFileWait();
		
		// Get files names, in order to choose other than the first opened one
		List<String> fileNames = CurrentPage.As(OpenPopup.class).getFilesValues();
//		// Delete first empty value
//		fileNames.remove(0);
			
		CurrentPage.As(OpenPopup.class).cmbText.click();
		
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		// Click Open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
		// Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean isEmpty = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + ",Is empty: " + isEmpty);
		assertFalse(isEmpty);
		
		// Highlight all and insert new text
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		String text2 = RandomText.getSentencesFromFile();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(text2);
		LogUltility.log(test, logger, "Highlight all and add next text: "+text2);
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Choose another file to overwrite
		fileNames.remove(chosenFile);
		Random randomizer = new Random();
		String overwriteFile = fileNames.get(randomizer.nextInt(fileNames.size()));
		
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		CurrentPage.As(SavePopup.class).FileName.clear();
		LogUltility.log(test, logger, "Enter a file name : "  + overwriteFile);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(overwriteFile);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the overwrite Save popup
		// Verify the overwrite winodw text
		Thread.sleep(2000);
//		CurrentPage.As(SavePopup.class).OverWritePopup.click();
		String overwriteMessage = CurrentPage.As(SavePopup.class).OverWritePopup.getText();
		LogUltility.log(test, logger, "Overwrite message: " + overwriteMessage);
		String expected = "A text with this name already exists. Do you want to overwrite?";
		assertEquals(overwriteMessage, expected);
		
		// Click the overwrite button - yes button
		CurrentPage.As(SavePopup.class).Yes.click();
		
		// Verify that the overwrited text saved to DB
		Thread.sleep(4000);
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, overwriteFile,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);
        boolean doExist = Saved_text.get(1).get(0).contains(text2);
        LogUltility.log(test, logger, "Is text found in DB: " + doExist);
        assertTrue(doExist);
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-198:Verify a overwrite/cancel popup appear when save text file with existing file name - cancel - part 2
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA198_Verify_a_overwrite_cancel_popup_appear_when_save_text_file_with_existing_file_name_cancel_part_2() throws InterruptedException, AWTException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("SaveTests-SRA-198", "Verify a overwrite/cancel popup appear when save text file with existing file name - cancel - part 2");
		
		// Select random domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
		LogUltility.log(test, logger, "Choose random domain: " + randomDomain);
	
		// Click the open button and check for open popup
		//Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		// Choose random file and click open
		//Thread.sleep(2000); 
		CurrentPage.As(OpenPopup.class).openFileWait();
		
		// Get files names, in order to choose other than the first opened one
		List<String> fileNames = CurrentPage.As(OpenPopup.class).getFilesValues();
		// Delete first empty value
//		fileNames.remove(0);
			
		CurrentPage.As(OpenPopup.class).cmbText.click();
		
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		// Click Open
		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
		// Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean isEmpty = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + isEmpty);
		assertFalse(isEmpty);
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Choose another file to overwrite
		fileNames.remove(chosenFile);
		Random randomizer = new Random();
		String overwriteFile = fileNames.get(randomizer.nextInt(fileNames.size()));
		
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		CurrentPage.As(SavePopup.class).FileName.clear();
		LogUltility.log(test, logger, "Enter a file name : "  + overwriteFile);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(overwriteFile);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the overwrite Save popup
		// Verify the overwrite winodw text
		Thread.sleep(2000);
//		CurrentPage.As(SavePopup.class).OverWritePopup.click();
		String overwriteMessage = CurrentPage.As(SavePopup.class).OverWritePopup.getText();
		LogUltility.log(test, logger, "Overwrite message: " + overwriteMessage);
		String expected = "A text with this name already exists. Do you want to overwrite?";
		assertEquals(overwriteMessage, expected);
		
		// Click the cancel button - no button
		CurrentPage.As(SavePopup.class).No.click();
		
		// Verify that the text is not overwrited
		CurrentPage = GetInstance(SavePopup.class);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, overwriteFile,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);
       // boolean doExist = Saved_text.get(1).get(0).contains(text);
        boolean doExist = Saved_text.get(1).contains(text);
        LogUltility.log(test, logger, "Is text found in DB: " + doExist);
        assertFalse(doExist);
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-198:Verify a overwrite/cancel popup appear when save text file with existing file name - New then Overwrite - part 3
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA198_Verify_a_overwrite_cancel_popup_appear_when_save_text_file_with_existing_file_name_New_then_Overwrite_part_3() throws InterruptedException, AWTException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("SaveTests-SRA-198", "Verify a overwrite/cancel popup appear when save text file with existing file name - New then Overwrite - part 3");

		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + randomDomain);
	
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the open button and check for open popup
		//Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
	
		// Choose random file and click open
		//Thread.sleep(2000); 
		CurrentPage.As(OpenPopup.class).openFileWait();
		
		// Get files names, in order to choose other than the first opened one
		List<String> fileNames = CurrentPage.As(OpenPopup.class).getFilesValues();
		// Delete first empty value
//		fileNames.remove(0);
			
		CurrentPage.As(OpenPopup.class).cmbText.click();
		
//		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
//		LogUltility.log(test, logger, "Selected file: " + chosenFile);
//		// Click Open
//		//Thread.sleep(2000);
		CurrentPage.As(OpenPopup.class).btnCancelInPopup.click();
		
		// Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean isEmpty = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + "Is empty: " + isEmpty);
		assertFalse(isEmpty);
		
		// Click the save button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Choose another file to overwrite
//		fileNames.remove(chosenFile); mibo
		Random randomizer = new Random();
		String overwriteFile = fileNames.get(randomizer.nextInt(fileNames.size()));
		
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		CurrentPage.As(SavePopup.class).FileName.clear();
		LogUltility.log(test, logger, "Enter a file name : "  + overwriteFile);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(overwriteFile);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Check the overwrite Save popup
		// Verify the overwrite winodw text
		Thread.sleep(2000);
//		CurrentPage.As(SavePopup.class).OverWritePopup.click();
		String overwriteMessage = CurrentPage.As(SavePopup.class).OverWritePopup.getText();
		LogUltility.log(test, logger, "Overwrite message: " + overwriteMessage);
		String expected = "A text with this name already exists. Do you want to overwrite?";
		assertEquals(overwriteMessage, expected);
		
		// Click the overwrite button - yes button
		CurrentPage.As(SavePopup.class).Yes.click();
		
		// Verify that the overwrited text saved to DB
		CurrentPage = GetInstance(SavePopup.class);
		Thread.sleep(2000);
    	List<List<String>> Saved_text =CurrentPage.As(SavePopup.class).getSavedText(con, overwriteFile,randomDomain);
        LogUltility.log(test, logger, "results from DB : " + Saved_text);
//        boolean doExist = Saved_text.get(1).get(0).contains(text);
        boolean doExist = Saved_text.get(1).get(Saved_text.get(1).size()-1).contains(text);
        LogUltility.log(test, logger, "Is text found in DB: " + doExist);
        assertTrue(doExist);
		
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * SRA-242:Verify that the "Open to public" checkbox is working successfully
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA242_Verify_that_the_Open_to_public_checkbox_is_working_successfully() throws InterruptedException, AWTException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("SaveTests-SRA-242", "Verify that the \"Open to public\" checkbox is working successfully");
	
		// Select domain that two different users can access it
		// Nabil's and Ashraf' users
//		List<ArrayList<String>> accounts_projects_domains = CurrentPage.As(HomePage.class).getAccessibleDomainForTwoUsers(con,ExcelUltility.ReadCell("Email",1),ExcelUltility.ReadCell("Email",2));
			
		DriverContext.browser.GoToUrl(Setting.AUT_URL);
		
		
		// Get account,project and domain with the requested language and logged-in email
		List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
		
		// get info for second user, and get the common accounts between both 
		List<ArrayList<String>> account_project_domain2 = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",2));
		account_project_domain.retainAll(account_project_domain2);
		
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
		Random random = new Random();
		int index = random.nextInt(account_project_domain.size());
		LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
		
		// Get app dropdown values
		 GetInstance(HomePage.class).isHomepageReady();
		List<String> smorphDomains = GetInstance(HomePage.class).getDomainValues();
		smorphDomains.remove("Select Domain");
		// Choose random domain
		String randomDomain = smorphDomains.get(random.nextInt(smorphDomains.size()));
		
		String chosenDomain = null;
		// To get private domains correct name, since they are not stored with "(P)" in the DB
		for(int i =0;i<smorphDomains.size();i++)
			if(smorphDomains.get(i).contains(randomDomain))
				chosenDomain = smorphDomains.get(i);
			
		// Select domain
		 GetInstance(HomePage.class).isHomepageReady();
		 GetInstance(HomePage.class).select_Domain(chosenDomain);
		 LogUltility.log(test, logger, "Select domain: "+chosenDomain);
		 
		 // Get the parent projcet to use later when we log in with the second user
//		 String projectToUse =  GetInstance(HomePage.class).getProjectforDomain(con,chosenDomain);
		 
		// Enter text in the text box
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Insert file name
		CurrentPage = GetInstance(SavePopup.class);
//		String fileName = RandomText.getSentencesFromFile();
		String fileName = generateInput.RandomString(5, "letters");
		CurrentPage.As(SavePopup.class).FileName.clear();
		LogUltility.log(test, logger, "Enter a file name : "  + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		
		// Enable the open to public checkbox
		CurrentPage.As(SavePopup.class).openToPublic.click();
		LogUltility.log(test, logger, "Enable the Open to public checkbox");
		
		// Press save button
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		LogUltility.log(test, logger, "Press save button");
		
		// Verify that the filename saved to DB
		Thread.sleep(6000);
    	boolean savedSuccessfully =CurrentPage.As(SavePopup.class).checkSavedFileWithOTP(con,fileName);
        LogUltility.log(test, logger, "Saved file successfully and it is open to public: " + savedSuccessfully);
        assertTrue(savedSuccessfully);
		
		// Logout from the page
        CurrentPage = GetInstance(HomePage.class);	
        CurrentPage.As(HomePage.class).lnkLogout.click();
        LogUltility.log(test, logger, "Logout from the current running user: " + ExcelUltility.ReadCell("Email",1));
        
        // Login with the second user
        CurrentPage = GetInstance(LoginPage.class);	
        
        // Fill username and password
        CurrentPage.As(LoginPage.class).txtUserName.sendKeys(ExcelUltility.ReadCell("Email",2));
        CurrentPage.As(LoginPage.class).txtPassword.sendKeys(ExcelUltility.ReadCell("Password",2));
        CurrentPage.As(LoginPage.class).btnLogin.click();
        LogUltility.log(test, logger, "Login with the second user: "+ExcelUltility.ReadCell("Email",2));
        
	    // Login with the second user

	    CurrentPage = GetInstance(SelectPage.class);	
	    CurrentPage.As(SelectPage.class).isSelectPageReady();
// 	   LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,"Bank Hapoalim","Bank Hapoalim",chosenDomain);
		LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),chosenDomain);

        
        // Check if the file exists in the files list
        CurrentPage = GetInstance(HomePage.class);	
        CurrentPage.As(HomePage.class).isHomepageReady();
        
//        // Choose same domain
//		GetInstance(HomePage.class).select_Domain(chosenDomain);
//		LogUltility.log(test, logger, "Select domain: "+chosenDomain);
	 
		// Press open button
        CurrentPage.As(HomePage.class).Openbtn.click();
        LogUltility.log(test, logger, "Click the open button");
        
        // Get the files list values
        CurrentPage = GetInstance(OpenPopup.class);	
        ArrayList<String> filesList =  CurrentPage.As(OpenPopup.class).getFilesValues();
        LogUltility.log(test, logger, "The list: "+filesList+" should contain the open to public file: "+fileName);
        boolean exists = filesList.contains(fileName);
        LogUltility.log(test, logger, "The open to public file is visible for the other users: "+exists);
        assertTrue(exists);
        
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-244:Verify that user can't save file with empty file name
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA244_Verify_that_user_cant_save_file_with_empty_file_name() throws InterruptedException, SQLException, FileNotFoundException 
	{	
		test = extent.createTest("SaveTests-SRA-244", "Verify that user can't save file with empty file name");
	
		// Type in the textbox
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// don't Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		LogUltility.log(test, logger, "Click the save button in the save popup");
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please enter filename     X");	
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {			
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Save options",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
	    }
}