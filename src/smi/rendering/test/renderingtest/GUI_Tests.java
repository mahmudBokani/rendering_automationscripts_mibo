package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import smi.rendering.test.pages.FeedbackPage;
import smi.rendering.test.pages.ForgetPasswordPage;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.SavePopup;
import smi.rendering.test.pages.SelectPage;
import smi.rendering.test.pages.SharePopup;
import smi.rendering.test.pages.SignupPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

/**
 * All tests for the GUI folder
 *
 */
public class GUI_Tests extends FrameworkInitialize{
	
	private static Connection con;
	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	public boolean isThereMoreThanOneAssignedDomain;
	
    /**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
     * @throws ClassNotFoundException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("GUI", Setting.Browser);
	
	// Logger
	logger = LogManager.getLogger(GUI_Tests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite= GUI_Tests ");
	logger.info("GUI Tests - FrameworkInitilize");
	
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	DriverContext.browser.Maximize();
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	
	// Check if we have only one domain within one project
	isThereMoreThanOneAssignedDomain =   GetInstance(HomePage.class).As(HomePage.class).isThereMoreThanOneAssignedDomain(con);

//	// Get username for later use
//	usernameLogin = LoginUtilities.getUserName();
////	private String chosenProject = "";
	}
		
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	 public void beforeTest(Method method) throws Exception {  
	  logger.info("");
	  logger.info("#####################################################");
	  logger.info("Starting Test: " + method.getName());
	  
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	 }
	
	/**
	 * SRA 37: Check Login Page
	 * @throws InterruptedException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA37_Check_Login_Page() throws InterruptedException
	{
		test = extent.createTest("GUI_Tests-SRA-37" ,"Check Login Page"); 
		
		// Navigated to the login page\
		LogUltility.log(test, logger, "Check the elements in the login page");
		CurrentPage= GetInstance(LoginPage.class);
    	//Thread.sleep(10000);
    	
    	// Check email text field
    	LogUltility.log(test, logger, "verify that the email address field appear in the login page");
    	boolean findUserName = CurrentPage.As(LoginPage.class).FindElement("txtUserName");
    	LogUltility.log(test, logger, "Username field: " + findUserName);
    	Assert.assertTrue(findUserName);
    	
    	// Check the password field
    	LogUltility.log(test, logger, "Verify that the Password field appear in the login page");
    	boolean findPassword = CurrentPage.As(LoginPage.class).FindElement("txtPassword");
    	LogUltility.log(test, logger, "Password field: " + findPassword);
    	Assert.assertTrue(findPassword);
    	
    	// Check the submit button
    	LogUltility.log(test, logger, "Verify that the submit button appear in the login page");
    	boolean findSubmit = CurrentPage.As(LoginPage.class).FindElement("btnLogin");
    	LogUltility.log(test, logger, "Submit button: " + findSubmit);
    	Assert.assertTrue(findSubmit);
    	
    	// Check the signed up link
    	LogUltility.log(test, logger, "Verify that the Not singed up link appear in the login page");
    	boolean findSignLink = CurrentPage.As(LoginPage.class).FindElement("NotSigned");
    	LogUltility.log(test, logger, "Signed up link: " + findSignLink);
    	Assert.assertTrue(findSignLink);
    	
    	// Check the forgot link 
    	LogUltility.log(test, logger, "Verify that the forget password link appear in the login page");
    	boolean findForgotLink = CurrentPage.As(LoginPage.class).FindElement("ForgetPassword");
    	LogUltility.log(test, logger, "Forgot link: " + findForgotLink);
    	Assert.assertTrue(findForgotLink);
    	LogUltility.log(test, logger, "Test Case PASSED");
	}
	
//	  /**
//     * SRA-128 : Verify the "login" link in the "Email taken�" message will navigate to the rendering Login screen
//     * @throws InterruptedException
//	 * @throws FileNotFoundException 
//     */
//	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
//    public void SRA128_Check_Login_link_from_Email_Taken_message() throws InterruptedException, FileNotFoundException
//    {
//		test = extent.createTest("GUI_Tests-SRA-128" ,"Verify the login link in the Email taken� message will navigate to the rendering Login screen");
//		
//		// Navigate from the login page to the sign up page
//    	//Thread.sleep(5000);
//    	CurrentPage= GetInstance(LoginPage.class);
//    	LogUltility.log(test, logger, "Click the not signed up link");
//    	CurrentPage.As(LoginPage.class).NotSigned.click();
//    	CurrentPage= GetInstance(SignupPage.class);
//    	
//    	// Try to sign up with a registered email
//    	//Thread.sleep(5000);
//    	String fname = generateInput.RandomString(5, "letters");
//    	String lname = generateInput.RandomString(5, "letters");
//        String email = "qwer@mailinator.com"; // already taken email
//    	String password = generateInput.RandomString(6, "letters");
//    	CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
//    	LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
//    	//Thread.sleep(2000);
//    	
//    	// Check the error message
//    	String errorMessage = CurrentPage.As(SignupPage.class).msgEmailError.getText();
//    	LogUltility.log(test, logger, "Error message: " + errorMessage);
//    	Assert.assertEquals(errorMessage, "User already registered");
//    	
//    	// Click the login link
//    	LogUltility.log(test, logger, "Press the login link from the taken email message");
//    	CurrentPage.As(SignupPage.class).LoginLink.click();
//    	//Thread.sleep(5000);
//    	
//    	// Check that we arrived to the login page
//    	CurrentPage= GetInstance(LoginPage.class);
//    	
//    	LogUltility.log(test, logger, "verify that the email address field appear in the login page");  
//        boolean findUserName = CurrentPage.As(LoginPage.class).FindElement("txtUserName");
//        LogUltility.log(test, logger, "Find username : " + findUserName);  
//        Assert.assertTrue(findUserName);
//        
//        LogUltility.log(test, logger, "Verify that the Password field appear in the login page "); 
//        boolean findPassword = CurrentPage.As(LoginPage.class).FindElement("txtPassword");
//        LogUltility.log(test, logger, "Find Password : " + findPassword); 
//        Assert.assertTrue(findPassword);
//        
//        LogUltility.log(test, logger, "Verify that the submit button appear in the login page");
//        boolean findSubmit = CurrentPage.As(LoginPage.class).FindElement("btnLogin");
//        LogUltility.log(test, logger, "Find submit button : " + findSubmit); 
//        Assert.assertTrue(findSubmit);
//        
//        LogUltility.log(test, logger, "Verify that the Not singed up link appear in the login page");
//        boolean findSignLink = CurrentPage.As(LoginPage.class).FindElement("NotSigned");
//        LogUltility.log(test, logger, "Find Signup link : " + findSignLink); 
//        Assert.assertTrue(findSignLink);
//        
//        LogUltility.log(test, logger, "Verify that the forget password link appear in the login page");
//        boolean findForgotLink = CurrentPage.As(LoginPage.class).FindElement("ForgetPassword");
//        LogUltility.log(test, logger, "Find Forgot password link : " + findForgotLink); 
//        Assert.assertTrue(findForgotLink);
// 	    LogUltility.log(test, logger, "Test Case PASSED");
//	}
	
	/**
	 * SRA 38: Check signup Page
	 * @throws InterruptedException
	 */
    @Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
    public void SRA38_Check_signup_Page() throws InterruptedException
    {
    	test = extent.createTest("GUI_Tests-SRA-38" ,"Check signup Page"); 
    	
    	DriverContext.browser.GoToUrl(Setting.AUT_URL);
    	
    	// Navigate to the signup page
    	CurrentPage= GetInstance(LoginPage.class);
    	LogUltility.log(test, logger, "Click the not signed up link");
		CurrentPage.As(LoginPage.class).NotSigned.click();
		//Thread.sleep(5000);
		CurrentPage= GetInstance(SignupPage.class);
		
		// Press the already signed up link
		LogUltility.log(test, logger, "Click the already signed up link");
		JavascriptExecutor jse = (JavascriptExecutor)DriverContext._Driver;
	   	jse.executeScript("scroll(0, 250);");
		CurrentPage.As(SignupPage.class).AlreadySinedup.click();
		//Thread.sleep(3000);
		
		// Check we returned to the login page
		CurrentPage= GetInstance(LoginPage.class);
		LogUltility.log(test, logger, "verify the forgot password link appear in the page");
		boolean findForgotLink = CurrentPage.As(LoginPage.class).FindElement("ForgetPassword");
		LogUltility.log(test, logger, "Forgot link: " + findForgotLink);
		Assert.assertTrue(findForgotLink);
		LogUltility.log(test, logger, "Test Case PASSED");
    }

   /**
    * SRA-89:Check the main screen 
    * @throws InterruptedException
 * @throws SQLException 
 * @throws IOException 
 * @throws BiffException 
    */
    @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA89_Check_the_Main_Screen() throws InterruptedException, SQLException, BiffException, IOException
	{
    	test = extent.createTest("GUI_Tests-SRA-89" ,"Check the main screen");
    	
		// Login and confirm logging in
	   	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	   	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	   	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
		Random random = new Random();
		int index = random.nextInt(account_project_domain.size());
		LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
	   	
	   	// Check voice title
		GetInstance(HomePage.class).As(HomePage.class).isHomepageReady();
	   	LogUltility.log(test, logger, "get the text from voice selection label VoiceLabel");
	   	String voiceTitle = CurrentPage.As(HomePage.class).VoiceLabel.getText();
	   	LogUltility.log(test, logger, "Voice title : " + voiceTitle);
	    assertEquals(voiceTitle, "Voice Selection");
	    
	    // Check domain title
	    LogUltility.log(test, logger, "get the text from domain selection label DomainLabel");
	   	String domainTitle = CurrentPage.As(HomePage.class).DomainLabel.getText();
	   	LogUltility.log(test, logger, "Domain title : " + domainTitle);
	    assertEquals(domainTitle, "Domain");
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
   
    /**
    * SRA-73:Check the save popup 
    * @throws InterruptedException
     * @throws SQLException 
     * @throws IOException 
     * @throws BiffException 
    */
   	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA73_Check_the_save_popup() throws InterruptedException, SQLException, BiffException, IOException
	{
   		test = extent.createTest("GUI_Tests-SRA-73" ,"Check the save popup");
   		
   	// Login and confirm logging in
   	DriverContext.browser.GoToUrl(Setting.AUT_URL);
   	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
   	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2)); 	
		
   		// Refresh the homepage
   		//Thread.sleep(5000);
//		DriverContext._Driver.navigate().refresh();
		//Thread.sleep(5000);
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
//		// Select a domain
//		logger.info("select domain");
//		CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
    	//Thread.sleep(2000);
    	
		// Click on clear text button
		CurrentPage.As(HomePage.class).clearText.click();
		
    	// Type in the text box
//    	String text = generateInput.RandomString(5, "words");
		String text = RandomText.getSentencesFromFile();
    	LogUltility.log(test, logger, "input text to the text box: " + text);
    	CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
    	
    	// Click the save button
    	LogUltility.log(test, logger, "Click the save button");
    	CurrentPage.As(HomePage.class).SaveButton.click();
    	
    	 // Check that save window is displayed
    	LogUltility.log(test, logger, "verify that the save title appear in the Save popup");
    	CurrentPage = GetInstance(SavePopup.class);
    	String saveTitle = CurrentPage.As(SavePopup.class).Saveheader.getText();
    	LogUltility.log(test, logger, "Save popup title: " + saveTitle);
    	assertEquals(saveTitle, "Save");
    	LogUltility.log(test, logger, "Test Case PASSED");
	}
    	
	/**
	 * SRA-74:Check the Save & Share popup
	 * @throws InterruptedException
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws BiffException 
	 */
   	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA74_Check_the_save_share_popup() throws InterruptedException, AWTException, SQLException, BiffException, IOException
	{
   		test = extent.createTest("GUI_Tests-SRA-74" ,"Check the Save & Share popup");
   		
   		
   	// Login and confirm logging in
   	   	DriverContext.browser.GoToUrl(Setting.AUT_URL);
   	   	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
   	   	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
   		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
   		Random random = new Random();
   		int index = random.nextInt(account_project_domain.size());
   		LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2)); 	
   			
   		
		// Refresh the homepage for the new test
   		//Thread.sleep(5000);
//		DriverContext._Driver.navigate().refresh();
		//Thread.sleep(5000);
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Select a domain
//		LogUltility.log(test, logger, "select domain");
//		CurrentPage.As(HomePage.class).chooseRandomDomain(con);
    	//Thread.sleep(2000);
		    	
		// Click on clear text button
		CurrentPage.As(HomePage.class).clearText.click();
		
    	// Type in the text box
//    	String text = generateInput.RandomString(5, "words");
    	String text = RandomText.getSentencesFromFile();
    	LogUltility.log(test, logger, "input text to the text box: " + text);
    	CurrentPage.As(HomePage.class).Textinput.sendKeys(text);
		    	
    	// Click the save button
    	LogUltility.log(test, logger, "Click the save button");
    	CurrentPage.As(HomePage.class).SaveButton.click();
		//Thread.sleep(2000);
    	CurrentPage = GetInstance(SavePopup.class);
    	
    	// Enter a file name and click the save share button
    	String fileName = generateInput.RandomString(5, "letters");
    	CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
    	LogUltility.log(test, logger, "File name entered: " + fileName);
    	CurrentPage.As(SavePopup.class).SaveShareBtn.click();
    	//Thread.sleep(2000);
    	
    	// Check the share window
    	LogUltility.log(test, logger, "verify that the share title appear in the share popup");
    	CurrentPage = GetInstance(SharePopup.class);
    	String title = CurrentPage.As(SharePopup.class).sharePopupCheck(test, logger);
    	assertEquals(title, "Share");
    	
//    	CurrentPage = GetInstance(SharePopup.class);
//    	String shareTitle = CurrentPage.As(SharePopup.class).ShareTitle.getText();
//    	LogUltility.log(test, logger, "Share popup title: " + shareTitle);
//    	assertEquals(shareTitle, "Share");
    	
    	LogUltility.log(test, logger, "Test Case PASSED");
	}	
   	
   	/**
   	 * SRA-95: Check the feedback popup
   	 * @throws InterruptedException
   	 * @throws IOException 
   	 * @throws BiffException 
   	 * @throws SQLException 
   	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA95_Check_the_Feedback_popup() throws InterruptedException, BiffException, IOException, SQLException
	{
		test = extent.createTest("GUI_Tests-SRA-95" ,"Check the feedback popup");		
		
		// Login and confirm logging in
	   	DriverContext.browser.GoToUrl(Setting.AUT_URL);
   	   	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
   	   	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
   		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
   		Random random = new Random();
   		int index = random.nextInt(account_project_domain.size());
   		LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2)); 			
   		
		//LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(HomePage.class), logger);
		// Refresh the website for the new test 
//		DriverContext._Driver.navigate().refresh();
		//Thread.sleep(5000);
		
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Scroll Down
		CurrentPage.As(HomePage.class).scrollDown();
				
		// Check the feedback window
		LogUltility.log(test, logger, "Click the feedback link in the home page");
    	CurrentPage.As(HomePage.class).Feedback.click();
    	//Thread.sleep(2000);
    
    	// Check the feedback window title
    	CurrentPage = GetInstance(FeedbackPage.class);
    	LogUltility.log(test, logger, "verify that the Feedback title appear in the Feedback popup");
    	String feedbackTitle = CurrentPage.As(FeedbackPage.class).Feedbackheader.getText();
    	LogUltility.log(test, logger, "Feedback popup title : " + feedbackTitle);
    	assertEquals(feedbackTitle, "Feedback");
    	LogUltility.log(test, logger, "Test Case PASSED");
	}
     
	 /**
	    * SRA39: Check Forgot Password Page
	    * @throws InterruptedException
	 * @throws FileNotFoundException 
	    */
	   @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	   public void SRA39_Check_Forgot_Password_Page() throws InterruptedException, FileNotFoundException
	   {
		   test = extent.createTest("GUI_Tests-SRA-39" ,"Check Forgot Password Page"); 
		   
		// Navigate to the forgot password page
	   DriverContext.browser.GoToUrl(Setting.AUT_URL);
	   CurrentPage= GetInstance(LoginPage.class);
	   //Thread.sleep(5000);
	   LogUltility.log(test, logger, "Click the forgot password link");
	   CurrentPage.As(LoginPage.class).ForgetPassword.click();

	   // Fill email address and click submit
	   //Thread.sleep(3000);
	   CurrentPage= GetInstance(ForgetPasswordPage.class);
	   LogUltility.log(test, logger, "fill in the address email and press the submit button");
	   Setting.Language = "6"; 
	   String email = generateInput.RandomString(5, "letters") + "@mailinator.com";
	   Setting.Language = "10";
	   CurrentPage.As(ForgetPasswordPage.class).ForgetPassword(email);
	   //CurrentPage.As(ForgetPasswordPage.class).btnSubmit.click();
	   
	   // Check the system message
	   //Thread.sleep(3000);
	   String doneMessage = CurrentPage.As(ForgetPasswordPage.class).SuccessMessage.getText();
	   LogUltility.log(test, logger, "Get the system confirmation message: " + doneMessage);
	   assertEquals(doneMessage, "A password reset message was sent to your email address. Please check your inbox.");
	   LogUltility.log(test, logger, "Test Case PASSED");
	}

	   /**
	    * SRA-218:Check the select project/domain page components
	    * @throws InterruptedException
	    */
	    @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA218_Check_the_select_project_domain_page_components() throws InterruptedException
		{
	    	test = extent.createTest("GUI_Tests-SRA-218" ,"Check the select project/domain page components");
	    	
//	    	if (!isThereMoreThanOneAssignedDomain) {
//	    		LogUltility.log(test, logger, "Skip this test, can't stop at select page");
//	    		return;
//	    	}
	    	
			// Login and confirm logging in
		   	DriverContext.browser.GoToUrl(Setting.AUT_URL);
		   	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
			
		   	// Wait for the select page to load
			CurrentPage = GetInstance(SelectPage.class);
			CurrentPage.As(SelectPage.class).isSelectPageReady();
			
		  	// Check account label
		   	LogUltility.log(test, logger, "get the text from account selection label");
		   	String accountLabelText = CurrentPage.As(SelectPage.class).lblAccount.getText();
		   	LogUltility.log(test, logger, "Account label : " + accountLabelText);
		    assertEquals(accountLabelText, "Account");
		    		    
		   	// Check project label
		   	LogUltility.log(test, logger, "get the text from project selection label");
		   	String projectLabelText = CurrentPage.As(SelectPage.class).lblProject.getText();
		   	LogUltility.log(test, logger, "Project label : " + projectLabelText);
		    assertEquals(projectLabelText, "Project");
		    
		    // Check domain label
		   	LogUltility.log(test, logger, "get the text from domain selection label");
		   	String domainLabelText = CurrentPage.As(SelectPage.class).lblDomain.getText();
		   	LogUltility.log(test, logger, "Project label : " + domainLabelText);
		    assertEquals(domainLabelText, "Domain");
		    
		    LogUltility.log(test, logger, "Test Case PASSED");
		}
	   
	    
	    @AfterMethod(alwaysRun = true)
	    public void getResult(ITestResult result, Method method) throws IOException
	    {		
			int testcaseID = 0;
			boolean writeToReport = false;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("GUI",method.getName());
			System.out.println("tryCount: " + tryCount);
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
				writeToReport = true;
			}
			else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
				extent.removeTest(test);
				}
				else {
					writeToReport = true;
					}
			
			// Write to report
			if (writeToReport) {
				tryCount = 0;

				 // Write to report
				 if(result.getStatus() == ITestResult.FAILURE)
				    {
				        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
				        test.fail(result.getThrowable());
				        String screenShotPath = Screenshot.captureScreenShot();
				        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
				        // Send result to testlink
				        if (enableReportTestlink == true){
				        	try {
				        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
				            	LogUltility.log(test, logger, "Report to testtlink: " + response);
							} catch (Exception e) {
								LogUltility.log(test, logger, "Testlink Error: " + e);
							}
				        	}
				    }
				    else if(result.getStatus() == ITestResult.SUCCESS)
				    {
				        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
				        // Send result to testlink
				        if (enableReportTestlink == true){
				        	try {
				        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
				            	LogUltility.log(test, logger, "Report to testtlink: " + response);
				    		} catch (Exception e) {
				    			LogUltility.log(test, logger, "Testlink Error: " + e);
				    		}
				        	
				        	}
				    }
				    else
				    {
				        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
				        test.skip(result.getThrowable());
				    }
			        // Get console report
			        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
				    extent.flush();
			}
	    	// Refresh the website for the new test
			DriverContext._Driver.navigate().refresh();
	    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 DriverContext._Driver.quit();
	    }
}