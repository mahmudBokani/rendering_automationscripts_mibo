package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import smi.rendering.test.pages.ActivevoicesSynthesisRequestsPage;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SavePopup;
import smi.rendering.test.pages.Say_As_Popup;
import smi.rendering.test.pages.SelectPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

/**
 * All tests in Voice play controller option folder
 *
 */
public class ZServerRepeatCheck  extends FrameworkInitialize {

	private static Connection con;
	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	String Browser;
	public boolean isThereMoreThanOneAssignedDomain;
	
	/**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException 
	 */
	@Parameters({"browser", "testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(String browser, boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
		String Browser = browser;
		
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("ServerRepeatCheck", browser);
		
	// Logger
	logger = LogManager.getLogger(ZServerRepeatCheck.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite = Server repeat check ");
	logger.info("Server repeat check - FrameworkInitilize");
	
//	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//	logger.info("Connect to DB " + con.toString());
//	
	InitializeBrowser(browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	// Login only once then run the following tests
//	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(HomePage.class), logger);
	CurrentPage= GetInstance(HomePage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Check if we have only one domain within one project
	isThereMoreThanOneAssignedDomain =   GetInstance(HomePage.class).As(HomePage.class).isThereMoreThanOneAssignedDomain(con);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Refresh the website for the new test
//		DriverContext._Driver.navigate().refresh();
//		CurrentPage = GetInstance(HomePage.class);
//		//Thread.sleep(10000);
//		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}
	
	@Test
	public void twoServers() throws InterruptedException, SQLException {
		
		String server1 = "http://smorph-demo-bp2-73391970.us-east-1.elb.amazonaws.com/RenderingApp/";
		String server2 = "http://smorph-demo-1002-1551317023.us-east-1.elb.amazonaws.com/RenderingApp/";
		
		while (true) {
		
		System.out.println("STARTING FIRST SERVER");
		
		// First Server
		DriverContext.browser.GoToUrl(server1);
		// Login only once then run the following tests
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
		CurrentPage= GetInstance(HomePage.class);
		// Implicit wait
		DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Run(server1);
		
		
		System.out.println("STARTING SECOND SERVER");
		// Second Server
		DriverContext.browser.GoToUrl(server2);
		// Login only once then run the following tests
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
		CurrentPage= GetInstance(HomePage.class);
		// Implicit wait
		DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Run(server2);
		
		// Wait 10 minutes
		System.out.println("WAIT 10 MINUTES FOR NEXT RUN");
		TimeUnit.MINUTES.sleep(10);
		}
	}
	
	@Test
	public void Run(String server) throws InterruptedException, SQLException {
		
		int voiceIndex = 0;
		int count = 0;
		
		// Get values from Voice dropdown
		List<String> voiceValues = CurrentPage.As(HomePage.class).getVoiceValues();
		System.out.println("Get values from Voice dropdown: " + voiceValues);
		//delete first empty value
		voiceValues.remove(0);
		CurrentPage.As(HomePage.class).lblDomain.click();
		
		// Number of voices
		int numberOfVoices = voiceValues.size();
		System.out.println("Number of voices: " + numberOfVoices);
		
		// Get values from Domain dropdown
		List<String> domainValues = CurrentPage.As(HomePage.class).getDomainValues();
		domainValues.remove(0);
		System.out.println("Get values from Domain dropdown: " + domainValues);
		CurrentPage.As(HomePage.class).lblDomain.click();
		
		for (int i = 0; i < numberOfVoices; i++) {
			
			// Choose next voice from the list
			voiceIndex = count % numberOfVoices;
			System.out.println("Next voice index: " + voiceIndex);
			String getNextVoice = voiceValues.get(voiceIndex);
			System.out.println("Next voice: " + getNextVoice);
			
			// Choose random domain
			Random randomizer = new Random();
			String randomDomain = domainValues.get(randomizer.nextInt(domainValues.size()));
			
			Run_Twice_per_Voice(getNextVoice, randomDomain, server);
			
			// Move next voice
			count++;
			
			// close all tabs
			   String originalHandle = DriverContext._Driver.getWindowHandle();

			    for(String handle : DriverContext._Driver.getWindowHandles()) {
			        if (!handle.equals(originalHandle)) {
			        	DriverContext._Driver.switchTo().window(handle);
			        	DriverContext._Driver.close();
			        }
			    }
			    DriverContext._Driver.switchTo().window(originalHandle);
		}
	}
	
	/**
	 *  Run a voice in two tabs
	 * @throws InterruptedException 
	 * @throws SQLException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  Run_Twice_per_Voice(String getNextVoice, String randomDomain, String server) throws InterruptedException, SQLException
	{		
		test = extent.createTest("Repeat Test", "Run a voice in two tabs");
		
		// Fill the first opened browser
		CurrentPage= GetInstance(ActivevoicesSynthesisRequestsPage.class);
		CurrentPage.As(ActivevoicesSynthesisRequestsPage.class).getReadyForRepeat(test, server, logger, getNextVoice, randomDomain, isThereMoreThanOneAssignedDomain);
			
		// Open new window
		((JavascriptExecutor)DriverContext._Driver).executeScript("window.open();");
		
		// Focus on last opened window
		for (String handle : DriverContext._Driver.getWindowHandles()) {
			DriverContext._Driver.switchTo().window(handle);
			}
		
		// Fill second tab
		CurrentPage= GetInstance(ActivevoicesSynthesisRequestsPage.class);
		CurrentPage.As(ActivevoicesSynthesisRequestsPage.class).getReadyForRepeat(test, server, logger, getNextVoice, randomDomain, isThereMoreThanOneAssignedDomain);
		
		// Click play in all the buttons
		for (String handle : DriverContext._Driver.getWindowHandles()) {
			DriverContext._Driver.switchTo().window(handle);
			
			// Click the play button
			CurrentPage.As(HomePage.class).btnplay.click();
			
			}

		// Check for error synthesizing messages
		for (String handle : DriverContext._Driver.getWindowHandles()) {
			DriverContext._Driver.switchTo().window(handle);
			
			// Check if clip is successfully loaded
			CurrentPage.As(HomePage.class).isHomepageReady();
			
			boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
			LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
			
			try {
				assertTrue(isWaveBarDisplayed);
			} catch (Exception e) {
				CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
				assertTrue(isWaveBarDisplayed);
			}
			
			}
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * Run after each test
	 */
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {

		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Zserver",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 DriverContext._Driver.quit();
	    }	
}