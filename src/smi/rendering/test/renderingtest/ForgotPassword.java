package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import smi.rendering.test.pages.ForgetPasswordPage;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.Mailinator;
import smi.rendering.test.pages.SelectPage;
import smi.rendering.test.pages.UMSPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

/**
 * All tests in Forgot password folder
 *
 */
public class ForgotPassword extends FrameworkInitialize {

	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	private static String Browser;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	
	/**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initialize the report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("ForgotPassword", Setting.Browser);
	
	// Logger
	logger = LogManager.getLogger(ForgotPassword.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite= ForgotPassword ");
	logger.info("Forgot Password Tests - FrameworkInitilize");
	
//	InitializeBrowser(browser);
//	DriverContext.browser.GoToUrl(Setting.AUT_URL);

	Browser = Setting.Browser;
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Refresh the website for the new test
//		DriverContext._Driver.navigate().refresh();
//		Thread.sleep(10000);
		
		InitializeBrowser(Browser);
		DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		DriverContext.browser.GoToUrl(Setting.AUT_URL);
		
		DriverContext.browser.Maximize();
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}


	 /**
	 * SRA-19:Verify user can Re-set his password successfully via Forgot password
	 * @throws InterruptedException
	 */
	@Test (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA19_Verify_user_can_Re_set_his_password_successfully_via_Forgot_password() throws InterruptedException
	{
		test = extent.createTest("ForgotPassword-SRA-19", "Verify user can Re-set his password successfully via Forgot password");

		   // Navigate to the forgot password page
		   DriverContext.browser.GoToUrl(Setting.AUT_URL);
		   CurrentPage= GetInstance(LoginPage.class);
		   //Thread.sleep(5000);
		   LogUltility.log(test, logger, "Click the forgot password link");
		   CurrentPage.As(LoginPage.class).ForgetPassword.click();

		   // Fill email address and click submit
		   //Thread.sleep(5000);
		   CurrentPage= GetInstance(ForgetPasswordPage.class);
		   LogUltility.log(test, logger, "fill in the address email and press the submit button");
		   String email = "fefo10@mailinator.com";
		   CurrentPage.As(ForgetPasswordPage.class).ForgetPassword(email);
		   
		   // Check the system message
		   //Thread.sleep(10000);
		   String doneMessage = CurrentPage.As(ForgetPasswordPage.class).SuccessMessage.getText();
		   logger.info("Get the system confirmation message: " + doneMessage);
		   assertEquals(doneMessage, "A password reset message was sent to your email address. Please check your inbox.");
		   
			// Navigate to mailinator website
			LogUltility.log(test, logger, "Navigate to mailinator");
			DriverContext.browser.GoToUrl("http://www.mailinator.com/");
			CurrentPage = GetInstance(Mailinator.class);
			
			// Fill the email field and click Go
			LogUltility.log(test, logger, "Fill the registered email: " + email + ", and click go");
			CurrentPage.As(Mailinator.class).FillInboxField(email);
			
			// Check that the email is received
			//Thread.sleep(5000);
			LogUltility.log(test, logger, "Check that the email is received");
			String emailTitle = CurrentPage.As(Mailinator.class).lnkEmailTitle.getText();
			LogUltility.log(test, logger, "the email title: " + emailTitle);
			boolean Results = emailTitle.contains("SpeechMorphing Password Reset");
			LogUltility.log(test, logger, "the email title contain : " + Results);
			assertTrue(Results);
			
			LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	 /**
	 * SRA-20:Verify the user can set a new password in the UMS server
	 * @throws InterruptedException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups="Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA20_Verify_the_user_can_set_a_new_password_in_the_UMS_server() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("ForgotPassword-SRA-20", "Verify the user can set a new password in the UMS server");
		
	   // Navigate to the forgot password page
	   DriverContext.browser.GoToUrl(Setting.AUT_URL);
	   CurrentPage= GetInstance(LoginPage.class);
	   //Thread.sleep(5000);
	   LogUltility.log(test, logger, "Click the forgot password link");
	   CurrentPage.As(LoginPage.class).ForgetPassword.click();

	   // Fill email address and click submit
	   //Thread.sleep(3000);
	   CurrentPage= GetInstance(ForgetPasswordPage.class);
	   LogUltility.log(test, logger, "fill in the address email and press the submit button");
	   String email = "fefo10@mailinator.com";
	   CurrentPage.As(ForgetPasswordPage.class).ForgetPassword(email);
	   //Thread.sleep(2000);
	   CurrentPage.As(ForgetPasswordPage.class).btnSubmit.click();
	   
	   // Check the system message
	   //Thread.sleep(10000);
	   String doneMessage = CurrentPage.As(ForgetPasswordPage.class).SuccessMessage.getText();
	   logger.info("Get the system confirmation message: " + doneMessage);
	   assertEquals(doneMessage, "A password reset message was sent to your email address. Please check your inbox.");
	   
		// Navigate to mailinator website
		LogUltility.log(test, logger, "Navigate to mailinator");
		DriverContext.browser.GoToUrl("http://www.mailinator.com/");
		CurrentPage = GetInstance(Mailinator.class);
		
		// Fill the email field and click Go
		LogUltility.log(test, logger, "Fill the registered email: " + email + ", and click go");
		CurrentPage.As(Mailinator.class).FillInboxField(email);

		// Click the received message
		//Thread.sleep(10000);
		LogUltility.log(test, logger, "Click the received confirmation email");
		CurrentPage.As(Mailinator.class).lnkEmailTitle.click();
		
		// Click the Reset link
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the Reset Password link");
		//DriverContext._Driver.switchTo().frame("msg_body");
		DriverContext._Driver.switchTo().frame("msg_body");
		CurrentPage.As(Mailinator.class).lnkResetPassword1.click();
	
		// UMS page
		//Thread.sleep(5000);
		CurrentPage = GetInstance(UMSPage.class);
		CurrentPage.As(UMSPage.class).focusOnLastOpenedTab();
		GetInstance(UMSPage.class).reloadUMS();
		Setting.Language = "6";
		String newPassword = generateInput.RandomString(6, "letters");
		Setting.Language = "10";
		LogUltility.log(test, logger, "Fill a new password: " + newPassword);
		CurrentPage.As(UMSPage.class).txtPassword.sendKeys(newPassword);
		CurrentPage.As(UMSPage.class).txtConfirmPassword.sendKeys(newPassword);
		CurrentPage.As(UMSPage.class).btnResetPassword.click();
		
		// Check for the update confirmation message
		//Thread.sleep(5000);
//		String updateMessage = CurrentPage.As(UMSPage.class).msgUpdateSuccess.getText();
//		boolean result = updateMessage.equals("Password is updated.");
//		LogUltility.log(test, logger, "Check the update confirmation message: " + result);
//		assertTrue(result);
		
		 CurrentPage.As(UMSPage.class).updateMessageCheck(test, logger, "Password is updated.");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	 
     /**
	 * SRA-21:Verify user can login successfully after changing the password via Forgot password
	 * @throws InterruptedException
     * @throws AWTException 
     * @throws FileNotFoundException 
     * @throws SQLException 
	 */
	@Test(groups="Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA21_Verify_user_can_login_successfully_after_changing_the_password_via_Forgot_password() throws InterruptedException, AWTException, FileNotFoundException, SQLException
	{
		test = extent.createTest("ForgotPassword-SRA-21", "Verify user can login successfully after changing the password via Forgot password");

	   // Navigate to the forgot password page
	   DriverContext.browser.GoToUrl(Setting.AUT_URL);
	   CurrentPage= GetInstance(LoginPage.class);
	   //Thread.sleep(5000);
	   LogUltility.log(test, logger, "Click the forgot password link");
	   CurrentPage.As(LoginPage.class).ForgetPassword.click();

	   // Fill email address and click submit
	   //Thread.sleep(3000);
	   CurrentPage= GetInstance(ForgetPasswordPage.class);
	   LogUltility.log(test, logger, "fill in the email and press the submit button");
	   String email = "fefo10@mailinator.com";
	   CurrentPage.As(ForgetPasswordPage.class).ForgetPassword(email);
	   //Thread.sleep(2000);
	   CurrentPage.As(ForgetPasswordPage.class).btnSubmit.click();
	   
	   // Check the system message
	   //Thread.sleep(10000);
	   String doneMessage = CurrentPage.As(ForgetPasswordPage.class).SuccessMessage.getText();
	   logger.info("Get the system confirmation message: " + doneMessage);
	   assertEquals(doneMessage, "A password reset message was sent to your email address. Please check your inbox.");
	   
		// Navigate to mailinator website
		LogUltility.log(test, logger, "Navigate to mailinator");
		DriverContext.browser.GoToUrl("http://www.mailinator.com/");
		CurrentPage = GetInstance(Mailinator.class);
		
		// Fill the email field and click Go
		LogUltility.log(test, logger, "Fill the registered email: " + email + ", and click go");
		CurrentPage.As(Mailinator.class).FillInboxField(email);

		// Click the received message
		//Thread.sleep(10000);
		LogUltility.log(test, logger, "Click the received confirmation email");
		CurrentPage.As(Mailinator.class).lnkEmailTitle.click();
		
		// Click the Reset link
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the Reset Password link");
		DriverContext._Driver.switchTo().frame("msg_body");
		CurrentPage.As(Mailinator.class).lnkResetPassword1.click();
	
		// UMS page
		//Thread.sleep(5000);
		CurrentPage = GetInstance(UMSPage.class);
		CurrentPage.As(UMSPage.class).focusOnLastOpenedTab();
		CurrentPage.As(UMSPage.class).reloadUMS();
		Setting.Language = "6";
		String newPassword = generateInput.RandomString(6, "letters");
		Setting.Language = "10";
		LogUltility.log(test, logger, "Fill a new password: " + newPassword);
		CurrentPage.As(UMSPage.class).txtPassword.sendKeys(newPassword);
		CurrentPage.As(UMSPage.class).txtConfirmPassword.sendKeys(newPassword);
		CurrentPage.As(UMSPage.class).btnResetPassword.click();
		
		// Check for the update confirmation message
		//Thread.sleep(3000);
//		String updateMessage = CurrentPage.As(UMSPage.class).msgUpdateSuccess.getText();
//		boolean result = updateMessage.equals("Password is updated.");
//		LogUltility.log(test, logger, "Check the update confirmation message: " + result);
//		assertTrue(result);
		LogUltility.log(test, logger, "Check the password update message");
		CurrentPage.As(UMSPage.class).updateMessageCheck(test, logger, "Password is updated.");
		
//		// Close confirm window
//		try {
//			//Thread.sleep(5000);
//			 Alert alert = DriverContext._Driver.switchTo().alert();
//			 alert.accept(); // for OK
//		} catch (Exception e) {
//		}
		    
		// Check logging in with the new password
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Navigate back to the login page");
		DriverContext.browser.GoToUrl(Setting.AUT_URL);
		//Thread.sleep(3000);
		
		LogUltility.log(test, logger, "Login with the new password");
		CurrentPage = GetInstance(LoginPage.class);
		Thread.sleep(3000);
		CurrentPage.As(LoginPage.class).Login(email, newPassword);
		// Wait for the page to load
		//Thread.sleep(15000);
		
		// Wait for the select page to load
		CurrentPage = GetInstance(SelectPage.class);
		String projectLabel = CurrentPage.As(SelectPage.class).lblProject.getText();
		String domainLabel = CurrentPage.As(SelectPage.class).lblDomain.getText();
		assertEquals(projectLabel,"Project");
		assertEquals(domainLabel,"Domain");
		LogUltility.log(test, logger, "User logged in succesffuly with the new password");
		
//		// Verify is logged in
//		try {
//			CurrentPage.As(SelectPage.class).lnkLogout.getText();
//			logger.info("Successfully logged in");
//		} catch (Exception e) {
//			logger.error("Login failed: " + e);
//		}
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	    /**
		 * SRA-22:Verify the user cannot set a new password in the UMS server with empty fields
		 * @throws InterruptedException
	     * @throws FileNotFoundException 
		 */
		@Test(groups="Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA22_Verify_the_user_cannot_set_a_new_password_in_the_UMS_server_with_empty_fields() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("ForgotPassword-SRA-22", "Verify the user cannot set a new password in the UMS server with empty fields");

		   // Navigate to the forgot password page
		   DriverContext.browser.GoToUrl(Setting.AUT_URL);
		   CurrentPage= GetInstance(LoginPage.class);
		   //Thread.sleep(5000);
		   LogUltility.log(test, logger, "Click the forgot password link");
		   CurrentPage.As(LoginPage.class).ForgetPassword.click();

		   // Fill email address and click submit
		   //Thread.sleep(3000);
		   CurrentPage= GetInstance(ForgetPasswordPage.class);
		   LogUltility.log(test, logger, "fill in the address email and press the submit button");
		   String email = "fefo10@mailinator.com";
		   CurrentPage.As(ForgetPasswordPage.class).ForgetPassword(email);
		   //Thread.sleep(2000);
		   CurrentPage.As(ForgetPasswordPage.class).btnSubmit.click();
		   
		   // Check the system message
		   //Thread.sleep(10000);
		   String doneMessage = CurrentPage.As(ForgetPasswordPage.class).SuccessMessage.getText();
		   logger.info("Get the system confirmation message: " + doneMessage);
		   assertEquals(doneMessage, "A password reset message was sent to your email address. Please check your inbox.");
		   
			// Navigate to mailinator website
			LogUltility.log(test, logger, "Navigate to mailinator");
			DriverContext.browser.GoToUrl("http://www.mailinator.com/");
			CurrentPage = GetInstance(Mailinator.class);
			
			// Fill the email field and click Go
			LogUltility.log(test, logger, "Fill the registered email: " + email + ", and click go");
			CurrentPage.As(Mailinator.class).FillInboxField(email);

			// Click the received message
			//Thread.sleep(10000);
			LogUltility.log(test, logger, "Click the received confirmation email");
			CurrentPage.As(Mailinator.class).lnkEmailTitle.click();
			
			// Click the Reset link
			//Thread.sleep(3000);
			LogUltility.log(test, logger, "Click the Reset Password link");
			DriverContext._Driver.switchTo().frame("msg_body");
			CurrentPage.As(Mailinator.class).lnkResetPassword1.click();
		
			// UMS page
			//Thread.sleep(3000);
			CurrentPage = GetInstance(UMSPage.class);
			CurrentPage.As(UMSPage.class).focusOnLastOpenedTab();
			CurrentPage.As(UMSPage.class).reloadUMS();
			Setting.Language = "6";
			String newPassword = generateInput.RandomString(6, "letters");
			 Setting.Language = "10";
			LogUltility.log(test, logger, "Fill a new password only for conirm passowrd: " + newPassword);
			CurrentPage.As(UMSPage.class).txtConfirmPassword.sendKeys(newPassword);
			
			// Check for the error message
			//Thread.sleep(3000);
			String errorMessage = CurrentPage.As(UMSPage.class).msgError.getText();
		    logger.info("Get the system error message: " + errorMessage);
		    assertEquals(errorMessage, "Not Matched");
		   
		    LogUltility.log(test, logger, "Test Case PASSED");
		}
	
	 /**
	 * SRA-23:Verify the user cannot set a new password in the UMS server with invalid pass
	 * @throws InterruptedException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups="Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA23_Verify_the_user_cannot_set_a_new_password_in_the_UMS_server_with_invalid_pass() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("ForgotPassword-SRA-23", "Verify the user cannot set a new password in the UMS server with invalid pass");

	   // Navigate to the forgot password page
	   DriverContext.browser.GoToUrl(Setting.AUT_URL);
	   CurrentPage= GetInstance(LoginPage.class);
	   //Thread.sleep(5000);
	   LogUltility.log(test, logger, "Click the forgot password link");
	   CurrentPage.As(LoginPage.class).ForgetPassword.click();

	   // Fill email address and click submit
	   //Thread.sleep(5000);
	   CurrentPage= GetInstance(ForgetPasswordPage.class);
	   LogUltility.log(test, logger, "fill in the address email and press the submit button");
	   String email = "fefo10@mailinator.com";
	   CurrentPage.As(ForgetPasswordPage.class).ForgetPassword(email);
	   //Thread.sleep(2000);
	   CurrentPage.As(ForgetPasswordPage.class).btnSubmit.click();
	   
	   // Wait for the success message
		
	   // Check the system message
	   //Thread.sleep(10000);
//	   String doneMessage = CurrentPage.As(ForgetPasswordPage.class).SuccessMessage.getText();
//	   logger.info("Get the system confirmation message: " + doneMessage);
//	   assertEquals(doneMessage, "A password reset message was sent to your email address. Please check your inbox.");
	   CurrentPage.As(ForgetPasswordPage.class).successMessageCheck(logger, "A password reset message was sent to your email address. Please check your inbox.");
	   
		// Navigate to mailinator website
		LogUltility.log(test, logger, "Navigate to mailinator");
		DriverContext.browser.GoToUrl("http://www.mailinator.com/");
		CurrentPage = GetInstance(Mailinator.class);
		
		// Fill the email field and click Go
		LogUltility.log(test, logger, "Fill the registered email: " + email + ", and click go");
		CurrentPage.As(Mailinator.class).FillInboxField(email);

		// Click the received message
		//Thread.sleep(10000);
		LogUltility.log(test, logger, "Click the received confirmation email");
		CurrentPage.As(Mailinator.class).lnkEmailTitle.click();
		
		// Click the Reset link
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the Reset Password link");
		DriverContext._Driver.switchTo().frame("msg_body");
		CurrentPage.As(Mailinator.class).lnkResetPassword1.click();
	
		// UMS page
		//Thread.sleep(3000);
		// Check if the page "UMS - reset password"  successfully loaded
		CurrentPage = GetInstance(UMSPage.class);
		CurrentPage.As(UMSPage.class).focusOnLastOpenedTab();
		CurrentPage.As(UMSPage.class).reloadUMS();

		// fill new password
		Setting.Language = "6";
		String newPassword = generateInput.RandomString(4, "letters");
		 Setting.Language = "10";
		LogUltility.log(test, logger, "Fill a new password only for conirm passowrd: " + newPassword);
		CurrentPage.As(UMSPage.class).txtPassword.sendKeys(newPassword);
		CurrentPage.As(UMSPage.class).txtConfirmPassword.sendKeys(newPassword);
		
		// Check for the error message
		//Thread.sleep(3000);
		String errorMessage = CurrentPage.As(UMSPage.class).msgError.getText();
	    logger.info("Get the system error message: " + errorMessage);
	    assertEquals(errorMessage, "password must be greater than 5");
	   
	    LogUltility.log(test, logger, "Test Case PASSED");
	}
			
	/**
	 * SRA-94:Verify user gets error message when submitting invalid email for forgot password
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA94_Verify_user_gets_error_message_when_submitting_invalid_email_for_forgot_password() throws InterruptedException, FileNotFoundException {
		test = extent.createTest("ForgotPassword-SRA-94", "Verify user gets error message when submitting invalid email for forgot password");

		   // Navigate to the forgot password page
		   DriverContext.browser.GoToUrl(Setting.AUT_URL);
		   CurrentPage= GetInstance(LoginPage.class);
		   //Thread.sleep(5000);
		   LogUltility.log(test, logger, "Click the forgot password link");
		   CurrentPage.As(LoginPage.class).ForgetPassword.click();

		   // Fill email address and click submit
		   //Thread.sleep(3000);
		   CurrentPage= GetInstance(ForgetPasswordPage.class);
		   LogUltility.log(test, logger, "fill in the address email");
		   String invalidEmail = generateInput.RandomString(10, "letters"); 
		   CurrentPage.As(ForgetPasswordPage.class).ForgetPassword(invalidEmail);
		   Thread.sleep(2000);
		   CurrentPage.As(ForgetPasswordPage.class).btnSubmit.click();
		   
		   // Check the system message
		   //Thread.sleep(5000);
		   String doneMessage = CurrentPage.As(ForgetPasswordPage.class).ErrorMessage.getText();
		   logger.info("Get the system error message: " + doneMessage);
		   assertEquals(doneMessage, "You have not given a correct e-mail address");
		   
		   LogUltility.log(test, logger, "Test Case PASSED");
	}
	 
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Forgot password",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
    	// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
		}
}
	