package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import jxl.read.biff.BiffException;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SelectPage;

/**
 * All tests in VoiceSelection folder
 *
 */
public class VoiceSelectionTests extends FrameworkInitialize {

	private static Connection con;
	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	public boolean isThereMoreThanOneAssignedDomain;
	private String usernameLogin = "";
	private String chosenProject = "";
	private String chosenAccount = "";


    /**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
     * @throws ClassNotFoundException 
     * @throws SQLException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("VoiceSelection", Setting.Browser);
			    
	// Logger
	logger = LogManager.getLogger(VoiceSelectionTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite = VoiceSelectionTests");
	logger.info("Voice selection Tests - FrameworkInitilize");
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	
	// Get account,project and domain with the requested language and logged-in email
	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
	chosenProject = account_project_domain.get(index).get(1);
	chosenAccount = account_project_domain.get(index).get(0);
	CurrentPage= GetInstance(HomePage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
//	generateInput = new RandomText();
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws InterruptedException 
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws InterruptedException {		
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		CurrentPage= GetInstance(HomePage.class);

		//Thread.sleep(10000);
		CurrentPage.As(HomePage.class).isHomepageReady();

		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}

	/**
	 * SRA 24: Check the gender dropdown
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA24_Check_the_gender_dropdown() throws InterruptedException, SQLException
	{
		test = extent.createTest("VoiceSelectionTests-SRA-24", "Check the gender dropdown");
		
		// Get values from Gender dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> genderValues = CurrentPage.As(HomePage.class).getGenderValues();
		LogUltility.log(test, logger, "Get values from Gender dropdown: " + genderValues);
		
		// Fetch the Gender values from the database
		List<String> assignedGenderValues = CurrentPage.As(HomePage.class).getGenderValuesFromDB(con);
		LogUltility.log(test, logger, "Fetch the Gender values from the database: " + assignedGenderValues);
		
		// Sort the two lists before comparing
		Collections.sort(genderValues);
		Collections.sort(assignedGenderValues);
		
		// Compare if the Gender dropdown values are the same in the database
		boolean compareResult = genderValues.equals(assignedGenderValues);
		LogUltility.log(test, logger, "Compare if the Gender dropdown values are the same in the database: " + compareResult);
		assertTrue(compareResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA 25: Check the age dropdown
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA25_Check_the_age_dropdown() throws InterruptedException, SQLException
	{
		test = extent.createTest("VoiceSelectionTests-SRA-25", "Check the age dropdown");
		
		// Get values from the Age dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> ageValues = CurrentPage.As(HomePage.class).getAgeValues();
		LogUltility.log(test, logger, "Get values from the Age dropdown: " + ageValues);
		
		// Fetch the Gender values from the database
		List<String> assignedAgeValues = CurrentPage.As(HomePage.class).getAgeValuesFromDB(con);
		LogUltility.log(test, logger, "Fetch the Gender values from the database: " + assignedAgeValues);
		
		// Sort the two lists before comparing
		Collections.sort(ageValues);
		Collections.sort(assignedAgeValues);
		
		// Compare if the Age dropdown values are the same in the database
		boolean compareResult = ageValues.equals(assignedAgeValues);
		LogUltility.log(test, logger, "Compare if the Age dropdown values are the same in the database: " + compareResult);
		assertTrue(compareResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA_26: Check the language dropdown
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA26_Check_the_language_dropdown() throws InterruptedException, SQLException
	{
		test = extent.createTest("VoiceSelectionTests-SRA-26", "Check the language dropdown");
		
		// Get values from Language dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
		List<String> languageValues = CurrentPage.As(HomePage.class).getLanguageValues();
		LogUltility.log(test, logger, "Get values from Language dropdown: " + languageValues);

		HashMap<String, String> LanguagesMap = new HashMap<String, String>();
		LanguagesMap.put("English","US English");
		LanguagesMap.put("עברית","Hebrew");
		
		List<String> mappedDropdownValues = new ArrayList<String>();
		for(int i=0;i<languageValues.size();i++)
			if(LanguagesMap.containsKey(languageValues.get(i)))
				mappedDropdownValues.add(LanguagesMap.get(languageValues.get(i)));
			else
				mappedDropdownValues.add(languageValues.get(i));
		
		// Fetch the Languages values from the database
		List<String> assignedLanguageValues = CurrentPage.As(HomePage.class).getLanguageValuesFromDB(con);
		LogUltility.log(test, logger, "Fetch the Languages values from the database: " + assignedLanguageValues);
		
		// Sort the two lists before comparing
		Collections.sort(mappedDropdownValues);
		Collections.sort(assignedLanguageValues);
		
		// Compare if the Language dropdown values are the same in the database
		boolean compareResult = mappedDropdownValues.equals(assignedLanguageValues);
		LogUltility.log(test, logger, "Compare if the Language dropdown values are the same in the database: " + compareResult);
		assertTrue(compareResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA_27: Check the voice dropdown
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA27_Check_the_voice_dropdown() throws InterruptedException, SQLException
	{
		test = extent.createTest("VoiceSelectionTests-SRA-27", "Check the voice dropdown");
		
		// Get values from Voice dropdown
		List<String> voiceValues = CurrentPage.As(HomePage.class).getVoiceValues();
		LogUltility.log(test, logger, "Get values from Voice dropdown: " + voiceValues);
		//delete first empty value
	//	voiceValues.remove(0);
		
		// Fetch the Voice values from the database
		String accountId = CurrentPage.As(HomePage.class).getAccountId(con,chosenAccount);
		List<String> assignedVoiceValues = CurrentPage.As(HomePage.class).getVoiceValuesFromDB(con, accountId);
		LogUltility.log(test, logger, "Fetch the Voice values from the database: " + assignedVoiceValues);
		
		// Sort the two lists before comparing
		Collections.sort(voiceValues);
		Collections.sort(assignedVoiceValues);
		
		LogUltility.log(test, logger,"Dropdown sorted voices: "+voiceValues);
		LogUltility.log(test, logger,"Database sorted voices: "+assignedVoiceValues);
		
		// Compare if the Voice dropdown values are the same in the database
		boolean compareResult = voiceValues.equals(assignedVoiceValues);
		LogUltility.log(test, logger, "Compare if the Voice dropdown values are the same in the database: " + compareResult);
		assertTrue(compareResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA_29: Check the Voice owner profile information
	 * @throws InterruptedException
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA29_Check_the_Voice_owner_profile_information() throws InterruptedException
	{
		test = extent.createTest("VoiceSelectionTests-SRA-29", "Check the Voice owner profile information");
		
		// Select Random voice from the voice dropdown
		CurrentPage= GetInstance(HomePage.class);
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Select Random voice from the voice dropdown: " + chosenVoice);
		
		// Get and compare the voice title
		//Thread.sleep(3000);
		CurrentPage.As(HomePage.class).checkProfileTitle();
		String voiceTitle = CurrentPage.As(HomePage.class).textVoiceTitle.getText();
		LogUltility.log(test, logger, "Get voiceTitle: " + voiceTitle);
		
		boolean containResult = voiceTitle.contains(chosenVoice);
		LogUltility.log(test, logger, "compare the voice title: " + containResult);
		assertTrue(containResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA_40: Verify user can search voice in the search field
	 * @throws InterruptedException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA40_Verify_user_can_search_voice_in_the_search_field() throws InterruptedException
	{
		test = extent.createTest("VoiceSelectionTests-SRA-40", "Verify user can search voice in the search field");
		
		// Is voice list ready and get the whole initial list
		List<String> initialList = CurrentPage.As(HomePage.class).areVoicesReady();

		// Filter by entering random voice
		String filteredByVoice = CurrentPage.As(HomePage.class).filterWithRandomVoice();
		LogUltility.log(test, logger, "Filter by entering random voice: " + filteredByVoice);
		
		// Click to close the voice dropdown
		CurrentPage.As(HomePage.class).lblDomain.click();
		
		// Check that we got a new list that includes the searched voice and this list
		// is not equal to the initialize voices list
		//Thread.sleep(3000);
		boolean result = CurrentPage.As(HomePage.class).checkFilteredVoice(test, logger, initialList, filteredByVoice);

//		List<String> voiceInDropdown = CurrentPage.As(HomePage.class).getVoiceValues();
//		LogUltility.log(test, logger, "Voice In Dropdown: " + voiceInDropdown);
//
//		boolean containResult = voiceInDropdown.contains(filteredByVoice);
		LogUltility.log(test, logger, "Check that the filtered by voice does exist in the voice dropdown values: " + result);
		assertTrue(result);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA_41: Verify user can filter with voice owners Age
	 * @throws InterruptedException
	 * @throws SQLException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	 public void SRA41_Verify_user_can_filter_with_voice_owners_Age() throws InterruptedException, SQLException 
	{
		test = extent.createTest("VoiceSelectionTests-SRA-41", "Verify user can filter with voice owners Age");
		
		 // Get random age for the active voices
		String chosenAge = CurrentPage.As(HomePage.class).getRandomAgeForActiveVoices(con);
		LogUltility.log(test, logger, "Get random age for the active voices then select it in the age dropdown: " + chosenAge);

        // Filter by the active chosen age by choosing it from the age dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
        CurrentPage.As(HomePage.class).select_age(chosenAge);

        //Thread.sleep(3000);
        
        // Check that the available voices are the voices that are connected to the chosen age in DB
        LogUltility.log(test, logger, "Check that the available voices are the voices that are connected to the chosen age in DB");
//        List<List<String>> table = new ArrayList<List<String>>();
        String accountId = CurrentPage.As(HomePage.class).getAccountId(con,chosenAccount);
        List<String> activeVoiceForChosenAge = new ArrayList<String>();
        activeVoiceForChosenAge = CurrentPage.As(HomePage.class).getActiveVoicesForAge(con, chosenAge, accountId);
//        List<String> activeVoiceForChosenAge = table.get(1);
        LogUltility.log(test, logger, "active Voice For Chosen Age: " + activeVoiceForChosenAge);
        
        // Get values from Voice dropdown
  		//Thread.sleep(10000);
  		List<String> voiceValuesFromDropdown = CurrentPage.As(HomePage.class).getVoiceValues();
  		// Delete first empty value
  	//	voiceValuesFromDropdown.remove(0);
  		LogUltility.log(test, logger, "Get values from Voice dropdown: " + voiceValuesFromDropdown);
  		
  		// Compare the voices in the dropdown to the db voices after we filtered with a voice active
  		Collections.sort(voiceValuesFromDropdown);
  		Collections.sort(activeVoiceForChosenAge);
  		boolean equalResult = voiceValuesFromDropdown.equals(activeVoiceForChosenAge);
  		LogUltility.log(test, logger, "Compare the voices in the dropdown to the db voices after we filtered with a voice active: " + equalResult);
  		System.out.println("EqualResult: " + equalResult);
		assertTrue(equalResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/*
	 /*
	 * SRA-42:Verify user can filter with voice owners Language
	 * @throws InterruptedException
	 * @throws SQLException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	 public void SRA42_Verify_user_can_filter_with_voice_owners_Language() throws InterruptedException, SQLException 
	{
		test = extent.createTest("VoiceSelectionTests-SRA-42", "Verify user can filter with voice owners Language");
		
		 // Get random age for the active voices
		String chosenLanguage = CurrentPage.As(HomePage.class).getRandomLanguageForActiveVoices(con);
		LogUltility.log(test, logger, "Get random Language for the active voices then select it in the language dropdown: " + chosenLanguage);
        
		// Map database value to dropdown value
		if(chosenLanguage.equals("Hebrew"))	chosenLanguage = "עברית";
		if(chosenLanguage.equals("US English")) chosenLanguage = "English";
		
        // Filter by the active chosen language by choosing it from the language dropdown
        CurrentPage.As(HomePage.class).select_language(chosenLanguage);
        
        //Thread.sleep(3000);
        
        // Check that the available voices are the voices that are connected to the chosen language in DB
        LogUltility.log(test, logger, "Check that the available voices are the voices that are connected to the chosen language in DB");
//        List<List<String>> table = new ArrayList<List<String>>();
        List<String> activeVoiceForChosenLanguage = new ArrayList<String>();
     
        // Map dropdown value to database value
     	if(chosenLanguage.equals("עברית"))	chosenLanguage = "Hebrew";
     	if(chosenLanguage.equals("English")) chosenLanguage = "US English";
     	
        String accountId = CurrentPage.As(HomePage.class).getAccountId(con,chosenAccount);
     	activeVoiceForChosenLanguage = CurrentPage.As(HomePage.class).getActiveVoicesForLanguage(con, chosenLanguage, accountId);
//        List<String> activeVoiceForChosenLanguage = table.get(1);
        LogUltility.log(test, logger, "active Voice For Chosen Language: " + activeVoiceForChosenLanguage);
        
        // Get values from Voice dropdown
  		//Thread.sleep(10000);
        CurrentPage.As(HomePage.class).isHomepageReady();
  		List<String> voiceValuesFromDropdown = CurrentPage.As(HomePage.class).getVoiceValues();
  		// Delete first empty value
  		//voiceValuesFromDropdown.remove(0);
  		LogUltility.log(test, logger, "Get values from Voice dropdown: " + voiceValuesFromDropdown);
  		
  		// Compare the voices in the dropdown to the db voices after we filtered with a voice active
  		Collections.sort(voiceValuesFromDropdown);
  		Collections.sort(activeVoiceForChosenLanguage);
  		boolean equalResult = voiceValuesFromDropdown.equals(activeVoiceForChosenLanguage);
  		LogUltility.log(test, logger, "Compare the voices in the dropdown to the db voices after we filtered with a voice active: " + equalResult);
  		System.out.println("EqualResult: " + equalResult);
		assertTrue(equalResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-43:Verify user can filter with voice owners Gender
	 * @throws InterruptedException
	 * @throws SQLException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	 public void SRA43_Verify_user_can_filter_with_voice_owners_Gender() throws InterruptedException, SQLException 
	{
		test = extent.createTest("VoiceSelectionTests-SRA-43", "Verify user can filter with voice owners Gender");
		
		 // Get random gender for the active voices
		String chosenGender = CurrentPage.As(HomePage.class).getRandomGenderForActiveVoices(con);
		LogUltility.log(test, logger, "Get random Gender for the active voices then select it in the gender dropdown: " + chosenGender);
        
        // Filter by the active chosen gender by choosing it from the gender dropdown
		CurrentPage.As(HomePage.class).isHomepageReady();
        CurrentPage.As(HomePage.class).select_gender(chosenGender);
        
        //Thread.sleep(3000);
        
        // Check that the available voices are the voices that are connected to the chosen gender in DB
        LogUltility.log(test, logger, "Check that the available voices are the voices that are connected to the chosen gender in DB");
//        List<List<String>> table = new ArrayList<List<String>>();
        List<String> activeVoiceForChosenGender = new ArrayList<String>();
        String accountId = CurrentPage.As(HomePage.class).getAccountId(con,chosenAccount);
        activeVoiceForChosenGender = CurrentPage.As(HomePage.class).getActiveVoicesForGender(con, chosenGender, accountId);
//        List<String> activeVoiceForChosenGender = table.get(1);
        LogUltility.log(test, logger, "active Voice For Chosen Gender: " + activeVoiceForChosenGender);
        
        // Get values from Voice dropdown
  		//Thread.sleep(10000);
  		List<String> voiceValuesFromDropdown = CurrentPage.As(HomePage.class).getVoiceValues();
  		// Delete first empty value
//  	voiceValuesFromDropdown.remove(0);
  		LogUltility.log(test, logger, "Get values from Voice dropdown: " + voiceValuesFromDropdown);
  		
  		// Compare the voices in the dropdown to the DB voices after we filtered with a voice active
  		Collections.sort(voiceValuesFromDropdown);
  		Collections.sort(activeVoiceForChosenGender);
  		boolean equalResult = voiceValuesFromDropdown.equals(activeVoiceForChosenGender);
  		LogUltility.log(test, logger, "Compare the voices in the dropdown to the db voices after we filtered with a voice active: " + equalResult);
		assertTrue(equalResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-44:Verify users can select voices successfully without using the filter options
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA44_Verify_users_can_select_voices_successfully_without_using_the_filter_options() throws InterruptedException, SQLException
	{
		test = extent.createTest("VoiceSelectionTests-SRA-44", "Verify users can select voices successfully without using the filter options");
		
		// Choose random voice
		String randomVoiceChosen = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Choose random voice: " + randomVoiceChosen);
		
		// Get the chosen voice is selected
		String selectedVoice = CurrentPage.As(HomePage.class).cmbVoice.getText();
		LogUltility.log(test, logger, "Selected value in voice dropdown: " + selectedVoice);
	
		// Compare if chosen voice is the selected voice in dropdown
		boolean compareResult = randomVoiceChosen.equals(selectedVoice);
		LogUltility.log(test, logger, "Compare if chosen voice is the selected voice in dropdown: " + compareResult);
		assertTrue(compareResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-116:Check the voice selection will reset after changing any value in the voice search engine
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA116_Check_the_voice_selection_will_reset_after_changing_any_value_in_the_voice_search_engine() throws InterruptedException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("VoiceSelectionTests-SRA-116", "Check the voice selection will reset after changing any value in the voice search engine");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		//CurrentPage.As(HomePage.class).chooseRandomDomain(con);
		
		// Get and compare the voice owner profile
		//Thread.sleep(3000);
		CurrentPage.As(HomePage.class).checkProfileTitle();
		String voiceTitle = CurrentPage.As(HomePage.class).textVoiceTitle.getText();
		LogUltility.log(test, logger, "Get voice title: " + voiceTitle);		
		boolean containResult = voiceTitle.contains(chosenVoice);
		LogUltility.log(test, logger, "compare the voice title: " + containResult);
		assertTrue(containResult);
		
		// Check if the text box is enabled
		boolean textBoxEnabled = CurrentPage.As(HomePage.class).Textinput.isEnabled();
		assertTrue(textBoxEnabled);
		
		// Enter text in the text box
//		String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Check that the entered text do exist
		String getTextBack = CurrentPage.As(HomePage.class).Textinput.getText();
		// Compare
		boolean compareResult = getTextBack.equals(randomText);
		LogUltility.log(test, logger, "Check that the entered text do exist: " + compareResult);
		assertTrue(compareResult);
				
		// Choose random value in the age dropdown
		Thread.sleep(1000);
		CurrentPage.As(HomePage.class).chooseRandomAge();
		
		// Check the voice owner profile does not exist
		CurrentPage.As(HomePage.class).checkProfileTitle();
		String voiceInfo = CurrentPage.As(HomePage.class).textVoiceTitle.getText();
		LogUltility.log(test, logger, "Voice profile info: " + voiceInfo);
		boolean isEmpty = voiceInfo.isEmpty();
		LogUltility.log(test, logger, "Is the Voice profile empty: " + isEmpty);
		assertTrue(isEmpty);
		
		// Also check that nothing chosen in the voice dropdown
		//Thread.sleep(3000);
		String selectedVoice = CurrentPage.As(HomePage.class).cmbVoice.getText();
		LogUltility.log(test, logger, "Selected value in voice dropdown: " + selectedVoice);
		// Compare
		compareResult = selectedVoice.equals("Select Voice");
		LogUltility.log(test, logger, "Check that nothing is chosen in the voice dropdown: " + compareResult);
		assertTrue(compareResult);
				
		//click the play button
		CurrentPage.As(HomePage.class).btnplay.click();
		
		//check popup displayed "select a voice"
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please select Voice     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-122:Verify the system does not returns value when searching with invalid keyword
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA122_Verify_the_system_does_not_returns_value_when_searching_with_invalid_keyword() throws InterruptedException 
	{
		test = extent.createTest("VoiceSelectionTests-SRA-122", "Verify the system does not returns value when searching with invalid keyword");
		
		// Input invalid symbols string and click the filter button
		String randomSymbols = CurrentPage.As(HomePage.class).randomSymbols(5);
		LogUltility.log(test, logger, "Random symbols to enter: " + randomSymbols);
		
		CurrentPage.As(HomePage.class).txtVoiceSearch.sendKeys(randomSymbols);
		CurrentPage.As(HomePage.class).btnVoiceSearch.click();
		
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Check that the voice dropdown is empty
		//Thread.sleep(3000);
		//Check if the voice dropdown is empty with timeiout 5 seconds
		boolean result = CurrentPage.As(HomePage.class).isVoiceListEmpty(test, logger);
		LogUltility.log(test, logger, "Is the voice dropdown empty: " + result);
		assertTrue(result);
		
//		
//		List<String> voicesInDropdown = CurrentPage.As(HomePage.class).getVoiceValues();
//		LogUltility.log(test, logger, "Values in voice dropdown: " + voicesInDropdown);
//		
//		boolean isItEmpty;
//		if (voicesInDropdown.size() == 1 && voicesInDropdown.get(0).length() == 0)
//			isItEmpty = true;
//		else isItEmpty = false;
//		LogUltility.log(test, logger, "Is the voice dropdown empty: " + isItEmpty);
//		assertTrue(isItEmpty);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-125:Verify the user filter and search the voices after opening exist file
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA125_Verify_the_user_filter_and_search_the_voices_after_opening_exist_file() throws InterruptedException, AWTException, SQLException 
	{
		test = extent.createTest("VoiceSelectionTests-SRA-125", "Verify the user filter and search the voices after opening exist file");
		
		// Select random domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithFiles(con, chosenProject,usernameLogin);
		LogUltility.log(test, logger, "Choose random domain with files: " + randomDomain);
		
		// Click the open button and check for open popup
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		//Thread.sleep(3000);
	
		// Choose random file and click open
		//String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile(); 
		CurrentPage.As(OpenPopup.class).openFileWait();
		CurrentPage.As(OpenPopup.class).openFileWait();
		String chosenFile = CurrentPage.As(OpenPopup.class).chooseRandomFile();
		LogUltility.log(test, logger, "Selected file: " + chosenFile);
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
			
		// Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		LogUltility.log(test, logger, "text in textbox: " + text);
			
		//choose a voice from the voice dropdown and check that the voice owner profile is displayed
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen voice: " + chosenVoice);
		
		// Get and compare the voice profile info
		//Thread.sleep(3000);
		CurrentPage.As(HomePage.class).checkProfileTitle();
		String voiceTitle = CurrentPage.As(HomePage.class).textVoiceTitle.getText();
		LogUltility.log(test, logger, "Get voiceTitle: " + voiceTitle);
		
		boolean containResult = voiceTitle.contains(chosenVoice);
		LogUltility.log(test, logger, "compare the voice title: " + containResult);
		assertTrue(containResult);
		LogUltility.log(test, logger, "Test Case PASSED");
	
}
	
	/**
	 * SRA-225:Verify that voice dropdown don't reset after pressing "clear text" 
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA225_Verify_that_voice_dropdown_dont_reset_after_pressing_clear_text() throws InterruptedException, SQLException
	{
		test = extent.createTest("VoiceSelectionTests-SRA-225", "Verify that voice dropdown don't reset after pressing \"clear text\" ");
		
		// Choose voice from the dorpdown
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Choose voice: "+chosenVoice);
		
		// Press the clear text button
		CurrentPage.As(HomePage.class).clearText.click();
		LogUltility.log(test, logger, "Press the clear text button");
		
		// Get the chosen voice value after clearing text
		String voiceAfterClear = CurrentPage.As(HomePage.class).cmbVoice.getText();
		LogUltility.log(test, logger, "Voice after pressing clear text: "+voiceAfterClear);
		
		// Check is same value
		boolean same = voiceAfterClear.equals(chosenVoice);
		LogUltility.log(test, logger, "Voice didn't change: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {		
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Voice Selection",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}
		
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 DriverContext._Driver.quit();
	    }
}