package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SavePopup;
import smi.rendering.test.pages.Say_As_Popup;
import smi.rendering.test.pages.SelectPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import static org.testng.Assert.assertEquals;

/**
 * All tests in Voice play controller option folder
 *
 */
public class VoicePlayTests  extends FrameworkInitialize {

	private static Connection con;
	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	private String usernameLogin = "";
	private String chosenProject = "";
	public boolean isThereMoreThanOneAssignedDomain;
	
	/**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("VoicePlay", Setting.Browser);
		
	// Logger
	logger = LogManager.getLogger(VoicePlayTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite = VoicePlayTests ");
	logger.info("Voice play controller option Tests - FrameworkInitilize");
	
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	usernameLogin = ExcelUltility.ReadCell("Email",1);

	// Get account,project and domain with the requested language and logged-in email
	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
	chosenProject = account_project_domain.get(index).get(1);
	
	CurrentPage= GetInstance(HomePage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		CurrentPage = GetInstance(HomePage.class);
		//Thread.sleep(10000);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}
	
	/**
	 * SRA-151:Verify the Volume feature applies to part of text
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA151_Verify_the_Volume_feature_applies_to_part_of_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-151", "Verify the Volume feature applies to part of text");

		// Select random domain
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen domain: " + chosenDomain);
		
		// Type in the textbox
	    String randomText = generateInput.RandomString(5, "letters") +
	    		" " + generateInput.RandomString(5, "letters");
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);

		// change the volume range bar
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();
		
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		for (int i=0; i<5; i++)
			CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));

		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
		
		// Verify that volume was applied to the text
//		String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
//	    LogUltility.log(test, logger, "What was applied: " + applied);
//	    assertEquals(applied, "volume");

		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-volume=");
		LogUltility.log(test, logger, "Is volume found: " + doContain);
		assertTrue(doContain);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA-152:Verify the Pitch feature applies to the whole text
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA152_Verify_the_Pitch_feature_applies_to_the_whole_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-152", "Verify the Pitch feature applies to the whole text");

		// Select random domain
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen domain: " + chosenDomain);
		
		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));

		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		// change the pitch range bar
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
				
//		// Verify that pitch was applied to the text
//		String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
//	    LogUltility.log(test, logger, "What was applied: " + applied);
//	    assertEquals(applied, "pitch");
		
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-pitch=");
		LogUltility.log(test, logger, "Is pitch found: " + doContain);
		assertTrue(doContain);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-153:Verify the Pitch feature applies to part of text
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA153_Verify_the_Pitch_feature_applies_to_part_of_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-153", " Verify the Pitch feature applies to part of text");
        
		// Select random domain
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen domain: " + chosenDomain);
		
		// Type in the textbox
	    String randomText = generateInput.RandomString(5, "letters") +
	    		" " + generateInput.RandomString(5, "letters");
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		for (int i=0; i<5; i++)
			CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		// change the pitch range bar
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
				
		// Verify that pitch was applied to the text
//		String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
//	    LogUltility.log(test, logger, "What was applied: " + applied);
//	    assertEquals(applied, "pitch");
		
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-pitch=");
		LogUltility.log(test, logger, "Is pitch found: " + doContain);
		assertTrue(doContain);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	

	/**
	 * SRA-154:User can undo the change pitch/speed/volume for part sentence
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA154_all_User_can_undo_the_change_pitch_speed_volume_for_part_sentence() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-154", "User can undo the change pitch/speed/volume for part sentence");
	
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
		// Select a domain
		//Thread.sleep(4000);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// change values for volume, speed and pitch
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the pitch range bar
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the volume range bar
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);

        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the speed range bar
		CurrentPage.As(HomePage.class).SpeedBar.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);

		// get bars values
//		String data_pitch = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-pitch");
//		String data_volume = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-volume");
//		String data_speed = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-speed");
		
		String data_pitch = CurrentPage.As(HomePage.class).pitchVal.getText();
		String data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();
		String data_speed = CurrentPage.As(HomePage.class).speedVal.getText();
		
		LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch
				+ " data_volume:" + data_volume + " data_speed:" + data_speed);
		
		// Check that the values are changed and not the defaults
		boolean valuesChanged = Integer.parseInt(data_pitch) > 100 && Integer.parseInt(data_volume) > 100
				&& Integer.parseInt(data_speed) > 100;
		assertTrue(valuesChanged);
		
		// Click the X button
		LogUltility.log(test, logger, "Click the X button");
		CurrentPage.As(HomePage.class).isHomepageReady();
		//CurrentPage.As(HomePage.class).xButton.click();
		CurrentPage.As(HomePage.class).xButtonVolume.click();
		CurrentPage.As(HomePage.class).xButtonPitch.click();
		CurrentPage.As(HomePage.class).xButtonSpeed.click();
		
		// CHECK NOW THAT THE BARS VALUES ARE THE DEFAULT NOW
		// get bars values
		data_pitch = CurrentPage.As(HomePage.class).pitchVal.getText();
		data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();
		data_speed = CurrentPage.As(HomePage.class).speedVal.getText();
		
		LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch
				+ " data_volume:" + data_volume + " data_speed:" + data_speed);
		
		// Check that the values are changed and not the defaults
		valuesChanged = Integer.parseInt(data_pitch) == 100 && Integer.parseInt(data_volume) == 100
				&& Integer.parseInt(data_speed) == 100;
		assertTrue(valuesChanged);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 *  SRA-158:check that the speed/volume/ Pitch values saved when saving file
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA158_check_that_the_speed_volume_Pitch_values_saved_when_saving_file() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-158", "check that the speed/volume/ Pitch values saved when saving file");
	
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
		// Select a domain
		//Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// change values for volume, speed and pitch
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the pitch range bar
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the volume range bar
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);

        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the speed range bar
		CurrentPage.As(HomePage.class).SpeedBar.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);

		String data_pitch = CurrentPage.As(HomePage.class).pitchVal.getText();
		String data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();
		String data_speed = CurrentPage.As(HomePage.class).speedVal.getText();
		
		LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch
				+ " data_volume:" + data_volume + " data_speed:" + data_speed);
		
		// Check that the values are changed and not the defaults
		boolean valuesChanged = Integer.parseInt(data_pitch) > 100 && Integer.parseInt(data_volume) > 100
				&& Integer.parseInt(data_speed) > 100;
		assertTrue(valuesChanged);
		
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		//Thread.sleep(2000);
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveBtn.click();
		
		// Refresh the web page
		DriverContext._Driver.navigate().refresh();
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Reselect the domain
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "Reselect the domain");
		
		// choose the previous domain
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, randomDomain);
		
	    // Click the open button and open the saved file
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		CurrentPage.As(OpenPopup.class).openFileWait();
//		CurrentPage.As(OpenPopup.class).cmbText.click();
		LogUltility.log(test, logger, "Open the saved file");
		CurrentPage.As(OpenPopup.class).SelectFromFileDropdown(fileName);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();

		
		// CHECK NOW THAT THE BARS VALUES ARE THE same before the save
		// get bars values
		CurrentPage = GetInstance(HomePage.class);
		data_pitch = CurrentPage.As(HomePage.class).pitchVal.getText();
		data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();
		data_speed = CurrentPage.As(HomePage.class).speedVal.getText();
		
		LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch
				+ " data_volume:" + data_volume + " data_speed:" + data_speed);
		
		// Check that the values are changed and not the defaults
		valuesChanged = Integer.parseInt(data_pitch) == 100 && Integer.parseInt(data_volume) == 100
				&& Integer.parseInt(data_speed) == 100;
		assertTrue(valuesChanged);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-161: Check selecting a bar values in stages
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA161_Check_selecting_a_bar_values_in_stages() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-161", "Check selecting a bar values in stages");
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		ArrayList<String> bars = new ArrayList<String>();
		bars.addAll(Arrays.asList("volume", "speed", "pitch"));
		Collections.shuffle(bars);
		
		LogUltility.log(test, logger, "Bars order for testing: " + bars);
		
		// Select a domain
		//Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// need to change the 3 bars values each after another and check the values
		String data_pitch = "";
		String data_volume = "";
		String data_speed = "";
		for (int b = 0; b < 3; b++) {
		String chosenBar = bars.get(b);
			
			switch (chosenBar) {
			case "pitch":
					LogUltility.log(test, logger, "PART 1");
					
			        robot.mouseMove(point.getX(),point.getY());
			        robot.mouseMove(point.getX()-10,point.getY()-10);
					// change the pitch range bar
					CurrentPage.As(HomePage.class).hsbPitch.click();
					for (int i=0; i<100; i++)
						CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
					// Check that the current bar change do exist in he text editor
					data_pitch = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-pitch");
					LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch);
					// if this is the second bar then do check the first bar change do also exist
					if (b == 1 || b == 2)	
						switch (bars.get(0)) {
							case "volume":
								data_volume = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-volume");
								LogUltility.log(test, logger, "Changed the bar values to - data_volume:" + data_volume);
								break;
							case "speed":
								data_speed = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-speed");
								LogUltility.log(test, logger, "Changed the bar values to - data_speed:" + data_speed);
								break;
							}
					if (b == 2)	
						switch (bars.get(1)) {
							case "volume":
								data_volume = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-volume");
								LogUltility.log(test, logger, "Changed the bar values to - data_volume:" + data_volume);
								break;
							case "speed":
								data_speed = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-speed");
								LogUltility.log(test, logger, "Changed the bar values to - data_speed:" + data_speed);
								break;
							}
				break;
			case "volume":
					LogUltility.log(test, logger, "PART 2");
					
			        robot.mouseMove(point.getX(),point.getY());
			        robot.mouseMove(point.getX()-10,point.getY()-10);
					// change the volume range bar
					CurrentPage.As(HomePage.class).hsbVolume.click();
					for (int i=0; i<100; i++)
						CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
					// Check that the current bar change do exist in he text editor
					data_volume = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-volume");
					LogUltility.log(test, logger, "Changed the bar values to - data_volume:" + data_volume);
					// if this is the second bar then do check the first bar change do also exist
					if (b == 1 || b == 2)	
						switch (bars.get(0)) {
							case "pitch":
								data_pitch = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-pitch");
								LogUltility.log(test, logger, "Changed the bar values to - data_speed:" + data_speed);
								break;
							case "speed":
								data_speed = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-speed");
								LogUltility.log(test, logger, "Changed the bar values to - data_speed:" + data_speed);
								break;
							}
					if (b == 2)	
						switch (bars.get(1)) {
							case "pitch":
								data_pitch = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-pitch");
								LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch);
								break;
							case "speed":
								data_speed = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-speed");
								LogUltility.log(test, logger, "Changed the bar values to - data_speed:" + data_speed);
								break;
							}
				break;
			case "speed":
				LogUltility.log(test, logger, "PART 3");
				
				
			        robot.mouseMove(point.getX(),point.getY());
			        robot.mouseMove(point.getX()-10,point.getY()-10);
					// change the speed range bar
					CurrentPage.As(HomePage.class).SpeedBar.click();
					for (int i=0; i<100; i++)
						CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
					// Check that the current bar change do exist in he text editor
					data_speed = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-speed");
					LogUltility.log(test, logger, "Changed the bar values to - data_speed:" + data_speed);
					// if this is the second bar then do check the first bar change do also exist
					if (b == 1 || b == 2)	
						switch (bars.get(0)) {
							case "pitch":
								data_pitch = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-pitch");
								LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch);
								break;
							case "volume":
								data_volume = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-volume");
								LogUltility.log(test, logger, "Changed the bar values to - data_volume:" + data_volume);
								break;
							}
					if (b == 2)	
						switch (bars.get(1)) {
							case "pitch":
								data_pitch = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-pitch");
								LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch);
								break;
							case "volume":
								data_volume = CurrentPage.As(HomePage.class).textEditorSpan.getAttribute("data-volume");
								LogUltility.log(test, logger, "Changed the bar values to - data_volume:" + data_volume);
								break;
							}
				break;
			}
			
		}


		LogUltility.log(test, logger, "Changed the bar values to - data_pitch:" + data_pitch
				+ " data_volume:" + data_volume + " data_speed:" + data_speed);
		
		// Check that the values are changed and not the defaults
		boolean valuesChanged = Integer.parseInt(data_pitch) > 100 && Integer.parseInt(data_volume) > 100
				&& Integer.parseInt(data_speed) > 100;
		assertTrue(valuesChanged);
		
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-155:User can the change pitch/speed/volume combine with emphasis
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA155_User_can_the_change_pitch_speed_volume_combine_with_emphasis() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-155", "User can the change pitch/speed/volume combine with emphasis");
	
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
		// Select a domain
		//Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();	    
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		// Click the bold button
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Click the bold button");
		CurrentPage.As(HomePage.class).btnBold.click();
		
		// change values for volume, speed and pitch
		// change the pitch range bar
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
		// change the volume range bar
		//Thread.sleep(1000);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
		// change the speed range bar
		//Thread.sleep(1000);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		CurrentPage.As(HomePage.class).SpeedBar.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
						
		// Check that the text is bolded
		//Thread.sleep(2000);
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);
		//boolean doContain = source.contains("data-type=\"emphasis\"");
		boolean doContain = source.contains("data-emphasis=");
		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
		assertTrue(doContain);

		// Check that the volume and pitch and speed are also set
		doContain = source.contains("data-pitch=");
		LogUltility.log(test, logger, "Is pitch found: " + doContain);
		assertTrue(doContain);
		doContain = source.contains("data-volume=");
		LogUltility.log(test, logger, "Is volume found: " + doContain);
		assertTrue(doContain);
		doContain = source.contains("data-speed=");
		LogUltility.log(test, logger, "Is speed found: " + doContain);
		assertTrue(doContain);
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-156:User can change pitch/speed/volume combine with Say As
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA156_User_can_change_pitch_speed_volume_combine_with_Say_As() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-156", "User can change pitch/speed/volume combine with Say As");
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
		// Select a domain
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(5, "words");
	    String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
//		        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		//Thread.sleep(5000);
		robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
  		builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
  		
	//	robot.mouseMove(point.getX()+60,point.getY()+105);
//			    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//			    robot.delay(1000);
//			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
	    String randomItalicText = generateInput.RandomString(5, "letters") + " " + generateInput.RandomString(5, "letters");
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		CurrentPage.As(Say_As_Popup.class).btnOK.click();
		
//		--------------------------

		//robot.mouseMove(point.getX()+60,point.getY()+105);
        robot.mouseMove(point.getX(),point.getY());
        CurrentPage = GetInstance(HomePage.class);
        
		// Mark the text in the text area
		//Thread.sleep(2000);
        CurrentPage.As(HomePage.class).Textinput.click();
        LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		
		// change values for volume, speed and pitch
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the pitch range bar
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the volume range bar
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the speed range bar
		CurrentPage.As(HomePage.class).SpeedBar.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
						
		// Check that the text is bolded
		//Thread.sleep(2000);
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);
//		boolean doContain = source.contains("data-type=\"emphasis\"");
//		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
//		assertTrue(doContain);

		// Check that the volume and pitch and speed are also set
		boolean doContain = source.contains("data-pitch=");
		LogUltility.log(test, logger, "Is pitch found: " + doContain);
		assertTrue(doContain);
		doContain = source.contains("data-volume=");
		LogUltility.log(test, logger, "Is volume found: " + doContain);
		assertTrue(doContain);
		doContain = source.contains("data-speed=");
		LogUltility.log(test, logger, "Is speed found: " + doContain);
		assertTrue(doContain);
		
		
		// Check if the entered IPA text do exist as italic in the text editor
		boolean exist = source.contains("font-style: italic;");
		LogUltility.log(test, logger, "Italic text also found: " + exist);
		assertTrue(exist);
			
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-167:Verify adding IPA for words with changed Volume /speed /Pitch
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA167_Verify_adding_IPA_for_words_with_changed_Volume_speed_Pitch() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-167", " SRA-167:Verify adding IPA for words with changed Volume /speed /Pitch");
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
		// Select a domain
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(5, "words");
	    String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
		
        CurrentPage = GetInstance(HomePage.class);
        
		// Mark the text in the text area
		//Thread.sleep(2000);
        CurrentPage.As(HomePage.class).Textinput.click();
        LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		
		// change values for volume, speed and pitch
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the pitch range bar
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the volume range bar
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the speed range bar
		CurrentPage.As(HomePage.class).SpeedBar.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
		
		LogUltility.log(test, logger, "Finished modifiying the bars");
		//-----------------------------------------------------
		//robot.mouseMove(point.getX(),point.getY());
//        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
//	    robot.delay(1000);
//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);

		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		// Click the italic button
		//Thread.sleep(5000);
		robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		LogUltility.log(test, logger, "Open the Say As window");
		Actions builder = new Actions(DriverContext._Driver);
			builder.moveToElement(CurrentPage.As(HomePage.class).Italicbtn).click().perform();
			
		//	robot.mouseMove(point.getX()+60,point.getY()+105);
		//	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
		//	    robot.delay(1000);
		//	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		
		// Write the say as text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(Say_As_Popup.class);
		CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
		String randomItalicText = generateInput.RandomString(5, "letters") + " " + generateInput.RandomString(5, "letters");
		LogUltility.log(test, logger, "input text to the text area: " + randomItalicText);
		CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
		
		// Click the OK button
		//Thread.sleep(2000);
		CurrentPage.As(Say_As_Popup.class).btnOK.click();


		// Check the text
		//Thread.sleep(2000);
		CurrentPage = GetInstance(HomePage.class);
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);
//		boolean doContain = source.contains("data-type=\"emphasis\"");
//		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
//		assertTrue(doContain);

		// Check that the volume and pitch and speed are also set
		boolean doContain = source.contains("data-pitch=");
		LogUltility.log(test, logger, "Is pitch found: " + doContain);
		assertTrue(doContain);
		doContain = source.contains("data-volume=");
		LogUltility.log(test, logger, "Is volume found: " + doContain);
		assertTrue(doContain);
		doContain = source.contains("data-speed=");
		LogUltility.log(test, logger, "Is speed found: " + doContain);
		assertTrue(doContain);
		
		
		// Check if the entered IPA text do exist as italic in the text editor
		boolean exist = source.contains("font-style: italic;");
		LogUltility.log(test, logger, "Italic text also found: " + exist);
		assertTrue(exist);
			
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 * SRA-61:Verify the speed feature applies to the whole text
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA61_Verify_the_speed_feature_applies_to_the_whole_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-61", "Verify the speed feature applies to the whole text");
		
		// Select random domain
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();
		
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		// change the speed range bar
		CurrentPage.As(HomePage.class).SpeedBar.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
		
		// Verify that speed was applied to the text
//		String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
//	    LogUltility.log(test, logger, "What was applied: " + applied);
//	    assertEquals(applied, "speed");
		
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-speed=");
		LogUltility.log(test, logger, "Is speed found: " + doContain);
		assertTrue(doContain);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-63:Verify the Volume feature applies to the whole text
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA63_Verify_the_Volume_feature_applies_to_the_whole_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-63", "Verify the Volume feature applies to the whole text");
        
		// Select random domain
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		// change the volume range bar
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
		
		// Verify that volume was applied to the text
//		String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
//	    LogUltility.log(test, logger, "What was applied: " + applied);
//	    assertEquals(applied, "volume");
		  
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-volume=");
		LogUltility.log(test, logger, "Is volume found: " + doContain);
		assertTrue(doContain);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-35:Check the play button starts the clip
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA35_Check_the_play_button_starts_the_clip() throws InterruptedException, SQLException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-35", "Check the play button starts the clip");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice );	
		
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();		
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				
		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-160:Try text with non regular quotes/double quotes
	 * @throws InterruptedException 
	 * @throws SQLException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA160_Try_text_with_non_regular_quotes_double_quotes() throws InterruptedException, SQLException
	{		
		test = extent.createTest("VoicePlayTests-SRA-160", "Try text with non regular quotes/double quotes");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
		
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
	    CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Enter text in the text box
		//Thread.sleep(3000);
		String randomText = "I said, �This isn�t going to happen.� I ended up passing all the ambulances because they would be actively triaging people.�";
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				
		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-36:Check the pause button
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA36_Check_the_pause_button() throws InterruptedException, SQLException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-36", "Check the pause button");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);	
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);
		
		// Click clear text first
		CurrentPage.As(HomePage.class).isHomepageReady();
		CurrentPage.As(HomePage.class).clearText.click();
		
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(40, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
		
		//Click the play button and Check that there is now that sound is pausing and there is a play icon
		CurrentPage.As(HomePage.class).btnplay.click();
		//Thread.sleep(1000);
		boolean buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPlay.isDisplayed();
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
//		Thread.sleep(3000);
		// Click the play button and check that there is now a pause icon
		CurrentPage.As(HomePage.class).btnplay.click();
//		Thread.sleep(3000);
		buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPause.isDisplayed();
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-52:Check the �Please select voice� popup message
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA52_Check_the_Please_select_voice_popup_message() throws InterruptedException, SQLException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-52", "Check the �Please select voice� popup message");
		
		// Select random domain
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		//check popup displayed "select a voice"
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please select Voice     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	

	
	/**
	 *  SRA-65:Check the clip simply resume after pausing the clip
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA65_Check_the_clip_simply_resume_after_pausing_the_clip() throws InterruptedException, SQLException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-65", "Check the clip simply resume after pausing the clip");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);	
		
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(40, "words");
		String randomText = RandomText.getSentencesFromFile() + RandomText.getSentencesFromFile()  + RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Click the play button");
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
		
		// Click the play button and Check that there now that sound is pausing and there is a play icon
		CurrentPage.As(HomePage.class).btnplay.click();
		//Thread.sleep(3000);
		boolean buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPlay.isDisplayed();
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
		
		// Click the play button and check that there is now a play button again 
		CurrentPage.As(HomePage.class).btnplay.click();
		//Thread.sleep(1000);
		buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPause.isDisplayed();
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-66:Verify the system must reload the clip after any change in the text field
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void  SRA66_Verify_the_system_must_reload_the_clip_after_any_change_in_the_text_field() throws InterruptedException, SQLException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-66", "Verify the system must reload the clip after any change in the text field");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);	
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);
		
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(10, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
		
		// Click the play button and Check that there now that sound is pausing and there is a play icon
		CurrentPage.As(HomePage.class).btnplay.click();
		//Thread.sleep(3000);
		boolean buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPlay.isDisplayed();
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
		
		// Add more text to the text editor
//		Thread.sleep(3000);
//		randomText = generateInput.RandomString(1, "words");
		randomText = RandomText.getSentencesFromFile();
//		randomText = " הוראת קבע ו - 2 שיקים לפיקדון מיועדים להפרע בעוד 4 ימים , והיתרה המשוערכת תהיה כ - אלף שלוש מאות שח .";
		LogUltility.log(test, logger, "Text to add: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button again
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		LogUltility.log(test, logger, "Click the play button again");
		
		// Check that text is being rendered again
		double timeTakenToRender = CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
//		Thread.sleep(3000);
		LogUltility.log(test, logger, "Time taken to render the text: " + timeTakenToRender);
		boolean result = timeTakenToRender > 0 ? true : false;
		LogUltility.log(test, logger, "timeTakenToRender > 0: " + result);
		assertTrue(result);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-120:Verify the system must reset the block of time in the wav clip after any change in the text field
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA120_Verify_the_system_must_reset_the_block_of_time_in_the_wav_clip_after_any_change_in_the_text_field() throws InterruptedException, AWTException, SQLException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-120", "Verify the system must reset the block of time in the wav clip after any change in the text field");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);	
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);
		
		// Enter text in the text box
		//Thread.sleep(3000);
//		String randomText = generateInput.RandomString(4, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
		
		//Click the play button and Check that there is now that sound is pausing and there is a play icon
		CurrentPage.As(HomePage.class).btnplay.click();
		//Thread.sleep(1000);
		boolean buttonStatus = false;
		try {
			buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPlay.isDisplayed();
		} catch (Exception e) {
			System.out.println("e: " + e);
		}
		
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
		
		// Add time block in the wave bar
		LogUltility.log(test, logger, "Add region block in the wav bar");
		Point point = CurrentPage.As(HomePage.class).barWave.getLocation();
        Robot robot = new Robot();
	    robot.mouseMove(point.getX()+70,point.getY()+140);
	    robot.mousePress(java.awt.event.InputEvent.BUTTON1_DOWN_MASK);
	    robot.mouseMove(point.getX()+150,point.getY()+140);
	    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_DOWN_MASK);
	    
	    // Check that the added time block does exist
	    boolean result = CurrentPage.As(HomePage.class).doRegionBlockExist();
	    LogUltility.log(test, logger, "Region exist: " + result);
	    assertTrue(result);
	    
	    // Change the text in the textbox
		//Thread.sleep(3000);
//		randomText = generateInput.RandomString(1, "words");
		randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
	    
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
		
		//Click the play button and Check that there is now that sound is pausing and there is a play icon
		CurrentPage.As(HomePage.class).btnplay.click();
		//Thread.sleep(1000);
		buttonStatus = CurrentPage.As(HomePage.class).PlayButtonPlay.isDisplayed();
		LogUltility.log(test, logger, "Pause button status: " + buttonStatus);
		assertTrue(buttonStatus);
		
		// Now check that previous region does not exist anymore
		result = CurrentPage.As(HomePage.class).doRegionBlockExist();
	    LogUltility.log(test, logger, "Region exist: " + result);
	    assertFalse(result);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-150:Verify the speed feature applies to part of the text
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA150_Verify_the_speed_feature_applies_to_part_of_the_text() throws InterruptedException, SQLException, AWTException, FileNotFoundException
	{		
		test = extent.createTest("VoicePlayTests-SRA-150", "Verify the speed feature applies to part of the text");
        
		// Select random domain
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen domain: " + chosenDomain);
		
		// Type in the textbox
	    String randomText = generateInput.RandomString(5, "letters") +
	    		" " + generateInput.RandomString(5, "letters");
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();
		
		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		for (int i=0; i<5; i++)
			CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
		// change the speed range bar
		CurrentPage.As(HomePage.class).SpeedBar.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
		
		// Verify that speed was applied to the text
//		String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
//	    LogUltility.log(test, logger, "What was applied: " + applied);
//	    assertEquals(applied, "speed");
		  
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-speed=");
		LogUltility.log(test, logger, "Is speed found: " + doContain);
		assertTrue(doContain);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  SRA-154:User can undo the change pitch for part sentence
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA154_pitch_User_can_undo_the_change_pitch_for_part_sentence() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-154", "User can undo the change pitch for part sentence");
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
		// Select a domain
		//Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(1, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// change values for volume, speed and pitch
		
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the pitch range bar
		CurrentPage.As(HomePage.class).hsbPitch.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);

		String data_pitch = CurrentPage.As(HomePage.class).pitchVal.getText();

		LogUltility.log(test, logger, "Changed the pitch bar value to - data_pitch:" + data_pitch);
		
		// Check that the values are changed and not the defaults
		boolean valuesChanged = Integer.parseInt(data_pitch) > 100;
		assertTrue(valuesChanged);
		
		CurrentPage.As(HomePage.class).isHomepageReady();
		// Click the X button
		LogUltility.log(test, logger, "Click the X button");
		//CurrentPage.As(HomePage.class).xButton.click();
		CurrentPage.As(HomePage.class).xButtonPitch.click();
		
		// Check that pitch was removed
		// Check that there is no pitch attribute		
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		// Check that the pitch is set
		boolean doContain = source.contains("data-pitch=");
		LogUltility.log(test, logger, "Is pitch found: " + doContain);
		assertFalse(doContain);

		// Check the pitch bar is back to default
		data_pitch = CurrentPage.As(HomePage.class).pitchVal.getText();
		valuesChanged = Integer.parseInt(data_pitch) == 100;
		LogUltility.log(test, logger, "Changed the pitch bar value to - data_pitch:" + data_pitch);
		assertTrue(valuesChanged);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 *  SRA-154:User can undo the change speed for part sentence
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA154_speed_User_can_undo_the_change_speed_for_part_sentence() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-154", "User can undo the change speed for part sentence");
        
		// Select a domain
		//Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
	    CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(1, "words");
	    String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// change values for volume, speed and pitch
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the speed range bar
		CurrentPage.As(HomePage.class).hsbSpeed.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbSpeed.sendKeys(Keys.RIGHT);

		String data_speed = CurrentPage.As(HomePage.class).speedVal.getText();

		LogUltility.log(test, logger, "Changed the speed bar value to - data_speed:" + data_speed);
		
		// Check that the values are changed and not the defaults
		boolean valuesChanged = Integer.parseInt(data_speed) > 100;
		assertTrue(valuesChanged);
		
		CurrentPage.As(HomePage.class).isHomepageReady();
		// Click the X button
		LogUltility.log(test, logger, "Click the X button");
		//CurrentPage.As(HomePage.class).xButton.click();
		CurrentPage.As(HomePage.class).xButtonSpeed.click();
		
		// Check that speed was removed
		// Check that there is no speed attribute		
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		// Check that the speed is set
		boolean doContain = source.contains("data-speed=");
		LogUltility.log(test, logger, "Is speed found: " + doContain);
		assertFalse(doContain);

		// Check the speed bar is back to default
		data_speed = CurrentPage.As(HomePage.class).speedVal.getText();
		valuesChanged = Integer.parseInt(data_speed) == 100;
		LogUltility.log(test, logger, "Changed the speed bar value to - data_speed:" + data_speed);
		assertTrue(valuesChanged);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	/**
	 *  SRA-154:User can undo the change volume for part sentence
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA154_volume_User_can_undo_the_change_volume_for_part_sentence() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("VoicePlayTests-SRA-154", "User can undo the change volume for part sentence");
        
		// Select a domain
		//Thread.sleep(4000);
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//			String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Type in the textbox
		//Thread.sleep(2000);
//	    String randomText = generateInput.RandomString(1, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Mark the text in the text area
		//Thread.sleep(2000);
		LogUltility.log(test, logger, "Highlight the text in the textbox");
		//CurrentPage.As(HomePage.class).Textinput.click();

		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		//Thread.sleep(2000);
		
		// change values for volume, speed and pitch
		
		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
		// change the volume range bar
		CurrentPage.As(HomePage.class).hsbVolume.click();
		for (int i=0; i<100; i++)
			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);

		String data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();

		LogUltility.log(test, logger, "Changed the volume bar value to - data_volume:" + data_volume);
		
		// Check that the values are changed and not the defaults
		boolean valuesChanged = Integer.parseInt(data_volume) > 100;
		assertTrue(valuesChanged);
		
		CurrentPage.As(HomePage.class).isHomepageReady();
		// Click the X button
		LogUltility.log(test, logger, "Click the X button");
		//CurrentPage.As(HomePage.class).xButton.click();
		CurrentPage.As(HomePage.class).xButtonVolume.click();
		
		// Check that volume was removed
		// Check that there is no volume attribute
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		// Check that the volume is set
		boolean doContain = source.contains("data-volume=");
		LogUltility.log(test, logger, "Is volume found: " + doContain);
		assertFalse(doContain);

		// Check the volume bar is back to default
		data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();
		valuesChanged = Integer.parseInt(data_volume) == 100;
		LogUltility.log(test, logger, "Changed the volume bar value to - data_volume:" + data_volume);
		assertTrue(valuesChanged);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	
	/**
	 * SRA-170:Verify that user can play only gestures without text
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws AWTException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  SRA170_Verify_that_user_can_play_only_gestures_without_text() throws InterruptedException, SQLException, AWTException
	{		
		test = extent.createTest("VoicePlayTests-SRA-170", "Verify that user can play only gestures without text");
		
		// Select random voice and domain
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);	
		
		// Choose random gesture
		//Thread.sleep(5000);
		LogUltility.log(test, logger, "Press into the textbox");
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).Textinput.click();
		//Thread.sleep(4000);
		CurrentPage.As(HomePage.class).btnGesture.click();
		String chosenGesture = CurrentPage.As(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Chosen Gesture: " + chosenGesture);
		CurrentPage.As(HomePage.class).btnGesture.click();
		chosenGesture = CurrentPage.As(HomePage.class).chooseRandomGesture();
		LogUltility.log(test, logger, "Chosen Gesture: " + chosenGesture);
		
		// Click the play button
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).btnplay.click();
		
		// Wait for the test to start playing
		//Thread.sleep(2000);
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				
		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
//	/**
//	 * SRA-175:Verify that user can`t change VSP for part of the word
//	 * @throws InterruptedException
//	 * @throws SQLException
//	 * @throws AWTException 
//	 * @throws FileNotFoundException 
//	 */
//	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
//	public void SRA175_Verify_that_user_can_t_change_VSP_for_part_of_the_word() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
//	{
//		test = extent.createTest("VoicePlayTests-SRA-175", "Verify that user can`t change VSP for part of the word");
//		
//		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
//        Robot robot = new Robot();
//        
//		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
//
//		// Type in the textbox
//		//Thread.sleep(2000);
//	    String randomAllText = generateInput.RandomString(6, "letters");
//		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomAllText);
//		
////		        robot.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
////			    robot.delay(1000);
////			    robot.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
//		
//		// Mark the text in the text area
//		//Thread.sleep(2000);
//		LogUltility.log(test, logger, "Highlight the text in the textbox");
//		
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
//		for (int i=0; i<3; i++)
//			CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
//		
////		--------------------------
//		
//		
//		// change values for volume, speed and pitch
//        robot.mouseMove(point.getX(),point.getY());
//        robot.mouseMove(point.getX()-10,point.getY()-10);
//		// change the pitch range bar
//		CurrentPage.As(HomePage.class).hsbPitch.click();
//		for (int i=0; i<100; i++)
//			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
//		
//        robot.mouseMove(point.getX(),point.getY());
//        robot.mouseMove(point.getX()-10,point.getY()-10);
//		// change the volume range bar
//		CurrentPage.As(HomePage.class).hsbVolume.click();
//		for (int i=0; i<100; i++)
//			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
//		
//        robot.mouseMove(point.getX(),point.getY());
//        robot.mouseMove(point.getX()-10,point.getY()-10);
//		// change the speed range bar
//		CurrentPage.As(HomePage.class).SpeedBar.click();
//		for (int i=0; i<100; i++)
//			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
//						
//		// Check that the text is bolded
//		//Thread.sleep(2000);
//		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
//		LogUltility.log(test, logger, "Source text in text editor: " + source);
////		boolean doContain = source.contains("data-type=\"emphasis\"");
////		LogUltility.log(test, logger, "Is text emphasized: " + doContain);
////		assertTrue(doContain);
//
//		// Check that the volume and pitch and speed are also set
//		boolean doContain = source.contains("data-pitch=");
//		LogUltility.log(test, logger, "Is pitch found: " + doContain);
//		assertTrue(doContain);
//		doContain = source.contains("data-volume=");
//		LogUltility.log(test, logger, "Is volume found: " + doContain);
//		assertTrue(doContain);
//		doContain = source.contains("data-speed=");
//		LogUltility.log(test, logger, "Is speed found: " + doContain);
//		assertTrue(doContain);
//		
//		
//		// Check if the entered IPA text do exist as italic in the text editor
//		boolean exist = source.contains("font-style: italic;");
//		LogUltility.log(test, logger, "Italic text also found: " + exist);
//		assertTrue(exist);
//			
//		
//		LogUltility.log(test, logger, "Test Case PASSED");
//	 }
	
//	
//	/**
//	 * SRA-175: VOLUME Verify that user can`t change VSP for part of the word
//	 * @throws InterruptedException 
//	 * @throws SQLException 
//	 * @throws AWTException 
//	 */
//	@Test  (priority = 1, groups= "Full Regression")
//	public void SRA175_VOLUME_Verify_that_user_can_t_change_VSP_for_part_of_the_word() throws InterruptedException, SQLException, AWTException
//	{		
//		test = extent.createTest("VoicePlayTests-SRA-175", "VOLUME Verify that user can`t change VSP for part of the word");
//		
//		// Select random domain
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen domain: " + chosenDomain);
//		
//		// Type in the textbox
//	    String randomText = CurrentPage.As(HomePage.class).RandomString(5) +
//	    		" " + CurrentPage.As(HomePage.class).RandomString(5);
//		LogUltility.log(test, logger, "input text to the text area: " + randomText);
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
//		
//		// change the volume range bar
//		// Mark the text in the text area
//		LogUltility.log(test, logger, "Highlight the text in the textbox");
//		//CurrentPage.As(HomePage.class).Textinput.click();
//		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
//        Robot robot = new Robot();
//        robot.mouseMove(point.getX(),point.getY());
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
//		for (int i=0; i<7; i++)
//			CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
//		
//		Thread.sleep(1000);
//		
//		//Actions builder = new Actions(DriverContext._Driver);
//		//builder.keyDown(Keys.SHIFT).perform();
//		
////		CurrentPage.As(HomePage.class).hsbVolume.click();
////		for (int i=0; i<100; i++)
////			CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
//		
//		//builder.keyUp(Keys.SHIFT).perform();
//		  
//		assertTrue(CurrentPage.As(HomePage.class).volumeRange.isDisplayed());
//
//		  Dimension sliderSize = CurrentPage.As(HomePage.class).volumeRange.getSize();
//		  int sliderWidth = sliderSize.getWidth();
//		  int xCoord = CurrentPage.As(HomePage.class).volumeRange.getLocation().getX();
//
//		  Actions builder = new Actions(DriverContext._Driver);
//		  builder.moveToElement(CurrentPage.As(HomePage.class).volumeRange)
//		         .click()
//		         .dragAndDropBy(CurrentPage.As(HomePage.class).volumeRange,xCoord+sliderWidth, 0)
//		         .build()
//		         .perform();
//		  
//		// Verify that volume was applied to the text
//		//String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");		
//		String data_volume = CurrentPage.As(HomePage.class).volumeVal.getText();
//	    LogUltility.log(test, logger, "What was applied: " + data_volume);
//	    assertEquals(data_volume, "100");
//
//		LogUltility.log(test, logger, "Test Case PASSED");
//	}
//	
//	/**
//	 * SRA-175: SPEED Verify that user can`t change VSP for part of the word
//	 * @return 
//	 * @throws InterruptedException 
//	 * @throws SQLException 
//	 * @throws AWTException 
//	 */
//	@Test  (priority = 14, groups= "Full Regression")
//	public void SRA175_SPEED_Verify_that_user_can_t_change_VSP_for_part_of_the_word() throws InterruptedException, SQLException, AWTException
//	{		
//		test = extent.createTest("VoicePlayTests-SRA-175", "SPEED Verify that user can`t change VSP for part of the word");
//		
//		// Select random domain
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen domain: " + chosenDomain);
//		
//		// Type in the textbox
//	    String randomText = CurrentPage.As(HomePage.class).RandomString(5) +
//	    		" " + CurrentPage.As(HomePage.class).RandomString(5);
//		LogUltility.log(test, logger, "input text to the text area: " + randomText);
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
//		
//		// Mark the text in the text area
//		LogUltility.log(test, logger, "Highlight the text in the textbox");
//		//CurrentPage.As(HomePage.class).Textinput.click();
//		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
//        Robot robot = new Robot();
//        robot.mouseMove(point.getX(),point.getY());
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
//		for (int i=0; i<7; i++)
//			CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
//		
//		
//		// change the speed range bar
//		CurrentPage.As(HomePage.class).SpeedBar.click();
//		for (int i=0; i<100; i++)
//			CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
//		
//		// Verify that speed was applied to the text
//		//String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
//		String data_speed = CurrentPage.As(HomePage.class).speedVal.getText();
//	    LogUltility.log(test, logger, "What was applied: " + data_speed);
//	    assertEquals(data_speed, "100");
//		  
//		LogUltility.log(test, logger, "Test Case PASSED");
//	}
//
//	/**
//	 *  SRA-175: PITCH Verify that user can`t change VSP for part of the word
//	 * @throws InterruptedException 
//	 * @throws SQLException 
//	 * @throws AWTException 
//	 */
//	@Test  (priority = 3, groups= "Full Regression")
//	public void  SRA175_PITCH_Verify_that_user_can_t_change_VSP_for_part_of_the_word() throws InterruptedException, SQLException, AWTException
//	{		
//		test = extent.createTest("VoicePlayTests-SRA-175", "PITCH Verify that user can`t change VSP for part of the word");
//		
//		// Select random domain
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen domain: " + chosenDomain);
//		
//		// Type in the textbox
//	    String randomText = CurrentPage.As(HomePage.class).RandomString(5) +
//	    		" " + CurrentPage.As(HomePage.class).RandomString(5);
//		LogUltility.log(test, logger, "input text to the text area: " + randomText);
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
//		
//		// Mark the text in the text area
//		LogUltility.log(test, logger, "Highlight the text in the textbox");
//		//CurrentPage.As(HomePage.class).Textinput.click();
//		Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
//        Robot robot = new Robot();
//        robot.mouseMove(point.getX(),point.getY());
//		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
//		for (int i=0; i<7; i++)
//			CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.SHIFT, Keys.ARROW_LEFT));
//		
//		// change the pitch range bar
//		CurrentPage.As(HomePage.class).hsbPitch.click();
//		for (int i=0; i<100; i++)
//			CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
//				
//		// Verify that pitch was applied to the text
//		//String applied = CurrentPage.As(HomePage.class).txtSpan.getAttribute("data-id");
//		String data_pitch = CurrentPage.As(HomePage.class).pitchVal.getText();
//	    LogUltility.log(test, logger, "What was applied: " + data_pitch);
//	    assertEquals(data_pitch, "pitch");
//		  
//		LogUltility.log(test, logger, "Test Case PASSED");
//	}
	
    /**
     * SRA-62:check the values in the speed / volume / Pitch bar
     * @throws InterruptedException 
     * @throws SQLException 
     * @throws AWTException 
     */
    @Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
    public void  SRA62_check_the_values_in_the_speed_volume_itch_bar() throws InterruptedException, SQLException, AWTException
    {        
        test = extent.createTest("VoicePlayTests-SRA-62", "check the values in the speed / volume / Pitch bar");
        CurrentPage = GetInstance(HomePage.class);
        
        // Check volume maximum and minimum ranges
        String maxValue =CurrentPage.As(HomePage.class).VolumeBar.getAttribute("max");
        String minValue =CurrentPage.As(HomePage.class).VolumeBar.getAttribute("min");
        LogUltility.log(test, logger, "Volume bar, Max range value: "+maxValue +" Min range value: "+minValue);
        assertEquals(maxValue,"300");
        assertEquals(minValue,"50");
        
        // Check speed maximum and minimum ranges
        maxValue =CurrentPage.As(HomePage.class).SpeedBar.getAttribute("max");
        minValue =CurrentPage.As(HomePage.class).SpeedBar.getAttribute("min");
        LogUltility.log(test, logger, "Speed bar, Max range value: "+maxValue +" Min range value: "+minValue);
        assertEquals(maxValue,"300");
        assertEquals(minValue,"50");
        
        // Check speed maximum and minimum ranges
        maxValue =CurrentPage.As(HomePage.class).hsbPitch.getAttribute("max");
        minValue =CurrentPage.As(HomePage.class).hsbPitch.getAttribute("min");
        LogUltility.log(test, logger, "Pitch bar, Max range value: "+maxValue +" Min range value: "+minValue);
        assertEquals(maxValue,"125");
        assertEquals(minValue,"85");
        
        LogUltility.log(test, logger, "Test Case PASSED");
    }
    
    
    /**
     * SRA-64:User can change pitch/speed/volume combine with any gesture/style/mood
     * @throws InterruptedException
     * @throws SQLException
     * @throws AWTException 
     * @throws FileNotFoundException 
     */
    @Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
    public void SRA64_User_can_change_pitch_speed_volume_combine_with_any_gesture_style_mood() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
    {
        test = extent.createTest("VoicePlayTests-SRA-64", "User can change pitch/speed/volume combine with any gesture/style/mood");
    
        Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
        // Select a domain
        //Thread.sleep(4000);
        String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMoodAndGesture(con, chosenProject, usernameLogin);
        LogUltility.log(test, logger, "select random domain: " + randomDomain);

        // Type in the textbox
        //Thread.sleep(2000);
//        String randomText = generateInput.RandomString(3, "words");
        String randomText = RandomText.getSentencesFromFile();
        LogUltility.log(test, logger, "input text to the text area: " + randomText);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
        
        // Add random gesture
       String gesture =  CurrentPage.As(HomePage.class).chooseRandomGesture();
        LogUltility.log(test, logger, "Choose random gesture: "+gesture);
        
        // Mark the text in the text area
//        CurrentPage.As(HomePage.class).Textinput.click();
        LogUltility.log(test, logger, "Highlight the text in the textbox");
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));

        // Click the mood button
        Actions builder = new Actions(DriverContext._Driver);
	  	builder.moveToElement(CurrentPage.As(HomePage.class).MoodValues).perform();
        CurrentPage.As(HomePage.class).chooseRandomMood();
        LogUltility.log(test, logger, "Choose random mood");
        
        // change values for volume, speed and pitch
        // change the pitch range bar
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
        CurrentPage.As(HomePage.class).hsbPitch.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
        // change the volume range bar
        //Thread.sleep(1000);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        CurrentPage.As(HomePage.class).hsbVolume.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
        // change the speed range bar
        //Thread.sleep(1000);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        CurrentPage.As(HomePage.class).SpeedBar.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
                        
        // Check that the text is with mood
        String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
        LogUltility.log(test, logger, "Source text in text editor: " + source);
        boolean doContain = source.contains("data-mood=");
        LogUltility.log(test, logger, "Is text with mood: " + doContain);
        assertTrue(doContain);

        // Check that the volume and pitch and speed are also set
        doContain = source.contains("data-pitch=");
        LogUltility.log(test, logger, "Is pitch found: " + doContain);
        assertTrue(doContain);
        doContain = source.contains("data-volume=");
        LogUltility.log(test, logger, "Is volume found: " + doContain);
        assertTrue(doContain);
        doContain = source.contains("data-speed=");
        LogUltility.log(test, logger, "Is speed found: " + doContain);
        assertTrue(doContain);
        LogUltility.log(test, logger, "Test Case PASSED");
     }
    
    
    /**
     * SRA-189:Check that range bars don�t reset after adding IPA� bug 644
     * @throws InterruptedException
     * @throws SQLException
     * @throws AWTException 
     * @throws FileNotFoundException 
     */
    @Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
    public void SRA189_Verify_that_range_bars_value_doesnt_reset_after_adding_IPA() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
    {
        test = extent.createTest("VoicePlayTests-SRA-189", "Check that range bars don�t reset after adding IPA� bug 644");
    
        Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
        // Select a domain
        //Thread.sleep(4000);
        String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithMood(con, chosenProject, usernameLogin);
        LogUltility.log(test, logger, "select random domain: " + randomDomain);

        // Type in the textbox
        //Thread.sleep(2000);
//        String randomText = generateInput.RandomString(5, "words");
        String randomText = RandomText.getSentencesFromFile();
        LogUltility.log(test, logger, "input text to the text area: " + randomText);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
        
        // Mark the text in the text area
        CurrentPage.As(HomePage.class).Textinput.click();
        LogUltility.log(test, logger, "Highlight the text in the textbox");
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));

        // Click the mood button
        CurrentPage.As(HomePage.class).chooseRandomMood();
        LogUltility.log(test, logger, "Choose random mood");
        
        // Change bar values
        CurrentPage.As(HomePage.class).hsbPitch.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
        
        // change the volume range bar
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        CurrentPage.As(HomePage.class).hsbVolume.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
        
        // change the speed range bar
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        CurrentPage.As(HomePage.class).SpeedBar.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
                    
        // Mark the text in the text area
        CurrentPage.As(HomePage.class).Textinput.click();
        LogUltility.log(test, logger, "Highlight the text in the textbox");
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));

        // Click Say-As for highlighted text
        CurrentPage.As(HomePage.class).Italicbtn.click();
        LogUltility.log(test, logger, "Click the Italic/Say-As button");
        // Insert Say-As text
        CurrentPage = GetInstance(Say_As_Popup.class);
        CurrentPage.As(Say_As_Popup.class).isSayAsWindowOpen();
//        String randomItalicText = generateInput.RandomString(2, "words");
        String randomItalicText = RandomText.getSentencesFromFile();
        LogUltility.log(test, logger, "insert Say-As text to the text area: " + randomItalicText);
        CurrentPage.As(Say_As_Popup.class).txtInput.sendKeys(randomItalicText);
        // Click ok
        CurrentPage.As(Say_As_Popup.class).btnOK.click();
        
        // Get volume value and check if reseted
        CurrentPage = GetInstance(HomePage.class);
        String volValue = CurrentPage.As(HomePage.class).volumeVal.getText();
        boolean sameVol = !volValue.equals("100");
        LogUltility.log(test, logger, "Volume value after adding IPA isn't reseted: "+sameVol);
        
        // Get speed value and check if reseted
        CurrentPage = GetInstance(HomePage.class);
        String speedValue = CurrentPage.As(HomePage.class).speedVal.getText();
        boolean sameSpeed = !speedValue.equals("100");
        LogUltility.log(test, logger, "Speed value after adding IPA isn't reseted: "+sameSpeed);
        
        // Get pitch value and check if reseted
        CurrentPage = GetInstance(HomePage.class);
        String pitchValue = CurrentPage.As(HomePage.class).pitchVal.getText();
        boolean samePitch = !pitchValue.equals("100");
        LogUltility.log(test, logger, "Pitch value after adding IPA isn't reseted: "+samePitch);
        
        LogUltility.log(test, logger, "Test Case PASSED");
     }
    
    
    /**
     * SRA-191:Verify that user Can change the bar values or add emphasis for words before question mark� bug 632
     * @throws InterruptedException
     * @throws SQLException
     * @throws AWTException 
     * @throws FileNotFoundException 
     */
    @Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
    public void SRA191_Verify_that_user_Can_change_the_bar_values_or_add_emphasis_for_words_before_question_mark() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
    {
        test = extent.createTest("VoicePlayTests-SRA-191", "Verify that user Can change the bar values or add emphasis for words before question mark� bug 632");
    
        Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
        // Select a domain
        //Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

        // Type in the textbox
        //Thread.sleep(2000);
//        String randomText = generateInput.RandomString(3, "words");
        String randomText = RandomText.getSentencesFromFile();
        randomText += "?";
        LogUltility.log(test, logger, "input text to the text area: " + randomText);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.LEFT,Keys.CONTROL,Keys.SHIFT,Keys.LEFT));
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
        // Click the bold button
        //Thread.sleep(5000);
        LogUltility.log(test, logger, "Click the bold button");
        CurrentPage.As(HomePage.class).btnBold.click();
        
        // change values for volume, speed and pitch
        // change the pitch range bar
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
        CurrentPage.As(HomePage.class).hsbPitch.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
        // change the volume range bar
        //Thread.sleep(1000);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        CurrentPage.As(HomePage.class).hsbVolume.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
        // change the speed range bar
        //Thread.sleep(1000);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        CurrentPage.As(HomePage.class).SpeedBar.click();
        for (int i=0; i<100; i++)
            CurrentPage.As(HomePage.class).SpeedBar.sendKeys(Keys.RIGHT);
                    
        // Check that the text is with mood
        String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
        LogUltility.log(test, logger, "Source text in text editor: " + source);

        // Check that the volume and pitch and speed are also set
        boolean doContain = source.contains("data-pitch=");
        LogUltility.log(test, logger, "Is pitch found: " + doContain);
        assertTrue(doContain);
        doContain = source.contains("data-volume=");
        LogUltility.log(test, logger, "Is volume found: " + doContain);
        assertTrue(doContain);
        doContain = source.contains("data-speed=");
        LogUltility.log(test, logger, "Is speed found: " + doContain);
        assertTrue(doContain);
                
        LogUltility.log(test, logger, "Test Case PASSED");
     }
    
    /**
     * SRA-196:Verify not getting strange behavior when change the v/s/p bars values for emphasis words-602
     * @throws InterruptedException
     * @throws SQLException
     * @throws AWTException 
     * @throws FileNotFoundException 
     */
    @Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
    public void SRA196_Verify_not_getting_strange_behavior_when_change_the_vsp_bars_values_for_emphasis_words() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
    {
        test = extent.createTest("VoicePlayTests-SRA-196", "Verify not getting strange behavior when change the v/s/p bars values for emphasis words-602");
    
        Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
        // Select a domain
        //Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
        List<String> domains = CurrentPage.As(HomePage.class).getDomainValues();
		Random random = new Random();
		String randomDomain = domains.get(random.nextInt(domains.size())) ;
		CurrentPage.As(HomePage.class).cmbDomain.click();
		CurrentPage.As(HomePage.class).select_Domain(randomDomain);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
        // Type in the textbox
        //Thread.sleep(2000);
//        String randomText = generateInput.RandomString(4, "words");
        String randomText = RandomText.getSentencesFromFile();
        LogUltility.log(test, logger, "input text to the text area: " + randomText);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
        
        // Highlight last word
        if(Setting.Language.equals("6"))
        	CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.LEFT));
        else
        	if(Setting.Language.equals("10"))
        		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.RIGHT));
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
        CurrentPage.As(HomePage.class).hsbPitch.click();
        for (int i=0; i<10; i++)
            CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
        LogUltility.log(test, logger, "Change pitch value before emphasizing");
        
        String pitchValueB4Bold = CurrentPage.As(HomePage.class).pitchVal.getText();
        LogUltility.log(test, logger, "Pitch value before emphasizing: "+pitchValueB4Bold);
        
        // Click the bold button
        LogUltility.log(test, logger, "Click the bold button");
        CurrentPage.As(HomePage.class).btnBold.click();
        
        CurrentPage.As(HomePage.class).hsbPitch.click();
        for (int i=0; i<5; i++)
            CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
        LogUltility.log(test, logger, "Change pitch value after emphasizing");
        String pitchValueAfterBold = CurrentPage.As(HomePage.class).pitchVal.getText();
        LogUltility.log(test, logger, "Pitch value after emphasizing: "+pitchValueAfterBold);
        
        boolean barWorks = !pitchValueAfterBold.equals(pitchValueB4Bold);
        LogUltility.log(test, logger, "The pitch bar value got the last updated value : "+barWorks);
        assertTrue(barWorks);
        
        LogUltility.log(test, logger, "Test Case PASSED");
     }

    
    /**
     * SRA-175:Verify that user can`t change VSP for part of the word - part 1 Pitch
     * @throws InterruptedException
     * @throws SQLException
     * @throws AWTException 
     * @throws FileNotFoundException 
     */
    @Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
    public void SRA175_Verify_that_user_cant_change_VSP_for_part_of_the_word_part_1_pitch() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
    {
        test = extent.createTest("VoicePlayTests-SRA-175", "Verify that user can`t change VSP for part of the word");
    
        Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
        // Select a domain
        //Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
//        String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
//        LogUltility.log(test, logger, "select random domain: " + randomDomain);

        // Type in the textbox
        //Thread.sleep(2000);
//        String randomText = generateInput.RandomString(4, "words");
        String randomText = RandomText.getSentencesFromFile().split(" ", 2)[0];
        LogUltility.log(test, logger, "input text to the text area: " + randomText);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
        
        // Send the mouse cursor to the end 
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
 
        // Highlight PART of the last word
        if(Setting.Language.equals("6"))
        {
            CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.LEFT);
        	CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.LEFT));
        }
        else
        	if(Setting.Language.equals("10"))
        	{
                CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.RIGHT);
        		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.RIGHT));
        	}
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
        CurrentPage.As(HomePage.class).hsbPitch.click();
        for (int i=0; i<10; i++)
            CurrentPage.As(HomePage.class).hsbPitch.sendKeys(Keys.RIGHT);
        LogUltility.log(test, logger, "Change pitch value");

		  
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-pitch=");
		LogUltility.log(test, logger, "Is pitch found: " + doContain);
		assertFalse(doContain);

        LogUltility.log(test, logger, "Test Case PASSED");
     }

    /**
     * SRA-175:Verify that user can`t change VSP for part of the word - part 2 volume
     * @throws InterruptedException
     * @throws SQLException
     * @throws AWTException 
     * @throws FileNotFoundException 
     */
    @Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
    public void SRA175_Verify_that_user_cant_change_VSP_for_part_of_the_word_part_2_volume() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
    {
        test = extent.createTest("VoicePlayTests-SRA-175", "Verify that user can`t change VSP for part of the word");
    
        Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
        // Select a domain
        //Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
//        String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
//        LogUltility.log(test, logger, "select random domain: " + randomDomain);

        // Type in the textbox one word
        String randomText = RandomText.getSentencesFromFile().split(" ", 2)[0];
        LogUltility.log(test, logger, "input text to the text area: " + randomText);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
        
        // Send the mouse cursor to the end 
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
 
        // Highlight PART of the last word
        if(Setting.Language.equals("6"))
        {
            CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.LEFT);
        	CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.LEFT));
        }
        else
        	if(Setting.Language.equals("10"))
        	{
                CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.RIGHT);
        		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.RIGHT));
        	}
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
        CurrentPage.As(HomePage.class).hsbVolume.click();
        for (int i=0; i<10; i++)
            CurrentPage.As(HomePage.class).hsbVolume.sendKeys(Keys.RIGHT);
        LogUltility.log(test, logger, "Change volume value");

		  
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-volume=");
		LogUltility.log(test, logger, "Is volume found: " + doContain);
		assertFalse(doContain);

        LogUltility.log(test, logger, "Test Case PASSED");
     }
    
    /**
     * SRA-175:Verify that user can`t change VSP for part of the word - part 3 speed
     * @throws InterruptedException
     * @throws SQLException
     * @throws AWTException 
     * @throws FileNotFoundException 
     */
    @Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
    public void SRA175_Verify_that_user_cant_change_VSP_for_part_of_the_word_part_3_speed() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
    {
        test = extent.createTest("VoicePlayTests-SRA-175", "Verify that user can`t change VSP for part of the word");
    
        Point point = CurrentPage.As(HomePage.class).Italicbtn.getLocation();
        Robot robot = new Robot();
        
        // Select a domain
        //Thread.sleep(4000);
        CurrentPage.As(HomePage.class).isHomepageReady();
//        String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
//        LogUltility.log(test, logger, "select random domain: " + randomDomain);

        // Type in the textbox one word
        String randomText = RandomText.getSentencesFromFile().split(" ", 2)[0];
        LogUltility.log(test, logger, "input text to the text area: " + randomText);
        CurrentPage.As(HomePage.class).Textinput.click();
        CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
        
        // Send the mouse cursor to the end 
        CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.END);
 
        // Highlight PART of the last word
        if(Setting.Language.equals("6"))
        {
            CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.LEFT);
        	CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.LEFT));
        }
        else
        	if(Setting.Language.equals("10"))
        	{
                CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.RIGHT);
        		CurrentPage.As(HomePage.class).Textinput.sendKeys(Keys.chord(Keys.CONTROL,Keys.SHIFT,Keys.RIGHT));
        	}
        
        robot.mouseMove(point.getX(),point.getY());
        robot.mouseMove(point.getX()-10,point.getY()-10);
        
        CurrentPage.As(HomePage.class).hsbSpeed.click();
        for (int i=0; i<10; i++)
            CurrentPage.As(HomePage.class).hsbSpeed.sendKeys(Keys.RIGHT);
        LogUltility.log(test, logger, "Change speed value");

		  
		String source = CurrentPage.As(HomePage.class).Textinput.getAttribute("innerHTML");
		LogUltility.log(test, logger, "Source text in text editor: " + source);

		boolean doContain = source.contains("data-speed=");
		LogUltility.log(test, logger, "Is speed found: " + doContain);
		assertFalse(doContain);

        LogUltility.log(test, logger, "Test Case PASSED");
     }

    
    /**
	 * SRA-223:Verify that VSP bar values are reset to default with templates part 1 - volume
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA223_Verify_that_VSP_bar_values_are_reset_to_default_with_templates_part1_volume() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-223", "Verify that VSP bar values are reset to default with templates part 1 - volume");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);	
		homePage.isHomepageReady();	
		
		// Select domain with templates
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		
		// Open open popup
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		LogUltility.log(test, logger, "Click on Open button");
		
		// Press the template radio button
		CurrentPage.As(OpenPopup.class).openFileWait();
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		LogUltility.log(test, logger, "Select the Template radio button");
		
	    // Choose random Template and click open
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected file: " + chosenTemplate);
		
		// Click Open
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		LogUltility.log(test, logger, "Open template");

		// Choose voice
		CurrentPage = GetInstance(HomePage.class);
		String voice = CurrentPage.As(HomePage.class).chooseRandomVoice();
		LogUltility.log(test, logger, "Choose voice: "+voice);
		
		// Click the play button
		CurrentPage.As(HomePage.class).btnplay.click();
		LogUltility.log(test, logger, "Click the play button");
		CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
		CurrentPage.As(HomePage.class).checkPauseAfterSynthesizing();
	
		// Get the default volume bar value before changing
		CurrentPage = GetInstance(HomePage.class);
		String default_volume = homePage.volumeVal.getText();
		LogUltility.log(test, logger, "Get the volume value before changing: "+default_volume);
		
		//Change the volume bar values
		homePage.hsbVolume.click();
		for (int j=0; j<50; j++)
			homePage.hsbVolume.sendKeys(Keys.RIGHT);
		LogUltility.log(test, logger, "Change the volume bar values to: "+homePage.volumeVal.getText());
		
		// Check that the remove red icon is displayed
		boolean removeButton = homePage.xButtonVolume.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		homePage.xButtonVolume.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
		
		// Check that the remove red icon is not displayed
		 removeButton = !homePage.xButtonVolume.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
		assertTrue(removeButton);
		
		// Get the value after resetting to default
		String volume_after_reset = homePage.volumeVal.getText();
		LogUltility.log(test, logger, "Get the volume value after restoring: "+volume_after_reset);
		
		// Check that values are equal
		boolean same = volume_after_reset.equals(default_volume);
		LogUltility.log(test, logger, "The default value was restored successfully: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	
	/**
	 * SRA-220:Verify that the remove icon is displayed when changing VSP without highlighting text - part 1 volume
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA220_Verify_that_the_remove_icon_is_displayed_when_changing_VSP_without_highlighting_text_part_1_volume() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-220", "Verify that the remove icon is displayed when changing VSP without highlighting text - part 1 volume");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);	
		homePage.isHomepageReady();
		
		// Insert text to the text area
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		homePage.Textinput.click();
		homePage.Textinput.sendKeys(randomAllText);
		
		// Get the default volume bar value before changing
		String default_volume = homePage.volumeVal.getText();
		LogUltility.log(test, logger, "Get the volume value before changing: "+default_volume);
		
		//Change the volume bar values
		homePage.hsbVolume.click();
		for (int j=0; j<50; j++)
			homePage.hsbVolume.sendKeys(Keys.RIGHT);
		LogUltility.log(test, logger, "Change the volume bar values to: "+homePage.volumeVal.getText());
		
		// Check that the remove red icon is displayed
		boolean removeButton = homePage.xButtonVolume.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		homePage.xButtonVolume.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
		
		// Check that the remove red icon is not displayed
		 removeButton = !homePage.xButtonVolume.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
		assertTrue(removeButton);
		
		// Get the value after resetting to default
		String volume_after_reset = homePage.volumeVal.getText();
		LogUltility.log(test, logger, "Get the volume value after restoring: "+volume_after_reset);
		
		// Check that values are equal
		boolean same = volume_after_reset.equals(default_volume);
		LogUltility.log(test, logger, "The default value was restored successfully: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	
	/**
	 * SRA-220:Verify that the remove icon is displayed when changing VSP without highlighting text part 2 - speed
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA220_Verify_that_the_remove_icon_is_displayed_when_changing_VSP_without_highlighting_text_part_2_speed() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-220", "Verify that the remove icon is displayed when changing VSP without highlighting text part 2 - speed");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);	
		homePage.isHomepageReady();
		
		// Insert text to the text area
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		homePage.Textinput.click();
		homePage.Textinput.sendKeys(randomAllText);
		
		
		// Get the default speed bar value before changing
		String default_speed = homePage.speedVal.getText();
		LogUltility.log(test, logger, "Get the speed value before changing: "+default_speed);
		
		//Change the speed bar values
		homePage.hsbSpeed.click();
		for (int j=0; j<50; j++)
			homePage.hsbSpeed.sendKeys(Keys.RIGHT);
		LogUltility.log(test, logger, "Change the speed bar values to: "+homePage.speedVal.getText());
		
		// Check that the remove red icon is displayed
		boolean removeButton = homePage.xButtonSpeed.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		homePage.xButtonSpeed.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
		
		// Check that the remove red icon is not displayed
		 removeButton = !homePage.xButtonSpeed.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
		assertTrue(removeButton);
		
		// Get the value after resetting to default
		String speed_after_reset = homePage.speedVal.getText();
		LogUltility.log(test, logger, "Get the speed value after restoring: "+speed_after_reset);
		
		// Check that values are equal
		boolean same = speed_after_reset.equals(default_speed);
		LogUltility.log(test, logger, "The default value was restored successfully: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
	
	/**
	 * SRA-220:Verify that the remove icon is displayed when changing VSP without highlighting text part 3 - pitch
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA220_Verify_that_the_remove_icon_is_displayed_when_changing_VSP_without_highlighting_text_part_3_pitch() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
	{
		test = extent.createTest("TextEditorTests-SRA-220", "Verify that the remove icon is displayed when changing VSP without highlighting text part 3 - pitch");
		
		DriverContext.browser.Maximize();
		
		// Initialize homepage
		CurrentPage = GetInstance(HomePage.class);
		HomePage homePage = CurrentPage.As(HomePage.class);	
		homePage.isHomepageReady();
		
		// Insert text to the text area
		String randomAllText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomAllText);
		homePage.Textinput.click();
		homePage.Textinput.sendKeys(randomAllText);
		
		// Get the default pitch bar value before changing
		String default_pitch = homePage.pitchVal.getText();
		LogUltility.log(test, logger, "Get the pitch value before changing: "+default_pitch);
		
		//Change the pitch bar values
		homePage.hsbPitch.click();
		for (int j=0; j<50; j++)
			homePage.hsbPitch.sendKeys(Keys.RIGHT);
		LogUltility.log(test, logger, "Change the pitch bar values to: "+homePage.pitchVal.getText());
		
		// Check that the remove red icon is displayed
		boolean removeButton = homePage.xButtonPitch.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
		assertTrue(removeButton);
		
		// Press the remove red icon
		homePage.xButtonPitch.click();
		LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
		
		// Check that the remove red icon is not displayed
		 removeButton = !homePage.xButtonPitch.isDisplayed();
		LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
		assertTrue(removeButton);
		
		// Get the value after resetting to default
		String pitch_after_reset = homePage.pitchVal.getText();
		LogUltility.log(test, logger, "Get the pitch value after restoring: "+pitch_after_reset);
		
		// Check that values are equal
		boolean same = pitch_after_reset.equals(default_pitch);
		LogUltility.log(test, logger, "The default value was restored successfully: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	 }
	
    
	
	  /**
		 * SRA-223:Verify that VSP bar values are reset to default with templates part 2 - speed
		 * @throws InterruptedException
		 * @throws SQLException
		 * @throws AWTException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA223_Verify_that_VSP_bar_values_are_reset_to_default_with_templates_part2_speed() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
		{
			test = extent.createTest("TextEditorTests-SRA-223", "Verify that VSP bar values are reset to default with templates part 2 - speed");
			
			DriverContext.browser.Maximize();
			
			// Initialize homepage
			CurrentPage = GetInstance(HomePage.class);
			HomePage homePage = CurrentPage.As(HomePage.class);	
			homePage.isHomepageReady();	
			
			// Select domain with templates
			String randomDomain = homePage.chooseRandomDomainWithTemplates(con, chosenProject);
			LogUltility.log(test, logger, "select random domain: " + randomDomain);
			
			// Open open popup
			CurrentPage = GetInstance(OpenPopup.class);
			CurrentPage.As(OpenPopup.class).btnOpen.click();
			LogUltility.log(test, logger, "Click on Open button");
			
			// Press the template radio button
			CurrentPage.As(OpenPopup.class).openFileWait();
			CurrentPage.As(OpenPopup.class).templateRBtn.click();
			LogUltility.log(test, logger, "Select the Template radio button");
			
		    // Choose random Template and click open
			CurrentPage.As(OpenPopup.class).openTemplateWait();
			String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
			LogUltility.log(test, logger, "Selected file: " + chosenTemplate);
			
			// Click Open
			CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
			LogUltility.log(test, logger, "Open template");

			// Choose voice
			CurrentPage = GetInstance(HomePage.class);
			String voice = CurrentPage.As(HomePage.class).chooseRandomVoice();
			LogUltility.log(test, logger, "Choose voice: "+voice);
			
			// Click the play button
			CurrentPage.As(HomePage.class).btnplay.click();
			LogUltility.log(test, logger, "Click the play button");
			CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
			CurrentPage.As(HomePage.class).checkPauseAfterSynthesizing();

			// Get the default speed bar value before changing
			CurrentPage = GetInstance(HomePage.class);
			String default_speed = homePage.speedVal.getText();
			LogUltility.log(test, logger, "Get the speed value before changing: "+default_speed);
			
			//Change the speed bar values
			homePage.hsbSpeed.click();
			for (int j=0; j<50; j++)
				homePage.hsbSpeed.sendKeys(Keys.RIGHT);
			LogUltility.log(test, logger, "Change the speed bar values to: "+homePage.hsbSpeed.getText());
			
			// Check that the remove red icon is displayed
			boolean removeButton = homePage.xButtonSpeed.isDisplayed();
			LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
			assertTrue(removeButton);
			
			// Press the remove red icon
			homePage.xButtonSpeed.click();
			LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
			
			// Check that the remove red icon is not displayed
			 removeButton = !homePage.xButtonSpeed.isDisplayed();
			LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
			assertTrue(removeButton);
			
			// Get the value after resetting to default
			String speed_after_reset = homePage.speedVal.getText();
			LogUltility.log(test, logger, "Get the speed value after restoring: "+speed_after_reset);
			
			// Check that values are equal
			boolean same = speed_after_reset.equals(default_speed);
			LogUltility.log(test, logger, "The default value was restored successfully: "+same);
			assertTrue(same);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		 }
		
		
		  /**
			 * SRA-223:Verify that VSP bar values are reset to default with templates part 3 - pitch
			 * @throws InterruptedException
			 * @throws SQLException
			 * @throws AWTException 
			 * @throws FileNotFoundException 
			 */
			@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
			public void SRA223_Verify_that_VSP_bar_values_are_reset_to_default_with_templates_part3_pitch() throws InterruptedException, SQLException, AWTException, FileNotFoundException 
			{
				test = extent.createTest("TextEditorTests-SRA-223", "Verify that VSP bar values are reset to default with templates part 3 - pitch");
				
				DriverContext.browser.Maximize();
				
				// Initialize homepage
				CurrentPage = GetInstance(HomePage.class);
				HomePage homePage = CurrentPage.As(HomePage.class);	
				homePage.isHomepageReady();	
				
				// Select domain with templates
				String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
				LogUltility.log(test, logger, "select random domain: " + randomDomain);
				
				// Open open popup
				CurrentPage = GetInstance(OpenPopup.class);
				CurrentPage.As(OpenPopup.class).btnOpen.click();
				LogUltility.log(test, logger, "Click on Open button");
				
				// Press the template radio button
				CurrentPage.As(OpenPopup.class).openFileWait();
				CurrentPage.As(OpenPopup.class).templateRBtn.click();
				LogUltility.log(test, logger, "Select the Template radio button");
				
			    // Choose random Template and click open
				CurrentPage.As(OpenPopup.class).openTemplateWait();
				String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
				LogUltility.log(test, logger, "Selected file: " + chosenTemplate);
				
				// Click Open
				CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
				LogUltility.log(test, logger, "Open template");

				// Choose voice
				CurrentPage = GetInstance(HomePage.class);
				String voice = CurrentPage.As(HomePage.class).chooseRandomVoice();
				LogUltility.log(test, logger, "Choose voice: "+voice);
				
				// Click the play button
				CurrentPage.As(HomePage.class).btnplay.click();
				LogUltility.log(test, logger, "Click the play button");
				CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				CurrentPage.As(HomePage.class).checkPauseAfterSynthesizing();

				// Get the default pitch bar value before changing
				CurrentPage = GetInstance(HomePage.class);
				String default_pitch = homePage.pitchVal.getText();
				LogUltility.log(test, logger, "Get the pitch value before changing: "+default_pitch);
				
				//Change the pitch bar values
//				Thread.sleep(2000);
				homePage.hsbPitch.click();
				for (int j=0; j<20; j++)
					homePage.hsbPitch.sendKeys(Keys.RIGHT);
				LogUltility.log(test, logger, "Change the pitch bar values to: "+homePage.hsbPitch.getText());
				
				// Check that the remove red icon is displayed
				boolean removeButton = homePage.xButtonPitch.isDisplayed();
				LogUltility.log(test, logger, "The remove red icon is displayed after changing the bar value: "+removeButton);
				assertTrue(removeButton);
				
				// Press the remove red icon
				homePage.xButtonPitch.click();
				LogUltility.log(test, logger, "Click on the 'X' red button to reset the bar value to default");
				
				// Check that the remove red icon is not displayed
				 removeButton = !homePage.xButtonPitch.isDisplayed();
				LogUltility.log(test, logger, "The remove red icon is NOT displayed after restoring the default value: "+removeButton);
				assertTrue(removeButton);
				
				// Get the value after resetting to default
				String pitch_after_reset = homePage.pitchVal.getText();
				LogUltility.log(test, logger, "Get the pitch value after restoring: "+pitch_after_reset);
				
				// Check that values are equal
				boolean same = pitch_after_reset.equals(default_pitch);
				LogUltility.log(test, logger, "The default value was restored successfully: "+same);
				assertTrue(same);
				
				LogUltility.log(test, logger, "Test Case PASSED");
			 }
			
		
    @AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    { 			
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Voice play controller option",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		
		// Refresh the website for the new test
    	DriverContext._Driver.navigate().refresh();
    	
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 DriverContext._Driver.quit();
	    }	
}