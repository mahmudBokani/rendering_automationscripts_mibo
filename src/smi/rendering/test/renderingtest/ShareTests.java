package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.Mailinator;
import smi.rendering.test.pages.OpenPopup;
import smi.rendering.test.pages.SavePopup;
import smi.rendering.test.pages.SelectPage;
import smi.rendering.test.pages.SharePopup;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * All tests in Share folder
 *
 */
public class ShareTests extends FrameworkInitialize {

	private static Logger logger = null;
	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	private String usernameLogin = "";
	private String chosenProject = "";
	public boolean isThereMoreThanOneAssignedDomain;
	
    /**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
     * @throws ClassNotFoundException 
     * @throws SQLException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Share", Setting.Browser);
		
	// Logger
	logger = LogManager.getLogger(ShareTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite = ShareTests ");
	logger.info("Share Tests - FrameworkInitilize");
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	
	// Get account,project and domain with the requested language and logged-in email
	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
	chosenProject = account_project_domain.get(index).get(1);

	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	
	usernameLogin = ExcelUltility.ReadCell("Email",1);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		//Thread.sleep(10000);
		CurrentPage= GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).isHomepageReady();
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}

	/**
	 * SRA-109:Verify user can share his own files
	 * @throws InterruptedException
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA109_Verify_user_can_share_his_own_files() throws InterruptedException, AWTException, SQLException, FileNotFoundException 
	{
		test = extent.createTest("ShareTests-SRA-109", "Verify user can share his own files");
		// Select a domain
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		//Thread.sleep(2000);

		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "input text to the text area: " + randomText);
		
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		//Thread.sleep(2000);
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[2];
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveShareBtn.click();
		//Thread.sleep(2000);
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
				
				
		// Check the Share window is displayed
		CurrentPage = GetInstance(SharePopup.class);
    	String shareTitle = CurrentPage.As(SharePopup.class).sharePopupCheck(test, logger);
		//String shareTitle = CurrentPage.As(SharePopup.class).ShareTitle.getText();
		LogUltility.log(test, logger, "verify that the share title appear in the share popup: " + shareTitle);
		assertEquals(shareTitle, "Share");
		//Thread.sleep(1000);
		
		// Test with valid email and text
		String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
//		randomText = generateInput.RandomString(5, "words");
		randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Share file with valid email and text -  randomEmail: " + randomEmail + ", randomText: " + randomText);
		CurrentPage.As(SharePopup.class).Notes.sendKeys(randomText);
		CurrentPage.As(SharePopup.class).Share_File(randomEmail);
		
		// Check the Share confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Shared Successfully     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA-111:Verify user can share texts with more than one person
	 * @throws InterruptedException
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA111_Verify_that_user_can_share_texts_with_more_than_one_person() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
		test = extent.createTest("ShareTests-SRA-111", "Verify user can share texts with more than one person");
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		//CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		//Thread.sleep(2000);

		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
	    LogUltility.log(test, logger, "input text to the text area: " + randomText);
	    CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		//Thread.sleep(2000);
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[2];
		LogUltility.log(test, logger, "Enter a file name" + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		//Thread.sleep(1000);
		CurrentPage.As(SavePopup.class).SaveShareBtn.click();
		//Thread.sleep(2000);
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
				
		// Check the Share window is displayed
		CurrentPage = GetInstance(SharePopup.class);
		//String shareTitle = CurrentPage.As(SharePopup.class).ShareTitle.getText();
		String shareTitle = CurrentPage.As(SharePopup.class).sharePopupCheck(test, logger);
		LogUltility.log(test, logger, "verify that the share title appear in the share popup: " + shareTitle);
		assertEquals(shareTitle, "Share");
		//Thread.sleep(1000);
		
		// Test with valid email and text
		String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
		String randomEmail2 = generateInput.RandomString(7, "letters") + "@mailinator.com";
		String randomEmail3 = generateInput.RandomString(7, "letters") + "@mailinator.com";
//		randomText = generateInput.RandomString(5, "words");
		randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Share file with valid email and text -  randomText: " + randomText + ", randomEmail: " + randomEmail
				+ ", randomEmail2: " + randomEmail2 + ", randomEmail3: " + randomEmail3);
		CurrentPage.As(SharePopup.class).Notes.sendKeys(randomText);
		String emailList = (randomEmail + ", " + randomEmail2 + ", " + randomEmail3);;
		CurrentPage.As(SharePopup.class).Share_File(emailList);
		
		// Check the Share confirmation message
		//Thread.sleep(1000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Shared Successfully     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
			
	/**
	 * SRA-147:Verify user can share Template successfully
	 * @throws InterruptedException
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA147_Verify_user_can_share_Template_successfully() throws InterruptedException, SQLException, FileNotFoundException {
		test = extent.createTest("ShareTests-SRA-147", "Verify user can share Template successfully");
		
		// Select a domain
		CurrentPage = GetInstance(HomePage.class);
//		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomainWithTemplates(con, chosenProject);
		//CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);

		// Click the open button and check for open popup
		//Thread.sleep(3000);
		LogUltility.log(test, logger, "Click the open button");
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).btnOpen.click();
		
		//Choose the Template radio button
		//Thread.sleep(3000);
		CurrentPage = GetInstance(OpenPopup.class);
		CurrentPage.As(OpenPopup.class).templateRBtn.click();
		
	    // Choose random Template and click open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).openTemplateWait();
		//CurrentPage.As(OpenPopup.class).cmbTemplate.click();
		String chosenTemplate = CurrentPage.As(OpenPopup.class).chooseRandomTemplate();
		LogUltility.log(test, logger, "Selected file: " + chosenTemplate);
		// Click Open
		//Thread.sleep(3000);
		CurrentPage.As(OpenPopup.class).btnOpenInPopup.click();
		
	    // Check that there is text displayed in the text box
		//Thread.sleep(3000);
		CurrentPage = GetInstance(HomePage.class);
		String text = CurrentPage.As(HomePage.class).Textinput.getText();
		boolean containsText = text.isEmpty();
		LogUltility.log(test, logger, "text in textbox: " + text + "is text are empty: " + containsText);
		assertFalse(containsText);		
		
		// Click the save button
		Thread.sleep(1000);
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		Thread.sleep(1000);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[2];
		LogUltility.log(test, logger, "Enter a file name" + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveShareBtn.click();
		//Thread.sleep(2000);

		// Check the Share window is displayed
		Thread.sleep(1000);
		CurrentPage = GetInstance(SharePopup.class);
		String shareTitle = CurrentPage.As(SharePopup.class).ShareTitle.getText();
		LogUltility.log(test, logger, "verify that the share title appear in the share popup: " + shareTitle);
		assertEquals(shareTitle, "Share");
		//Thread.sleep(1000);
		
		// Test with valid email and text
		String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
		String randomEmail2 = generateInput.RandomString(7, "letters") + "@mailinator.com";
		String randomEmail3 = generateInput.RandomString(7, "letters") + "@mailinator.com";
//		String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Share file with valid email and text -  randomText: " + randomText + ", randomEmail: " + randomEmail
				+ ", randomEmail2: " + randomEmail2 + ", randomEmail3: " + randomEmail3);
		CurrentPage.As(SharePopup.class).Notes.sendKeys(randomText);
		String emailList = (randomEmail + ", " + randomEmail2 + ", " + randomEmail3);;
		CurrentPage.As(SharePopup.class).Share_File(emailList);
		
//		// Check the Save confirmation message
//		CurrentPage = GetInstance(HomePage.class);
//		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
	//	
		// Check the Share confirmation message
		//Thread.sleep(1000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Shared Successfully     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * SRA-112:Check the share notes saved in the DB
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	 @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA112_Check_the_shared_notes_saved_in_the_DB() throws InterruptedException, SQLException, AWTException, FileNotFoundException {
		test = extent.createTest("ShareTests-SRA-112", "Check the share notes saved in the DB");
		// Select a domain
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		//CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		//Thread.sleep(2000);

		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "words");
	    String randomText = RandomText.getSentencesFromFile();
	    LogUltility.log(test, logger, "input text to the text area: " + randomText);
	    CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		//Thread.sleep(2000);
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[2];
		LogUltility.log(test, logger, "Enter a file name" + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		//Thread.sleep(1000);
		CurrentPage.As(SavePopup.class).SaveShareBtn.click();
		//Thread.sleep(2000);
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");
				
		// Check the Share window is displayed
		CurrentPage = GetInstance(SharePopup.class);
		//String shareTitle = CurrentPage.As(SharePopup.class).ShareTitle.getText();
		String shareTitle = CurrentPage.As(SharePopup.class).sharePopupCheck(test, logger);
		LogUltility.log(test, logger, "verify that the share title appear in the share popup: " + shareTitle);
		assertEquals(shareTitle, "Share");
		//Thread.sleep(1000);
		
		// Test with valid email and text
		String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
//		randomText = generateInput.RandomString(5, "words");
		randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Share file with valid email and text -  randomEmail: " + randomEmail + ", randomText: " + randomText);
		CurrentPage.As(SharePopup.class).Notes.sendKeys(randomText);
		CurrentPage.As(SharePopup.class).Share_File(randomEmail);
		
		// Check the Share confirmation message
		//Thread.sleep(1000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Shared Successfully     X");

    	// Verify that the notes saved to DB
		CurrentPage = GetInstance(SharePopup.class);
    	List<List<String>> renderer_text_notes =CurrentPage.As(SharePopup.class).getNotesValues(con, randomEmail, randomText);
    	LogUltility.log(test, logger, "results from DB" + renderer_text_notes);
        
        assertEquals(randomText, renderer_text_notes.get(1).get(0));
        assertEquals(randomEmail, renderer_text_notes.get(1).get(1));
        LogUltility.log(test, logger, "Test Case PASSED");
	 }
	 
 	/**
 	 * SRA-114:verify user cannot share with blank email field
 	 * @throws InterruptedException
 	 * @throws AWTException 
 	 * @throws SQLException 
 	 * @throws FileNotFoundException 
 	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA114_Verify_user_cannot_share_with_blank_email_fields() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
		test = extent.createTest("ShareTests-SRA-114", "verify user cannot share with blank email field");
		// Select a domain
		CurrentPage = GetInstance(HomePage.class);
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		//CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		//Thread.sleep(2000);
		
		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
	    LogUltility.log(test, logger, "input text to the text area: " + randomText);
	    CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		//Thread.sleep(2000);
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[2];
		LogUltility.log(test, logger, "Enter a file name" + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		CurrentPage.As(SavePopup.class).SaveShareBtn.click();
		//Thread.sleep(2000);

		// Check the Share window is displayed
		CurrentPage = GetInstance(SharePopup.class);
		//String shareTitle = CurrentPage.As(SharePopup.class).ShareTitle.getText();
		String shareTitle = CurrentPage.As(SharePopup.class).sharePopupCheck(test, logger);
		LogUltility.log(test, logger, "verify that the share title appear in the share popup: " + shareTitle);
		assertEquals(shareTitle, "Share");
		//Thread.sleep(1000);
		
		// Test with an empty email
		String randomEmail = "";
//		randomText = generateInput.RandomString(5, "words");
		randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Share file with valid email and text -  randomEmail: " + randomEmail + ", randomText: " + randomText);
		CurrentPage.As(SharePopup.class).Notes.sendKeys(randomText);
		CurrentPage.As(SharePopup.class).Share_File(randomEmail);
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please add recipients     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}  
	
	/**
	 * SRA-110:Check the person you shared with him, gets email notification.
	 * @throws InterruptedException
	 * @throws AWTException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA110_check_that_the_person_you_shared_with_him_gets_email_notification() throws InterruptedException, AWTException, SQLException, FileNotFoundException {
		test = extent.createTest("ShareTests-SRA-110", "Check the person you shared with him, gets email notification");
		// Select a domain
		CurrentPage = GetInstance(HomePage.class);
		String randomDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con, chosenProject, usernameLogin);
		//CurrentPage.As(HomePage.class).select_Domain(randomDomain);
		LogUltility.log(test, logger, "select random domain: " + randomDomain);
		//Thread.sleep(2000);

		// Type in the textbox
//	    String randomText = generateInput.RandomString(5, "words");
		String randomText = RandomText.getSentencesFromFile();
	    LogUltility.log(test, logger, "input text to the text area: " + randomText);
	    CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		// Click the save button
		LogUltility.log(test, logger, "Click the save button");
		CurrentPage.As(HomePage.class).SaveButton.click();
		//Thread.sleep(2000);
		
		// Enter file name
		CurrentPage = GetInstance(SavePopup.class);
		String fileName = generateInput.RandomString(5, "letters");
//		String fileName = RandomText.getSentencesFromFile().split(" ")[2];
		LogUltility.log(test, logger, "Enter a file name: " + fileName);
		CurrentPage.As(SavePopup.class).FileName.sendKeys(fileName);
		//Thread.sleep(1000);
		CurrentPage.As(SavePopup.class).SaveShareBtn.click();
		//Thread.sleep(2000);
		
		// Check the Save confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Saved Successfully     X");

		// Check the Share window is displayed
		CurrentPage = GetInstance(SharePopup.class);
		//String shareTitle = CurrentPage.As(SharePopup.class).ShareTitle.getText();
		String shareTitle = CurrentPage.As(SharePopup.class).sharePopupCheck(test, logger);
		LogUltility.log(test, logger, "verify that the share title appear in the share popup: " + shareTitle);
		assertEquals(shareTitle, "Share");
		//Thread.sleep(1000);
		
		// Test with valid email and text
		Setting.Language = "6";
		String randomEmail = generateInput.RandomString(5, "letters") + "@mailinator.com";
		Setting.Language = "10";
//		randomText = generateInput.RandomString(5, "words");
		randomText = RandomText.getSentencesFromFile();
		LogUltility.log(test, logger, "Share file with valid email and text -  randomEmail: " + randomEmail + ", randomText: " + randomText);
		CurrentPage.As(SharePopup.class).Notes.sendKeys(randomText);
		CurrentPage.As(SharePopup.class).Share_File(randomEmail);
		
		// check notes is saved in DB 
		Thread.sleep(3000);
		boolean notesSavedInDB = CurrentPage.As(SharePopup.class).checkNotesDB(con,randomEmail,randomText);
		LogUltility.log(test, logger, "notes saved in DB : " + notesSavedInDB);
		assertTrue(notesSavedInDB);
		
		// Check the Share confirmation message
		//Thread.sleep(1000);
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Text Shared Successfully     X");
		
		// Navigate to mailinator website
		LogUltility.log(test, logger, "Navigate to mailinator");
		DriverContext.browser.GoToUrl("http://www.mailinator.com/");
		CurrentPage = GetInstance(Mailinator.class);

		// Fill the email field and click Go
		LogUltility.log(test, logger, "Fill the registered email: " + randomEmail + ", and click go");
		CurrentPage.As(Mailinator.class).FillInboxField(randomEmail);
		//Thread.sleep(10000);

		// Click the received message
		LogUltility.log(test, logger, "Click the received message ");
		CurrentPage.As(Mailinator.class).lnkEmailTitle.click();
		
		Thread.sleep(3000);
		
		DriverContext._Driver.switchTo().frame("msg_body");
		
		String messageText = CurrentPage.As(Mailinator.class).bodyText.getText();
//		String messageText = CurrentPage.As(Mailinator.class).msg_body.getAttribute("innerHTML");
//		String messageText = DriverContext._Driver.getPageSource();
		
		LogUltility.log(test, logger, "Email text: " + messageText);
		
//		String emailTitle = CurrentPage.As(Mailinator.class).lnkEmailTitle.getText();
//		LogUltility.log(test, logger, "the email title: " + emailTitle);
		boolean Results = messageText.contains(randomText);
		LogUltility.log(test, logger, "the email title contain : " + Results);
		assertTrue(Results);
	
		
		LogUltility.log(test, logger, "Test Case PASSED");		
	}
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {		
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Share options",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}

	/**
	 * Closing the browser after running all the TCs
	 */
	@AfterClass(alwaysRun = true) 
	public void CloseBrowser() {
		 DriverContext._Driver.quit();
    	}

}
	