package com.smi.framework.base;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class FrameworkInitialize extends Base {

	
	public void InitializeBrowser(String browser) throws IOException{
		WebDriver driver=null;
		switch(browser)
		{
		   case "Chrome":
		   {
			   System.setProperty("webdriver.chrome.driver", ".//src//driver//chromedriver.exe");
			   driver=new ChromeDriver();
			   break;
		   } 	
		   case "FireFox":
		   {
			   System.setProperty("webdriver.gecko.driver", ".//src//driver//geckodriver.exe");
//			   ProfilesIni profile = new ProfilesIni();
//			   FirefoxProfile myprofile = profile.getProfile("selenium");
//			   driver = new FirefoxDriver (myprofile);
			   
//			   System.setProperty("webdriver.firefox.marionette", ".//src//driver//geckodriver.exe");
//		       FirefoxProfile profile = new FirefoxProfile();
//		       profile.setAcceptUntrustedCertificates(false);
//		       profile.setAssumeUntrustedCertificateIssuer(true);
//		       DesiredCapabilities dc = DesiredCapabilities.firefox();
//		       dc.setCapability(FirefoxDriver.PROFILE, profile);
//		       driver = new FirefoxDriver(dc);
			   
//		       DesiredCapabilities dc=DesiredCapabilities.firefox();
//		       FirefoxProfile profile = new FirefoxProfile();
//		       dc.setCapability(FirefoxDriver.PROFILE, profile);
//		       dc.setCapability("marionette", true);
//		       driver =  new FirefoxDriver(dc);
		       
		       FirefoxOptions firefoxOptions = new FirefoxOptions();
		       firefoxOptions.setCapability("marionette", true);
		       driver = new FirefoxDriver(firefoxOptions);
			   break;
		   }
		   case "IE":
		   {
			   System.setProperty("webdriver.gecko.driver", ".//src//driver//IEDriverServer.exe");
			   driver = new InternetExplorerDriver();
			   break;
		   }
		   case "Edge":
		   {
			   System.setProperty("webdriver.edge.driver", ".//src//driver//MicrosoftWebDriver.exe");
			   driver = new EdgeDriver();

//			   EdgeOptions options = new EdgeOptions();
//			   options.setPageLoadStrategy("eager");
//			   driver = new EdgeDriver(options);
			   
//			   EdgeOptions edgeOptions = new EdgeOptions();
//			   
//			   //edgeOptions.setCapability(capabilityName, value);
//			   DesiredCapabilities capabilities = DesiredCapabilities.edge();
//			   edgeOptions.setCapability("BROWSER_NAME", "EDGE");
//			   edgeOptions.setCapability("PLATFORM", "WINDOWS");
//			   driver = new EdgeDriver(edgeOptions);
			   
//			   EdgeDriverService service = new EdgeDriverService.Builder()
//				         .usingDriverExecutable(new File(".//src//driver//MicrosoftWebDriver.exe"))
//				         .usingAnyFreePort()
//				         .build();
//				     service.start();
			   
//			   EdgeOptions options = new EdgeOptions();
//			   driver = new EdgeDriver(options);
//			   RemoteWebDriver driver2 = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options.toCapabilities());
			   
			   /*
			   EdgeOptions options = new EdgeOptions();
			   options.setPageLoadStrategy( "eager");
			   driver = new EdgeDriver(options );
			   driver.get("https://www.baidu.com");
			   driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			   driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			   driver.manage().window().maximize();*/
			   
			   break;
		   }
		   case "Safari":
		   {
//			   System.setProperty("webdriver.gecko.driver", ".//src//driver//MicrosoftWebDriver.exe");
//			   driver = new EdgeDriver();
			   break;
		   }
		}
		
		//set Driver for context
		 DriverContext.set_Driver(driver);
		//Set Browser
		 DriverContext.browser=new Browser(driver);
		 
	}
	
}
