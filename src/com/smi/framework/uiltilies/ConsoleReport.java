package com.smi.framework.uiltilies;

import static org.testng.Assert.assertTrue;

import java.util.logging.Level;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.apache.logging.log4j.Logger;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.DriverContext;

public class ConsoleReport {
	
	public static void initializeConsoleReport()
	{
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		// Set logging preference In Google Chrome browser capability to log browser errors.
		LoggingPreferences pref = new LoggingPreferences();
		pref.enable(LogType.BROWSER, Level.ALL);
		cap.setCapability(CapabilityType.LOGGING_PREFS, pref);
//		DriverContext._Driver = new ChromeDriver(cap);
	}
	
	
	public static void GetJSErrosLog(ExtentTest test,Logger logger,String testName) {
		if(DriverContext._Driver instanceof ChromeDriver)
		{
			  // Capture all JSerrors and print In console.
			  LogEntries jserrors = DriverContext._Driver.manage().logs().get(LogType.BROWSER);
			  LogUltility.log(test, logger,"CONSOLE REPORT: "+testName);
			  for (LogEntry error : jserrors) {
				  if (error.getMessage().toLowerCase().contains("error") && !error.getMessage().toLowerCase().contains("mailinator")) {
					  LogUltility.log(test, logger,("Error Message in Console:"+error.getMessage()));
		            } else if (error.getMessage().toLowerCase().contains("warning")){
		            	LogUltility.log(test, logger,("Warning Message in Console:"+error.getMessage()));
		            }else{
		            	LogUltility.log(test,logger,"Information Message in Console:"+error.getMessage()); }
				  }
		}

		 }
}
