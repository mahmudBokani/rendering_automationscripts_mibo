package com.smi.framework.uiltilies;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.smi.framework.config.Setting;

public class ReportUtilities {

	static ExtentHtmlReporter htmlReporter;
    static ExtentReports extent;
    
    public static ExtentReports report(String name, String browser) {
    	// Initiate a report
    	//htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/RenderingReport_" + name + ".html");
    	htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/RenderingReports/RenderingReport.html");
    	htmlReporter.setAppendExisting(true);
    	
    	extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
         
        extent.setSystemInfo("OS", System.getProperty("os.name"));
        extent.setSystemInfo("browser", browser);
        extent.setSystemInfo("URL", Setting.AUT_URL);
        extent.setSystemInfo("DB Host", Setting.DB_Host);
        extent.setSystemInfo("DB Name", Setting.DB_Name);       		
        
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("Rendering automation Report");
        htmlReporter.config().setReportName("Run");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.STANDARD);
		return extent;
	}
	    
    
}
