package com.smi.framework.uiltilies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import com.smi.framework.config.Setting;

public class RandomText {
	
	static List<String> dictionaryRecords;
	
	// fetch the records from the dictionary file
	public RandomText() throws FileNotFoundException{
		dictionaryRecords = this.getRecordsFromDictionary();
	}
	
	/**
	 * Get random text or sentence
	 * @param length
	 * @return
	 * @throws FileNotFoundException 
	 */
	public String RandomString(int length, String type) throws FileNotFoundException {
		String output = "";
		StringBuilder sb = new StringBuilder();
		
		if (type.equals("letters")) {
	    	
			char[]  chars = null;
			switch(Setting.Language)
	 		{
	 		case "10":
	 			chars = "אבגדהוזחטיכלמנסעפצקרשת".toCharArray(); break;
	 		case "6":
	 			chars = "abcdefghijklmnopqrstuvwxyz".toCharArray(); break;
	 		}
			Random random = new Random();
			for (int i = 0; i < length; i++) {
			    char c = chars[random.nextInt(chars.length)];
			    sb.append(c);
			}
			output = sb.toString();
		}
		else if (type.equals("words")) {
			Random random = new Random();
			for (int i = 0; i < length; i++) {
			    String c = dictionaryRecords.get(random.nextInt(dictionaryRecords.size()));
			    if (i < length-1) c += " ";
			    sb.append(c);
			}
			output = sb.toString();
		} 
		return output;
	}
	
	/**
	 * Get random sentence from a file
	 * @param length
	 * @return
	 * @throws FileNotFoundException 
	 */
	public static String getSentencesFromFile() throws FileNotFoundException {
	
		Scanner in = null ;
		switch(Setting.Language)
		{
		case "10":
			in = new Scanner(new File("hebrew_sentences.txt"), "UTF-8");break;
		case "6":
			in = new Scanner(new File("english_sentences.txt"), "UTF-8");break; 
		}
   		
   		List<String> lines = new ArrayList<>();
   		while(in.hasNextLine()) {
   		    String line = in.nextLine().trim();
   		    lines.add(line);
   		}
   		in.close();
   		
   		Random random = new Random();
   		String sentence = lines.get(random.nextInt(lines.size()));
		return sentence;
	}
	
	/**
     * Get clean records names from the dictionary.txt files
     * @return
     * @throws FileNotFoundException
     */
     private static List<String> getRecordsFromDictionary() throws FileNotFoundException {
    	 	
    	 	Scanner in = null;
	    	 switch(Setting.Language)
	 		{
	 		case "10":
	 			in = new Scanner(new File("DictionaryHE.txt"), "UTF-8");break;
	 		case "6":
	 			in = new Scanner(new File("DictionaryEN.txt"), "UTF-8");break; 
	 		}
	    	 
	   		List<String> lines = new ArrayList<>();
	   		while(in.hasNextLine()) {
	   		    String line = in.nextLine().trim();
	   		    String[] splitted = line.split("	");
	   		    
	   		    String[] removeLeft = splitted[0].split("}");
	   		    String[] removeRight = removeLeft[1].split("~");
	   		    lines.add(removeRight[0]);
	   		}
	   		
	   		in.close();
			return lines;
     }
}
