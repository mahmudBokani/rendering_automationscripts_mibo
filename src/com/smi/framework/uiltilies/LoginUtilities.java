package com.smi.framework.uiltilies;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.logging.log4j.Logger;

import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.Setting;

import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.SelectPage;

public class LoginUtilities extends FrameworkInitialize{
	
			public static String login(BasePage LoginPage, BasePage SelectPage, BasePage HomePage , Logger logger, boolean isThereMoreThanOneAssignedDomain) throws InterruptedException {
				
				try
				{
					@SuppressWarnings("unused")
					ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
				}
				catch(Exception e)
				{
					logger.info("Excel Error: " + e);
				}
				
				logger.info("logging with Username: " + ExcelUltility.ReadCell("Email",1)
				+ ", and Password: " + ExcelUltility.ReadCell("Password",1));
				
				int timesTried = 5;
				do {
					System.out.println("Logging in...");
					// Login
					Thread.sleep(2000);
					try {
						LoginPage.As(LoginPage.class).Login(ExcelUltility.ReadCell("Email",1),ExcelUltility.ReadCell("Password",1));
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					if (isThereMoreThanOneAssignedDomain) {
						// Wait for the page to load
						timesTried = SelectPage.As(SelectPage.class).isSelectPageReady();
						
						System.out.println("timesTried - login: " + timesTried);
						if(timesTried == 0)
							DriverContext._Driver.navigate().refresh();
					}
					else timesTried = 5;
				} while (timesTried == 0);
				
				try {
					DriverContext.browser.Maximize();
				} catch (Exception e) {
					System.out.println("maximize e: " + e);
				}
				
				if (isThereMoreThanOneAssignedDomain) {
				// Verify is logged in
				try {
					String logoutText = SelectPage.As(SelectPage.class).subTitleText.getText();
					logger.info("Successfully logged in");
				} catch (Exception e) {
					logger.error("Login failed: " + e);
				}
				}
				return ExcelUltility.ReadCell("Email",1);
			}
			
			// Function to fetch the username
			public static String getUserName() throws InterruptedException {
				
				try
				{
					@SuppressWarnings("unused")
					ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
				}
				catch(Exception e)
				{
					System.out.println("Excel read exception: " + e);
				}
				
				return ExcelUltility.ReadCell("Email",1);
			}

			/**
			 * Select random project and domain
			 * @throws InterruptedException 
			 * @throws SQLException 
			 */
			public static String selectRandomProjectDomain(BasePage SelectPage, Logger logger, Connection con) throws InterruptedException, SQLException
			{
			
				// Select random project
				String project = SelectPage.As(SelectPage.class).chooseRandomProject(con);
				logger.info("Choose Project: " + project);
				ArrayList<String> availableDomains = SelectPage.As(SelectPage.class).getDomainsProjectForUser(con,project,ExcelUltility.ReadCell("Email",1));

				if(availableDomains.size()>1)
				{
					// Select random domain
					String domain = SelectPage.As(SelectPage.class).chooseRandomDomain();
					logger.info("Choose Domain: " + domain);
					logger.info("Navigating to the main page");
				}

					return project;
			}
			
			/**
			 * Select specific project and domain
			 * @throws InterruptedException 
			 */
			public static void selectSpecificProjectDomain(BasePage SelectPage, Logger logger, String project, String domain) throws InterruptedException
			{
				// Select specific project
				SelectPage.As(SelectPage.class).cmbProjectSelect.click();
				SelectPage.As(SelectPage.class).Select_Item_In_ListBox(SelectPage.As(SelectPage.class).cmbProjectSelect, project);
				logger.info("Choose Project: " + project);
				
				Thread.sleep(1000);
				// Select specific domain
				SelectPage.As(SelectPage.class).cmbDomainSelect.click();
				
				SelectPage.As(SelectPage.class).Select_Item_In_ListBox(SelectPage.As(SelectPage.class).cmbDomainSelect, domain);
				logger.info("Choose Domain: " + domain);
				logger.info("Navigating to the main page");
			}
			
			/**
			 * Select specific account,project and domain
			 * @throws InterruptedException 
			 */
			public static void selectSpecificAccountProjectDomain(BasePage SelectPage, Logger logger, String account,String project, String domain) throws InterruptedException
			{
				// Select specific account
				Thread.sleep(1000);
				SelectPage.As(SelectPage.class).chooseAccount(account);
				logger.info("Choose account: " + account);	
				Thread.sleep(1000);
				
				// Select specific project
				SelectPage.As(SelectPage.class).chooseProject(project);
				logger.info("Choose Project: " + project);		
				Thread.sleep(1000);
	
				// Select specific domain
				SelectPage.As(SelectPage.class).chooseDomain(domain);
				logger.info("Choose Domain: " + domain);
				
				logger.info("Navigating to the main page");
			}
			
}
