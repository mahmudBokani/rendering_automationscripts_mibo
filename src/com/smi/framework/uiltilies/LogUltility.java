package com.smi.framework.uiltilies;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.smi.framework.config.Setting;

public class LogUltility {
	
	

	
//	
//	//file format for the log name
//	
//	private BufferedWriter buff=null;
//	ZonedDateTime date=ZonedDateTime.now();
//	DateTimeFormatter formatter= DateTimeFormatter.ofPattern("ddMMyyyyHHMMSS");
//	String fileName=date.format(formatter);
//	
//	//Create the log file
//    public void CreateLogFile()
//    {
//    	try{
//    		
//    		File dir=new File(Setting.LogPath);
//    		if(!dir.exists())
//    			dir.mkdir();
//    		//Create a file
//    		File logFile=new File(dir+"/"+ fileName+".log");
//    		FileWriter writer=new FileWriter(logFile.getAbsoluteFile());
//    		buff=new BufferedWriter(writer);
//    		
//    		
//    	}catch(Exception e)
//    	{
//    		System.out.println(e);
//    	}
//    	
//    }
//    
//	
//
//	//Write message  to the log file
//    
//    
//    public void write(String Message)
//    {
//    	try{
//    		buff.write(Message);
////    		buff.close();
//    	}catch(Exception e)
//    	{
//    		System.out.println(e);
//    	}
//    	
//    }
	
	//static StringBuilder sb = new StringBuilder();
	public static StringBuilder sb;
	
	public static void newSB() {
		sb = new StringBuilder();
	}
	
//	public static String getSB() {
//		return sb.toString();
//	}
//	
//	public static void append(String str) {
//		sb.append(str);
//	}
//	
	public static void sbClean() {
		sb.setLength(0);
	}
	
	
	public static void log(ExtentTest test, Logger logger, String text) {
		logger.info(text);
		test.log(Status.INFO, text);
		sb.append(text + System.getProperty("line.separator"));
	}
	
	public static void logError(ExtentTest test, Logger logger, String text) {
		logger.error(text);
		test.log(Status.ERROR, text);
		sb.append("Error - " + text + System.getProperty("line.separator"));
	}
	
	public static void logPass(ExtentTest test, Logger logger, String text) {
		logger.info(text);
		test.log(Status.PASS, MarkupHelper.createLabel(text, ExtentColor.GREEN));
		sb.append("Pass - " + text + System.getProperty("line.separator"));
	}
	
	public static void logFail(ExtentTest test, Logger logger, String text, ITestResult result) throws IOException {
		logger.error(text);
		test.log(Status.FAIL, MarkupHelper.createLabel(text, ExtentColor.RED));
	  	String screenShotPath = Screenshot.captureScreenShot();
        test.fail(result.getThrowable());
        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
        test.addScreenCaptureFromPath(screenShotPath);
        sb.append("Fail - " + text + System.getProperty("line.separator"));
	}
	
	public static void logSkip(ExtentTest test, Logger logger, String text, ITestResult result) throws IOException {
		logger.warn(text);
	    test.log(Status.SKIP, MarkupHelper.createLabel(text, ExtentColor.ORANGE));
        test.skip(result.getThrowable());
        sb.append("Skip - " + text + System.getProperty("line.separator"));
	}
}
