package com.smi.framework.uiltilies;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import com.smi.framework.config.Setting;

public class RetryAnalyzer implements IRetryAnalyzer  {  
	    private int count = 1;
	    //private static int maxTry = 3;
	    
	    @Override
	    public boolean retry(ITestResult iTestResult) {
	    	int maxTry = Integer.parseInt(Setting.RetryFailed);
			
	        if (!iTestResult.isSuccess()) {                      //Check if test not succeed
	        	try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	            if (count < maxTry) {                            //Check if maxtry count is reached
	                count++;                                     //Increase the maxTry count by 1
	                //iTestResult.setStatus(ITestResult.FAILURE);  //Mark test as failed
	                return true;                                 //Tells TestNG to re-run the test
	            } else {
	              //  iTestResult.setStatus(ITestResult.FAILURE);  //If maxCount reached,test marked as failed
	            }
	        } else {
	           // iTestResult.setStatus(ITestResult.SUCCESS);      //If test passes, TestNG marks it as passed
	        }
	        return false;
	    }
}